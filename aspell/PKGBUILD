# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Jochem Kossen <j.kossen@home.nl>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Contributor (Arch): Thayer Williams <thayer@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: rachad
# Contributor: Jesús E.

pkgname=aspell
pkgver=0.60.8
_debver=$pkgver
_debrel=4
_pkgmajorver=0.60
pkgrel=5
pkgdesc="A spell checker designed to eventually replace Ispell"
arch=(i686 x86_64)
url='http://aspell.net/'
license=('LGPL-2.1')
depends=('gcc-libs' 'ncurses')
makedepends=('quilt')
provides=('gnu-aspell' 'gnuaspell' 'gaspell')
optdepends=('perl: to import old dictionaries')
source=("$pkgname-$pkgver.tar.gz::https://github.com/GNUAspell/aspell/archive/rel-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/a/aspell/aspell_$_debver-$_debrel.debian.tar.xz")
sha512sums=('b7ca2ed063f003581020d2043b1686915997261695bb0aef6e530431ea55781861ba19e16ce1fb74eb892203c9433c8652d105d380c0a6a24590f13fb1a401be'
            '0e8145294df53c684c6b350c9b6963045dd2e9a02059c24a2f02073336a8e043e116d5b2376579cbc770bdb6517de1b1252a9fb3548e3e2e21df1cebf3338c0b')

prepare() {
  cd $pkgname-rel-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/07_filter.diff || true
    rm -v debian/patches/09_debian-dictdir.diff || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-rel-$pkgver
  PERL_USE_UNSAFE_INC=1 ./autogen
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc
  make
}

package() {
  cd $pkgname-rel-$pkgver
  make DESTDIR="$pkgdir" install
  ln -s $pkgname-$_pkgmajorver "$pkgdir"/usr/lib/$pkgname

  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname/
}
