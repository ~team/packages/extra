# Maintainer (Arch): Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor (Arch): Aurelien Foret <orelien@chez.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=patchutils
pkgver=0.4.2
_debver=0.4.2
_debrel=1
pkgrel=1
pkgdesc="A small collection of programs that operate on patch files"
arch=('i686' 'x86_64')
license=('GPL-2')
url='http://cyberelk.net/tim/patchutils/'
depends=('python' 'pcre2')
makedepends=('xmlto' 'docbook-xsl' 'quilt')
source=("https://deb.debian.org/debian/pool/main/p/patchutils/patchutils_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/patchutils/patchutils_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('6740c3a6feefc1e41f112a3b126ac6369f8dcc938ae8b7dcd6e368a5b3a23b37a7db42edcd63ff2f2a61c083d24c8205d9a460ef8f8983ccb8188d6e374477e3'
            'd3c5ff6d0a6b50b24b6bfde37123966c064259310db1841091894fcc8618d5b6be14e894683225765dcc2e5d64ce1557a5f06735532771044a78db940e3eb41c')

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname/
}
