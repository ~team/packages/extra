# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Dan McGee <dan@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=irssi
pkgver=1.4.5
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Modular text mode IRC client with Perl scripting"
url='https://irssi.org/'
arch=('i686' 'x86_64')
license=('custom:GPL-2+OpenSSL-Linking-Exception')
depends=('glibc' 'glib2' 'libressl' 'libotr' 'libutf8proc' 'perl' 'ncurses')
makedepends=('meson' 'ninja' 'quilt')
optdepends=('perl-libwww: for the scriptassist script')
backup=('etc/irssi.conf')
source=("https://github.com/irssi/irssi/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "https://deb.debian.org/debian/pool/main/i/irssi/irssi_${_debver}-${_debrel}~bpo12+1.debian.tar.xz")
sha512sums=('5d80107ad625feb6d2b3ac807f517c5cfeae58986d3fbfe53ea82f2dee70396f5092abdc6ef137c5651e4515e081591079b37f0e428f69990db493cfe14e4f36'
            'SKIP'
            '1ec71310fbde78f5b3b3820e88dc4431ea401e95d3563d4e611c8fce8d297bfb40c6e127c36e4e6e3da65c96a83fec0723930e97bdc27a502c4bd8b484dfd84d')
validpgpkeys=('7EE65E3082A5FB06AC7C368D00CCB587DDBEF0E1') # The Irssi project <staff@irssi.org>

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/03firsttimer_text || true

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  hyperbola-meson build \
    -Dwith-perl-lib=vendor \
    -Dwith-proxy=yes \
    -Dwith-otr=yes \
    -Dwith-perl=yes \
    -Denable-true-color=yes
  ninja -C build
}

package() {
  cd ${pkgname}-${pkgver}
  DESTDIR="${pkgdir}" ninja -C build install
  install -Dm644 irssi.conf -t "${pkgdir}/etc/"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}