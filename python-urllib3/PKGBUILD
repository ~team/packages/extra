# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Patrice Peterson <runiq at archlinux dot us>
# Contributor (Arch): Chris Brannon <cmbrannon79@gmail.com>
# Contributor (Arch): BorgHunter <borghunter at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-urllib3
pkgname=('python-urllib3' 'tauthon-urllib3')
pkgver=1.25.4
pkgrel=1
pkgdesc="HTTP library with thread-safe connection pooling and file post support"
arch=('any')
url='https://github.com/shazow/urllib3'
license=('Expat')
makedepends=('python-setuptools' 'tauthon-setuptools' 'python-ndg-httpsclient'
             'tauthon-ndg-httpsclient' 'python-pyasn1' 'tauthon-pyasn1' 'python-pyopenssl'
             'tauthon-pyopenssl' 'python-pysocks' 'tauthon-pysocks' 'python-mock' 'tauthon-mock'
             'quilt')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/shazow/urllib3/archive/$pkgver.tar.gz")
sha512sums=('b1b598d98644d293471a92a2e7d096d19ade0dd33b43c1af9a30a713cbfefd5cfc583a4cf240725b5b5d33a1c6ad351811310209befb7460a8b6a07dc226119d')

prepare() {
  cd urllib3-$pkgver
  cp -a "$srcdir"/urllib3-$pkgver{,-tauthon}
}

build() {
  cd "$srcdir"/urllib3-$pkgver
  python setup.py build

  cd "$srcdir"/urllib3-$pkgver-tauthon
  tauthon setup.py build
}

package_python-urllib3() {
  depends=('python')
  optdepends=('python-pysocks: SOCKS support'
              'python-pyopenssl: security support')

  cd urllib3-$pkgver
  python setup.py install --root="$pkgdir"
  install -Dm644 LICENSE.txt -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_tauthon-urllib3() {
  depends=('tauthon')
  optdepends=('tauthon-pysocks: SOCKS support'
              'tauthon-pyopenssl: security support')

  cd urllib3-$pkgver-tauthon
  tauthon setup.py install --root="$pkgdir"
  install -Dm644 LICENSE.txt -t "$pkgdir"/usr/share/licenses/$pkgname
}
