# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Aaron Griffin <aaron@archlinux.org>
# Maintainer: Jesús E. <heckyel@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=libofx
pkgname=('libofx' 'libofx-doc')
pkgver=0.10.9
pkgrel=1
pkgdesc="The libofx OFX banking protocol abstraction library"
arch=('i686' 'x86_64')
url='https://github.com/libofx'
license=('GPL-2')
depends=('opensp' 'curl' 'libxml++2.6')
source=("https://github.com/$pkgname/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz")
sha512sums=('89425912fa8c800ede9d7177ccd5cb3ea2a2301b42aa6580ddb105406fe68c770e2c461eefdea5bc7e883967c90e3519a219bf0b56d12c7de1030d0964d50cac')

build() {
  cd $pkgbase-$pkgver
  ./configure --prefix=/usr
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make CXXFLAGS+=-std=c++11
}

package_libofx() {
  options=('!docs')
  cd $pkgbase-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_libofx-doc() {
  pkgdesc="Documention of the OFX banking standard API"
  depends=()
  cd $pkgbase-$pkgver
  make DESTDIR="$pkgdir" install-docDATA
  make -C doc DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}