# Maintainer (Arch): Florian Pritz <flo@xinu.at>
# Contributor (Arch): Testuser_01 <mail@nico-siebler.de>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=swaks
pkgver=20201014.0
pkgrel=1
_debver=${pkgver}
_debrel=1
pkgdesc='Swiss Army Knife SMTP; Command line SMTP testing, including TLS and AUTH'
arch=('any')
url="https://jetmore.org/john/code/${pkgname}/"
license=('GPL-2')
depends=('perl-net-dns' 'perl-net-ssleay')
makedepends=('quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/jetmore/swaks/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/${pkgname::1}/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('3c4b8dfcb3574af94ac75a31c619960d9578ba5fb2a1da46de1f8b92f8c6fe726e4d763f1fcf7b0fbb46b9262ad0136295852f7bf53c663979c53693a65270e0'
            '4d8462fc5a5e0a1e9deed3aa55a89ef9e111994943188f4599fcf533d12ea65d32e2b6c45084cdd0b13af7c2607652fca77ba27215a26ef340bdf5eab0b9ede2')

prepare() {
  cd "${pkgname}-${pkgver}"

  if [ ${pkgver%.*} = ${_debver%.*} ]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package() {
  cd "${pkgname}-${pkgver}"
  install -m755 -D "${pkgname}" "$pkgdir/usr/bin/${pkgname}"
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
  cd doc
  pod2man base.pod "${pkgname}.1"
  install -m644 -D "${pkgname}.1" "$pkgdir/usr/share/man/man1/${pkgname}.1"
}
