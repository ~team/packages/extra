# Maintainer (Arch): juergen <juergen@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=metalog
pkgver=20230719
pkgrel=1
pkgdesc="A modern, free and libre syslog daemon"
url='https://metalog.sourceforge.net'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('pcre2')
makedepends=('autoconf-archive')
conflicts=('logger')
replaces=('logger')
provides=('logger')
backup=('etc/metalog.conf'
        'etc/conf.d/metalog')
source=("https://github.com/hvisage/metalog/archive/${pkgname}-${pkgver}.tar.gz"
        "$pkgname.confd"
        "$pkgname.initd"
        "$pkgname.run")
sha512sums=('a717dca5e00e9e66b97a84c3dbb0cdda522cbe628fad4cfbcb5256ee69659c63149bbb1e8797028bbab55d97787661c1d7316c7c2cbfe3b65932f84d64af15f6'
            '4fea6b7f48e55d5f69a9c9a69f54623bb9d515e2fd2eda6829f8b769eec85e3de448eb42feb6bf65645400480169b3d70d56dff090103cfc95a3810515c42b2e'
            '45eb2f4c5a99723ac24bdb90726a81efef422aab7c991d1502a0bb728a4138cda74b2f8efb4cc70f1375b21b92039639e8bd5ab01af310991a81fce668f6f096'
            '58e698951af620dbc5cc2734e45bcc5a57b7d85babe3056f6925003bc0ca89fbb4fc87db0afd2dbf052edeccb94aa631f65be818cceca65f756b35cfd2dcb136')

build() {
  cd $pkgname-$pkgname-${pkgver}
  ./autogen.sh
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc
  make
}

package() {
  cd $pkgname-$pkgname-${pkgver}
  make DESTDIR="$pkgdir" install

  install -Dm644 $pkgname.conf "$pkgdir/etc/$pkgname.conf"

  install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
  install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
  install -Dm755 "$srcdir/$pkgname.run" "${pkgdir}/etc/sv/$pkgname/run"

  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname
}