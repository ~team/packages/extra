# Maintainer (Arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor (Arch): Florian Walch <florian+aur@fwalch.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=lua-mpack
pkgname=(lua51-mpack lua52-mpack lua-mpack)
pkgver=1.0.7
pkgrel=1
pkgdesc="libmpack lua binding"
arch=('i686' 'x86_64')
url='https://github.com/libmpack/libmpack-lua'
license=('Expat')
depends=('libmpack')
makedepends=('lua51' 'lua52' 'lua')
source=("libmpack-lua-${pkgver}.tar.gz::https://github.com/libmpack/libmpack-lua/archive/${pkgver}.tar.gz")
sha512sums=('301eb78ca22a75bcabc61eebf6d67f6e5a2d3535d34c0ab6e034495e5bdd0571842397a8f36d42abbdfacf240860f807c93e3ace8ed42543e2322914d84c1a36')

build() {
  cd "libmpack-lua-${pkgver}"

  gcc -O2 -fPIC -DMPACK_USE_SYSTEM -I/usr/include/lua5.1 -c lmpack.c -o lmpack.o
  gcc -shared -lmpack -o mpack.so.5.1 lmpack.o

  gcc -O2 -fPIC -DMPACK_USE_SYSTEM -I/usr/include/lua5.2 -c lmpack.c -o lmpack.o
  gcc -shared -lmpack -o mpack.so.5.2 lmpack.o

  gcc -O2 -fPIC -DMPACK_USE_SYSTEM -I/usr/include -c lmpack.c -o lmpack.o
  gcc -shared -lmpack -o mpack.so.5.3 lmpack.o
}

package_lua51-mpack() {
  pkgdesc='Simple implementation of msgpack in C Lua 5.1'
  depends+=('lua51')

  cd "libmpack-lua-${pkgver}"
  install -Dm755 mpack.so.5.1 "${pkgdir}/usr/lib/lua/5.1/mpack.so"
  install -Dm644 LICENSE-MIT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_lua52-mpack() {
  pkgdesc='Simple implementation of msgpack in C Lua 5.2'
  depends+=('lua52')

  cd "libmpack-lua-${pkgver}"
  install -Dm755 mpack.so.5.2 "${pkgdir}/usr/lib/lua/5.2/mpack.so"
  install -Dm644 LICENSE-MIT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_lua-mpack() {
  pkgdesc='Simple implementation of msgpack in C Lua 5.3'
  depends+=('lua')

  cd "libmpack-lua-${pkgver}"
  install -Dm755 mpack.so.5.3 "${pkgdir}/usr/lib/lua/5.3/mpack.so"
  install -Dm644 LICENSE-MIT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
