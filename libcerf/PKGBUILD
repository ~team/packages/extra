# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libcerf
pkgver=1.3
_debver=$pkgver
_debrel=2.1
pkgrel=1
pkgdesc="Self-contained numeric library that provides an efficient and accurate implementation of complex error functions"
arch=('i686' 'x86_64')
url='http://apps.jcns.fz-juelich.de/doku/sc/libcerf'
license=('Expat')
depends=('glibc')
makedepends=('cmake' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/libc/libcerf/libcerf_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/libc/libcerf/libcerf_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('274fa7f3b149bb27582a54a39fe76b478cd5acc6716dd0588a2f637b77c87a6bbc4f467b52bb067ebad2d1ba18750211cc71c6a6feef75fa08d01ac1dbf5dc56'
            '446a32cdd1f5cf377d10725bd2248586b1a8b4dcc730b5fc6a12e893a26c214bb6630fc4111df5bf1d6fbcd5b25c04f8dc96f2291a68dc33584429ee0933b32a')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
