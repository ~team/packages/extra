# Maintainer (Arch): jelle van der Waa <jelle@vdwaa.nl>
# Contributor (Arch): Felix Kaiser <felix.kaiser@fxkr.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-mock
pkgname=('tauthon-mock' 'python-mock')
pkgver=4.0.3
_debver=$pkgver
_debrel=4
pkgrel=1
pkgdesc="Mocking and Patching Library for Testing"
url='https://github.com/testing-cabal/mock'
makedepends=('tauthon' 'python' 'python-pbr' 'tauthon-pbr' 'quilt')
license=('Simplified-BSD')
arch=('any')
source=(mock-$pkgver.tar.gz::https://github.com/testing-cabal/mock/archive/$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/p/python-mock/python-mock_$_debver-$_debrel.debian.tar.xz)
sha512sums=('adfdab253eb3bc1b6cb767c58ffa3a8a5c5f88da0f04ea6680e0d87da59177972d2d99bfe0a770ac2ed4f809ca6a090a9d0f789eea8f4365ef2c54f8e8792e89'
            '4cd8cd58bf38b9253e4b45320232680cde50bf3f454142f4b67898541505af25cfcf5e91a63a7ddd7bd8b52a629bdb8300ace17ecb92c0a9a58b75e89fa20636')

prepare() {
  cd "$srcdir/mock-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # use unittest instead of unittest2 as they are the same on recent python*
  sed -i 's/unittest2/unittest/g' mock/tests/*.py

  cd "$srcdir"
  cp -rf "mock-$pkgver" "mock-$pkgver-tauthon"
}

build() {
  cd "$srcdir/mock-$pkgver"
  python setup.py build

  cd "$srcdir/mock-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-mock() {
  depends=('python' 'python-six' 'python-pbr')

  cd "$srcdir/mock-$pkgver"
  python setup.py install --optimize=1 --root="$pkgdir"
  install -Dm644 LICENSE.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.txt
}

package_tauthon-mock() {
  depends=('tauthon' 'tauthon-six' 'tauthon-pbr')

  cd "$srcdir/mock-$pkgver-tauthon"
  tauthon setup.py install --optimize=1 --root="$pkgdir"
  install -Dm644 LICENSE.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.txt
}
