# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Aaron Griffin <aaron@archlinux.org>
# Contributor (Arch): Jeff Mickey <j@codemac.net>
# Contributor (Arch): ciccio.a
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=squashfs-tools
pkgver=4.4
pkgrel=2
pkgdesc="Tools for squashfs, a highly compressed read-only filesystem for GNU/Linux-libre"
url='https://github.com/plougher/squashfs-tools'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('zlib' 'lzo' 'xz' 'lz4')
source=("$pkgname-$pkgver.tar.gz::https://github.com/plougher/squashfs-tools/archive/$pkgver.tar.gz")
sha512sums=('133ce437fb8c929933d52cff710b61dd9181f6f8be58250b0d6a59a7bb79a2b350f68f456b06a0e17c469409a71272d586802d570248273ddcd5dad088c00308')

prepare() {
  cd $pkgname-$pkgver
  sed -i '1,1i#include <sys/sysmacros.h>' $pkgname/mksquashfs.c $pkgname/unsquashfs.c
}

build() {
  cd $pkgname-$pkgver/$pkgname
  make \
    GZIP_SUPPORT=1 \
    XZ_SUPPORT=1 \
    LZO_SUPPORT=1 \
    LZMA_XZ_SUPPORT=1 \
    LZ4_SUPPORT=1 \
    XATTR_SUPPORT=1
}

package() {
  cd $pkgname-$pkgver/$pkgname
  make install INSTALL_DIR=$pkgdir/usr/bin
  install -Dm644 ../COPYING -t $pkgdir/usr/share/licenses/$pkgname
}
