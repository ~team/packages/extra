# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Michael Laß <bevan@bi-co.net>
# Contributor (Arch): Marat "Morion" Talipov <morion.self@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=keepassx
pkgver=2.0.3
_debver=$pkgver
_debrel=2.2
pkgrel=4
pkgdesc="Cross Platform Password Manager"
arch=('i686' 'x86_64')
url='https://github.com/keepassx/keepassx'
license=('GPL-2' 'GPL-3' 'LGPL-2.1' 'LGPL-3' 'CC0-1.0' 'Boost-1.0' 'Modified-BSD')
depends=('qt-x11extras' 'shared-mime-info' 'hicolor-icon-theme' 'desktop-file-utils')
makedepends=('cmake' 'qt-base' 'qt-tools' 'zlib' 'libgcrypt' 'optipng' 'intltool' 'quilt')
options=(!emptydirs)
source=("https://www.keepassx.org/releases/$pkgver/$pkgname-$pkgver.tar.gz"
        "latest-commit.patch"
        "https://deb.debian.org/debian/pool/main/k/$pkgname/${pkgname}_$_debver+git20190121.1682ab9-${_debrel}.debian.tar.xz")
sha512sums=('bf1a4ffa49fc4a6b7a27e6292981c9c13920712b4cd86759a99976f7e0593a243ea14575c57d664ba7e55d2449b5d83bc3d43a64a9a6972335e52234da79d773'
            '9403c73aac86e2ff0632a3710d84012e3614c03214ff60d6f0bb044e50450494c632669838af215925100f8675203e83926d6d588f7c7e91d851d1047bbc0cee'
            'bdc6b2b06abdf96e01da50ed5f607f63b3a53c1b2ca4260ba96bb40fbd0ecc49a9673f97bcf0946f6a648728ac84a0566d14fb099c4cd3587e84c0b2fdfcae51')

prepare() {
  cd $pkgname-$pkgver

  patch -p1 -i $srcdir/latest-commit.patch

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake \
    -S $pkgname-$pkgver \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_BINDIR=/usr/bin \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DCMAKE_BUILD_TYPE=Release
  make -C build
}

package() {
  make DESTDIR="${pkgdir}" -C build install
  install -Dm644 $srcdir/$pkgname-$pkgver/COPYING -t $pkgdir/usr/share/licenses/$pkgname
}
