# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Alexander Fehr <pizzapunk gmail com>
# Contributor (Arch): Andrew Simmons <andrew.simmons@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgbase=xarchiver
pkgname=('xarchiver' 'xarchiver-gtk2')
pkgver=0.5.4.17
pkgrel=4
pkgdesc="GTK+ frontend to various command line archivers"
arch=('i686' 'x86_64')
url='https://github.com/ib/xarchiver'
license=('GPL-2' 'LGPL-2' 'LGPL-3')
depends=('gtk2' 'gtk')
makedepends=('docbook-xsl' 'intltool' 'xmlto' 'gettext-tiny')
optdepends=('binutils: deb support'
            'bzip2: bzip2 support'
            'cpio: RPM support'
            'gzip: gzip support'
            'lha: LHA support'
            'lrzip: lrzip support'
            'lz4: LZ4 support'
            'lzip: lzip support'
            'lzop: LZOP support'
            'p7zip: 7z support'
            'tar: tar support'
            'unzip: ZIP support'
            'xdg-utils: recognize more file types to open'
            'xz: xz support'
            'zip: ZIP support')
source=("https://github.com/ib/xarchiver/archive/${pkgver}/${pkgbase}-${pkgver}.tar.gz"
        "xarchiver.appdata.xml"
        "libre.patch")
sha512sums=('5b19fd0fb90f99aee0ac2576bc4efce9ef4eef9119fc118815f6d925e3877c0895475fbdf7363d8adfb6371fb7fd73299ea3a7cb3b6d9fff19c89d1d287d3a84'
            'e204bb433cd17f10808f89d75749f5a39359da0c47af2d13310d3224cf6237ea842066fb3bec96b0b68eec4f6523a137fe5206de3a5c330ba17cca935b54bea0'
            'fba3befc70b8df786346f8c4f90e715d40e2f3afab2040b2483bbbea625e6029dad11c2c41e8daff66d1670e44f283941768826f26cdd4bbca797ad8beba8a4e')

prepare() {
  cd $pkgbase-$pkgver

  # remove nonfree unRAR support and fix build xgettext check version
  patch -p1 -i "$srcdir/libre.patch"

  touch {NEWS,AUTHORS}
  autoreconf -vi
}

build() {
  # GTK+ 3 version
  [ -d gtk ] || cp -r $pkgbase-$pkgver gtk
  cd gtk
  ./configure \
    --prefix=/usr \
    --disable-plugin
  make

  cd "$srcdir"
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure \
    --prefix=/usr \
    --enable-gtk2 \
    --disable-plugin
  make
}

package_xarchiver() {
  depends=(gtk)

  cd gtk
  make DESTDIR="$pkgdir" install
  install -Dm644 "$srcdir/$pkgname.appdata.xml" -t "$pkgdir/usr/share/metainfo"

  cd ../$pkgbase-$pkgver
  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname
}

package_xarchiver-gtk2() {
  pkgdesc+=' (GTK+ 2 version)'
  depends=(gtk2)
  conflicts=(xarchiver)

  cd gtk2
  make DESTDIR="$pkgdir" install

  cd ../$pkgbase-$pkgver
  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname

  # ignore package by appstream to avoid duplicated ids
  echo 'X-AppStream-Ignore=true' >> "$pkgdir/usr/share/applications/xarchiver.desktop"
}
