# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Sebastian A. Liem <sebastian at liem dot se>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=slock
pkgver=1.4
pkgrel=7
pkgdesc="A simple screen locker for X"
arch=('i686' 'x86_64')
url='https://tools.suckless.org/slock'
license=('X11')
depends=('libxext' 'libxrandr' 'imlib2')
source=("https://dl.suckless.org/tools/$pkgname-$pkgver.tar.gz"
        "backspace.patch"
        "blur-pixelated-screen.patch"
        "dpms.patch")
sha512sums=('ad285360dd3f16a225159abaf2f82fabf2c675bd74478cf717f68cbe5941a6c620e3c88544ce675ce3ff19af4bb0675c9405685e0f74ee4e84f7d34c61a0532f'
            'e89764ff75a0691521ab5763b67e41df84a5c36480b3ac55f6c264d642c1bcdcf39582a7fd0fe577156a35ef21f76b973ba8923631d0b43840486e5a477a85d3'
            'dbb033b3e4448f0aa0a29efd74ff560723af2faa1edeef8b4b27fce743060cf98c6f27cc7afe9888b2f0d4685c3a53c696ec7a8e0980cf16ad27c68107d622c5'
            '01f8ec2917a8bc1793d8a791a3bbc3480cdddb877d45496400b76541250f75823ece738241c1490e11ca54efe5bb1fdd0a6f7cbe1bd203948c6897b71020c5fb')

prepare() {
  cd "$srcdir/slock-$pkgver"
  sed -i 's|static const char \*group = "nogroup";|static const char *group = "nobody";|' config.def.h
  sed -ri 's/((CPP|C|LD)FLAGS) =/\1 +=/g' config.mk
  patch -p1 < $srcdir/backspace.patch
  patch -p1 < $srcdir/blur-pixelated-screen.patch
  patch -p1 < $srcdir/dpms.patch
}

build() {
  cd "$srcdir/slock-$pkgver"
  make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
  cd "$srcdir/slock-$pkgver"
  make PREFIX=/usr DESTDIR="$pkgdir" install
  install -m644 -D LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
