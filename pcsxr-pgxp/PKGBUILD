# Maintainer (Arch): Stelios Tsampas <loathingkernel @at gmail .dot com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pcsxr-pgxp
pkgbasename=pcsxr
pkgver=1.9.94
pkgrel=2
pkgdesc="A PSX emulator with Parallel/Precision Geometry Transform Pipeline"
arch=('i686' 'x86_64')
url='https://github.com/iCatButler/pcsxr'
license=('GPL-3')
depends=('ffmpeg' 'gtk' 'libarchive' 'libcdio' 'libxv' 'sdl2')
makedepends=('cmake' 'intltool' 'mesa' 'mesa-libgl' 'pango' 'gettext-tiny' 'libxtst')
source=("${pkgname}-${pkgver}.zip::https://github.com/iCatButler/pcsxr/archive/refs/heads/master.zip"
        "fix-pango.patch")
sha512sums=('eb7a4917990424351d397f162e13c32f993c919f55a42bd1c3786b66693fa07056ea30da504c9c110c239f257e5692f8d7bebdc9d23398d89704368205853290'
            '0fb9005bfd5854da3a778aeef3d787b9092d897ff73ea0b203e4241944fb9ffaa5a6e289c7135ea21e739c947fe4ca206d17b6ee5b6fd2bc46ef02f1102b7ddb')

prepare() {
  mv ${pkgbasename}-master ${pkgname}-${pkgver}
  cd "${pkgname}-${pkgver}"
  patch -p1 -i "$srcdir"/fix-pango.patch
}

build() {
  cmake \
    -S ${pkgname}-${pkgver} \
    -B build \
    -DCMAKE_BUILD_TYPE='Release' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -DCMAKE_INSTALL_LIBDIR='/usr/lib' \
    -DSND_BACKEND='sdl' \
    -DENABLE_CCDDA='ON' \
    -DUSE_LIBARCHIVE='ON' \
    -DUSE_LIBCDIO='ON'
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
