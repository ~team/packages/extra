# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): Lukas Sabota <punkrockguy318@cocmast.net> (Timidity Patch)
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=sdl_mixer
pkgver=1.2.12
_debver=1.2.12
_debrel=16
pkgrel=1
pkgdesc='A simple multi-channel audio mixer'
url='https://www.libsdl.org/projects/SDL_mixer/'
arch=('i686' 'x86_64')
license=('zlib')
depends=('libmikmod' 'libvorbis' 'sdl' 'smpeg')
makedepends=('fluidsynth' 'quilt')
optdepends=('fluidsynth: MIDI software synth, replaces built-in timidity')
source=(https://www.libsdl.org/projects/SDL_mixer/release/SDL_mixer-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/s/sdl-mixer1.2/sdl-mixer1.2_$_debver-$_debrel.debian.tar.xz
        mikmod1.patch mikmod2.patch fluidsynth-volume.patch double-free-crash.patch)
sha512sums=('230f6c5a73f4bea364f8aa3d75f76694305571dea45f357def742b2b50849b2d896af71e08689981207edc99a9836088bee2d0bd98d92c7f4ca52b12b3d8cf96'
            '33cbfe6de66b1cd2c22114f26f8e9db945ac032bfef5039d76e8c2f4d76cad439966eaf2863a4c705dbab5fb467877080dbdeb73a44ebbbd33aee2303904991a'
            'f93704b3a0e19b12801aa8542483f88c867726ab09edd74d69df794e507c579e3aef87eb659152f526d3f57c1091db5dfd0e4d1a80165b1700c9ee58fe0430f3'
            '715242bc3d435e6960fcda2fa7f626f6e1c33a703183ca92c5d83b0b980d9cf5f76465241793742efe1b38566b4909154d0adaefb1f528cb7813300eb2de8631'
            '259e35757c136b1546c0607d1bc03781b0066fb4cdaf337c041e3363d5fca0489f8ed4e983b89e6fdfb8ae5149663dd5fe57b3d7122bbf9fb2938d485f28f9de'
            'fa49304fcb4560d54c9fc9a10f5a80d41645f2240dc85d7d85f8af3c55a10a2d79e08a46d52594dfd9f23612e709556acf48b2fade3c8b176a71887750302912')

prepare() {
  cd SDL_mixer-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  else
    patch -Np1 -i ../double-free-crash.patch
  fi

  patch -Np1 -i ../mikmod1.patch
  patch -Np1 -i ../mikmod2.patch
  patch -Np1 -i ../fluidsynth-volume.patch

  sed -e "/CONFIG_FILE_ETC/s|/etc/timidity.cfg|/etc/timidity++/timidity.cfg|" \
      -e "/DEFAULT_PATH/s|/etc/timidity|/etc/timidity++|" \
      -e "/DEFAULT_PATH2/s|/usr/local/lib/timidity|/usr/lib/timidity|" \
      -i timidity/config.h
}

build() {
  cd SDL_mixer-$pkgver
  ./configure --prefix=/usr --disable-static
  make
}

package() {
  cd SDL_mixer-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
