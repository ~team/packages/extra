# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Dmitry N. Shilov <stormblast@land.ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=sakura
pkgver=3.8.7
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="A terminal emulator based on GTK+ and VTE"
arch=('i686' 'x86_64')
url='https://www.pleyades.net/david/projects/sakura'
license=('GPL-2' 'Public-Domain')
depends=('vte' 'libxft')
makedepends=('cmake' 'quilt')
source=("${pkgname}-${pkgver}.tar.bz2::https://deb.debian.org/debian/pool/main/s/sakura/sakura_${pkgver}.orig.tar.bz2"
        "https://deb.debian.org/debian/pool/main/s/sakura/sakura_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ec606af0ab808e93366a08ef36199ef7e23677e7aac4765573604230c9ea498a14d6f22983ab7285030e6d10f432b7ad924f33660bce8bc05bd76a2c31c1a76b'
            'f9e5c666c7c69bcb097ecd217aef1b24e38a5f3d3dc24ce394d4859d5c3353152f7cd01c46e00eaa8eacc5ebd5392ef863623e6b5bb9e3f73253b59e710fa548')

prepare() {
  cd "$srcdir/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # set default font size a bit smaller
  sed -i 's|#define DEFAULT_FONT "Bitstream Vera Sans Mono 14"|#define DEFAULT_FONT "Bitstream Vera Sans Mono 10"|g' src/sakura.c
}

build() {
  cd "$srcdir/${pkgname}-${pkgver}"

  cmake -DCMAKE_INSTALL_PREFIX=/usr . 
  make 
}

package() {
  cd "$srcdir/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install
  install -Dm644 debian/copyright -t "$pkgdir/usr/share/licenses/$pkgname/"
}