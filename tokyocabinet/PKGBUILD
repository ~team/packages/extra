# Maintainer (Arch): Patrick Northon <northon_patrick3@yahoo.ca>
# Contributor (Arch): Mark Foxwell <fastfret79@archlinux.org.uk>
# Contributor (Arch): Nicolas Martyanoff <khaelin@gmail.com> 
# Contributor (Arch): Rick Chen <stuffcorpse@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=tokyocabinet
pkgver=1.4.48
_debver=$pkgver
_debrel=15
pkgrel=2
pkgdesc="A modern implementation of DBM"
arch=('i686' 'x86_64')
url='https://fallabs.com/tokyocabinet/'
license=('LGPL-2.1' 'Modified-BSD' 'zlib')
depends=('zlib' 'bzip2')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/t/tokyocabinet/tokyocabinet_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/t/tokyocabinet/tokyocabinet_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('b9995c838e713c13f781ab41062caefeb5c613f696dab08039f992f6860413d60a513114b83468cafdf7fc5b5e51ab880226972465d532f873f2a55f615e1440'
            '879418573e0eee3e891ec5967083891647808a79e03200b1031f541338d7aa26d93b3ed722c0cf26834570adec86a5ee291eac58d8bc644066c1b0f764bbc76a')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --enable-off64 \
    --enable-fastest
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install
  install -Dm644 debian/copyright COPYING -t $pkgdir/usr/share/licenses/$pkgname/
}
