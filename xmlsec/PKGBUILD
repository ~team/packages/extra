# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Hussam Al-Tayeb <ht990332@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=xmlsec
pkgver=1.2.32
_debver=1.2.32
_debrel=1
pkgrel=2
pkgdesc="XML Security Library is a C library based on LibXML2"
license=('X11')
arch=('i686' 'x86_64')
url="https://www.aleksey.com/xmlsec/index.html"
depends=('libxslt' 'libressl' 'gnutls' 'nss' 'libltdl')
makedepends=('quilt')
source=(https://www.aleksey.com/xmlsec/download/${pkgname}1-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/x/xmlsec1/xmlsec1_$_debver-$_debrel.debian.tar.xz)
sha512sums=('39a90e154fe760b9cb01090c0232aa6c88c595fea87a771bebdd7b38ab274e3be719d6314065d9f02b854c7be9c8ec3557e57e0158ff3696413de3424f14389e'
            '1187c62e578e2e3a5e34034b1d381c78eb375876022c849ee4623b76d18f24ba6e6fe7638c5b0b7f297ee9376dc0a15f045ef4ee36430b9fc06e0f95b553a584')

prepare() {
  cd ${pkgname}1-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}1-${pkgver}
  ./configure --prefix=/usr --disable-static
 sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd ${pkgname}1-$pkgver
  make -k check
}

package() {
  cd ${pkgname}1-${pkgver}
  make DESTDIR=${pkgdir} install
  install -m755 -d ${pkgdir}/usr/share/licenses/${pkgname}
  install -m644 COPYING Copyright ${pkgdir}/usr/share/licenses/${pkgname}
}
