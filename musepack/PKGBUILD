# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=musepack
pkgname=('musepack-tools' 'libmpcdec')
pkgver=0.1~r495
_debver=$pkgver
_debrel=2
pkgrel=2
epoch=1
_rel=495
pkgdesc="MusePack commandline utilities and decoder library"
arch=('i686' 'x86_64')
url='https://musepack.net/'
license=('LGPL-2.1' 'Modified-BSD' 'GPL-2')
depends=('glibc')
makedepends=('libreplaygain' 'libcue' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/libm/libmpc/libmpc_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/libm/libmpc/libmpc_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('89fa7729c469873a1839c6e6c8c2d994af5acd7170f8bf11f8c55e09700f64fe6476b189f9842d4c8cc575370dc4b0eb13e9c1495195e03906dab476fff7b047'
            '9398122cba3a884752007a8aeb845cd8f944e475486b2ed1829ddc91f1b32f458d756392d16cc7e07f72fdb27ebbf3313db0a03aca859598f5789977077226c4')

prepare() {
  cd musepack_src_r${_rel}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  mv configure.in configure.ac
  autoreconf -fi
}

build() {
  cd musepack_src_r${_rel}
  ./configure \
    --prefix=/usr \
    --enable-mpcchap
  make
}

package_libmpcdec() {
  pkgdesc="MusePack decoding library"

  cd musepack_src_r${_rel}
  make -C include DESTDIR="${pkgdir}" install
  make -C libmpcdec DESTDIR="${pkgdir}" install
  install -Dm644 debian/copyright -t ${pkgdir}/usr/share/licenses/${pkgname}
}

package_musepack-tools() {
  depends=('libcue' 'libreplaygain' "libmpcdec>=$epoch:$pkgver")
  pkgdesc="MusePack commandline tools"
  options=(!emptydirs)

  cd musepack_src_r${_rel}
  make DESTDIR="${pkgdir}" install
  make -C libmpcdec DESTDIR="${pkgdir}" uninstall
  make -C include DESTDIR="${pkgdir}" uninstall
  install -Dm644 debian/copyright -t ${pkgdir}/usr/share/licenses/${pkgname}
}