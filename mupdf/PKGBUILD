# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Brad Fanella <bradfanella@archlinux.us>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Pierre-Paul Paquin <pierrepaulpaquin@gmail.com>
# Contributor (Arch): xduugu
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=mupdf
pkgname=('libmupdf' 'mupdf' 'mupdf-gl' 'mupdf-tools')
pkgver=1.17.0
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc='Lightweight PDF and XPS viewer'
arch=('i686' 'x86_64')
url='https://mupdf.com/'
license=('AGPL-3')
makedepends=('desktop-file-utils' 'freetype2' 'freeglut' 'glu' 'harfbuzz'
             'jbig2dec' 'libjpeg-turbo' 'mesa-libgl' 'openjpeg2' 'libxext' 'quilt')
# we need static libs for zathura-pdf-mupdf
options=('staticlibs')
source=("https://mupdf.com/downloads/archive/mupdf-${pkgver/_/}-source.tar.xz"
        "https://deb.debian.org/debian/pool/main/m/mupdf/mupdf_${_debver}+ds1-${_debrel}.debian.tar.xz")
sha512sums=('39188e6ce3eaefb525b2c32767c4bf52ed881b41889edef086aa64bfe1c38e6f3cb853450c8284d175ef8854f32e9bc67415a692048ead26cf31c35645f9e0e5'
            'af31334b22e30b5d91329c277c1f5a865393700eea7fc7a9651c7291c3a7f65485d0c1f67f4262b03e55cf1035d4e204e24f19969566b374195ad2021c7e94f5')

prepare() {
  cd $pkgbase-${pkgver/_/}-source

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0003-use-debian-flavor-build-options.patch || true
    rm -v debian/patches/0010-Prevent-thirdparty-archive-build.patch || true

    quilt push -av
  fi

  # remove bundled packages, we want our system libraries
  rm -rf thirdparty/{freeglut,freetype,harfbuzz,jbig2dec,libjpeg,openjpeg,zlib}
}

build() {
  CFLAGS+=' -fPIC'
  CXXFLAGS+=' -fPIC'
  export CFLAGS CXXFLAGS

  USE_SYSTEM_LIBS='yes'
  export USE_SYSTEM_LIBS

  cd $pkgbase-${pkgver/_/}-source
  make build=release libs apps
}

package_libmupdf() {
  pkgdesc='Library for Lightweight PDF and XPS viewer'

  cd $pkgbase-${pkgver/_/}-source

  make build=release prefix="$pkgdir"/usr install

  install -D -m0644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  rm -rf "$pkgdir"/usr/{bin,share/man}
  mv "$pkgdir"/usr/share/doc/mupdf "$pkgdir"/usr/share/doc/libmupdf

  find "$pkgdir"/usr/include "$pkgdir"/usr/share "$pkgdir"/usr/lib \
    -type f -exec chmod 0644 {} +
}

package_mupdf() {
  pkgdesc='Lightweight PDF and XPS viewer'
  depends=('desktop-file-utils' 'freetype2' 'harfbuzz' 'jbig2dec'
           'libjpeg-turbo' 'openjpeg2' 'libressl' 'libxext')

  cd $pkgbase-${pkgver/_/}-source

  install -D -m0644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  install -D -m0755 build/release/mupdf-x11 "$pkgdir"/usr/bin/mupdf

  install -D -m0644 docs/man/mupdf.1 "$pkgdir"/usr/share/man/man1/mupdf.1

  install -d "$pkgdir"/usr/share/doc/mupdf
  install -m0644  README CHANGES "$pkgdir"/usr/share/doc/mupdf

  install -D -m0644 debian/mupdf.desktop -t "$pkgdir"/usr/share/applications
  install -D -m0644 debian/mupdf.png -t "$pkgdir"/usr/share/pixmaps
}

package_mupdf-gl() {
  pkgdesc='Lightweight PDF and XPS viewer with OpenGL backend'
  conflicts=('mupdf')
  provides=("mupdf=${pkgver}")
  depends=('desktop-file-utils' 'freetype2' 'freeglut' 'glu' 'harfbuzz' 'jbig2dec'
           'libjpeg-turbo' 'openjpeg2' 'libressl')

  cd $pkgbase-${pkgver/_/}-source

  install -D -m0644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  install -D -m0755 build/release/mupdf-gl "$pkgdir"/usr/bin/mupdf

  install -D -m0644 docs/man/mupdf.1 "$pkgdir"/usr/share/man/man1/mupdf.1

  install -d "$pkgdir"/usr/share/doc/mupdf
  install -m0644 README CHANGES "$pkgdir"/usr/share/doc/mupdf

  install -D -m0644 debian/mupdf.desktop -t "$pkgdir"/usr/share/applications
  install -D -m0644 debian/mupdf.png -t "$pkgdir"/usr/share/pixmaps
}

package_mupdf-tools() {
  pkgdesc='Tools for Lightweight PDF and XPS viewer'
  depends=('mupdf')
  depends=('freetype2' 'jbig2dec' 'libjpeg-turbo' 'openjpeg2' 'harfbuzz' 'libressl')

  cd $pkgbase-${pkgver/_/}-source

  install -D -m0644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING

  install -D -m0755 build/release/mutool "$pkgdir"/usr/bin/mutool
  install -D -m0755 build/release/muraster "$pkgdir"/usr/bin/muraster

  install -D -m0644 docs/man/mutool.1 "$pkgdir"/usr/share/man/man1/mutool.1

  install -d "$pkgdir"/usr/share/doc/mupdf-tools
  install -m0644 README CHANGES "$pkgdir"/usr/share/doc/mupdf-tools
}
