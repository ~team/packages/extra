# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=json-glib
pkgver=1.6.0
_debver=1.6.0
_debrel=2
pkgrel=1
pkgdesc="JSON library built on GLib"
url="https://wiki.gnome.org/Projects/JsonGlib"
arch=(i686 x86_64)
license=(LGPL-2.1)
depends=(glib2)
makedepends=(gobject-introspection meson quilt)
options=(!emptydirs)
source=(https://ftp.gnome.org/pub/GNOME/sources/${pkgname}/${pkgver:0:3}/${pkgname}-${pkgver}.tar.xz
        https://deb.debian.org/debian/pool/main/j/json-glib/json-glib_$_debver-$_debrel.debian.tar.xz)
sha512sums=('0025f913c54a223e6c5f7e65c081afc8ea65ab5a30ed9f30d2d2bb28d17c5695f6e308c64dfdf128e47ddc99d3178421204b5273e78305a096c0b7dfe67dd406'
            'c040d5de852a9475dd76d84ce5d0803beff3663daaf593f5dbc9940a0b6ebbbe37a172de7f50d8dad1a9a786399cf23bf4fc6b75e044937d7b480f952f0ac34f')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  hyperbola-meson $pkgname-$pkgver build -D gtk_doc=disabled -D man=true
  ninja -C build
}

check() {
  meson test -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  rm -r "$pkgdir"/usr/{libexec,share}/installed-tests
  install -Dm644 $pkgname-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
