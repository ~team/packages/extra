# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@koon.fr>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=lxappearance
pkgname=(lxappearance-gtk2 lxappearance)
pkgver=0.6.3
pkgrel=1
pkgdesc='Feature-rich GTK+ theme switcher'
arch=('i686' 'x86_64')
license=('GPL-2')
url='https://wiki.lxde.org/en/LXAppearance'
makedepends=('gtk2' 'gtk' 'intltool' 'gettext-tiny')
source=(https://downloads.sourceforge.net/lxde/$pkgbase-$pkgver.tar.xz)
sha512sums=('035cc952a33ac25408a4158cb4e745b17af3f29b5ceedfbe6b37235d16c801658403e862cd35ad7c8ca20fe6186fab1dce74a6e8d3b2eec12a30d532fc21662c')

build() {
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure --sysconfdir=/etc --prefix=/usr --disable-dbus
  make

  cd "$srcdir"
  # GTK+ 3 version
  [ -d gtk3 ] || cp -r $pkgbase-$pkgver gtk3
  cd gtk3
  ./configure --sysconfdir=/etc --prefix=/usr --disable-dbus --enable-gtk3
  make
}

package_lxappearance-gtk2() {
  depends=('gtk2')
  conflicts=('lxappearance')

  cd gtk2
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_lxappearance() {
  pkgdesc+=' (GTK+ 3 version)'
  depends=('gtk')
  conflicts=('lxappearance-gtk2')

  cd gtk3
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
