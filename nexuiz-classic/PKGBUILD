# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=nexuiz-classic
_realpkgname=nexuiz
pkgver=2.5.2
_debver=$pkgver
_debrel=9
_debreldata=12
pkgrel=1
pkgdesc="Fast-paced, free and libre 3D first-person shooter"
arch=('any')
license=('GPL-2')
depends=('darkplaces')
makedepends=('quilt' 'p7zip' 'fteqcc' 'xz')
groups=('games')
source=("${_realpkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/n/nexuiz/nexuiz_${pkgver}+dp.orig.tar.gz"
        "${_realpkgname}-data-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/n/nexuiz-data/nexuiz-data_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/n/nexuiz/nexuiz_${_debver}+dp-${_debrel}.debian.tar.xz"
        "https://deb.debian.org/debian/pool/main/n/nexuiz-data/nexuiz-data_${_debver}-${_debreldata}.debian.tar.xz"
        "nexuiz"
        "nexuiz-server")
sha512sums=('9082040dc0704f6f226eba967d36a7f0ef75cb85819455a621544ef766904fe0a5b211667f2fa9d8c039969467ce9cc3c7cd14e990aa47a60a48532eb5619995'
            '195a6c87770db7c12155013b6b183d69111b34769ce61295b5b5f56c014e0d16c7364a4f153780f5fc89d2ea0c62b60040cd53d89dfb4ac0bf18e380ab57e66a'
            'a33c317228f4c34ba23025ec61eedb854a92c1361847f23380d0c1dc718d7f621458897a5efc7b76b7cde3e489081ff8e026282984d8d24c06ac5c5568aab5b2'
            '321dde73c655b44a8b18950c761d903253069f5832d7dfd20688907c35b8987052c73f042b8db305beb8ae7953239b63f78b0a177e4eb7a71db337d2a0de10d3'
            '69158e3b22fce7a2a11b188af66352423672d33d977733de47608e0e6294333566a1ce3466d8348565cd515f2f99f8103c84c95a37c745f44f0f333c1f7f1172'
            '2036db1bdf274b39683f031473a07dc0f56dd5a0628815b72728d576e418c2551fea3da9598e2f53e07556f9de2da48198d02d573f47b65b077793460279b50a')
noextract=("nexuiz_${_debver}+dp-${_debrel}.debian.tar.xz")

prepare() {
  mv "${_realpkgname}-${pkgver}+dp.orig" "${_realpkgname}-${pkgver}"
  mkdir -p "${_realpkgname}-${pkgver}/debian"
  tar -xf "nexuiz_${_debver}+dp-${_debrel}.debian.tar.xz" --strip 1 \
      -C  "${_realpkgname}-${pkgver}/debian" 

  cd "${_realpkgname}-data-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/04_exclude_music_in_datapk3.diff || true
    rm -v debian/patches/exclude_textures_from_data.pk3.diff || true

    quilt push -av
  fi
}

build() {
  cd "${_realpkgname}-data-${pkgver}"
  make -C data pk3here
}

package() {
  cd "${_realpkgname}-data-${pkgver}"
  install -Dm755 $srcdir/nexuiz{,-server} -t "$pkgdir/usr/games"
  install -Dm644 data/data.pk3 -t "$pkgdir/usr/share/games/nexuiz/data"

  # server documentation files
  install -d "$pkgdir/usr/share/doc/$pkgname/examples"
  cp -R server/* "$pkgdir/usr/share/doc/$pkgname/examples"

  # metadata for package: manpages, license and desktop-file
  install -Dm644 $srcdir/$_realpkgname-$pkgver/COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
  install -Dm644 $srcdir/$_realpkgname-$pkgver/nexuiz.xpm -t "$pkgdir/usr/share/pixmaps"
  install -Dm644 $srcdir/$_realpkgname-$pkgver/debian/nexuiz.desktop -t "$pkgdir/usr/share/applications"
  install -Dm644 $srcdir/$_realpkgname-$pkgver/debian/nexuiz{,-server}.6 -t "$pkgdir/usr/share/man/man6"
}