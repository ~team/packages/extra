# Maintainer (Arch): Thore Bödecker <foxxx0@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): François Charette <firmicus ατ gmx δοτ net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-io-stringy
pkgver=2.111
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="I/O on in-core objects like strings/arrays"
arch=('any')
url='https://metacpan.org/pod/IO::Stringy'
license=('GPL-1')
depends=('perl')
makedepends=('quilt')
options=('!emptydirs')
source=("https://www.cpan.org/authors/id/D/DS/DSKOLL/IO-stringy-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/i/io-stringy/io-stringy_${_debver}-${_debrel}.debian.tar.xz") 
sha512sums=('9da8fb03945bc4a59cea3b3b2e22b15d56b133470e581d1cb03920bda2b4953e3c9e4a25d095ed8a384efb5a4c58db437c92cca6a81d583791191f1a37691a4a'
            '1aeb1a7adcdcf36caa237cea5f1834434f1518ed4c231500d3eb0b8b2b6b8580743dcdbc545e5ce9ef187a0ce85034f632e96f61657f520575b52d783e79ee11')

prepare() {
  cd  "${srcdir}/IO-stringy-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd  "${srcdir}/IO-stringy-${pkgver}"
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd  "${srcdir}/IO-stringy-${pkgver}"
  make install DESTDIR="${pkgdir}"

  find "${pkgdir}" -name '.packlist' -delete
  find "${pkgdir}" -name '*.pod' -delete

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
