# Maintainer (Arch): Brett Cornwall <ainola@archlinux.org>
# Contributor (Arch): Luca Weiss
# Contributor (Arch): Michael Yang
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=spdlog
pkgver=1.10.0
pkgrel=1
pkgdesc="Very fast, header-only/compiled, C++ logging library"
arch=('i686' 'x86_64')
url='https://github.com/gabime/spdlog/'
license=('Expat')
depends=('fmt')
makedepends=('cmake')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/gabime/${pkgname}/archive/v${pkgver}.tar.gz")
sha512sums=('e82ec0a0c813ed2f1c8a31a0f21dbb733d0a7bd8d05284feae3bd66040bc53ad47a93b26c3e389c7e5623cfdeba1854d690992c842748e072aab3e6e6ecc5666')

build() {
  cd ${pkgname}-${pkgver}

  export CFLAGS+=" ${CPPFLAGS}"
  export CXXFLAGS+=" ${CPPFLAGS}"
  cmake \
    -B build \
    -DCMAKE_BUILD_TYPE=None \
    -DSPDLOG_BUILD_BENCH=OFF \
    -DSPDLOG_FMT_EXTERNAL=ON \
    -DSPDLOG_BUILD_SHARED=ON \
    -DSPDLOG_BUILD_TESTS=ON \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -Wno-dev
  make -C build
}

package() {
  cd ${pkgname}-${pkgver}
  make -C build DESTDIR=${pkgdir} install
  install -Dm644 LICENSE -t ${pkgdir}/usr/share/licenses/${pkgname}
}
