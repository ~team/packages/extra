# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libdca
pkgver=0.0.7
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Free software library for decoding DTS Coherent Acoustics streams"
arch=('i686' 'x86_64')
license=('GPL-2')
url='https://www.videolan.org/developers/libdca.html'
depends=('sh' 'libsndio')
makedepends=('quilt')
provides=('libdts')
replaces=('libdts')
options=('!emptydirs')
source=("https://download.videolan.org/pub/videolan/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/libd/libdca/libdca_${_debver}-${_debrel}.debian.tar.xz"
        "sndio.patch")
sha512sums=('dae15d77d066687c882833d5bed8d65a585c1fc0277b7276563c89ddd5a83b35389ec94cca445f38af28a9b01430b72647e9afd1b08f030959e711de1a08924a'
            '2776028a318216e548eb3655ac145e1e0b81518ddba5d97610c044908b2b38364fb0b65d120222c641a7bdae91e99886dc495ca7e7c50275f2259364a0c94c78'
            '0e61c60a728cefd7c847e4cc4d04254386204c82eab213f23b907aebc6f6bc092c1f5278fbaa84d2046c4ff050b9be079c4155202a2d2813e3a19d6c038b1751')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i $srcdir/sndio.patch
  ./bootstrap
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --disable-static
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"

  rm -f "${pkgdir}/usr/share/man/man1"/{extract_dts,dtsdec}.1
  ln -s dcadec.1.gz "${pkgdir}/usr/share/man/man1/dtsdec.1.gz"
  ln -s extract_dca.1.gz "${pkgdir}/usr/share/man/man1/extract_dts.1.gz"
  rm -f "${pkgdir}/usr/lib/libdts.a"
}
