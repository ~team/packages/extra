# Maintainer (Arch): Laurent Carlier <lordheavym@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=libxfont2
_openbsdver=6.9
pkgver=2.0.4
pkgrel=2
pkgdesc="X11 font rasterisation library, provided by Xenocara"
arch=(i686 x86_64)
url="https://www.xenocara.org"
license=('X11')
depends=('freetype2' 'libfontenc' 'xenocara-proto')
makedepends=('xenocara-util-macros' 'libxtrans')
provides=('libxfont')
conflicts=('libxfont')
replaces=('libxfont')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxfont2-$pkgver.tar.lz{,.sig})
sha512sums=('3ae1bbbb699da49fd9f4c43bf5bff2501edce8c6625d85ad761bb2889869296ec3a66b6648ae550e0f1f7c6cbd524eee55253db692393de959d293b97b8d33f8'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXfont2"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/libXfont2"
  ./configure --prefix=/usr --sysconfdir=/etc --disable-static
  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXfont2"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
