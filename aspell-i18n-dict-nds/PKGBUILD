#!/bin/ksh

# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

# Based on aspell-en

_pkgname='aspell'
_pkglang='nds'
_srclang="${_pkglang}"
_srcsv='6'
pkgname="${_pkgname}-i18n-dict-${_pkglang}"
# Example: _srcname='aspell6-en'
_srcname="${_pkgname}${_srcsv}-${_srclang}"
pkgver='0.01.0'
_srcver='0.01-0'
pkgrel='2'
pkgdesc='Low Saxon dictionary for Aspell'
arch=('i686' 'x86_64')
url='https://ftp.gnu.org/gnu/aspell/dict/0index.html'
license=('LGPL-2.1')
groups=("${_pkgname}-i18n-dict" "${_pkgname}-dict"
        "g${_pkgname}-i18n-dict" "g${_pkgname}-dict"
        "gnu${_pkgname}-i18n-dict" "gnu${_pkgname}-dict"
        "gnu-${_pkgname}-i18n-dict" "gnu-${_pkgname}-dict")
depends=('aspell')
provides=("${_pkgname}-dict-${_pkglang}"
          "g${pkgname}" "g${_pkgname}-dict-${_pkglang}"
          "gnu${pkgname}" "gnu${_pkgname}-dict-${_pkglang}"
          "gnu-${pkgname}" "gnu-${_pkgname}-dict-${_pkglang}"
          "${_pkgname}-${_pkglang}")
conflicts=("${_pkgname}-${_pkglang}")
replaces=("${_pkgname}-${_pkglang}")
_source=("https://ftp.gnu.org/gnu/${_pkgname}/dict/${_srclang}/")
source=("${_source[0]}/${_srcname}-${_srcver}.tar.bz2"
        "${_source[0]}/${_srcname}-${_srcver}.tar.bz2.sig")
sha512sums=('88ef6f6773e8699a5200ff8ae3ef9935fee21958fa35868bc10d37c22b55ea721ad9c59ab4843fdc6fedbf5a9ed10c7ea144548713742b8094b99c3c46a552a5'
            'SKIP')
validpgpkeys=(# Kevin Atkinson
              '22A48912622D7BF4C6D15E5B19EA26E56FAB4DD4')
unset _pkgname _pkglang _srclang _srcsv _source

build() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  ./configure
  make 'V=1'
}

package() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  make "DESTDIR=${pkgdir}" 'install'

  # The "Copyright" file was not added,
  # due "README" and "info" files says that
  # this package is licensed under "LGPL-2.1".
  # And "Copyright" file says that
  # this package is licensed under "GPL-2".
  for i in 'COPYING'; do
    install -Dm '644' "${i}" -t "${pkgdir}/usr/share/licenses/${pkgname}"
  done
  unset i _srcname _srcver
}
