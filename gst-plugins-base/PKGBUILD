# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=gst-plugins-base
pkgname=(gst-plugins-base-libs gst-plugins-base)
pkgver=1.18.3
pkgrel=3
pkgdesc="Multimedia graph framework - base plugins"
url='https://gstreamer.freedesktop.org/'
arch=(i686 x86_64)
license=(LGPL-2)
depends=(gstreamer orc libxv iso-codes mesa)
makedepends=(alsa-lib cdparanoia libvisual libvorbis libtheora pango opus
             graphene libpng libjpeg gobject-introspection meson
             gtk qt-{base,tools,x11extras} clutter sdl2 glu libsndio
             gettext-tiny)
options=(!emptydirs)
source=(https://gstreamer.freedesktop.org/src/$pkgbase/$pkgbase-$pkgver.tar.xz
        sndio.patch)
sha512sums=('5b1850c82d6c29e260cadcde6a412a75eac47b14a1778aa969b0a33540eb843f6cef2209c91a1d7230468ec9fbf00b53ca890b8c336378d39ea3cec0512f8e7f'
            '7ac11cb09cad7996940dc725e93a00f235dd2d37f2874929db2f0062c3cc60b8be42d3336b546f16ab3e208bf8605df29688e0e20364e88656c02b0f3a0f975e')

prepare() {
  cd $pkgbase-$pkgver

  patch -p0 -i $srcdir/sndio.patch
}

build() {
  hyperbola-meson $pkgbase-$pkgver build \
    -D doc=disabled \
    -D tremor=disabled \
    -D gobject-cast-checks=disabled \
    -D package-name="GStreamer Base Plugins (Hyperbola GNU/Linux-libre)" \
    -D package-origin="https://www.hyperbola.info/"
  meson compile -C build
}

package_gst-plugins-base-libs() {
  pkgdesc="${pkgdesc% plugins}"

  DESTDIR="$pkgdir" meson install -C build

  mkdir -p ext/lib/gstreamer-1.0
  for _x in alsa cdparanoia libvisual ogg opengl opus pango theora vorbis; do
    _x="lib/gstreamer-1.0/libgst${_x}.so"
    mv "$pkgdir/usr/$_x" "ext/$_x"
  done

  install -Dm644 $srcdir/$pkgbase-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}

package_gst-plugins-base() {
  depends=("gst-plugins-base-libs=$pkgver"
           alsa-lib cdparanoia libvisual libvorbis libtheora pango opus graphene libpng libjpeg libsndio)

  mv ext "$pkgdir/usr"

  install -Dm644 $srcdir/$pkgbase-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
