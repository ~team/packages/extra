# Maintainer (Arch): Levente Polyak <anthraxx@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Hugo Doria <hugo@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=mplayer
pkgname=('mplayer' 'mencoder')
pkgver=1.4
_debver=$pkgver
_debrel=1
pkgrel=8
pkgdesc="Free and libre media player"
url='http://www.mplayerhq.hu/'
arch=('i686' 'x86_64')
license=('GPL-2')
makedepends=('libxxf86dga' 'libmad' 'libxinerama' 'libmng' 'libxss' 'aalib' 'jack'
             'libcaca' 'faad2' 'libxvmc' 'enca' 'libdca' 'a52dec' 'unzip'
             'mesa' 'yasm' 'mpg123' 'ladspa' 'libcdio-paranoia' 'x264' 'libdvdcss'
             'libdvdread' 'libdvdnav' 'ffmpeg' 'quilt')
options=('!buildflags' '!emptydirs')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/m/mplayer/mplayer_${pkgver}+ds1.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/m/mplayer/mplayer_${_debver}+ds1-${_debrel}+deb11u1.debian.tar.xz"
        "mplayer.desktop"
        "revert-icl-fixes.patch"
        "fix-libmpcodecs-ad_spdif_ffmpeg44.patch")
sha512sums=('23f7ed12fb40bd126524c523ee1c655fb1ebbda3f3a8506da67a0c39bf8c34a04b6f2183ae680ae6237a74934f28fbc725625af8301da09fd369fe52f7e0b077'
            '9ca1906bbe619cc3a2ff9f10b687d829255ca911f3d273cee34e53d8772ecdd2316d24b48a1a09c9ba4cad34c15b384577070aea833bd7402c86505115b6387d'
            'd3c5cbf0035279c6f307e4e225473d7b77f9b56566537a26aa694e68446b9e3240333296da627ad5af83b04cc8f476d1a3f8c05e4cf81cd6e77153feb4ed74bc'
            '0cae0b26d3d97fd4c962962c43a481de20335369cbca406cadfc9bda1a0608b32f5374e76c477cb9a85bda83a568a1ed17126df224ae61579d0a402c1824aea8'
            'b866f0cf9ef3009e3d16ccbe64600a485968e288e6b0965dcb443f6867e353e01f4f5de86eea637e0a3b98d8177c6c50d0f8e74ca21a99649a5b3716ca402c2b')

prepare() {
  cd MPlayer-${pkgver}

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0001_version.patch || true
    rm -v debian/patches/0002_mplayer_debug_printf.patch || true
    rm -v debian/patches/0201_PATH_MAX_HURD.patch || true
    rm -v debian/patches/0203_generic arch fallback.patch || true

    quilt push -av
  fi

  patch -p0 < "$srcdir/revert-icl-fixes.patch"
  patch -Np1 -i "$srcdir/fix-libmpcodecs-ad_spdif_ffmpeg44.patch"
  ./version.sh
}

build() {
  cd MPlayer-${pkgver}

  ./configure --prefix=/usr \
    --disable-gui \
    --disable-arts \
    --disable-liblzo \
    --disable-speex \
    --disable-openal \
    --disable-libdv \
    --disable-musepack \
    --disable-esd \
    --disable-mga \
    --disable-ass-internal \
    --disable-faac \
    --disable-unrarexec \
    --disable-cdparanoia \
    --disable-ffmpeg_a \
    --disable-smb \
    --disable-vdpau \
    --disable-librtmp \
    --disable-libvpx-lavc \
    --disable-apple-remote \
    --disable-apple-ir \
    --disable-lirc \
    --disable-lircc \
    --enable-runtime-cpudetection \
    --enable-xvmc \
    --enable-radio \
    --enable-radio-capture \
    --language=all \
    --confdir=/etc/mplayer \
    $extra
  [[ "${CARCH}" = "i686" ]] && sed 's|-march=i486|-march=i686|g' -i config.mak
  make
}

package_mplayer() {
  pkgdesc="Free and libre media player"
  backup=('etc/mplayer/codecs.conf' 'etc/mplayer/input.conf')
  depends=('desktop-file-utils' 'ttf-font' 'enca' 'libxss' 'a52dec'
           'x264' 'libmng' 'libdca' 'aalib' 'libxinerama'
           'jack' 'libmad' 'libcaca' 'libxxf86dga' 'faad2' 'libxvmc' 'mpg123'
           'libcdio-paranoia' 'libdvdnav' 'ffmpeg')

  cd MPlayer-${pkgver}
  make DESTDIR="${pkgdir}" install-mplayer install-mplayer-man

  install -Dm 644 etc/{codecs.conf,input.conf,example.conf} "${pkgdir}/etc/mplayer"
  install -Dm 644 "${srcdir}/mplayer.desktop" -t "${pkgdir}/usr/share/applications"
  install -Dm 644 etc/mplayer256x256.png "${pkgdir}/usr/share/pixmaps/mplayer.png"

  install -Dm644 "${srcdir}/MPlayer-${pkgver}/LICENSE" -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_mencoder() {
  pkgdesc="Free and libre command line video decoding, encoding and filtering tool"
  depends=('enca' 'a52dec' 'x264' 'libmng' 'libdca' 'libmad'
           'faad2' 'mpg123' 'libcdio-paranoia' 'libdvdnav' 'ffmpeg')

  make -C MPlayer-${pkgver} DESTDIR="${pkgdir}" install-mencoder install-mencoder-man
  find "${pkgdir}/usr/share/man" -name mplayer.1 -exec rename mplayer.1 mencoder.1 {} +

  install -Dm644 "${srcdir}/MPlayer-${pkgver}/LICENSE" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
