# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=qt-imageformats
_qtver=5.15.2
pkgver=${_qtver/-/}
pkgrel=2
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Plugins for additional image formats: TIFF, MNG, TGA, WBMP'
depends=('qt-base' 'jasper' 'libmng')
groups=('qt')
replaces=('qt5-imageformats')
conflicts=('qt5-imageformats')
provides=('qt5-imageformats')
_pkgfqn="${pkgname/-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz")
sha512sums=('3c821fac83b8d6177af256dc1d68aca14ae6d5cbdedb8d8665158ebcec0f6e6fb790b5d210a1aa8b0679ecff60fafd4e5d1387c6954498b41409ea44177e0d7e')

prepare() {
  mkdir -p build
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}
