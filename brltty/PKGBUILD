# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Alexander Epaneshnikov <aarnaarn2@gmail.com>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=brltty
pkgver=6.3
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Braille display driver"
arch=('i686' 'x86_64')
url='https://brltty.app'
license=('LGPL-2.1')
depends=('gcc-libs' 'glibc' 'liblouis' 'libspeechd' 'pcre2' 'tcl')
makedepends=('alsa-lib' 'atk' 'python-cython' 'espeak' 'expat' 'glib2' 'gpm' 'icu' 'libxaw'
             'ncurses' 'speech-dispatcher' 'quilt')
optdepends=('atk: ATK bridge for X11/GNOME accessibility'
            'espeak: espeak driver'
            'libxaw: X11 support'
            'libxt: X11 support'
            'libx11: for xbrlapi'
            'libxfixes: for xbrlapi'
            'libxtst: for xbrlapi'
            'python: Python support'
            'speech-dispatcher: speech-dispatcher driver')
backup=(etc/brltty.conf)
options=('!emptydirs')
install=brltty.install
source=("https://brltty.app/archive/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/b/brltty/brltty_${_debver}+dfsg-${_debrel}+deb11u1.debian.tar.xz"
        "fix-build.patch"
        "$pkgname.initd"
        "$pkgname.run")
sha512sums=('687fbb30703d4c9fd7b55d27fabecf26c58f7c032c82bcdee1405259da74f85516e268fc49d1bdb2f704b92532cf5c466712cc09b1a4ba5304d4e021af74b7b8'
            '5350de6070b7bb2e343a8e328cdad6557fb47178145bde1c2085833a8bd48acc7bf4e366bc91c480a37976abc4df09cb725ab0d682d7bd7987e086c6d73ac6f4'
            'd8d3e8f6851fefc90a5855cd36fe78675c09f6c92af98cf7f0ef68d90f3d1d470a0db83b25125d2197da18873768b4c95826fcece602c813d93ff97524e129a9'
            '2be94c464898a9156d67e300c19008343bd34376ccbcd268c49ecbd05149b4bbbf03102a2fd6c972156432e9b9b45491c012dd343c0e27b8d0bee71ee9e12828'
            '58a4eab3513593705541cfb1e8a8a20ac31028a0e94f9f3f4665721c15e5e4020864668082cee51c79710d9224d02f416bc228eb0d8cca9e9b0b0555eb4c7cce')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/fix-build.patch
  ./autogen
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --mandir=/usr/share/man \
    --with-scripts-directory=/usr/lib/brltty \
    --with-tables-directory=/usr/share/brltty \
    --with-writable-directory=/run/brltty \
    --enable-gpm \
    --disable-stripping \
    --disable-polkit \
    --disable-java-bindings \
    --disable-ocaml-bindings
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make INSTALL_ROOT="${pkgdir}" install
  make INSTALL_ROOT="${pkgdir}" install-udev

  install -vDm 644 "Documents/${pkgname}.conf" -t "${pkgdir}/etc/"

  install -Dm755 ${srcdir}/$pkgname.initd "${pkgdir}/etc/init.d/$pkgname"
  install -Dm755 ${srcdir}/$pkgname.run "${pkgdir}/etc/sv/$pkgname/run"

  install -Dm644 LICENSE-LGPL -t "${pkgdir}/usr/share/licenses/$pkgname"
}
