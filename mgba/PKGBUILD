# Maintainer (Arch):  Jonathan Steel <jsteel at archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Duck Hunt <vaporeon@tfwno.gf>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=mgba
pkgname=('libmgba' 'mgba-sdl' 'mgba-qt')
pkgver=0.10.1
_debver=$pkgver
_debrel=1
pkgrel=2
arch=('i686' 'x86_64')
url='https://mgba.io'
license=('MPL-2.0')
makedepends=('cmake' 'qt-multimedia' 'sdl2' 'zlib' 'libpng' 'libzip' 'libedit'
             'ffmpeg' 'qt-tools' 'quilt')
source=("${pkgbase}-${pkgver}.tar.gz::https://github.com/mgba-emu/mgba/archive/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/mgba/mgba_${_debver}+dfsg-${_debrel}+deb12u1.debian.tar.xz"
        "$pkgbase.desktop")
sha512sums=('bb79d2380a4708b70daf95c9b403427f77254391b1e11d68411384f265a670907e64b842c9978c9be558ffad337b738d9d83988d52890f08aed7e7fc124f19d4'
            '2cbbb59acd5ce1d3d0f29b87687448fed157282a7d51729878b75876c9126fdef1878a00808cc8f2e8ff89b9f6e2a7768a6445842adb3d85184f4f79c8535f74'
            '2949dae0522390cc9b0bbae8cfdcd70905d2c7f029ca171bf641913164090f423dbec542d0aa11adb11baed299a5ca679ced85f8e594f83adfe4c9a0380a14bf')

prepare() {
  [[ ! -d build ]] && mkdir build || rm -rf build
  cd mgba-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/exclude-inih.patch || true

    quilt push -av
  fi

  # remove non-free parts before further processing
  rm -rf ./src/third-party/discord-rpc
}

build() {
  cd build
  cmake \
    "$srcdir"/mgba-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DUSE_MINIZIP=OFF \
    -DUSE_EPOXY=OFF \
    -DUSE_LUA=OFF \
    -DUSE_DISCORD_RPC=OFF
  make
}

package_libmgba() {
  pkgdesc="Shared library of mGBA"
  depends=('zlib' 'libpng' 'libzip' 'libedit' 'ffmpeg')

  cmake -DCOMPONENT=libmgba mgba-$pkgver -DCMAKE_INSTALL_PREFIX="$pkgdir/usr" \
    -P build/cmake_install.cmake

  # license
  install -Dm644 mgba-$pkgver/LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_mgba-sdl() {
  pkgdesc="An Emulator focusing on both speed and accuracy (SDL)"
  depends=('libmgba' 'sdl2')

  cmake -DCOMPONENT=mgba-sdl mgba-$pkgver -DCMAKE_INSTALL_PREFIX="$pkgdir/usr" \
    -P build/cmake_install.cmake

  # license
  install -Dm644 mgba-$pkgver/LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_mgba-qt() {
  pkgdesc="An Emulator focusing on both speed and accuracy (Qt UI)"
  depends=('libmgba' 'qt-multimedia' 'sdl2')

  cmake -DCOMPONENT=mgba-qt mgba-$pkgver -DCMAKE_INSTALL_PREFIX="$pkgdir/usr" \
    -P build/cmake_install.cmake

  # desktop-shortcut and icon
  rm -rf "$pkgdir"/usr/share/applications
  install -Dm644 mgba-$pkgver/res/mgba-256.png "$pkgdir"/usr/share/pixmaps/mgba.png
  install -Dm644 $srcdir/$pkgbase.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop

  # license
  install -Dm644 mgba-$pkgver/LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}
