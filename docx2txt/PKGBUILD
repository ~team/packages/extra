# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=docx2txt
pkgver=1.4
_debver=1.4
_debrel=5
pkgrel=1
pkgdesc="Recovers text from DOCX files, with good formatting."
arch=('any')
url='http://docx2txt.sourceforge.net'
license=('GPL-3')
depends=('perl' 'unzip')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tgz
        https://deb.debian.org/debian/pool/main/d/docx2txt/docx2txt_$_debver-$_debrel.debian.tar.xz)
sha512sums=('5e59cd4065f409c20e17b29d3935a2637147392e86a325b28b78748d3b3867ce91f14f3349621b3c95b2ce6a8e38e33cf0816d1f9dccbebfdde9ba29fbd7f5b3'
            'fad758f67726b5b4c87041fc9839d9681f2c298d186b4bdbd44632f6b46f915b02f1cd5588090b7341e8904d84c81c26e5c1a50dc05c5c7b0003978183e10743')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  install -Dm644 docx2txt.config "$pkgdir/etc/docx2txt.config"
  install -Dm755 docx2txt.pl "$pkgdir/usr/bin/docx2txt"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}

