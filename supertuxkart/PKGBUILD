# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor (Arch): Daenyth <Daenyth+Arch [AT] gmail [DOT] com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): rabyte <rabyte__gmail>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_pkgname=supertuxkart-libre
_datalibrename=supertuxkart-data-libre
pkgname=supertuxkart
pkgver=1.2
_debver=$pkgver
_debrel=1
pkgrel=4
pkgdesc="Free software kart racing game"
arch=('i686' 'x86_64')
url='https://supertuxkart.net'
license=('GPL-2' 'GPL-3' 'CC-BY-3.0' 'CC-BY-4.0' 'CC-BY-SA-3.0' 'CC-BY-SA-4.0' 'Public-Domain' 'zlib' 'Expat' 'Unicode' 'CC0-1.0')
depends=('openal' 'libvorbis' 'fribidi' 'curl' 'libxrandr' 'glew'
         'libxkbcommon-x11' 'sqlite' 'libjpeg-turbo' 'libraqm'
         'hicolor-icon-theme' 'sdl2' 'mcpp' 'libsquish')
makedepends=('cmake' 'ninja' 'quilt')
groups=('games')
mksource=("https://github.com/supertuxkart/stk-code/releases/download/${pkgver}/SuperTuxKart-${pkgver}-src.tar.xz"
          "https://repo.hyperbola.info:50000/sources/${_datalibrename}/${_datalibrename}-${pkgver}.tar.gz"{,.sig})
mksha512sums=('bc7079af9b3d85b3e4839ebb3eee293fb8bfe95450165172caa28b8ad1a9e97c59618d77c2208a86090f1840aa9a4b4b6898c1053fa6f5d7dfbfe17b69536835'
              'bee80e5a56d9f1a325704bf8e4dd34641b243172d2cba692d5d96d75e5c43a917be29164c148a99c218de56167074b63801004e660960eb5333dbdd7ba489f23'
              'SKIP')
source=("https://repo.hyperbola.info:50000/sources/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/s/supertuxkart/supertuxkart_${_debver}+ds2-${_debrel}.debian.tar.xz"
        "libre.patch")
sha512sums=('95519a9d8bb33a361828beb39a755b0b9f0d806ef16b07ca9ce81855ccef82819b9b9fe78a065d94739979a6150643205ca5ba95d2d7c9e27251fe1b1f68623a'
            'SKIP'
            '7a921b3fa10ff95a563e7b1c2c6023ec3f6b1a116ab247c515b0391130d1831b4a11cedee6ba44895afa6c8bd485d34a31c5e8611996f001fa77484606d6f9ff'
            '38d954f742bc6087d0ba73eb67937151b942cda58f6ad27ea8b6d48328f062d88bf9da461f053448e08920cd642ff6bfcf869a3241f43ec8f0e0f70e15fbd473')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

mksource() {
  cd "$srcdir"/SuperTuxKart-${pkgver}-src

  # remove non-free and unneeded data
  rm -rf android
  rm -rf tools/windows_installer
  rm -rf lib/{glew,libsquish,mcpp}
  rm -rf data/karts/{beastie,hexley}
  rm data/replay/{standard_novice_mines,standard_reverse_supertux_mines,standard_expert_ravenbridge_mansion,standard_expert_volcano_island}.replay

  # replace non-free data
  cp "${srcdir}/${_datalibrename}-${pkgver}/icon-sara-racer.png" "data/karts/sara_the_racer/icon-sara.png"
  cp "${srcdir}/${_datalibrename}-${pkgver}/licenses-sara-racer.txt" "data/karts/sara_the_racer/licenses.txt"
  cp "${srcdir}/${_datalibrename}-${pkgver}/icon-sara-wizard.png" "data/karts/sara_the_wizard/icon-sara.png"
  cp "${srcdir}/${_datalibrename}-${pkgver}/licenses-sara-wizard.txt" "data/karts/sara_the_wizard/licenses.txt"
  cp "${srcdir}/${_datalibrename}-${pkgver}/jump.ogg" "data/sfx"
  cp "${srcdir}/${_datalibrename}-${pkgver}/plopp.ogg" "data/sfx"
  cp "${srcdir}/${_datalibrename}-${pkgver}/licenses-sfx.txt" "data/sfx/licenses.txt"
}

prepare() {
  cd "$srcdir"/SuperTuxKart-${pkgver}-src

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # making installation fhs-compliant
  sed -i -e 's/STK_INSTALL_BINARY_DIR "bin"/STK_INSTALL_BINARY_DIR "games"/g' \
         -e 's/STK_INSTALL_DATA_DIR "share\/supertuxkart"/STK_INSTALL_DATA_DIR "share\/games\/supertuxkart"/g' \
         CMakeLists.txt

  # remove non-free parts in source-code and configuration
  patch -Np1 -i ${srcdir}/libre.patch
}

build() {
  cd "$srcdir"/SuperTuxKart-${pkgver}-src

  cmake . \
    -Bbuild \
    -GNinja \
    -Wno-dev \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DUSE_WIIUSE=0 \
    -DBUILD_RECORDER=0

  ninja -C build
}

package() {
  cd "$srcdir"/SuperTuxKart-${pkgver}-src

  DESTDIR="$pkgdir" ninja -C build install

  install -Dm644 "$srcdir"/SuperTuxKart-${pkgver}-src/COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 "$srcdir"/SuperTuxKart-${pkgver}-src/debian/copyright -t "${pkgdir}/usr/share/licenses/$pkgname"
}
