# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): damir <damir@archlinux.org>
# Contributor (Arch): Brice Carpentier <brice.carpentier@orange.fr>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=loudmouth
pkgver=1.5.3
_debver=$pkgver
_debrel=6
pkgrel=2
pkgdesc='A lightweight Jabber client library written in C/Glib'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
url='https://mcabber.com/'
depends=('glib2' 'gnutls' 'libidn' 'krb5')
makedepends=('intltool' 'pkgconfig' 'quilt')
source=(https://mcabber.com/files/loudmouth/${pkgname}-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/l/loudmouth/loudmouth_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('ab6b16b4e644b69cdb7d8df1753d1bb5b43c2f1e76944e639339169b332e1b5c6a4246053c5b8a579ee9b53c845cef610b0040dfdffeb857180b6bab71adfcce'
            '2bb1ae8ec0079ec84f7e243e7b6e86ad09975a3fdee76d93024c8e4862c397ab6a7c16f9b4010fa75be269f235ef66633c9b455e690a1665f5e69510324e9ff4')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr --disable-static --disable-gtk-doc
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # we don't support gtk-doc
  rm -rf $pkgdir/usr/share/gtk-doc

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
