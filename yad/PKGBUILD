# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Arch): Aaron Fischer <mail@aaron-fischer.net>
# Contributor (Arch): Steven Allen <steven@stebalien.com>
# Contributor (Arch): trile7 at gmail dot com
# Contributor (Arch): Ernia <monghitri@aruba.it>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=yad
pkgver=14.1
pkgrel=1
pkgdesc="Display graphical dialogs from shell scripts or command line"
url='https://github.com/v1cont/yad'
arch=('i686' 'x86_64')
license=('GPL-3')
depends=('gtk' 'webkitgtk' 'gtksourceview3')
makedepends=('autoconf' 'automake' 'intltool' 'gettext-tiny')
source=("https://github.com/v1cont/yad/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.xz")
sha512sums=('c2949846d9d6c34bc0c8f482a5b30fb978865308bcafe656694d8eb1bb903e9af76f9d3cdd748ecb0dac389e1a0204a15b10ab29b99262cdeffc44ed16ce902e')

prepare() {
  cd ${pkgname}-${pkgver}

  autoreconf -vfi
  intltoolize
}

build() {
  cd ${pkgname}-${pkgver}

  export GLIB_COMPILE_SCHEMAS=/usr/bin/glib-compile-schemas
  ./configure \
    --prefix=/usr \
    --enable-icon-browser \
    --enable-html \
    --enable-gio \
    --enable-sourceview

  make
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
