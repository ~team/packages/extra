# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): tobias <tobias@archlinux.org>
# Contributor (Arch): red_over_blue
# Contributor (Arch): neri
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=povray
pkgver=3.7.0.10
_debver=$pkgver
_debrel=2
pkgrel=2
_v=${pkgver%.*.*} # 3.7
pkgdesc="Script based raytracer for creating 3D graphics"
arch=('i686' 'x86_64')
license=('AGPL-3')
url='http://povray.org/'
depends=('boost-libs' 'libpng' 'libtiff')
makedepends=('boost' 'glu' 'quilt')
backup=(etc/povray/$_v/povray.conf
        etc/povray/$_v/povray.ini)
source=("$pkgname-$pkgver.tar.gz::https://github.com/POV-Ray/povray/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/povray/povray_$_debver-$_debrel.debian.tar.xz")
sha512sums=('fb947f2231f44c548b53ad27008e6a53909b375970c504938d7d7ea5dd336672891b50d3b41076598d8d025db5bcee01880d399094b4727d942d1c02a3594c47'
            '44df398d87074ac4e47ffd6f5880dfd2c4e7eeffa09db5694002c58a158fa19492bd47ff10eacdabcaa1d781ad6a905032bb8b5058f311a7833e6f9ab0f0a017')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cd ./unix
  sed 's/automake --w/automake --add-missing --w/g' -i prebuild.sh
  sed 's/dist-bzip2/dist-bzip2 subdir-objects/g' -i configure.ac
  ./prebuild.sh
  cd ..
  ./bootstrap
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    LIBS="-lboost_system -lboost_thread" \
    COMPILED_BY='Hyperbola GNU/Linux-libre' \
    --prefix=/usr \
    --sysconfdir=/etc
  make CXXFLAGS+='-std=c++11 -lboost_system -lboost_thread -DBOOST_BIND_GLOBAL_PLACEHOLDERS=1 -w'
}

package() {
  cd $pkgname-$pkgver
  install -d "$pkgdir"/usr/share/{doc/,}"$pkgname-$_v"
  cp -r icons include ini scenes scripts "$pkgdir/usr/share/$pkgname-$_v"
  cp -r doc "$pkgdir/usr/share/doc/$pkgname-$_v"
  install -Dm755 unix/povray "$pkgdir/usr/bin/povray"
  install -Dm644 povray.conf "$pkgdir/etc/povray/$_v/povray.conf"
  install -Dm644 povray.ini "$pkgdir/etc/povray/$_v/povray.ini"
  install -Dm644 povray.1 "$pkgdir/usr/share/man/man1/povray.1"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
