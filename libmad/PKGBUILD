# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=libmad
pkgver=0.15.1b
_debver=$pkgver
_debrel=10
pkgrel=2
pkgdesc="A high-quality MPEG audio decoder"
arch=('i686' 'x86_64')
url='https://www.underbit.com/products/mad/'
license=('GPL-2')
depends=('glibc')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/sourceforge/mad/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libm/$pkgname/${pkgname}_$_debver-$_debrel.diff.gz"
        "libmad.patch")
sha512sums=('2cad30347fb310dc605c46bacd9da117f447a5cabedd8fefdb24ab5de641429e5ec5ce8af7aefa6a75a3f545d3adfa255e3fa0a2d50971f76bc0c4fc0400cc45'
            'ddd6a3c84531d0aed145c9671d08e0bb0332b2d71acc9c21bd255973a095b6242eff39eccb9f22a74cbfa0a34d27f655cc851d6ee13f88fb1b37e5169e76b07c'
            'ff815f5aa32aec4230351b258430ca2184c0a44f80845c92b46aedb9942b3cd85c7b3aa575f4f562a5e02f7fadf6f3d6fe06e64d2b65418dbcd10762214695b1')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    patch -p1 -i ../${pkgname}_$_debver-$_debrel.diff

    quilt push -av
  fi

  patch -p1 -i "${srcdir}/libmad.patch"
}

build() {
  cd ${pkgname}-${pkgver}
  CFLAGS="$CFLAGS -ftree-vectorize -ftree-vectorizer-verbose=1"
  autoconf
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname
}
