# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): tobias <tobias@arhlinux.org>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=exiv2
pkgver=0.27.3
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc="Exif, Iptc and XMP metadata manipulation library and tools"
url='https://exiv2.org'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('curl' 'expat' 'gcc-libs' 'gettext-tiny' 'libssh' 'zlib')
makedepends=('cmake' 'quilt')
source=("https://github.com/Exiv2/${pkgname}/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/e/${pkgname}/${pkgname}_${_debver}-${_debrel}+deb11u2.debian.tar.xz")
sha512sums=('92775e1c79f06f7b4fb4c214cb2732982cbf01d8bc07d0e8f440facca712a41e579ef2baf72a0471889bb74d3f4975fbc76b47adbe6839decb6770458f76a341'
            '85025eb45975ca5764126c5dd8bd1c681f99ec03dc3e033b6dc8b616e9f9159fa80605db8ef376aac192549f619b83bbc21bcd395aa29099ff517ba0168103df')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  cmake \
    -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_SKIP_RPATH=ON \
    -DEXIV2_BUILD_SAMPLES=OFF \
    -DEXIV2_BUILD_UNIT_TESTS=OFF \
    -DEXIV2_ENABLE_CURL=ON \
    -DEXIV2_ENABLE_NLS=ON \
    -DEXIV2_ENABLE_VIDEO=ON \
    -DEXIV2_ENABLE_SSH=ON \
    -DEXIV2_ENABLE_WEBREADY=ON
  make -C build VERBOSE=1
}

package() {
  cd ${pkgname}-${pkgver}
  make -C build DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
