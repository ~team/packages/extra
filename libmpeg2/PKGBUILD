# Contributor (Arch): Sarah Hay <sarah@archlinux.org>
# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libmpeg2
pkgver=0.5.1
pkgrel=6
pkgdesc="Library for decoding MPEG-1 and MPEG-2 video streams."
arch=('i686' 'x86_64')
url="https://libmpeg2.sourceforge.io/"
depends=('glibc')
makedepends=('sdl' 'libxv')
optdepends=('sdl: required for mpeg2dec'
            'libxv: required for mpeg2dec')
source=(https://libmpeg2.sourceforge.io/files/${pkgname}-${pkgver}.tar.gz
        libmpeg2-0.5.1-gcc4.6.patch)
license=('GPL-2')
provides=('mpeg2dec')
sha512sums=('3648a2b3d7e2056d5adb328acd2fb983a1fa9a05ccb6f9388cc686c819445421811f42e8439418a0491a13080977f074a0d8bf8fa6bc101ff245ddea65a46fbc'
            '5eef5e283f0f4e8901a1aa1c16e9a2d1e5896a7a09dd5ae107379ec27001f7cd22db62ab731328f2c5e11089e6a8371ced2def4fa8a5a834072ca1e4a4e2ca5d')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  patch -Np1 -i "${srcdir}/libmpeg2-0.5.1-gcc4.6.patch"

  sed '/AC_PATH_XTRA/d' -i configure.ac
  autoreconf --force --install
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr --enable-shared --disable-static
  make	OPT_CFLAGS="${CFLAGS}" \
	MPEG2DEC_CFLAGS="${CFLAGS}" \
	LIBMPEG2_CFLAGS=""
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}
