# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgbase=python-chardet
pkgname=('python-chardet' 'tauthon-chardet')
pkgver=3.0.4
pkgrel=1
arch=('any')
url='https://github.com/chardet/chardet'
license=('LGPL-2.1')
makedepends=('python-setuptools' 'tauthon-setuptools')
source=("$pkgname-$pkgver.tar.gz::https://pypi.io/packages/source/c/chardet/chardet-$pkgver.tar.gz"
        '000-support-pytest-4.patch')
sha512sums=('61a03b23447a2bfe52ceed4dd1b9afdb5784da1933a623776883ee9f297e341f633e27f0ce0230bd5fdc5fdb5382105ab42736a74a417ddeb9f83af57455dba5'
            '57bfeb5752123fbf9f9f61386039b7ceb9255290225f73453d2d52f2366e1949ed92692912571c8b451b521aedfa8b605879b2742aec7f1fdce4db10705cbe53')

prepare() {
  pushd "$srcdir/chardet-$pkgver"
  patch -Np1 -i "$srcdir/000-support-pytest-4.patch"
  popd

  cp -a chardet-$pkgver{,-tauthon}
}

package_python-chardet() {
  depends=('python-setuptools')
  pkgdesc="Python module for character encoding auto-detection"

  cd chardet-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1

  # License
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_tauthon-chardet() {
  depends=('tauthon-setuptools')
  pkgdesc="Tauthon module for character encoding auto-detection"

  cd chardet-$pkgver-tauthon
  tauthon setup.py install --root="$pkgdir" --optimize=1

  # To avoid file conflict with the python version
  mv "$pkgdir"/usr/bin/chardetect{,-tauthon}

  # License
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:
