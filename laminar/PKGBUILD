# Maintainer (Arch): Oliver Giles <web ohwg net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=laminar
pkgver=1.0
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="Free software, fast and lightweight Continuous Integration server (with Hyperbola-branding)"
arch=('i686' 'x86_64')
url='https://laminar.ohwg.net'
license=('GPL-3' 'Expat')
depends=('capnproto' 'sqlite' 'rapidjson' 'boost-libs')
makedepends=('cmake' 'boost' 'quilt')
options=('strip')
source=("$pkgname-$pkgver.tar.gz::https://github.com/ohwgiles/laminar/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/l/laminar/laminar_$_debver-$_debrel.debian.tar.xz"
        "fix-build.patch"
        "ansi_up.js"
        "Chart.min.js"
        "vue.min.js"
        "vue-router.min.js"
        "icon.png"
        "laminard.initd"
        "laminard.confd")
sha512sums=('5d9f9c9fe14a90733ab5b2c7f135d2d232e17a8f6bfe054b34f925ed37f589a40e6c2b1ec5fd73cc52f433f1562a6db94c7a4d649631d28b715a0cbbbd184a55'
            'f41e881320e08052940cfb891c38ce45c9c74e5576aaf99c98a961b1278ff73899eb7541090dedbdddd3f165fa673c25cba8cf6d6ce2b54301bfc70a5227bb1d'
            '94d8b3e428b3223903a0b7d79eca7a70ff56614dd39ac57352696cd5d5385e427f84e4e379574863a6d38a270b82e28731edea6b23d91ff2efa27cd204b18b14'
            'b5833288efea1636ce702701ac88a0cca2064f3f281c68a71d7773a411aefdd07a47f29aa17b86e2f7fe11dab154ca5d94eb97622bf2770bca450a5dde99a702'
            '1aca85f35d1c3701cc08310b4f08b7620588070298425bc2d564c027a7e4f34aed07accddc859da6842e8c1402b8e30ec575e4b045b01349382eb6fe37cec365'
            '04a6d247e71fcb12dd300b04d2768b45e1522e0f3fa636e07f11e1fe4fe4502f361f2eebe87b51e612e1a1b6a59f681c4efce4cb27a1add444763a6c430cb627'
            '0e938bf1c6e5854c162036596387239920a7607173860620f069896590468894aedcb2743d8a466d926355081dcee595f530a9adc0d6ebc2ed252763626f11d4'
            '8712c82d558d06aa495f5ff2464b1cf83890868fc28dbc74dc7aa917f2de23f7b9c5953c1f5c02ca3a624f2dc167302db280d0929c72bf87fb1e3d8a14800b2d'
            '533af672db0154b1dd3b9516a0bdafdadb427c469145bf8f30e2cf4e5a0fbca73c19f7180ea5f70c45a8fffcff0c4cd320a422dbfa37b92e37af600cac516eb0'
            '5394447f3ec020d1e41f9ea123d5b68710c5673fc2f3698d35bc11aa6583eb733dcd930016a757788a4d13ea2af53070ffda678943998d12340954b2e2f12933')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0000-Remove-dependency-on-vue-router.patch || true
    rm -v debian/patches/0001-Remove-vue-router-in-the-build-process.patch || true
    rm -v debian/patches/0002-Move-ansi-up-to-latest-version.patch || true
    rm -v debian/patches/0003-Patch-build-system-to-use-JS-libraries-from-Debian-p.patch || true
    rm -v debian/patches/0005-Add-Documentation-links-to-the-systemd-service.patch || true

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/fix-build.patch

  # copy 3rd-party files for compile
  mkdir -p ./js
  cp "$srcdir"/{ansi_up,Chart.min,vue.min,vue-router.min}.js "$srcdir/$pkgname-$pkgver/js"

  # insert branding
  rm "$srcdir/$pkgname-$pkgver/src/resources/icon.png"
  cp "$srcdir"/icon.png "$srcdir/$pkgname-$pkgver/src/resources"
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLAMINAR_VERSION=$pkgver .
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install
  install -d "${pkgdir}/var/lib/laminar/cfg/"{jobs,contexts,scripts}
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"

  # install services
  install -Dm755 "${srcdir}/laminard.initd" "${pkgdir}/etc/init.d/laminard"
  install -Dm644 "${srcdir}/laminard.confd" "${pkgdir}/etc/conf.d/laminard"
}
