# Maintainer (Arch): Sébastien "Seblu" Luttringer <seblu@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Kevin Edmonds <edmondskevin@hotmail.com>
# Contributor (Arch): Filip Wojciechowski, filip at loka dot pl
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libnetfilter_log
pkgver=1.0.1
_debver=1.0.1
_debrel=3
pkgrel=1
pkgdesc="Library providing API to packets that have been logged by netfilter"
arch=('i686' 'x86_64')
url='https://www.netfilter.org/projects/libnetfilter_log/'
license=('GPL-2')
depends=('libnfnetlink')
makedepends=('quilt')
validpgpkeys=('57FF5E9C9AA67A860B557AF7A4111F89BB5F58CC') # Netfilter Core Team
source=("https://www.netfilter.org/projects/$pkgname/files/$pkgname-$pkgver.tar.bz2"{,.sig}
        "https://deb.debian.org/debian/pool/main/libn/libnetfilter-log/libnetfilter-log_$_debver-$_debrel.debian.tar.xz")
sha512sums=('4088b9bf055fb4cd5f2e201a46a00def7642116b5d1695a371936b7b45eb93877944456506b45761bf2f3f81b40c161c1ecdcfb90532009f073dac3cedc6f436'
            'SKIP'
            '1d5a46a3d5d8652e0cd019c42fdb7fe782c77ce3b40c37c67b5ad1ad300c21ab99ded7ad549eff5b0d08f049dccf571d6fb36c9627bd17718fe71444ba4bdc0c')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
