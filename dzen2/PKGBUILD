# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Mark Taylor <skymt0@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dzen2
_pkgname=dzen
pkgver=0.9.5~svn271
_debver=0.9.5~svn271
_debrel=4
pkgrel=1
pkgdesc="General purpose messaging, notification and menuing program for X11"
url='https://github.com/robm/dzen'
arch=('i686' 'x86_64')
license=('Expat')
depends=('libx11' 'libxpm' 'libxinerama' 'libxft')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/d/dzen2/dzen2_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/dzen2/dzen2_${_debver}-${_debrel}.debian.tar.gz")
sha512sums=('303b19c2a20eb1d4bd7f38a23aea56e51e150a8bd394c0d340416ec6a784940cf5333f7517ed79100984f6c04f5aa82773368825336d941e4694f558ca821be7'
            'be8b22170a3d86bf0211dd250355ea2b4ff84691d9040f2688718b56052fabaf6bbfb9c00b1355ab623bb84077aadf8ffa264f97ea4bbaa5400fc8e4be31245f')

prepare() {
  cd ${_pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  sed -i 's|DZEN_XMP|DZEN_XPM|g' main.c

  CFLAGS="${CFLAGS} -Wall -Os \${INCS} -DVERSION=\\\"\${VERSION}\\\" -DDZEN_XINERAMA -DDZEN_XPM -DDZEN_XFT `pkg-config --cflags xft`"
  LIBS=" -L/usr/lib -lc -lXft -lXpm -lXinerama -lX11"

  echo "CFLAGS=${CFLAGS}" >> config.mk
  echo "LIBS=${LIBS}" >> config.mk
  echo "LDFLAGS=${LDFLAGS} ${LIBS}" >> config.mk
}

build() {
  cd ${_pkgname}-${pkgver}

  make X11INC=/usr/include X11LIB=/usr/lib
  make -C gadgets X11INC=/usr/include X11LIB=/usr/lib
}

package() {
  cd ${_pkgname}-${pkgver}

  make PREFIX=/usr MANPREFIX=/usr/man DESTDIR="${pkgdir}" install
  make -C gadgets PREFIX=/usr MANPREFIX=/usr/man DESTDIR="${pkgdir}" install
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 README* -t "${pkgdir}/usr/share/doc/${pkgname}"
}
