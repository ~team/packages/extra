# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Sarah Hay <sarahhay@mb.sympatico.ca>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgbase=xsane
pkgname=('xsane' 'xsane-gimp')
pkgver=0.999
_debver=0.999
_debrel=10
pkgrel=2
arch=('i686' 'x86_64')
url='https://gitlab.com/sane-project/frontend/xsane'
license=('GPL-2')
makedepends=('gtk2' 'lcms2' 'sane' 'zlib' 'libjpeg-turbo' 'gimp' 'quilt')
source=(https://deb.debian.org/debian/pool/main/x/xsane/xsane_${pkgver}.orig.tar.gz
        https://deb.debian.org/debian/pool/main/x/xsane/xsane_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('73ec961fce1a86b5d6f5bac0995d222785eb4b077dc8e72492b092d2bf4500455426e80e4d27233721cd38ec84f77fb9f92190a6afe45bdaf7ffd1ee50b431ed'
            'ebb0fbdb76bc3feaefebc779a1d18cbb50f4cd33562b90915aac00a8c95a28c39242b6aeb45705b7c7f4111946d0866b5fc6e611e928ef83578bb51eb5461ad9')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  sed -i -e 's:png_ptr->jmpbuf:png_jmpbuf(png_ptr):' src/xsane-save.c
}

build() {
  cd "$srcdir/$pkgbase-$pkgver"

  ./configure --prefix=/usr \
    --mandir=/usr/share/man \
    --enable-gimp
  make
  mv src/xsane src/xsane-gimp

  # build again with gimp disabled
  make clean
  ./configure --prefix=/usr --mandir=/usr/share/man --disable-gimp
  make
}

package_xsane() {
  pkgdesc='A GTK-based X11 frontend for SANE and plugin for Gimp.'
  install=$pkgname.install

  depends=('gtk2' 'lcms2' 'sane' 'zlib' 'libjpeg-turbo')  
  optdepends=('xsane-gimp: for gimp plugin support')

  cd "$srcdir/$pkgbase-$pkgver"
  make DESTDIR="$pkgdir" install

  # Install license
  install -Dm644 xsane.COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_xsane-gimp() {
  pkgdesc='XSane Gimp plugin'
  depends=('xsane' 'gimp')

  cd "$srcdir/$pkgbase-$pkgver"
  install -D -m755 src/xsane-gimp "${pkgdir}/usr/bin/xsane-gimp"

  # For making Gimp Plugin available 
  mkdir -p "$pkgdir/usr/lib/gimp/2.0/plug-ins"
  ln -sf /usr/bin/xsane-gimp "$pkgdir"/usr/lib/gimp/2.0/plug-ins/xsane

  # Install license
  install -Dm644 xsane.COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
