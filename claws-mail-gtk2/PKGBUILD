# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=claws-mail-gtk2
realpkgname=claws-mail
pkgver=3.19.1
pkgrel=2
pkgdesc="A GTK+2 based e-mail client"
arch=('i686' 'x86_64')
license=('GPL-3')
conflicts=('claws-mail')
provides=('claws-mail')
url='https://www.claws-mail.org'
depends=('gtk2' 'gnutls' 'startup-notification' 'enchant' 'gpgme'
         'libetpan' 'compface' 'libsm')
makedepends=('poppler-glib' 'libytnef' 'libical'
             'bogofilter' 'valgrind' 'dillo' 'python' 'gumbo-parser' 'intltool' 'gettext-tiny')
optdepends=('perl: needed for perl plugin'
            'bogofilter: adds support for spamfiltering'
            'libxml2: for rssyl plugins'
            'curl: for vcalendar, rssyl and spamreport plugins'
            'libarchive: for archive plugin and various other plugins'
            'libytnef: for tnef_parse plugin'
            'poppler-glib: for pdf viewer plugin'
            'ghostscript: for pdf viewer plugin'
            'libical: for vcalendar plugin'
            'dillo: for html viewer plugin'
            'gumbo-parser: for litehtml plugin')
source=("https://www.claws-mail.org/releases/${realpkgname}-${pkgver}.tar.gz"
        "bash_completion"
        "rework-defs.patch"
        "uintptr_t.patch")
sha512sums=('f32eecb68162163eda707457dd98fecdc7c0a4b040521e2e789507e74fdcd35f0de3b6c6ced45cfc7d98278327ab9dfc5ebd3a43de856702ad0efb2cbaab3a35'
            '4779669133d077aaa52fc171d693b7ba93208cbc6fee5234550aa3f66ec12e5a5d200aab420ab9ceff65a9509cab3f643280b7b64a35d085d1e64a202d9f7c28'
            '327d4c8f065405108391f7baf9af0c57db8294dc2da3df58cab2031a748414da3a959a51aad78797a61fde541dda30a99be6669f5ba57317b854ff0bc5e66188'
            'bfa5911070d60fb979fce6776aa416b2a6bc0fec7a174b40698f3b8ced57fb2cc6235ba4209ffe1fe8a513e297f67aa804dd01f7d1414df7619c9c8622fabe17')

prepare() {
  cd "${realpkgname}"-${pkgver}
  patch -Np1 -i ${srcdir}/rework-defs.patch
  patch -Np0 < ${srcdir}/uintptr_t.patch
}

build() {
  cd "${realpkgname}"-${pkgver}
  ./configure --prefix=/usr --disable-static \
    --enable-enchant \
    --enable-gnutls \
    --enable-ldap \
    --enable-crash-dialog \
    --enable-pgpmime-plugin \
    --enable-bogofilter-plugin \
    --disable-dbus \
    --disable-networkmanager \
    --disable-gdata-plugin \
    --disable-notification-plugin \
    --disable-spamassassin-plugin \
    --disable-fancy-plugin
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd "${realpkgname}"-${pkgver}
  make DESTDIR="${pkgdir}" install

  install -Dm 644 "$srcdir"/bash_completion "${pkgdir}"/usr/share/bash-completion/completions/claws-mail

  # license
  install -Dm644 "${srcdir}/${realpkgname}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
