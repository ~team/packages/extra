# Maintainer (Arch): Popa Ioan Alexandru <alexioanpopa11@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=catcodec
pkgver=1.0.5
_debver=$pkgver
_debrel=4
pkgrel=1
pkgdesc="A tool for decoding/encoding .cat files for OpenTTD"
arch=('i686' 'x86_64')
url='https://github.com/OpenTTD/catcodec'
license=('GPL-2')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/c/catcodec/catcodec_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/catcodec/catcodec_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('8f8479b90e674515400573228bf0c8cb2ac38e40f861a1747e05f508d97a727b834e9b7505550ef71242f85c444e2db6dd4dad2b9b8577966dd458735f7059ba'
            'ed4e7c0bf58c4f2ef6bdb9374b196f720340924a1cfcf6fd5023479d3fa5ff298f19caff4f71b5dd3608dc7d6ae308875e463f18616feb1f5ada990d87375b9f')

prepare() {
  cd ${srcdir}/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${srcdir}/$pkgname-$pkgver
  make prefix=/usr
}

package() {
  cd ${srcdir}/$pkgname-$pkgver
  make prefix=/usr DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
