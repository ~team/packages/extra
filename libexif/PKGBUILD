# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Tobias Kieslich <tobias@justdreams.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libexif
pkgver=0.6.22
_debver=0.6.22
_debrel=3
pkgrel=1
pkgdesc='Library to parse an EXIF file and read the data from those tags'
url='https://github.com/libexif/libexif'
arch=(i686 x86_64)
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=(${url}/archive/${pkgname}-${pkgver//./_}-release.tar.gz
        https://deb.debian.org/debian/pool/main/libe/libexif/libexif_$_debver-$_debrel.debian.tar.xz)
sha512sums=('6c63abe2734c9e83fb04adb00bdd77f687165007c0efd0279df26c101363b990604050c430c7dd73dfa8735dd2fd196334d321bdb114d4869998f21e7bed5b43'
            '87dfece8707e44e99ffc9d08560e55d6b6f6806ca6f1a07a9646631e46b7508cf637590b02821299a98034afd0ac805b80105752199bff0c39f024945879ab88')

prepare() {
  cd ${pkgname}-${pkgname}-${pkgver//./_}-release
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -fiv
}

build() {
  cd ${pkgname}-${pkgname}-${pkgver//./_}-release
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgname}-${pkgver//./_}-release
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
