# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libdrm
pkgver=2.4.104
_debver=2.4.104
_debrel=1
pkgrel=1
pkgdesc="Userspace interface to kernel DRM services"
arch=(i686 x86_64)
license=('Expat')
depends=('libpciaccess')
makedepends=('xenocara-util-macros' 'python-docutils' 'meson' 'quilt')
replaces=('libdrm-new' 'libdrm-nouveau')
url="https://dri.freedesktop.org/"
source=(https://dri.freedesktop.org/$pkgname/$pkgname-$pkgver.tar.xz{,.sig}
        https://deb.debian.org/debian/pool/main/libd/libdrm/libdrm_$_debver-$_debrel.debian.tar.xz
        COPYING)
sha512sums=('0fdbef53e0e7c441c805c95ac55ca2c94f11e8fa18e36b4dc7534c22e2b5bc8eca7283fdf41785da753f98d0b589023111abdba70db7e79837729b1540253d6e'
            'SKIP'
            'f4e18613f814cd39a3ef985ad96e3ca7d392c55dbb5fedc47c2f971e0f0cbaeca03162eaad023a0eed4ca788c909b8d7450107031a764b5b6667e159b9654562'
            'b0ca349b882a4326b19f81f22804fabdb6fb7aef31cdc7b16b0a7ae191bfbb50c7daddb2fc4e6c33f1136af06d060a273de36f6f3412ea326f16fa4309fda660')
validpgpkeys=('B97BD6A80CAC4981091AE547FE558C72A67013C3') # Maarten Lankhorst <maarten.lankhorst@canonical.com>
validpgpkeys+=('34FF9526CFEF0E97A340E2E40FDE7BE0E88F5E48') # emersion <contact@emersion.fr>
validpgpkeys+=('215DEE688925CCB965BE5DA97C03D7797B6E1AE2') # Damien Lespiau <damien.lespiau@intel.com>
validpgpkeys+=('10A6D91DA1B05BD29F6DEBAC0C74F35979C486BE') # David Airlie <airlied@redhat.com>
validpgpkeys+=('8703B6700E7EE06D7A39B8D6EDAE37B02CEB490D') # Emil Velikov <emil.l.velikov@gmail.com>
validpgpkeys+=('D6285B5E899299F3DA746184191C9B905522B045') # Rob Clark <robclark@freedesktop.org>
validpgpkeys+=('E8EB5B34081CE1EEA26EFE195B5BDA071D49CC38') # Kenneth Graunke <kenneth.w.graunke@intel.com>
validpgpkeys+=('FC9BAE1435A9F7F664B82057B5D62936D1FC9EE8') # Eric Anholt <eric@anholt.net>
validpgpkeys+=('3BB639E56F861FA2E86505690FDD682D974CA72A') # Matt Turner <mattst88@gmail.com>
validpgpkeys+=('C20F5C4490D7D64B4C9A09998CD1DF552975297B') # Robert Bragg <robert@sixbynine.org>
validpgpkeys+=('CD47C5341A375F33BEF7BAFAFDD15D5ACEF0F2B1') # Marek Olšák <maraeo@gmail.com>
validpgpkeys+=('A66D805F7C9329B4C5D82767CCC4F07FAC641EFF') # Daniel Stone <daniels@collabora.com>
validpgpkeys+=('CFD0E654BCBE5DD2D030D222CFCC297C6D0A120B') # Lucas Stach <l.stach@pengutronix.de>

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  hyperbola-meson $pkgname-$pkgver build \
    -Dudev=false \
    -Dvalgrind=false
  ninja -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
