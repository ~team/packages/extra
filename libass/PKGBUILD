# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): G_Syme <demichan@mail.upb.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libass
pkgver=0.15.0
pkgrel=1
pkgdesc='A portable library for SSA/ASS subtitles rendering'
arch=('i686' 'x86_64')
url='https://github.com/libass/libass/'
license=('ISC')
depends=('fontconfig' 'fribidi' 'glib2' 'glibc' 'harfbuzz'
         'freetype2')
makedepends=('nasm')
source=("https://github.com/libass/libass/releases/download/${pkgver}/libass-${pkgver}.tar.xz")
sha512sums=('067ed519e64da7e2e45436bb09e390f1a77d966aeb9420729f36c9725d89491655ead69eb61b43f3220b8ff8f9de3891911b62757466f843ab13ee7149d88459')

build() {
  cd libass-${pkgver}

  ./configure \
    --prefix='/usr' \
    --enable-harfbuzz \
    --enable-fontconfig
  make
}

package() {
  cd libass-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -Dm 644 COPYING -t "${pkgdir}"/usr/share/licenses/libass/
}

# vim: ts=2 sw=2 et:
