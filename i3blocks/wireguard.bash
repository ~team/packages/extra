#!/bin/bash

INSTANCE="${BLOCK_INSTANCE:-}"

# Check VPN status
GET_VPN=$(ip route get 4.2.2.1 | grep -E -- "wg0|$INSTANCE" | awk '{ print $3 }')

# Store status
if [[ $GET_VPN == *"wg0"* ||  $GET_VPN == "$INSTANCE" ]]
then
    echo -e "<span color='#00FF00'>ON</span>\n"
else
    echo -e "<span color='#FFDD00'>OFF</span>\n"
fi
