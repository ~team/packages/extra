# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=perl-dbi
pkgver=1.643
_debver=1.643
_debrel=3
pkgrel=1
pkgdesc="Database independent interface for Perl"
arch=('i686' 'x86_64')
url="https://metacpan.org/release/DBI"
license=('GPL-1')
depends=('perl')
makedepends=('quilt')
checkdepends=('perl-test-pod' 'perl-test-pod-coverage')
options=('!emptydirs')
source=("https://www.cpan.org/authors/id/T/TI/TIMB/DBI-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libd/libdbi-perl/libdbi-perl_$_debver-$_debrel.debian.tar.xz")
sha512sums=('03812f3eb1e43c8290dadb8cb14bbced9ec6e237228ea2a2ba91f22e52143906a91a7e82945dab30b1d1b9fc925073721111adafd9a09fac070808ab88f908b8'
            'b57a0a78a8bcf3e6eb8c122f34e399913b928db8004de982bd2fd8809462b77e51d20923ca6f20b60bb31a6397c58799a6130bda17a392c29b4d6e9d6c00fa43')

prepare() {
  cd DBI-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd DBI-$pkgver
  perl Makefile.PL INSTALLDIRS=vendor
  make
}

check() {
  cd DBI-$pkgver
  make test
}

package() {
  cd DBI-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 LICENSE $pkgdir/usr/share/licenses/$pkgname/LICENSE
}
