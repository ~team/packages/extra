# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=gcompris-data
_pkgbase=GCompris-data
_pkgmain=gcompris-qt
pkgname=('gcompris-data-backgroundmusic' 'gcompris-data-voices' 'gcompris-data-words')
pkgver=20211201
pkgrel=1
pkgdesc="Datafiles for the free educational software suite GCompris"
arch=('any')
url="https://gcompris.net/"
makedepends=('qt-base')
source=("${pkgbase}-${pkgver}.zip::https://github.com/gcompris/GCompris-data/archive/refs/heads/master.zip"
        "fix-path.patch")
sha512sums=('d01ae252cd97f6badddc73a2ec66e88cc101025583a68023ddb6254c98fcf3a38f7500e9f748d44626b9c0b08c5a593fbe1dc79803f7d456704783e08f356160'
            '40bed718f3ce631cac1e2207e0d94c27e6c090cd292edd3b244ec2efd5cdcd6aeeb2c8f2882e4faef02c84099c23e2d1dd0033f725ba705106cdaef26a5f789b')

prepare() {
  mv "${srcdir}/${_pkgbase}-master" "${srcdir}/${pkgbase}-${pkgver}"

  cd ${srcdir}/${pkgbase}-${pkgver}/words
  sed -i 's/RCC_DEFAULT=$Qt5_DIR\/bin\/rcc/RCC_DEFAULT=\/usr\/bin\/rcc/g' \
         generate_lang_rcc.sh

  cd ${srcdir}/${pkgbase}-${pkgver}/voices
  sed -i 's/RCC_DEFAULT=$Qt5_DIR\/bin\/rcc/RCC_DEFAULT=\/usr\/bin\/rcc/g' \
         generate_voices_rcc.sh

  # additional fixes
  cd ${srcdir}/${pkgbase}-${pkgver}
  patch -Np1 -i ${srcdir}/fix-path.patch
}

build() {
  cd ${pkgbase}-${pkgver}

  # building background music
  cd ./background-music
  ./generate_backgroundMusic_rcc.sh ogg

  # building words
  cd ../words
  ./generate_lang_rcc.sh words

  # building voices
  cd ../voices
  ./generate_voices_rcc.sh ogg
}

package_gcompris-data-backgroundmusic() {
  pkgdesc+=" (background music)"
  license=('CC-BY-SA-3.0' 'CC-BY-SA-4.0' 'CC0-1.0')

  cd ${pkgbase}-${pkgver}/background-music

  install -d "${pkgdir}"/usr/share/$_pkgmain/data2/backgroundMusic
  cp .rcc/backgroundMusic-ogg.rcc "${pkgdir}"/usr/share/$_pkgmain/data2/backgroundMusic/

  install -Dm644 backgroundMusic/README -t "${pkgdir}"/usr/share/licenses/$pkgname
}

package_gcompris-data-voices() {
  pkgdesc+=" (voices)"
  license=('GPL-3' 'Public-Domain' 'CC-BY-SA-4.0')

  cd ${pkgbase}-${pkgver}/voices
  install -d "${pkgdir}"/usr/share/$_pkgmain/data2/voices-ogg
  cp .rcc/voices-ogg/voices-*.rcc "${pkgdir}"/usr/share/$_pkgmain/data2/voices-ogg/
  cp .rcc/full-ogg.rcc "${pkgdir}"/usr/share/$_pkgmain/data2/

  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/$pkgname
}

package_gcompris-data-words() {
  pkgdesc+=" (words)"
  license=('GPL-3')

  cd ${pkgbase}-${pkgver}/words
  install -d "${pkgdir}"/usr/share/$_pkgmain/data2/words
  cp .rcc/words.rcc "${pkgdir}"/usr/share/$_pkgmain/data2/words/

  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/$pkgname
}
