# Maintainer (Arch): Pierre Schmitz <pierre@archlinux.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=gd
pkgver=2.3.0
_debver=$pkgver
_debrel=2
pkgrel=3
pkgdesc="Library for the dynamic creation of images by programmers"
arch=('i686' 'x86_64')
url='https://libgd.github.io/'
license=('custom:GD')
depends=('fontconfig' 'libxpm' 'libtiff' 'libjpeg-turbo')
makedepends=('quilt')
optdepends=('perl: bdftogd script')
source=("https://github.com/libgd/libgd/releases/download/gd-${pkgver}/libgd-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/libg/libgd2/libgd2_$_debver-$_debrel.debian.tar.xz")
sha512sums=('5b201d22560e147a3d5471010b898ad0268c3a2453b870d1267b6ba92e540cf9f75099336c1ab08217e41827ac86fe04525726bf29ad117e5dcbaef9a8d0622a'
            '852899c79b4f0bb1862193150dc5bb05de10a536956b8064421dd02b5fdaed298997485bb9c356b0bf9144136cd0b16ca5c302bec6a5ed7e3bc7663f2f7f3efb')

prepare() {
  cd libgd-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0003-tests-make-a-little-change-for-autopkgtest.patch || true

    quilt push -av
  fi

  autoreconf -vfi
}

build() {
  cd libgd-${pkgver}

  ./configure \
    --prefix=/usr \
    --disable-rpath
  make
}

package() {
  cd libgd-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
