# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_pkgname=IPy
pkgbase=python-ipy
pkgname=('python-ipy' 'tauthon-ipy')
pkgver=0.83
pkgrel=1
pkgdesc="Class and tools for handling of IPv4 and IPv6 addresses and networks"
arch=('any')
url="https://github.com/autocracy/python-ipy"
license=('Modified-BSD')
makedepends=('python-setuptools' 'tauthon-setuptools')
source=("https://files.pythonhosted.org/packages/source/${_pkgname::1}/${_pkgname}/${_pkgname}-${pkgver}.tar.gz")
sha512sums=('6bd69d96fa77dc4ceaf119b81c19a524b871badb37665a763a055e1c8b2ce3433c6e9534f03aa64d02e559dca2fac2c4e1d09b7fd78ed79c0fded88f803ad2de')

prepare() {
  cp -rf "$_pkgname-$pkgver" "$_pkgname-$pkgver-tauthon"
}

build() {
  cd "$srcdir/$_pkgname-$pkgver"
  python setup.py build

  cd "$srcdir/$_pkgname-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-ipy() {
  pkgdesc+=" (Python)"
  depends=('python')
  cd "$srcdir/$_pkgname-$pkgver"
  python setup.py install --skip-build --optimize=1 --prefix=/usr --root="${pkgdir}"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm644 {AUTHORS,ChangeLog,README} -t "${pkgdir}/usr/share/doc/${pkgname}"
}

package_tauthon-ipy() {
  pkgdesc+=" (Tauthon)"
  depends=('tauthon')
  cd "$srcdir/$_pkgname-$pkgver-tauthon"
  tauthon setup.py install --skip-build --optimize=1 --prefix=/usr --root="${pkgdir}"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm644 {AUTHORS,ChangeLog,README} -t "${pkgdir}/usr/share/doc/${pkgname}"
}
