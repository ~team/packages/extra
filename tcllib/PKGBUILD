# Maintainer (Arch):  Gabriel Souza Franco
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): dtw <dibble.at.thewrecker.dot.net>
# Contributor (Arch): Mathieu Gauthier <mathgl@freesurf.fr>
# Contributor (Arch): Pawel Bogur <jid:smeagol@uaznia.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_pkgname=tcllib-libre
pkgname=tcllib
pkgver=1.20
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Set of pure-Tcl extensions."
arch=('any')
url="http://core.tcl.tk/tcllib/"
license=('custom:BSD' 'custom:RSA' 'custom:RSA2' 'custom:RSA3' 'Expat')
depends=('tcl')
makedepends=('quilt')
mksource=("https://core.tcl-lang.org/tcllib/uv/${pkgname}-${pkgver}.tar.gz")
source=("https://repo.hyperbola.info:50000/sources/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/t/tcllib/tcllib_${_debver}+dfsg-${_debrel}.debian.tar.xz"
        "libre.patch")
mksha512sums=('c7c386c08620724e058e0c2ef7b0900337d711d093cb6cf1c25f602152abb2d042a4411cb3ae712223171ff95629f7790e56c4a965e827a27bb70ab6bfe09f99')
sha512sums=('fc284f1f39019b128bf50f616119911e5720d5ffc30fd92f4a1121f3bfa776f5a83e5564434aba3642b771cfb43576e6fa44f606ced5d04ebcab607ef12696f2'
            'SKIP'
            '5d42040ade39b4b35777bdb132b44cce57673f8a30a4a0280603d2e4d176ac7762321c09af22dfb5f434afb503e484d5714ac566e124f7c5be2827156647996c'
            '675633a1e022b8f521f8eb84b24970c9991e4f9d6a175509dc6dd4a2ca87ef6c6ace3f119dea761763d371ace5101dfcd8c50fc5b1038f7c632bcc4b6c203a59')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

mksource() {
  cd "$srcdir"/$pkgname-$pkgver

  # Remove nonfree Amazon S3
  rm -rf modules/amazon-s3
  rm -rf idoc/man/files/modules/amazon-s3
}

prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/libre.patch
}

package(){
  cd "$srcdir"/$pkgname-$pkgver
  tclsh installer.tcl -pkg-path "$pkgdir"/usr/lib/tcllib \
	-app-path "$pkgdir"/usr/bin \
	-nroff-path "$pkgdir"/usr/share/man/mann \
	-no-examples -no-html \
	-no-wait  -no-gui
  install -Dm644 license.terms -t "$pkgdir"/usr/share/licenses/$pkgname
  for manp in graph; do
    mv "$pkgdir"/usr/share/man/mann/{,tcllib-}$manp.n
  done
}