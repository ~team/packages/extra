# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=poly2tri-c
pkgver=0.1.0
pkgrel=1
pkgdesc='C+GLib port of poly2tri - A 2D constrained Delaunay triangulation library'
depends=(glib2)
arch=(i686 x86_64)
url="https://code.google.com/archive/p/$pkgname/"
license=(Modified-BSD)
source=($pkgname-$pkgver.zip::https://storage.googleapis.com/google-code-archive-source/v2/code.google.com/$pkgname/source-archive.zip)
sha512sums=('c7b332402c8014cd03b1d6908224e9f3bb54676e01bceb9bd559bdab4e7632db16ea688b847f19cbcb5fa5177f3979a5653dc85def957aff0ec79d5ac8e903bf')

prepare() {
  cd $pkgname

  msg 'Remove pedantic errors'
  sed -i '\|CFLAGS| s|[ ][-]pedantic||' configure.ac

  msg "fix 'rcdt.lo' and 'rutils.lo' errors in $pkgname/refine/Makefile.am"
  sed -i 's|[ ]cdt.c| rcdt.c|; s|[ ]cdt.h| rcdt.h|' \
    $pkgname/refine/Makefile.am
  sed -i 's|[ ]utils.c| rutils.c|; s|[ ]utils.h| rutils.h|' \
    $pkgname/refine/Makefile.am
}

build() {
  cd $pkgname

  NOCONFIGURE=1 ./autogen.sh
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname
  install -Dm644 LICENSE-Poly2Tri.txt -t $pkgdir/usr/share/licenses/$pkgname
  install -Dm644 LICENSE-Poly2Tri-C.txt -t $pkgdir/usr/share/licenses/$pkgname
}

# vim:set sw=2 et:
