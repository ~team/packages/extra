# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=opensp
pkgver=1.5.2
pkgrel=1
pkgdesc="A library and a set of tools for validating, parsing and manipulating SGML and XML documents"
arch=('i686' 'x86_64')
url="http://openjade.sourceforge.net/"
license=('Expat')
depends=('sgml-common' 'perl')
makedepends=('xmlto' 'docbook-xsl')
source=("https://downloads.sourceforge.net/project/openjade/opensp/$pkgver/OpenSP-$pkgver.tar.gz")
sha512sums=('a7dcc246ba7f58969ecd6d107c7b82dede811e65f375b7aa3e683621f2c6ff3e7dccefdd79098fcadad6cca8bb94c2933c63f4701be2c002f9a56f1bbe6b047e')

build() {
  cd OpenSP-$pkgver
  ./configure --prefix=/usr \
    --mandir=/usr/share/man \
    --disable-nls \
    --enable-http \
    --enable-default-catalog=/etc/sgml/catalog \
    --enable-default-search-path=/usr/share/sgml:/usr/share/xml \
    --enable-xml-messages
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd OpenSP-$pkgver
  make check || /bin/true
}

package() {
  cd OpenSP-$pkgver
  make DESTDIR="${pkgdir}" install

  # add symlinks for compatibility with jade, see FS#49775
  for file in nsgmls sgmlnorm spam spcat spent sx ; do
    ln -s o$file "$pkgdir"/usr/bin/$file
    echo ".so man1/o${file}.1" > "$pkgdir"/usr/share/man/man1/${file}.1
  done
  ln -v -sf libosp.so "$pkgdir"/usr/lib/libsp.so

  # Rename sx to sgml2xml
  mv "$pkgdir"/usr/bin/sx "$pkgdir"/usr/bin/sgml2xml
  mv "$pkgdir"/usr/share/man/man1/{sx,sgml2xml}.1
  
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname
}
