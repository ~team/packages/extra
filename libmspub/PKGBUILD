# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): Simone Sclavi 'Ito' <darkhado@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=libmspub
pkgver=0.1.4
pkgrel=2
pkgdesc="A file format parser library"
arch=('x86_64' 'i686')
url='https://wiki.documentfoundation.org/DLP/Libraries/libmspub'
license=('MPL-2.0')
depends=('icu' 'librevenge' 'libwpd')
makedepends=('boost' 'doxygen' 'libwpg')
source=("https://dev-www.libreoffice.org/src/${pkgname}/${pkgname}-${pkgver}.tar.xz")
sha512sums=('7275f890645961b3fd56df4584788962e8c064fe3f99f5834c6ba6177ce76d00d544fbe9a25b7ab2f4180d2f3a90c609fe0bb68d61ea24e95b086190390fff31')

prepare() {
  cd ${pkgname}-${pkgver}

  # fix missing <cstdint> during building.
  sed -ie 's#<vector>#&\n\#include <cstdint>#' src/lib/MSPUBMetaData.h
  autoreconf -fiv
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure \
    --prefix=/usr \
    --with-docs
  make VERBOSE=1
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} VERBOSE=1 install
  install -Dvm644 COPYING.MPL -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
