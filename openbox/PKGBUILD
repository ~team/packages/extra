# Maintainer (Arch): Chih-Hsuan Yen <yan12125@archlinux.org>
# Contributor (Arch): Florian pritz <bluewind@xinu.at>
# Contributor (Arch): Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor (Arch): Brad Fanella <bradfanella@archlinux.us>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): tobias <tobias@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=openbox
pkgver=3.6.1
_debver=$pkgver
_debrel=10
pkgrel=5
pkgdesc="Highly configurable and lightweight X11 window manager"
arch=('i686' 'x86_64')
url='https://openbox.org'
license=('GPL-2')
depends=('startup-notification' 'libxml2' 'libxinerama' 'libxrandr'
         'libxcursor' 'pango' 'imlib2' 'librsvg-legacy' 'libsm' 'libxi')
makedepends=('python' 'quilt')
optdepends=('python-pyxdg: for the openbox-xdg-autostart script')
backup=('etc/xdg/openbox/menu.xml'
        'etc/xdg/openbox/rc.xml'
        'etc/xdg/openbox/autostart'
        'etc/xdg/openbox/environment')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/o/openbox/openbox_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/o/openbox/openbox_${_debver}-${_debrel}.debian.tar.xz"
        "libre.patch")
sha512sums=('5e6f4a214005bea8b26bc8959fe5bb67356a387ddd317e014f43cb5b5bf263ec617a5973e2982eb76a08dc7d3ca5ec9e72e64c9b5efd751001a8999b420b1ad0'
            '91eae46531a66554d57db3411414e5320a309f5ef75a14722b83f298b5bff3c1af2016df834b7ee6bb8937410da7b532ddfc29b670f03f73ffe6500b88216d87'
            'ffa2673a49b48dc41a6323667668ad5ecb4c08ba2355f3654e52957c15fb0144c633874fe338fd7085e587979bba210a34ee562e6ba175988729ece4139932d5')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # remove non-free and not supported applications
  patch -Np1 -i "${srcdir}/libre.patch"

  autoreconf -ifv
}

build() {
  cd "$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --with-x \
    --enable-startup-notification \
    --sysconfdir=/etc \
    --libexecdir=/usr/libexec/openbox
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install

  # remove gnome and included parts as being not part of Hyperbola
  rm -r "$pkgdir"/usr/bin/{gdm-control,gnome-panel-control,openbox-gnome-session} \
    "$pkgdir"/usr/share/gnome{,-session} \
    "$pkgdir"/usr/share/man/man1/openbox-gnome-session.1 \
    "$pkgdir"/usr/share/xsessions/openbox-gnome.desktop

  # remove kde and included parts as being not part of Hyperbola
  rm -rf "$pkgdir"/usr/bin/openbox-kde-session \
    "$pkgdir"/usr/share/man/man1/openbox-kde-session.1 \
    "$pkgdir"/usr/share/xsessions/openbox-kde.desktop

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}