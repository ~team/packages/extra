# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Gilles CHAUVIN <gcnweb@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libdvbpsi
pkgver=1.3.3
_debver=1.3.3
_debrel=1
pkgrel=1
epoch=1
pkgdesc='Library designed for decoding and generation of MPEG TS and DVB PSI tables'
url='https://www.videolan.org/developers/libdvbpsi.html'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=(https://code.videolan.org/videolan/${pkgname}/-/archive/${pkgver}/${pkgname}-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/libd/libdvbpsi/libdvbpsi_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('30bc8ce28754d6488b47f71e7e3adbb39e0d8927f7a8371fd146dbf42bb022fd2091bbf802c4554856de8c6a65a0773b0258e95a9c6b77e347fcf1b2e27261ee'
            '36c5348db15ab903094187d9ffdaa2cde601ffdd2c94c30503fdfb53d8f9a1cc22e5e74fbb85fc01f386175a5f6745d69126402409e5bb8cf6d9c1b86ceb883d')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -fiv
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
