# lighttpd logrotate script

/var/log/lighttpd/*.log {
        daily
        missingok
		copytruncate
        rotate 7
        compress
        notifempty
        sharedscripts
        postrotate
		test -e /run/openrc/softlevel && /etc/init.d/lighttpd reload 1>/dev/null || true
        endscript
}
