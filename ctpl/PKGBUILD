# Maintainer (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>   
# Contributor (Arch): Patrick Melo <patrick@patrickmelo.eti.br>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ctpl
pkgver=0.3.4
_debver=$pkgver
_debrel=1.1
pkgrel=2
pkgdesc="Template engine library written in C"
arch=('i686' 'x86_64')
url='https://ctpl.tuxfamily.org/'
license=('GPL-3')
depends=('glib2')
makedepends=('intltool' 'automake-1.14' 'quilt')
source=("https://download.tuxfamily.org/ctpl/releases/$pkgname-$pkgver.tar.gz"
        "http://deb.debian.org/debian/pool/main/c/ctpl/ctpl_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('5e4c7c4df2589008b6bcc0b5e83e953feef71d3553b8c8040cfd4f07ca7ddb33c6a9f17ba2366103a8353e3fdfd9fa200139277ae49a1768a7b7dfcb2e998b38'
            'c0a3119b3bb5d438ab2e3d5f40414a143d333929ea1c527b8ab52caf4ba99e749db233da3490e22cb1a0be678f117e1199ebaa7cdc46ac7b176674d6deceb69f')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"

  ./configure --prefix=/usr
  make 
}

package() {
  make -C "$pkgname-$pkgver" DESTDIR="$pkgdir" install

  # we don't support gtk-doc
  rm -rf $pkgdir/usr/share/gtk-doc

  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
