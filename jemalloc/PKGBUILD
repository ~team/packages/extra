# Maintainer (Arch):  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Kovivchak Evgen <oneonfire@gmail.com>
# Contributor (Arch): Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=jemalloc
pkgver=5.3.0
pkgrel=1
pkgdesc="General-purpose scalable concurrent malloc implementation"
arch=('i686' 'x86_64')
url='https://jemalloc.net/'
license=('Simplified-BSD')
makedepends=('clang')
optdepends=('perl: for jeprof')
source=("https://github.com/${pkgname}/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.bz2")
sha512sums=('22907bb052096e2caffb6e4e23548aecc5cc9283dce476896a2b1127eee64170e3562fa2e7db9571298814a7a2c7df6e8d1fbe152bd3f3b0c1abec22a2de34b1')

build() {
  cd ${pkgname}-${pkgver}
  export CC=clang && export CXX=clang++
  ./configure \
    --enable-prof \
    --enable-autogen \
    --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install
  chmod 644 ${pkgdir}/usr/lib/lib${pkgname}_pic.a
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}