# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Harry Jeffery <harry@exec64.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=imv
pkgver=4.3.0
_debver=$pkgver
_debrel=1.1
pkgrel=3
pkgdesc="Free software image viewer for X11"
url='https://github.com/eXeC64/imv'
arch=('i686' 'x86_64')
license=('Expat')
depends=('glu' 'libinih' 'libnsgif' 'libxkbcommon' 'libxkbcommon-x11' 'pango' 'freeimage')
makedepends=('asciidoc' 'cmocka' 'meson' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/eXeC64/imv/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/i/imv/imv_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('1735c03c48888004b11b3aaf0215a9530c035468a8efdb210d856551ebb75030c906eee2b6ab80e5d3646206e6051a73a8c5e15c010dd09bca7f9e90f9b0e34b'
            '7df76333ddbf1a8fedec86d8d38d00b59685813f0ad476c057e070bb08a9c9cd9652ffeda3389dae054ac7020fa1f85b3a186dc83cabd2a65b2edf19a3e4ca6f')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0002-Fix-segfault-with-latest-wlroots.patch || true

    quilt push -av
  fi
}

build() {
  hyperbola-meson build $pkgname-$pkgver \
   -D windows="x11" \
   -D librsvg=disabled \
   -D libheif=disabled
  ninja -C build
}

package() {
  DESTDIR="$pkgdir" ninja -C build install
  install -Dm644 "${srcdir}/$pkgname-$pkgver/LICENSE" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
