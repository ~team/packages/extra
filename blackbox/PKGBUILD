# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): durbatuluk <dvdscripter@gmail.org>
# Contributor (Arch): Steve Ponsford <sp4d@lavabit.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=blackbox
pkgname=('blackbox' 'libbt')
pkgver=0.70.1
_debver=$pkgver
_debrel=39
pkgrel=5
license=('Expat' 'GPL-2')
url='https://blackboxwm.sourceforge.net/'
arch=('i686' 'x86_64')
makedepends=('quilt' 'gcc-libs' 'libxft' 'libxext' 'libx11' 'libxt')
options=('staticlibs')
source=("https://downloads.sourceforge.net/blackboxwm/${pkgbase}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/b/blackbox/blackbox_${_debver}-${_debrel}.debian.tar.xz"
        "menu.patch")
sha512sums=('99d1cd344b37c012828d2892bb6dfab2385aba6f1e3d981f7d976affe553856acadd6e17809fc01d5f6e7c1b8a423acab05c3c44405a716d3141e87a71e38798'
            '7c0535bfdc437051cc5811a9ff8aaf65e546a24756b532220fa47456184716e9a84fefab451ad3e7ccb30d48913844f58af2560ee6c802219f6f1d55add8974a'
            'e109026c8aed4899d78d6c5b839f0f026e72e28881cf1194b24fe47a25a161754bb92fc318c9fe83d098a21bd20a3dfbcc39464995ee13cbda2f254545d1cc42')

prepare() {
  cd "$srcdir"/$pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/menu.patch
  autoreconf -fi
}

build() {
  cd "$srcdir"/$pkgbase-$pkgver
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --enable-shared
  make
}

package_blackbox() {
  pkgdesc="A window manager for X11"
  depends=('libbt')

  cd "$srcdir"/$pkgbase-$pkgver
  make DESTDIR="$pkgdir" install

  # cleanup, we provide a separate package for this
  rm -rf "$pkgdir"/usr/{include,lib}

  install -D -m644 debian/$pkgname.desktop -t "$pkgdir"/usr/share/xsessions
  install -D -m644 debian/pixmaps/*.xpm -t "$pkgdir"/usr/share/pixmaps/
  install -D -m644 debian/pixmaps/blackbox_48x48.xpm "$pkgdir"/usr/share/pixmaps/$pkgname.xpm
  install -D -m644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_libbt() {
  pkgdesc="Shared library for Blackbox"
  depends=('gcc-libs' 'libxft' 'libxext' 'libx11' 'libxt')

  cd "$srcdir"/$pkgbase-$pkgver
  make DESTDIR="$pkgdir" install

  # cleanup, using therefore the window-manager package
  rm -rf "$pkgdir"/usr/{bin,share}

  install -D -m644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}
