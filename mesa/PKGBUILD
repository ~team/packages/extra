# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on mesa package

pkgbase=mesa
pkgname=('mesa' 'mesa-libgl')
pkgdesc="A free software implementation for graphics drivers and rendering specifications"
pkgver=20.3.5
_debver=$pkgver
_debrel=1
pkgrel=9
arch=('i686' 'x86_64')
makedepends=('python-mako' 'libxml2' 'libx11' 'xenocara-proto' 'libdrm' 'libxshmfence' 'libxxf86vm'
             'libxdamage' 'elfutils' 'llvm' 'libomxil-bellagio' 'clang' 'libunwind' 'libxrandr'
             'glslang' 'meson' 'quilt')
url='https://www.mesa3d.org/'
license=('Expat')
source=("https://archive.mesa3d.org//older-versions/20.x/mesa-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/m/mesa/mesa_${_debver}-${_debrel}.diff.gz"
        "LICENSE"
        "drirc")
sha512sums=('481e710ed80d8f215a8d541cfe51b960862c2403c2b9e7e6932c8236b8decb5d478871d6c73559d6d795ada143803764f3a93b2329588c80f62e2e5ec98c78a9'
            '68f030adedc00fd1b607055e3b99cfa855062c43310d326c838d16515c2f652b3b2b1cdb62bdecf5bf067a583d8cdcfc2f46083db614c22585e23ff416ac07d0'
            'f9f0d0ccf166fe6cb684478b6f1e1ab1f2850431c06aa041738563eb1808a004e52cdec823c103c9e180f03ffc083e95974d291353f0220fe52ae6d4897fecc7'
            '03438356f152cbfede359314c100f4a93c04a856a12a6daafe3bdbf749661bdc8a28f11c92b73c317cfccf5c1f022f80434cd918a53ebe2c3df1fe1504164bee')

prepare() {
  cd $srcdir/mesa-$pkgver

  if [ ${pkgver%.*} = ${_debver%.*} ]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    patch -p1 -i "$srcdir"/mesa_$_debver-$_debrel.diff

    # Doesn't apply
    rm -v debian/patches/fix-python-shebang.diff || true

    quilt push -av
  fi
}

build() {
  DRI=i915,i965,r100,r200,nouveau
  GALLIUM=iris,r300,r600,radeonsi,nouveau,lima,panfrost,etnaviv,kmsro,virgl,svga,swrast,swr
  SWR_ARCHES=avx,avx2,knl,skx

  # Configure
  hyperbola-meson mesa-$pkgver build \
    -D b_lto=true \
    -D b_ndebug=true \
    -D platforms=x11 \
    -D dri-drivers=$DRI \
    -D gallium-drivers=$GALLIUM \
    -D swr-arches=$SWR_ARCHES \
    -D zstd=disabled \
    -D dri3=enabled \
    -D egl=enabled \
    -D gallium-extra-hud=true \
    -D gallium-nine=true \
    -D gallium-omx=bellagio \
    -D gallium-opencl=disabled \
    -D gallium-va=disabled \
    -D gallium-vdpau=disabled \
    -D gallium-xa=enabled \
    -D gallium-xvmc=disabled \
    -D gbm=enabled \
    -D gles1=enabled \
    -D gles2=enabled \
    -D glvnd=false \
    -D glx=dri \
    -D libunwind=enabled \
    -D llvm=enabled \
    -D lmsensors=disabled \
    -D opencl-spirv=false \
    -D osmesa=gallium \
    -D shared-glapi=enabled \
    -D shared-llvm=enabled \
    -D valgrind=disabled

  # Print configuration
  meson configure build

  # Build
  ninja -C build
  meson compile -C build
}

package_mesa() {
  depends=('libdrm' 'libxxf86vm' 'libxdamage' 'libxshmfence' 'libelf'
           'libomxil-bellagio' 'libunwind' 'llvm-libs')
  provides=('ati-dri' 'intel-dri' 'nouveau-dri' 'svga-dri'
            'swrast-dri' 'virgl-dri' 'swr-dri' 'mesa-dri' 'opengl-driver')
  conflicts=('ati-dri' 'intel-dri' 'nouveau-dri' 'svga-dri'
             'swrast-dri' 'virgl-dri' 'swr-dri' 'mesa-dri'
             'nvidia-'{,{390,340,304,173,96}xx-}'utils' 'catalyst-utils'
             'libtxc_dxtn')
  replaces=('ati-dri' 'intel-dri' 'nouveau-dri' 'svga-dri'
            'swrast-dri' 'virgl-dri' 'swr-dri' 'mesa-dri'
            'nvidia-'{,{390,340,304,173,96}xx-}'utils' 'catalyst-utils'
            'libtxc_dxtn')
  backups=('etc/drirc')

  # Fake installation to be seperated into packages
  # outside of fakeroot but mesa does not need to chown and chmod
  DESTDIR=$srcdir/fakeinstall meson install -C build

  # Copy drirc
  install -Dm 644 $srcdir/drirc -t $pkgdir/etc

  # Move usr/include/d3dadapter and usr/include/*.h
  install -dm 755 $pkgdir/usr/include
  mv -v $srcdir/fakeinstall/usr/include/d3dadapter $pkgdir/usr/include
  mv -v $srcdir/fakeinstall/usr/include/*.h $pkgdir/usr/include

  # Move usr/lib/{d3d,libomxil-bellagio0}
  install -dm 755 $pkgdir/usr/lib
  for i in d3d libomxil-bellagio0; do
    mv -v $srcdir/fakeinstall/usr/lib/${i} $pkgdir/usr/lib
  done
  unset i

  # Move usr/lib/dri/*_dri.so
  install -dm 755 $pkgdir/usr/lib/dri
  mv -v $srcdir/fakeinstall/usr/lib/dri/*_dri.so $pkgdir/usr/lib/dri

  # Move usr/lib/pkgconfig/{d3d,dri,gbm,osmesa,xatracker}.pc
  install -dm 755 $pkgdir/usr/lib/pkgconfig
  for i in d3d dri gbm osmesa xatracker; do
    mv -v $srcdir/fakeinstall/usr/lib/pkgconfig/${i}.pc \
      $pkgdir/usr/lib/pkgconfig
  done
  unset i

  # Move usr/lib/lib{OS,gbm,glapi,swr,xatracker}*.so*
  for i in OS gbm glapi swr xatracker; do
    mv -v $srcdir/fakeinstall/usr/lib/lib${i}*.so* $pkgdir/usr/lib
  done
  unset i

  # Copy LICENSE
  install -Dm 644 $srcdir/LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_mesa-libgl() {
  pkgdesc="Mesa 3D graphics library"
  depends=('mesa')
  provides=('libgl' 'libegl' 'libgles')
  conflicts=('nvidia-libgl' 'nvidia-340xx-libgl' 'nvidia-304xx-libgl' 'nvidia-cg-toolkit' 'catalyst-libgl')
  replaces=('nvidia-libgl' 'nvidia-340xx-libgl' 'nvidia-304xx-libgl' 'nvidia-cg-toolkit' 'catalyst-libgl')

  # Move usr/include/{EGL,GL,GLES2,GLES3,KHR}
  install -dm 755 $pkgdir/usr/include
  for i in EGL GL GLES GLES2 GLES3 KHR; do
    mv -v $srcdir/fakeinstall/usr/include/${i} $pkgdir/usr/include
  done
  unset i

  # Move usr/lib/pkgconfig/{egl,gl,glesv1_cm,glesv2}.pc
  install -dm 755 $pkgdir/usr/lib/pkgconfig
  for i in egl gl glesv1_cm glesv2; do
    mv -v $srcdir/fakeinstall/usr/lib/pkgconfig/${i}.pc \
      $pkgdir/usr/lib/pkgconfig
  done
  unset i

  # Move usr/lib/lib{EGL,GL,GLESv1_CM,GLES2}.so
  for i in EGL GL GLESv1_CM GLESv2; do
    mv -v $srcdir/fakeinstall/usr/lib/lib${i}.so* $pkgdir/usr/lib
  done
  unset i

  # Make sure there are no files left to install
  find fakeinstall -depth -print0 | xargs -0 rm -rf

  # Copy LICENSE
  install -Dm 644 $srcdir/LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}