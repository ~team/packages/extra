# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=harfbuzz
pkgname=('harfbuzz' 'harfbuzz-icu')
pkgver=2.6.7
pkgrel=3
pkgdesc="OpenType text shaping engine"
url='https://harfbuzz.github.io/'
arch=('i686' 'x86_64')
license=('custom:Old-Style-Expat')
makedepends=('glib2' 'freetype2' 'graphite' 'cairo' 'icu' 'gobject-introspection' 'ragel' 'python')
source=("https://github.com/harfbuzz/harfbuzz/releases/download/${pkgver}/${pkgbase}-${pkgver}.tar.xz")
sha512sums=('6fdd6e0952a73e1949349aa5416ef8fb3fc351b15c95be4fe1f341b111159fe58113b73a334db2697f4e3aaef5a761bd8f1d8964514406cad40f9862768d59de')

prepare() {
  cd $pkgbase-$pkgver
  autoreconf
}

build() {
  cd $pkgbase-$pkgver
  ./configure \
    --prefix=/usr \
    --with-cairo \
    --with-freetype \
    --with-glib \
    --with-gobject \
    --with-graphite2 \
    --with-icu \
    --disable-gtk-doc
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package_harfbuzz() {
  depends=('glib2' 'freetype2' 'graphite')
  optdepends=('cairo: hb-view program')

  cd $pkgbase-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 COPYING

  # split up harfbuzz-icu
  mkdir -p ../hb-icu/usr/{include/harfbuzz,lib/pkgconfig}; cd ../hb-icu
  mv "$pkgdir"/usr/lib/libharfbuzz-icu* ./usr/lib
  mv "$pkgdir"/usr/lib/pkgconfig/harfbuzz-icu.pc ./usr/lib/pkgconfig
  mv "$pkgdir"/usr/include/harfbuzz/hb-icu.h ./usr/include/harfbuzz
}

package_harfbuzz-icu() {
  pkgdesc="$pkgdesc (ICU integration)"
  depends=("harfbuzz=$pkgver-$pkgrel" 'icu')

  mv hb-icu/* "$pkgdir"

  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 $pkgbase-$pkgver/COPYING
}
