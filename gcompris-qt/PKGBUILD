# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Jose Riha <jose1711 gmail com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gcompris-qt
pkgver=1.1
pkgrel=2
pkgdesc="Free and libre educational software suite comprising of numerous activities"
arch=('i686' 'x86_64')
url='https://gcompris.net/'
license=('AGPL-3')
depends=('qt-multimedia' 'qt-svg' 'qt-sensors'
         'qt-quickcontrols' 'qt-graphicaleffects' 'qt-xmlpatterns'
         'qml-box2d' 'hicolor-icon-theme' 'gcompris-data-backgroundmusic'
         'gcompris-data-voices' 'gcompris-data-words')
makedepends=('cmake' 'extra-cmake-modules' 'qt-tools' 'kdoctools')
source=("https://gcompris.net/download/qt/src/$pkgname-$pkgver.tar.xz"
        "fix-build.patch"
        "gcompris-qt.sh")
sha512sums=('754087dbd8c409136dd82bcc8c308ecb85ec49341e91c99ace727ae6742cb3b9479b2d85fdff99a4d117788d77e253d9d26e3d7fa835193e6644bd7057c26719'
            'dd9125fcc52449e4f8d0efd4bbc5d09b2bea091b3c8970e861fb993ab7d90bae563effd3edb1080b8daa3f8fb89ceb32840f3bcb44d0f6eb7b805b6eb9cce390'
            '364fb76e4b5f7304a155e38c5728795cf9ca180fee16bffb58fb26543aed39cab1790def02daad2fcfef77ceec8b4dafc886d87f21283260dc5d44523219e8da')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i "${srcdir}/fix-build.patch"
}

build() {
  cmake -B build -S $pkgname-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DQML_BOX2D_LIBRARY=/usr/lib/qt/qml/Box2D.2.0 \
    -DWITH_DOWNLOAD=OFF
  make -C build
}

package() {
  make -C build DESTDIR="${pkgdir}" install

  # rename binary and insert alternative startup-routine
  mv "${pkgdir}"/usr/bin/$pkgname "${pkgdir}"/usr/bin/$pkgname.bin
  install -Dm755 "${srcdir}"/$pkgname.sh "$pkgdir"/usr/bin/$pkgname

  # license
  install -Dm644 "${srcdir}"/$pkgname-$pkgver/COPYING.txt -t "${pkgdir}"/usr/share/licenses/$pkgname
}
