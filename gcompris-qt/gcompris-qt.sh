#!/usr/bin/env bash

CURR_USER=$(whoami)
DIR="/home/$CURR_USER/.cache/KDE/gcompris-qt/"

if [ -d "$DIR" ]; then
	exec "/usr/bin/gcompris-qt.bin"
else
	mkdir -p $DIR
	cp -R /usr/share/gcompris-qt/data2 $DIR
	exec "/usr/bin/gcompris-qt.bin"
fi
