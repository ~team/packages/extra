# Maintainer (Arch): Lubosz Sarnecki <lubosz@gmail.com>
# Contributor (Arch): Kerrick Staley <mail@kerrickstaley.com>
# Contributor (Arch): Thijs Vermeir <thijsvermeir@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gst-editing-services
pkgver=1.18.3
pkgrel=1
pkgdesc="Multimedia graph framework - editing services"
arch=('i686' 'x86_64')
license=('LGPL-2')
url='https://gstreamer.freedesktop.org/modules/gst-editing-services.html'
depends=('gst-plugins-base' 'python')
makedepends=('intltool' 'pkgconfig' 'meson' 'ninja' 'cmake' 'gobject-introspection' 'bash-completion')
options=('!libtool')
source=("https://gstreamer.freedesktop.org/src/gst-editing-services/${pkgname}-${pkgver}.tar.xz")
sha512sums=('7d4ee5d755fca0c895ebc7121c3af52538299a4452594cccf4926417253df5c5a6f3eee8bce26a937e0c2380d9220f52b12aefdc6a9a0a55b32fea13a62b3443')

build() {
  local meson_options=(
    -D tests=disabled
    -D doc=disabled
    -D examples=disabled
    -D validate=disabled
  )
  hyperbola-meson $pkgname-$pkgver build "${meson_options[@]}"
  meson compile -C build
}

package() {
  cd build
  DESTDIR="${pkgdir}/" meson install
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING.LIB" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
