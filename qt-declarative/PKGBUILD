# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=qt-declarative
_qtver=5.15.2
pkgver=${_qtver/-/}
_debver=$pkgver
_debrel=6
pkgrel=3
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Classes for QML and JavaScript languages'
depends=('qt-base')
makedepends=('python' 'quilt')
groups=('qt')
replaces=('qt5-declarative')
conflicts=('qt5-declarative' 'qtchooser')
provides=('qt5-declarative')
_pkgfqn="${pkgname/-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz"
        "https://deb.debian.org/debian/pool/main/q/qtdeclarative-opensource-src/qtdeclarative-opensource-src_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('a084e4ace0d6868668c95f1b62598a7dd0f455bfb0943ac8956802d7041436686f20c7ccdde7d6fd6c4b8173c936dd8600cf3b87bf8575f55514edfbb51111d3'
            '3616a8d70aa7e092c1f34da9bdadb9f3358451620f9b5a052c2514b644f76138fd53ea8462d832fa8389ee66c1322c73d7d85f5ff943293f7c101f986cdd48e9')

prepare() {
  mkdir -p build

  cd ${_pkgfqn}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Symlinks for backwards compatibility
  for b in "$pkgdir"/usr/bin/*; do
    ln -s $(basename $b) "$pkgdir"/usr/bin/$(basename $b)-qt5
  done

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}