# Maintainer (Arch): Brian Bidulock <bidulock@openss7.org>
# Contributor (Arch): lth <manuelung@gmail.com>
# Contributor (Arch): nizox <nizzox@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dhcpcd-ui
pkgver=0.7.8
pkgrel=4
pkgdesc="dhcpcd Monitor in GTK+"
url='https://roy.marples.name/projects/dhcpcd-ui'
arch=('i686' 'x86_64')
license=('Simplified-BSD')
depends=('gtk2' 'dhcpcd' 'hicolor-icon-theme')
makedepends=('graphicsmagick')
source=("https://repo.hyperbola.info:50000/sources/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "fix-icons.patch"
        "fix-messages.patch"
        "dhcpcd-gtk.desktop")
sha512sums=('5f0d2c32c32fc141feca0ab43a531d9b4f45e071a8a1e3ee1923d6394a3e5d6322e6f7e607b91154af4cfee93d649164c7ed3b805731dfc242c3ac367ec156c4'
            'SKIP'
            'a612c65d6620f319f0e901dde7345a2a73a622b979d70a80c234fcab83b1d216786a5f6a22b5efbc11df71e3525a650e15c12abf36f3badb455ad0aa67246888'
            'c1c4ec6df1438fd9b9cdb99a7183269f3d188706b94fa1d798678f653f417e661aaf9c69d41c936b6e4b6e8ddb4d8ed18da2f010b044ecc3a651edf71107f997'
            'aa7dc78af5374356b0f0a465c17ce2679a9bd24b93558ca915872d3a7dac65434d5151f6a7f729495c9ce4223998b3a6a84391610103b2b0610d1062f76f0bf2')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  # additional fixes
  patch -Np1 -i ../fix-icons.patch
  patch -Np1 -i ../fix-messages.patch

  # fixing icons, 16x16
  cd $srcdir/$pkgname-$pkgver/icons/16x16/actions
  for file in ./*.png; do
    gm convert "$file" -resize 16x16 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/16x16/devices
  for file in ./*.png; do
    gm convert "$file" -resize 16x16 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/16x16/status
  for file in ./*.png; do
    gm convert "$file" -resize 16x16 "$file"
  done

  # fixing icons, 22x22
  cd $srcdir/$pkgname-$pkgver/icons/22x22/actions
  for file in ./*.png; do
    gm convert "$file" -resize 22x22 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/22x22/devices
  for file in ./*.png; do
    gm convert "$file" -resize 22x22 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/22x22/status
  for file in ./*.png; do
    gm convert "$file" -resize 22x22 "$file"
  done

  # fixing icons, 24x24
  cd $srcdir/$pkgname-$pkgver/icons/24x24/actions
  for file in ./*.png; do
    gm convert "$file" -resize 24x24 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/24x24/devices
  for file in ./*.png; do
    gm convert "$file" -resize 24x24 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/24x24/status
  for file in ./*.png; do
    gm convert "$file" -resize 24x24 "$file"
  done

  # fixing icons, 32x32
  cd $srcdir/$pkgname-$pkgver/icons/32x32/actions
  for file in ./*.png; do
    gm convert "$file" -resize 32x32 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/32x32/devices
  for file in ./*.png; do
    gm convert "$file" -resize 32x32 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/32x32/status
  for file in ./*.png; do
    gm convert "$file" -resize 32x32 "$file"
  done

  # fixing icons, 48x48
  cd $srcdir/$pkgname-$pkgver/icons/48x48/actions
  for file in ./*.png; do
    gm convert "$file" -resize 48x48 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/48x48/devices
  for file in ./*.png; do
    gm convert "$file" -resize 48x48 "$file"
  done
  cd $srcdir/$pkgname-$pkgver/icons/48x48/status
  for file in ./*.png; do
    gm convert "$file" -resize 48x48 "$file"
  done
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --without-qt \
    --disable-notification \
    --with-icons
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install

  # additional files and license
  install -Dm644 "$srcdir/dhcpcd-gtk.desktop" -t "$pkgdir/usr/share/applications/"
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  sed -n '1,/^$/p' src/dhcpcd-gtk/main.c \
	>"$pkgdir/usr/share/licenses/$pkgname/license.txt"
}
