# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Rémy Oudompheng <remy@archlinux.org>
# Contributor (Arch): Morgan LEFIEUX <comete@archlinuxfr.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jorge López <jorginho@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: aloniv

pkgname=hplip-drivers
_pkgname=hplip
pkgver=3.21.2
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Free and libre device drivers for HP-printer (DeskJet, OfficeJet, Photosmart, Business Inkjet and some LaserJet)"
arch=('i686' 'x86_64')
url='https://developers.hp.com/hp-linux-imaging-and-printing'
license=('GPL-2' 'GPL-3' 'Expat' 'Modified-BSD')
depends=('ghostscript' 'net-snmp' 'foomatic-db-engine' 'python')
makedepends=('sane' 'cups' 'libusb' 'quilt')
optdepends=('cups: for printing support'
            'sane: for scanner support'
            'xsane: sane scanner frontend'
            'libusb: for advanced usb support')
backup=('etc/hp/hplip.conf'
        'etc/sane.d/dll.d/hpaio')
source=(https://downloads.sourceforge.net/${_pkgname}/${_pkgname}-${pkgver}.tar.gz{,.asc}
        https://deb.debian.org/debian/pool/main/h/hplip/hplip_${_debver}+dfsg1-${_debrel}.debian.tar.xz
        disable_upgrade.patch
        add-include-cups-ppd.h-in-various-places-as-CUPS-2.2.patch
        remove-systemd-support.patch)
sha512sums=('73ba37275cfe34a58b81c9656514e15da67c1a69af5471ad132a1538d324efe640879cb7e60c359915607e41b63e653e7ae757661e553235f6e83e378ab46474'
            'SKIP'
            '771bfae13f452f637696ef7947e95c21e5cb514cd613fb15d182f0e0fbb02ce60684c75a7cb8245423bbcffe2c7d1a155f4109fe1769b0badb7d006bb9406ff2'
            '6abd3b3fbc827b9a618e54b3b96b6fa574fa6477845ed385ebd972c26eeb33139591ddc808d110fbb19ce9e002898590b900cc8dcbc933fd837de781f9ada71f'
            '22aeb5b851f78bc6bc62e0bc3da99fecaf42d7604af41e2f3343f8d3666541f7b06b7d1a7d0ddf24f1731ac7b12dfe582375a98e3b94dfa323d6ce954549ca67'
            'abd64e02965190cdfb7f7fd65d1875da4420594b627cd85b7f1da1f69a823b55358146e02d81e50e90000e21ba6e69d25fbb785dd489f1f3f461e50ce16f11e7')
validpgpkeys=('4ABA2F66DBD5A95894910E0673D770CDA59047B9') # HPLIP (HP Linux Imaging and Printing) <hplip@hp.com>

prepare() {
  cd $_pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0022-LaserJet-PostScript-4000-PPD-bugfix.patch || true
    rm -v debian/patches/0052-IEEE1284-Device-ID-for-HP-LaserJet-4000.patch || true
    rm -v debian/patches/0053-Fix-ImageableArea-for-Laserjet-8150-9000.patch || true

    quilt push -av
  else
    # add missing 'include <cups/ppd.h>' at various places
    patch -Np1 -i ${srcdir}/0022-Add-include-cups-ppd.h-in-various-places-as-CUPS-2.2.patch
  fi

  # based on https://devel.trisquel.info/trisquel/package-helpers/raw/master/helpers/make-hplip
  # keep header license
  sed  '/\[/,99999d' data/models/models.dat > mktemp

  for model in $(grep '\[' data/models/models.dat | sed 's/\[//; s/\]//'); do
    sed -n "/\[$model\]/,/^$/p;" data/models/models.dat > mktemp1
    grep '^download=True' -q mktemp1 && continue
    grep '^plugin=1' -q mktemp1 && continue
    grep '^support-type=0' -q mktemp1 && continue
    cat mktemp1 >> mktemp
  done

  sed -i 's/plugin=2/plugin=0/g' mktemp

  cp mktemp data/models/models.dat

  rm -v mktemp{,1}

  # remove systemd support
  patch -p1 -i ../remove-systemd-support.patch

  # remove nonfree software recommendation
  sed -i 's/\, requires proprietary plugin//' $(grep -rlI '[,] requires proprietary plugin')

  # disable insecure update
  patch -Np0 -i ${srcdir}/disable_upgrade.patch

  export AUTOMAKE='automake --foreign'
  autoreconf --force --install
}

build() {
  cd $_pkgname-$pkgver

  # disable dbus for build including fax
  # avahi is needed for network also disabled
  # no gui-support needed furthermore
  ./configure --prefix=/usr \
              --with-cupsbackenddir=/usr/libexec/cups/backend \
              --with-cupsfilterdir=/usr/libexec/cups/filter \
              --enable-hpcups-install \
              --enable-new-hpcups \
              --enable-cups-ppd-install \
              --enable-cups-drv-install \
              --enable-foomatic-drv-install \
              --enable-foomatic-ppd-install \
              --enable-pp-build \
              --disable-dbus-build \
              --disable-fax-build \
              --disable-gui-build \
              --disable-qt3 \
              --disable-qt4 \
              --disable-qt5 \
              --disable-foomatic-rip-hplip-install \
              --disable-polkit \
              --disable-network-build \
              --disable-doc-build
  make
}

package() {
  cd $_pkgname-$pkgver
  make -j1 rulesdir=/lib/udev/rules.d DESTDIR="$pkgdir/" install

  # install license
  install -Dm644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname

  # remove config provided by sane and autostart of hp-daemon
  rm -rf "$pkgdir"/etc/{sane.d,xdg}
  install -dm755 ${pkgdir}/etc/sane.d/dll.d
  echo hpaio > ${pkgdir}/etc/sane.d/dll.d/hpaio

  # remove HAL .fdi file because HAL is no longer used
  rm -vrf "$pkgdir"/usr/share/hal

  # cleanup
  rm -rf "$pkgdir"/usr/share/applications
  rm -rf "$pkgdir"/usr/share/hplip/data/localization
  rm -rf "$pkgdir"/usr/bin
  rm -rf "$pkgdir"/usr/share/hplip/*.py
  rm -rf "$pkgdir"/usr/share/hplip/hplip_clean.sh
  rm -rf "$pkgdir"/usr/share/hplip/scan
  rm -rf "$pkgdir"/usr/share/hplip/prnt
  rm -rf "$pkgdir"/usr/share/hplip/pcard
  rm -rf "$pkgdir"/usr/share/hplip/installer
  rm -rf "$pkgdir"/usr/share/hplip/copier
  rm -rf "$pkgdir"/usr/share/hplip/base
}
