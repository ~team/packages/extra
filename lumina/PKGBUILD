# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Sagar Acharya
# Contributor: Jesús E.

pkgbase=lumina
pkgname=(lumina-core lumina-core-utils
         lumina-archiver lumina-fileinfo lumina-fm
         lumina-photo lumina-screenshot
         lumina-textedit lumina-calculator)
pkgver=1.6.0
pkgrel=8
pkgdesc="Lumina Desktop Environment"
arch=('i686' 'x86_64')
url='https://lumina-desktop.org/'
license=('Modified-BSD')
makedepends=(libxcb libxcursor libxdamage qt-base qt-multimedia qt-svg qt-x11extras qt-xcb-private-headers xcb-util xcb-util-image xcb-util-wm)
source=($pkgbase-$pkgver.tar.gz::https://github.com/lumina-desktop/lumina/archive/refs/tags/v$pkgver.tar.gz
        $pkgbase-calculator-$pkgver.tar.gz::https://github.com/lumina-desktop/lumina-calculator/archive/refs/tags/v$pkgver.tar.gz
        distributor-logo-dark.svg
        distributor-logo-light.svg
        libre.patch
        boot-splash.patch
        remove-xscreensaver-support.patch)
sha512sums=('e923961568ab9cc32206386066daef8cd1fd2db2d5c3e260e5c1da1dcda39a516576418f607cefbe5a1c47d8684591d0e595fb344e7d6aa2859fc3b326e73e30'
            'e4005a3b17ff46a90b07f6dd22c7b2c6156c9c26396aefd33394fa5bbb434437e5361a276868b8e2b8753a17b13604b448094b1c04a34e3360fae4f78a271e37'
            '58646a70b7a7586662592e7adafed30a679d5a9b06d3f7e0936b7bab9012cde6d92f413019cbff2e90f1864cc484d5b322c57ec4fb978bfc2a7f304fedbe1d74'
            '73220ecc0a67e91dca646a81b34351c1933b110aecc34abe0901b37c525bcfc1cc23f395958308fd88074d43926ebb674de6c2007df1db0e1f3aeb4fe80158ae'
            '7f8a7ee6613405a9141d891acf4223b01c2584305f193e5897907c149c8e6f50a414f0b95842a0ef8634c7a20cf8881f86e943af587f92a8e9196904f69e482e'
            '3e03896bcd450cfd6077526de93ede976170fee71935e43f88687fdd18d2a6f04111e291adcb134f745002366c6169f7c44fb7123ffdbac9e585599c4952419b'
            '0bc569cf61b20251d1480631d6eaaa0f8d1f348aa8ea6ad41400b3984625aec6f2b4a0401d8ad5d064ee99edf6bd7e0a9f3746be54098dc0e30ec27ad7658fe0')

prepare() {
  cd $pkgbase-$pkgver

  # remove non-free data
  patch -p1 -i ${srcdir}/libre.patch

  # remove unneeded messages within boot-splash
  # we want to provide a clean and fitting experience
  patch -Np1 -i ${srcdir}/boot-splash.patch

  # remove xscreensaver-support
  # we do not want further work with this project
  patch -Np1 -i ${srcdir}/remove-xscreensaver-support.patch

  # array declaration for removals in icon-sets
  # we do not want to provide non-free data
  declare -a non_free=(
    # icons that have problematic issues
    'nodejs'
    'npm'
    'umbraco'

    # icons that are related to non-free applications
    'apple-finder'
    'apple-safari'
    'edge'
    'emby'
    'evernote'
    'google-chrome'
    'google-earth'
    'internet-explorer'
    'itunes'
    'jira'
    'opera'
    'plex'
    'quicktime'
    'skype'
    'slack'
    'steam'
    'teamviewer'
    'unity'
    'visualstudio'
    'whatsapp'

    # icons that are related to non-free games
    'black-mesa'
    'minecraft'

    # icons that are related to non-free network services
    'amazon'
    'appnet'
    'basecamp'
    'bing'
    'bitbucket'
    'blogger'
    'deviantart'
    'disqus'
    'dribbble'
    'dropbox'
    'ebay'
    'etsy'
    'facebook'
    'flattr'
    'foursquare'
    'github'
    'gmail'
    'google-drive'
    'google-maps'
    'google-photos'
    'google-play'
    'google-plus'
    'google-translate'
    'google-wallet'
    'instagram'
    'jsfiddle'
    'lastfm'
    'linkedin'
    'linode'
    'mixcloud'
    'onedrive'
    'pandora'
    'pinterest'
    'rdio'
    'reddit'
    'soundcloud'
    'spotify'
    'stackexchange'
    'stackoverflow'
    'telegram'
    'tumblr'
    'twitch'
    'twitter'
    'vimeo'
    'vine'
    'vk'
    'wechat'
    'xing'
    'yelp'
    'youtube'

    # icons that are related to non-FSDG operating systems
    'android'
    'ubuntu'

    # icons that are related to non-free operating systems
    'apple-ios'

    # icons that are trademarked brands and products
    'apple'
    'beats'
    'blackberry'
    'dolby'
    'google'
    'google-cardboard'
    'google-glass'
    'microsoft'
    'playstation'
    'wii'
    'wiiu'

    # icons that are trademarked characters
    'clippy'
  )

  for i in "${non_free[@]}";
  do
    rm -rf icon-theme/material-design-{dark,light}/applications/$i.svg
  done

  unset non_free
}

build() {
  cd $srcdir/$pkgbase-$pkgver
  qmake CONFIG+=nostrip L_ETCDIR=/etc PREFIX=/usr L_MANDIR=/usr/share/man DEFINES=QT_NO_DBUS
  make

  cd $srcdir/$pkgbase-calculator-$pkgver/src-qt5
  qmake L_ETCDIR=/etc PREFIX=/usr L_MANDIR=/usr/share/man
  make
}

package_lumina-core() {
  pkgdesc+=" - Core Components"
  depends=(fluxbox libxcursor libxdamage libxcb qt-base qt-multimedia qt-svg qt-x11extras xcb-util xcb-util-image xcb-util-wm xenocara-xinit)
  optdepends=('alsa-utils: audio mixer control using amixer'
              'numlockx: used to toggle the numlock key on session start'
              'xenocara-xbacklight: screen brightness control'
              'xterm: XDG standards require the availability of a graphical terminal to launch particular types of applications'
              'xenocara-xcompmgr: compositor to provide cross-application transparency effects and graphical smoothing'
              'picom: compositor to provide cross-application transparency effects and graphical smoothing')
  groups=(lumina)
  backup=(etc/luminaDesktop.conf)

  cd $pkgbase-$pkgver/src-qt5/core
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
  mv -v $pkgdir/etc/luminaDesktop.conf{.dist,}

  # Set distributor logo
  dirdark='usr/share/icons/material-design-dark/scalable/places'
  dirlight='usr/share/icons/material-design-light/scalable/places'

  install -Dm644 "$srcdir/distributor-logo-dark.svg" $pkgdir/$dirdark/distributor-logo.svg
  install -Dm644 "$srcdir/distributor-logo-light.svg" $pkgdir/$dirlight/distributor-logo.svg

  # symlinks logo
  for i in /usr/share/icons/material-design-{dark,light}/scalable/places
  do
    ln -sv "$i/distributor-logo.svg" "$pkgdir/${i/\/}/distributor-logo-hyperbola.svg"
  done

  declare -a _distributor=('hyperbola' 'lumina')
  for i in "${_distributor[@]}"
  do
    ln -sv "/$dirdark/distributor-logo.svg" "$pkgdir/$dirdark/start-here-$i.svg"
    ln -sv "/$dirlight/distributor-logo.svg" "$pkgdir/$dirlight/start-here-$i.svg"
  done
  unset _distributor
}

package_lumina-core-utils() {
  pkgdesc+=" - Core Utilities"
  depends=(qt-base qt-multimedia qt-svg qt-x11extras xenocara-xrandr)
  groups=(lumina)

  cd $pkgbase-$pkgver/src-qt5/core-utils
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-archiver() {
  pkgdesc+=" - Archive Manager"
  depends=(coreutils qt-base qt-multimedia qt-svg tar)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-fileinfo() {
  pkgdesc+=" - File Information Utility"
  depends=(qt-base qt-multimedia qt-svg)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-fm() {
  pkgdesc+=" - File Manager"
  depends=(lumina-archiver lumina-core lumina-fileinfo qt-base qt-multimedia qt-svg)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-photo() {
  pkgdesc+=" - Image Viewer"
  depends=(qt-base qt-multimedia qt-svg)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-screenshot() {
  pkgdesc+=" - Screenshot Utility"
  depends=(libxcb libxdamage qt-base qt-multimedia qt-svg qt-x11extras xcb-util xcb-util-image xcb-util-wm)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-textedit() {
  pkgdesc+=" - Plaintext Editor"
  depends=(qt-base qt-multimedia qt-svg)
  groups=(lumina-extra)

  cd $pkgbase-$pkgver/src-qt5/desktop-utils/$pkgname
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../../../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}

package_lumina-calculator() {
  pkgdesc+=" - Calculator"
  depends=(qt-base qt-multimedia qt-svg)
  groups=(lumina-extra)

  cd $pkgbase-calculator-$pkgver/src-qt5
  make INSTALL_ROOT=$pkgdir install
  install -Dm644 ../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}
