# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# Based on xorg-xrdb package

pkgname=xenocara-xrdb
_openbsdver=6.9
pkgver=1.2.0
pkgrel=2
pkgdesc="X server resource database utility, provided by Xenocara"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('Expat')
depends=('libx11' 'libxmu')
makedepends=('xenocara-util-macros')
optdepends=('gcc: for preprocessing'
            'mcpp: a lightweight alternative for preprocessing')
groups=('xenocara-apps' 'xenocara' 'xorg-apps' 'xorg')
provides=('xorg-xrdb')
conflicts=('xorg-xrdb')
replaces=('xorg-xrdb')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/app/xrdb-$pkgver.tar.lz{,.sig})
sha512sums=('7262f35c4c894172904774668fe1f16dbab38ef3915fdbfa37d736cf8e8ee9be5047b6a1f391bfecd3bf09e960156936da4bed90fa74274b96b5aaff3db16acc'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/app/xrdb"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/app/xrdb"
  ./configure --prefix=/usr --with-cpp=/usr/bin/cpp,/usr/bin/mcpp
  make
}

package() {
  cd "xenocara-$_openbsdver/app/xrdb"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
