# Contributor (Arch): Ivy Foster <code@escondida.tk>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libbulletml
pkgver=0.0.6
_debver=$pkgver
_debrel=7
pkgrel=1
pkgdesc="C++ library to handle the markup language BulletML easily"
arch=('i686' 'x86_64')
url='http://www.asahi-net.or.jp/~cs8k-cyu/bulletml/index_e.html'
license=('GPL-2' 'zlib' 'Modified-BSD' 'Simplified-BSD')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/b/bulletml/bulletml_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/b/bulletml/bulletml_${_debver}-${_debrel}.debian.tar.xz"
        "Makefile")
sha512sums=('916eff772459384bfbf9b646ebe267dc23f0d54dc2ac1d783fecefa6a212b09566bfc254609be3a0f0fe536fa8252b67ad4892f043d731a2a33b57b9544695e9'
            '843158d73989df2bd0d2e8c6921c0e53572aea9f66118e1e80e587587b035cf4a2d0efe4556117be9f7c654bef4721b9593f9871d6482a73b35b25634afdade9'
            'ae7136201f4fa5c8225df55dc8c5719bf65f459926b21e07f6a77e00b7b51273c3d8761bcae44c05be2276826e2eda664e65808e59103564c8cd65d91e2a9316')

prepare() {
  cd bulletml
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  cp "${srcdir}/Makefile" src
}

build() {
  make -C bulletml/src
}

package() {
  cd bulletml
  make -C src DESTDIR="$pkgdir" prefix=/usr install

  install -D -m 644 README "$pkgdir/usr/share/doc/libbulletml/README.jp"
  install -m 644 README.en "$pkgdir/usr/share/doc/libbulletml"
  install -m 644 README.bulletml "$pkgdir/usr/share/doc/libbulletml"

  install -D -m 644 debian/copyright -t "$pkgdir/usr/share/licenses/$pkgname"
}
