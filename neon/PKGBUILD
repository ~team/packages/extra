# Maintainer: Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=neon
pkgver=0.31.2
_debver=0.31.2
_debrel=1
pkgrel=1
pkgdesc="HTTP and WebDAV client library with a C interface"
arch=('i686' 'x86_64')
url="https://notroj.github.io/neon/"
license=('LGPL-2' 'GPL-2')
depends=('krb5' 'expat' 'ca-certificates' 'zlib')
makedepends=('quilt')
options=('libtool') # FS#16067
source=(https://notroj.github.io/neon/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/n/neon27/neon27_$_debver-$_debrel.debian.tar.xz)
sha512sums=('1e402b40a0445f68ed24d2697ee60d21636f61ebc98edcde37ff9e26c54430acabf3969ac22a942d1dd51bddee0f312c04073b423b0af3a3e7c9bf60cd53e48c'
            'ead1c73f7c765398caa2320fdae70e698a06a5d52233a108f285f039a1309cfdcb6413f61ee7edd9e3ef764f3c984284cfa10d75da663c60f0670a442a9e131a')
validpgpkeys=('190555472DCC589BEF01609C608A86DF9833CC49') # Joe Orton

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --with-expat \
    --enable-shared \
    --disable-static \
    --with-ssl=openssl \
    --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -d -m755 "${pkgdir}"/usr/share/licenses/${pkgname}
  install -Dm644 src/COPYING.LIB test/COPYING "${pkgdir}"/usr/share/licenses/${pkgname}
}
