# Maintainer (Arch): SanskritFritz (gmail)
# Maintainer (Arch): Laurent Carlier <lordheavym@gmail.com>
# Maintainer (Arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor (Arch): Arkham <arkham at archlinux dot us>
# Contributor (Arch): rabyte <rabyte*gmail>
# Contributor (Arch): Andres Blanc <andresblanc@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=egoboo
pkgver=2.8.1
pkgrel=3
arch=('i686' 'x86_64')
license=('GPL-2')
pkgdesc='An action RPG/dungeon crawling adventure with OpenGL 3D graphics.'
url='http://egoboo.sourceforge.net/'
depends=('sdl_image' 'sdl_mixer' 'sdl_ttf' 'physfs' 'enet' 'mesa' 'glu' 'glew')
groups=('games')
source=("http://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tar.gz"
        "keyboard_directions.patch"
        "$pkgname.png"
        "$pkgname.desktop")
sha512sums=('5793a63ba90f461ae99bcb81bf4ddf287b203bc2d401e1f697f1f4f93e1d681954e09b23938eb1a36e7dd535e2d12b8882efd77572cc63392cad5f9462ac3054'
            '75a6be15f2fad61f98ae420acfdc5d371282c2b37feadd686e18f2f31041754d1d985c4f95f33d194a83d7c564d6c13e1205bfed29d161a1c3ba4b7f75e37780'
            '5f89be790066c5e7047c87729cc67f49e33f8eedee39f705331d2a4a3b0857bb2bcaa0220dfd98fa4cdaadb10518212fb415b3e12fc7031679749080fcbdf9b5'
            '7bb3a33eecbe7bb955213c7f51a5aa6225cf8efe92dbe92ea22ba4a439c11c29f3a7c1de144f2a769529ecde37d5972cb43f361f1022db99a8b6baf364275f11')

build() {
  cd "$srcdir/egoboo-$pkgver"

  # Patch default depth to 24 and sound
  sed -i -e 's/\[COLOR_DEPTH\] : "32"/\[COLOR_DEPTH\] : "24"/g' \
         -e 's/\[Z_DEPTH\] : "32"/\[Z_DEPTH\] : "24"/g' \
         -e 's/\[OUTPUT_BUFFER_SIZE\] : "2548/\[OUTPUT_BUFFER_SIZE\] : "2048/g' \
         setup.txt

  cd "$srcdir/egoboo-$pkgver/src"

  # fix data paths
  sed -i -e 's#egoboo-2.x#egoboo#g' \
         -e 's#etc#share/games#g' \
         game/platform/file_linux.c

  # fix linking
  sed -i -e 's#-lenet#-lenet -lm#g' game/Makefile

  # fix keyboard bug
  patch -Np1 -i $srcdir/keyboard_directions.patch

  make all
}

package() {
  cd "$srcdir/egoboo-$pkgver"

  install -Dm755 src/game/egoboo-2.x $pkgdir/usr/games/egoboo
  install -d $pkgdir/usr/share/doc
  cp -rf doc/ $pkgdir/usr/share/doc/$pkgname/

  # Copy data and fix permissions
  install -d ${pkgdir}/usr/share/games/egoboo
  cp -rf controls.txt setup.txt basicdat/ modules/ ${pkgdir}/usr/share/games/egoboo/
  find ${pkgdir}/usr/share/games/egoboo -type f -exec chmod 644 {} +

  # Install icon and desktop files
  install -Dm 644 $srcdir/$pkgname.png $pkgdir/usr/share/pixmaps/$pkgname.png
  install -Dm 644 $srcdir/$pkgname.desktop $pkgdir/usr/share/applications/$pkgname.desktop

  # License
  install -Dm644 license.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
