# Contributor (Arch): Geoffroy Carrier <geoffroy@archlinux.org>
# Contributor (Arch): Thorsten Tasch <tht@thorstentasch.de>
# Contributor (Arch): JJDaNiMoTh <jjdanimoth@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=rsnapshot
pkgver=1.4.5
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="A remote filesystem snapshot utility"
arch=('any')
url='https://www.rsnapshot.org'
license=('GPL-2')
depends=('perl' 'rsync' 'openssh' 'perl-lchown')
makedepends=('quilt')
backup=('etc/rsnapshot.conf')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/r/rsnapshot/rsnapshot_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz"
        "rsnapshot"
        "AC_INIT-version.patch")
sha512sums=('a597fee40bc48d2f1fd2bd9db487be16a6410e55c3978e21a6e6a09aab43e7c022036d7842e833e0d9864570bf9302bc6538f1fafbb31fdafd9fc1e89e54b1c5'
            'a1608ca9a9cd9e9f5b03e58ddc39f26a9e3390855c9f66c21cd6bb4c754835a4a6b1cf110708ebe75cecbc6d061f82378659a8ff02a4bc1c52069881987dd13d'
            '29bb052de02c4079367b11c9c834a176fc0534c365c8eca5ff6c498c1d6d34b25cc6baad8717c5db9224daf0ea0154752fcf98f34689213f02b5d9a85882eb08'
            'de41863eac70da5c0d535ae4a0096029e890efb4dcabcb0d1116ae1aef0fbf10f10e396488d3ea460ca97cebc318043332576799e7be05f515ad102cb105a3d1')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/00_AC_INIT-verison.diff || true

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/AC_INIT-version.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./autogen.sh
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --mandir=/usr/share/man
  sed -i 's:/usr/bin/pod2man:/usr/bin/core_perl/pod2man:' Makefile
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR=${pkgdir} install
  install -Dm755 utils/rsnapreport.pl "$pkgdir/usr/bin/rsnapreport.pl"
  mv "${pkgdir}/etc/rsnapshot.conf.default" "${pkgdir}/etc/rsnapshot.conf"
  install -Dm644 "${srcdir}/rsnapshot" "${pkgdir}/etc/logrotate.d/rsnapshot"
  install -Dm644 COPYING -t $pkgdir/usr/share/licenses/$pkgname
}
