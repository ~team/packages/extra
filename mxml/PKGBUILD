# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Roman Kyrylych <roman@archlinux.org>
# Contributor (Arch): Johannes Sjolund <j.sjolund@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mxml
pkgver=3.2
_debver=3.2
_debrel=1
pkgrel=1
pkgdesc='A small XML parsing library'
arch=('i686' 'x86_64')
url='https://www.msweet.org/mxml'
license=('Apache-2.0')
depends=('glibc')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/michaelrsweet/${pkgname}/archive/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/mxml/mxml_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ed3f2b361394ef225a708b0032976a3d191d5eaea3022e190b29e0ee140e8f998f1d65c64e4898872b169516cee44f4f5f18401c6410c3b5aa1e9680b23e8675'
            '47005138b24060bc914b1f28e6ce88962e8842c6aebd99b40dde06e030dcd33cc2a309af948c6cfe3ebb0c0d4c7520c288f9d03337d4bfe5a49334a58593c2fa')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --enable-shared
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DSTROOT="${pkgdir}" install
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
