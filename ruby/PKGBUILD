# Contributor (Arch): Thomas Dziedzic <gostrc@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): John Proctor <jproctor@prium.net>
# Contributor (Arch): Jeramy Rutley <jrutley@gmail.com>
# Maintainer (Parabola): Daniel Milewski <niitotantei@riseup.net>
# Contributor (Parabola): fauno <fauno@parabola.nu>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=ruby
pkgver=2.7.2
_jsonver=2.3.0
pkgrel=5
pkgdesc="An object-oriented language for quick and easy programming"
arch=(i686 x86_64)
url='https://www.ruby-lang.org/en/'
license=('Simplified-BSD')
depends=('libxcrypt' 'gdbm' 'libressl' 'libffi' 'libyaml' 'gmp' 'zlib')
makedepends=('doxygen' 'graphviz' 'ttf-dejavu' 'tk')
optdepends=('tk: for Ruby/TK')
options=(!emptydirs)
provides=(rubygems rake)
conflicts=(rake)
backup=(etc/gemrc)
install=$pkgname.install
mksource=(https://cache.ruby-lang.org/pub/ruby/${pkgver:0:3}/ruby-${pkgver}.tar.xz
          https://repo.hyperbola.info:50000/sources/ruby-libre/json_pure-${_jsonver}-gem.tar.gz)
source=(https://repo.hyperbola.info:50000/sources/ruby-libre/ruby-libre-${pkgver}.tar.xz{,.sig}
        patch-ext_openssl_extconf_rb.patch
        patch-ext_openssl_openssl_missing_h.patch
        remove-nonfree-ruby-references.patch
        CVE-2021-41816.patch
        CVE-2021-41817-1.patch
        CVE-2021-41817-2.patch
        CVE-2021-41817-3.patch
        gemrc)
mksha512sums=('7972278b096aa768c7adf2befd26003e18781a29ca317640317d30d93d6e963ded197724c8e2f1dfe1e838c5647176d414a74732a62e931fb50d6f2e0f777349'
              'eeb6111f18b5cf1acafa6c532be0f58750a2976462e05cc6262baf2f3615259e4ce2ccab115b8dd2c6d37d4ecc9b23bc911efd8823c0d608cb90297a4e4a1ace')
sha512sums=('4775ccc2c1bc74ba5fee4e5a904ee2754809b14e36673389f1c6eea7b2955ef8c3ccd374681e845e2e99457a13c742e3b48142a28a1b8d25cd0f571d55be7b45'
            'SKIP'
            'ec14f367d8357409e1c5660c76758c596dac6499aa0cc79b13d36cb525d69bd708f498eb5caba098e24e4913f36d3dc2f7b36ba6410418feab7a71f920ab174a'
            'd4cf308971976c846391f84f822c12e1e005f00c6868e0a1901e82732ff85219e8c1251b56f0a8007700e97a6cd29f372ca444fd6a7aac9dafc909309e0f70b8'
            '0fad188d5aecbc633418d6c563da1a208dbeaf8f9398014b42dce23be697ddfa6a9b1214bcb7701118b4878a2a0c234b722d4175efe56bda74da0685d1d89794'
            '5bb4421e36ba0888076cd2b7ff577a417e5bf08de8ca13b1030befce8435057ec330f22e62d78ed13445378db75d19bd2356db4ff061c8f2f65c1a2cb07fe23a'
            'd44cd0d66e09c058256e76f5f440e73de81098f250d566cf69ed7a50a3ccea79c837f1fddad25df465918a03d2f6d3b722676ffe1efa1bd77afc8b1234438fb7'
            'b3b6e81e21b37689f23c048569ffb70c524a206f9e6c28f0bf815d2f02abb97200e37fd6a8b8d91ac9fb2a87aae158d8aca461a1c2c87ea4e90ff97b9af6eb21'
            '158e0fdd44e53b5416dc70775c6ae4f6b5769459e349393e64f3b3e6540e6737a176ee8d58d24de01c0a72d8e30b89476db5798582a00717af8e8e3990e133dd'
            '8cafd14d414ee3c16aa94f79072bc6c100262f925dc1300e785846c3fabbbbffc1356b8e2223af5684e3340c55032d41231179ffa948bb12e01dbae0f4131911')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

mksource() {
  rm -rv ruby-${pkgver}/ext/json
  rm -rv ruby-${pkgver}/test/json
  cp json_pure-2.3.0.gem ruby-${pkgver}/
}

prepare() {
  cd ${srcdir}/ruby-${pkgver}

  install -D -m644 json_pure-${_jsonver}.gem gems

  patch -Np1 -i ${srcdir}/remove-nonfree-ruby-references.patch

  # add LibreSSL 2.7 compatibility patch
  patch -Np0 -i ${srcdir}/patch-ext_openssl_extconf_rb.patch
  patch -Np0 -i ${srcdir}/patch-ext_openssl_openssl_missing_h.patch

  # CVE-2021-41816
  patch -Np1 -i ${srcdir}/CVE-2021-41816.patch

  # CVE-2021-41817
  patch -Np1 -i ${srcdir}/CVE-2021-41817-1.patch
  patch -Np1 -i ${srcdir}/CVE-2021-41817-2.patch
  patch -Np1 -i ${srcdir}/CVE-2021-41817-3.patch

  # extract the gem. If it isn't extract like the other bundled gems,
  # the gem dir will be created with u-w permissions for some reason.
  install -d -m 755 gems/json_pure-${_jsonver}
  cd gems/json_pure-${_jsonver}
  bsdtar xqOf ../json_pure-${_jsonver}.gem data.tar.gz | bsdtar xvzf -
}

build() {
  cd ${srcdir}/ruby-${pkgver}

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --sharedstatedir=/var/lib \
    --enable-shared \
    --disable-rpath \
    --with-dbm-type=gdbm_compat

  make
}

package() {
  cd ${srcdir}/ruby-${pkgver}

  make DESTDIR="${pkgdir}" install-nodoc
  make DESTDIR="${pkgdir}" install-doc install-capi

  install -D -m644 ${srcdir}/gemrc "${pkgdir}/etc/gemrc"

  install -D -m644 BSDL "${pkgdir}/usr/share/licenses/ruby/BSDL"
}
