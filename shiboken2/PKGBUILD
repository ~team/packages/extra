# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=shiboken2
pkgname=('shiboken2' 'python-shiboken2')
_qtver=5.15.2
_clangver=11.0.0
pkgver=${_qtver/-/}
_debver=5.15.2
_debrel=1
pkgrel=1
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Generates bindings for C++ libraries using CPython source code'
makedepends=('clang' 'llvm' 'cmake' 'libxslt' 'qt-xmlpatterns' 'python-sphinx' 'quilt')
_pkgfqn=pyside-setup-opensource-src-$_qtver
source=("https://download.qt.io/official_releases/QtForPython/pyside2/PySide2-$pkgver-src/${_pkgfqn}.tar.xz"
        "https://deb.debian.org/debian/pool/main/p/pyside2/pyside2_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('5043c1c9db4ba0133baaabb4d4943182e9741b1007bedd992394758707a37bba0688dc6413ea46620e7c2fb09daa3c74e6a7bb2c302b9d08e5f3bb7575d02b62'
            'e83a01e3dbcbcf260f63af2854b6b69b527f286b63997191ea7325a7106c4c14ba386d4a0b9502281547e18eaa4934970c4457af5250984816a8212d4d38a7a0')

prepare() {
  cd ${_pkgfqn}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake -B build -S ${_pkgfqn}/sources/shiboken2 \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_TESTS=OFF \
    -DUSE_PYTHON_VERSION=3
  cmake --build build
}

package_shiboken2() {
  depends=("clang=${_clangver}" 'llvm' 'libxslt' 'qt-xmlpatterns')

  DESTDIR="$pkgdir" cmake --install build
  # Provided in python-shiboken2
  rm -r "$pkgdir"/usr/lib/{python*,libshiboken*}
  # Conflicts with shiboken6 and doesn't work anyway
  rm "$pkgdir"/usr/bin/shiboken_tool.py
  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPLv3 -t "${pkgdir}"/usr/share/licenses/${pkgname}
}

package_python-shiboken2() {
  depends=('python')

  DESTDIR="$pkgdir" cmake --install build
  # Provided in shiboken2
  rm -r "$pkgdir"/usr/{bin,include,lib/{cmake,pkgconfig}}

  # Install egg-info
  cd $_pkgfqn
  python setup.py egg_info --build-type=shiboken2
  _pythonpath=`python -c "from sysconfig import get_path; print(get_path('platlib'))"`
  cp -r shiboken2.egg-info "$pkgdir"/$_pythonpath
  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPLv3 -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
