# Maintainer (Arch): jianingy <jianingy.yang AT gmail.com>
# Contributor (Arch): cmb <chris AT the-brannons.com>
# Contributor (Arch): hatred <hatred AT inbox.ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=daemontools
pkgver=0.76
pkgrel=2
pkgdesc="A collection of tools for managing services"
arch=('i686' 'x86_64')
url='http://cr.yp.to/daemontools.html'
license=('Public-Domain')
source=("https://cr.yp.to/daemontools/${pkgname}-${pkgver}.tar.gz"
        "http://smarden.org/pape/djb/manpages/$pkgname-$pkgver-man.tar.gz"
        "daemontools-0.76.errno.patch"
        "daemontools-0.76.service-path-fix.patch"
        "svscan.initd"
        "svscan.confd"
        "svscan.run"
        "LICENSE")
sha512sums=('e4a7938352b745a03ccc41acdddba1e6782f0ca245e5cae060de62ab6c5a23c841a994c30140298d274405a7f26d53ba7e84e5810a3d185b2c01e4c0feffe6c7'
            '1767fafb2d92902f903637f08363031971bda007d1b7b53059551c740976d1c643229ccc44171796c1dcb43cbd875a17178edc634e140671817e0ef0ba3ad80b'
            '02a2d45f221b70e6d0d13127e8bacae0f37e96876d551d445d582c664f604555765e62c9906d0f1faa1e4eb7f5c2ed61ebeeda75c216de99bab7ee14932068eb'
            'd3af2a13316be8740cd3593f4980fa1b7de345357d0b87dd963bed919e59c0f4f68732632f4b266877646b5cd14f023ea9968eecbdd0b578ef9bb9c417204e76'
            '0a43467f5354f2f651af4a196d8553eedf53472341e57579551cac12ecf2552074078d508843baa1a8ce90ff5b61fceb06f62fafee4eb11c64e4d01167826f60'
            '5a5d363744d56da338605037dd610b25543e906cbac26486ff99ecea712cdabd15a8a8f476ccdebb14051672c3a15e43e30fc7a69182841511a5662df56a9dc8'
            '363c628677afc3a70d6ab20b379626744bd8f440bc0d064909b54e74aa93083fe2a5f851585468e42d9efbc5a1f78efb82e9dfd1685c63ec64db4a97701f3c72'
            '0f2cd6120ff16af8266f99db48264a2646d9c95727fbf0c7401fc45b39467944ce0ed91106edb9443389b67d5cb9b659b86948a7a59b7e740031098c25d864d5')

prepare() {
  cd admin/$pkgname-$pkgver
  patch -p1 < $srcdir/daemontools-0.76.errno.patch
  patch -p1 < $srcdir/daemontools-0.76.service-path-fix.patch
}

build() {
  cd admin/$pkgname-$pkgver
  package/compile
}

package() {
  cd admin/$pkgname-$pkgver
  install -d $pkgdir/usr/sbin
  cp -a command/* $pkgdir/usr/sbin
  install -d $pkgdir/usr/share/man/man8
  cp -a $srcdir/daemontools-man/*.8 $pkgdir/usr/share/man/man8

  # install services
  install -Dm755 "${srcdir}/svscan.initd" "${pkgdir}/etc/init.d/svscan"
  install -Dm644 "${srcdir}/svscan.confd" "${pkgdir}/etc/conf.d/svscan"
  install -Dm755 "${srcdir}/svscan.run" "${pkgdir}/etc/sv/svscan/run"

  # install license
  install -Dm644 "${srcdir}/LICENSE" -t "$pkgdir"/usr/share/licenses/$pkgname
}
