# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): André Klitzing <aklitzing () gmail () com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=clazy
pkgver=1.9
pkgrel=2
pkgdesc="Qt oriented code checker based on clang framework"
url="https://www.kdab.com/"
license=('LGPL-2')
depends=('clang')
makedepends=('cmake' 'llvm')
arch=('i686' 'x86_64')
source=(https://download.kde.org/stable/$pkgname/$pkgver/src/$pkgname-$pkgver.tar.xz
        fix-build.patch)
sha512sums=('2e8bec44a027366263de23c50d14192e310fd38fa2b369afb21413da9cb78da9882b2153daf1784c4c9076cc62e2867b5211c75ff9a1eabcb583e405f20f5912'
            '319b1ca829b81aa0e3a98ce24bcf8bd442d341509fb5b2b8bd5a2135da3387fa61aa96d2e243dfeef91518446a4325c87f3dec9134311ef7c29d30b4d88e9ef8')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i ${srcdir}/fix-build.patch
}

build() {
  cmake -B build $pkgname-$pkgver \
        -DCMAKE_BUILD_TYPE=None \
        -DCMAKE_C_COMPILER=gcc \
        -DCMAKE_CXX_COMPILER=g++ \
        -DCMAKE_CXX_STANDARD_REQUIRED=ON \
        -DCMAKE_INSTALL_LIBDIR=/usr/lib \
        -DCMAKE_INSTALL_PREFIX=/usr
  make --trace -C build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING-LGPL2.txt" -t "$pkgdir/usr/share/licenses/$pkgname"
}