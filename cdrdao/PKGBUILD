# Maintainer (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=cdrdao
pkgver=1.2.4
_debver=1.2.4
_debrel=2
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL-2')
url='http://cdrdao.sourceforge.net/'
pkgdesc='Records audio/data CD-Rs in disk-at-once (DAO) mode'
depends=('gcc-libs' 'lame' 'libmad' 'libvorbis' 'libao')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/c/cdrdao/cdrdao_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('41f20275ca44ec9003d0e3ed280cc98012353005cda6f544ebfa44f3f79f991845c0ef17af74db9456f1bacc342a7fd48c9e942d757927a4a9ff91808f7bbb09'
            '7e56ac10f8c2a715abc5832ce4a1de61d80d66a118d4b6378e1cb923cf30b1730ed9421b5f3c4cb57b12e1c98df1e15009bdf49e3cc6af9e0371b8c7ea603667')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
	--mandir=/usr/share/man \
	--sysconfdir=/etc \
	--without-xdao --with-lame \
	--with-ogg-support --with-mp3-support
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
