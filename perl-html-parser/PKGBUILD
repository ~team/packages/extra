# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): kevin <kevin@archlinux.org>
# Contributor (Arch): Manolis Tzanidakis
# Contributor (Arch): Firmicus <francois.archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=perl-html-parser
pkgver=3.72
_debver=3.72
_debrel=5
pkgrel=3
pkgdesc="Perl HTML parser class"
arch=('i686' 'x86_64')
license=('GPL-1')
url="https://search.cpan.org/dist/HTML-Parser/"
depends=('perl-html-tagset')
makedepends=('quilt')
checkdepends=('perl-test-pod')
options=('!emptydirs')
source=(https://www.cpan.org/authors/id/G/GA/GAAS/HTML-Parser-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/libh/libhtml-parser-perl/libhtml-parser-perl_$_debver-$_debrel.debian.tar.xz
        COPYING)
sha512sums=('9fded69219791af085018ffbe6737e74e370c543af52aa221aec174daf52d1b8d3cdd07f025176c0212ae572a924555262f2eefc73781f82cdb7d9cff58ae716'
            'f29914c9e20b3844af4a5e46e9a4a8788bc38bab86368697207dc6f94ec647e04527c206233d0d6bf62403859e25a20f048105fbb6cdf13d980c7cd41855be42'
            'e239b539f2c46c1ae3da87c4983a0fda67dc8ae622d607b507b5c523af3bdf99e7bea8931e3a7c6007910bfe9e21a761e03e8203de95b5aceea425a97d0a84c9')

prepare() {
  cd HTML-Parser-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian_examples_location.patch || true

    quilt push -av
  fi
}

build() {
  cd HTML-Parser-${pkgver}
  perl Makefile.PL INSTALLDIRS=vendor
  make
}

check() {
  cd HTML-Parser-${pkgver}
  make test
}

package() {
  cd HTML-Parser-${pkgver}
  make install DESTDIR="${pkgdir}"
  install -Dm644 ${srcdir}/COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}
