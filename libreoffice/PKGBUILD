# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=libreoffice
pkgname=('libreoffice-sdk' 'libreoffice')
_pkgname=${pkgbase}
pkgver=7.0.4
_pkgver=${pkgver}.2
_debver=$pkgver
_debrel=4
pkgrel=5
arch=('i686' 'x86_64')
license=('LGPL-3' 'GPL-3' 'MPL-2.0')
url="https://www.libreoffice.org/"
makedepends=('boost' 'cppunit' 'curl' 'desktop-file-utils' 'doxygen'
             'gcc-libs' 'gdb' 'glm' 'gperf' 'gpgme' 'graphite' 'gst-plugins-base-libs' 'harfbuzz-icu'
             'hicolor-icon-theme' 'hunspell' 'hyphen' 'icu' 'lcms2' 'lpsolve' 'mdds'
             'mesa-libgl' 'neon' 'nspr' 'nss' 'pango' 'perl-archive-zip' 'poppler' 'postgresql-libs'
             'python' 'qt-base' 'qt-x11extras' 'quilt' 'redland' 'sane' 'shared-mime-info'
             'ttf-dejavu' 'ttf-liberation' 'unixodbc' 'unzip' 'xmlsec' 'zip' 'libcdr' 'libmythes'
             'libe-book' 'libetonyek' 'libffi' 'libfreehand' 'libjpeg-turbo' 'liblangtag' 'libmspub'
             'libodfgen' 'liborcus' 'libpagemaker' 'libtommath' 'libvisio' 'libwpd' 'libwpg' 'libwps'
             'libepubgen' 'libexttextcat' 'libstaroffice' 'libmwaw' 'libnumbertext' 'libqxp' 
             'libzmf' 'libxinerama' 'libxrandr' 'libxslt' 'libepoxy' 'clang')
_source_url="https://dev-www.libreoffice.org"
source=("https://download.documentfoundation.org/${_pkgname}/src/${pkgver}/${_pkgname}"{,-help,-translations}"-${_pkgver}.tar.xz"{,.asc}
	"https://deb.debian.org/debian/pool/main/libr/libreoffice/libreoffice_${_debver}-${_debrel}+deb11u10.debian.tar.xz"
	"${_pkgname}.sh"
	"${_pkgname}.csh"
	"make-pyuno-work-with-system-wide-module-install.diff"
	"help-${_pkgname}-languages.js"
	"template-soffice.desktop.in"
	"${_source_url}/src/CoinMP-1.7.6.tgz"
	"${_source_url}/src/Firebird-3.0.0.32483-0.tar.bz2"
	"${_source_url}/src/QR-Code-generator-1.4.0.tar.gz"
	"${_source_url}/src/dtoa-20180411.tgz"
	"${_source_url}/src/pdfium-4306.tar.bz2"
        "${_source_url}/src/serf-1.3.9.tar.bz2"
        "${_source_url}/src/libatomic_ops-7.6.8.tar.gz"
        "${_source_url}/src/libabw/libabw-0.1.3.tar.xz"
        "${_source_url}/src/libcmis-0.5.2.tar.xz"
	"${_source_url}/src/skia-m85-e684c6daef6bfb774a325a069eda1f76ca6ac26c.tar.xz"
	"${_source_url}/src/0168229624cfac409e766913506961a8-ucpp-1.3.2.tar.gz"
	"${_source_url}/src/a7983f859eafb2677d7ff386a023bc40-xsltml_2.1.2.zip"
        "${_source_url}/src/48d647fbd8ef8889e5a7f422c1bfda94-clucene-core-2.3.3.4.tar.gz"
        "${_source_url}/src/mariadb-connector-c-3.1.8-src.tar.gz"
	"${_source_url}/src/884ed41809687c3e168fc7c19b16585149ff058eca79acbf3ee784f6630704cc-opens___.ttf")
sha512sums=('cc65497b8d27f0e48ebfc5fa66b09d45948febe9454149300b447d9a6aafecd4be728403c3095c058852b9dc53570841d612bc196979f25595f6b8981f706d4b'
	    'SKIP'
	    'ba346fa8ee015479d799d9d0b2a315cb901071b5fba2c4d7eba2eeba41dd4cac72210d12542704220c8cbb82d6a7b37a9285c1933fe2407e8e149510df4d58fe'
	    'SKIP'
	    'a1da20b4e4ae4952ca619871fc74c5693202e1f909f16ca1321a46dbc9abf979239c989bc56799c3e0c1f0930e8a41bd9aca2212bf5435e733951bdef8a5315c'
	    'SKIP'
	    '62214e1a22337fe1a2f65c19a369f6a642a523f4e4a31d04ea4fbd8b69a7ea416816c56f568ab88fce3d64892d47a5da3821fb7382816dfddb8d30db6c06d392'
	    'b4d9c0f0088ed1b8046e441c2db16406ee502373eb897a8241eb132424c9db78ab26dbb589b492ceb02d158b925b069a0beb356c8e9dd0092c8998e41ef0492b'
	    'd17f85dcc464685ae1955c5e2fdac9cd1ca02d55c2d76c3616bd1b5fcecb8eaec5fb639b30d1bdece1251bdddcde5a1d5d26d20a0a76bbfa17f7e2eefb7868ad'
	    'be4d752ee1e26de5dd0f83635a54c3e07f6e88eb98bed044e544a0ec4238df227a5b3e9b59cf58fce46c80f5d3e9056e6504b795167282daea80ff7dde053e4c'
	    'a99788691719b74bee93dd938b34df73d0aa4d4366ad249e5030f6a97bfeaf8cae34771d51294d8edd5054ccb87e411b054d398893e9582e9856be38b9b61da8'
	    'c759a5650bce25ec5ed4bea5ea71b77b1a6045bf552bac786492ef828749a04d40617ada6966714b7bb8394011d2b2ecbab8f569611703aa6364009428763f72'
	    '1612d43d52d0ee29b4e6ca328e1535c59722dfd5c7a7e0811a4180ad6033ef27111ced6497f9d5cea816c047a4e11c2aed6d1936feac8ded797eeec55d9539ce'
	    '17d170ce3ced3830a10fc7e517d5ef53d151c5affe9676485b7a860e94f68bbb5a003123fbcaa5fffc04b2d91aec161a8f3ef1ee2c0b197a8546904a4c6e6dee'
	    '314a757bfdf9a38bcf07fd7a0103d28a2d1dd7311b8234761304efeef7bfdb740db78ab01e6b67e99a28d523cc2be9c1073b2de9d65e853d191c3dad19af56d0'
	    '722aa814c33a34bfffe6c0201b0035cc3b65854a0ba9ae2f51620a89d68019353e2c306651b35bca337186b22b2e9865ef3c5e3df8e9328006f882e4577f8c85'
	    '3f0adc03d1a105bba18e8d2384f698214c98c6b5c2602eb0cbc7d74fd3f7afe6c51fd8ff751fc5054d5671ce8b5b8205c4d2a1f044d5ebadac4d4a3b92701e10'
            '9f5418d991840a08d293d1ecba70cd9534a207696d002f22dbe62354e7b005955112a0d144a76c89c7f7ad3b4c882e54974441fafa0c09c4aa25c49c021ca75d'
            'bc448fadcf8e2936fa933a5872d5550ecdff04b0df27120d3182dcbb2147a6594ec6bfc5b214e21b37ffa1b5100c1c56d301ba9cae7df26cee5e6b999dcda14c'
            '0d2646e1bad1e11b3da43714ac5931fc67ffdbc4e7a25a44ef5b6e6a41de1e0ae14596b4a87cceb07bf56dbbe9344622b3d60afcb054ee0ab8577ca8e9b5c289'
            '295ab15115e75b1f6074f17d3538afe0de9b2b77ab454f5c63cb05e8df11886d82942fbf21ba01486052e3f6c75b0636b99d8e660cd3472dc4b87c31d3cd557b'
	    '4aeba56400d20e5fe234c7927b3d196233e19513a3202e074a88fad0d14d168221574a5a72b8dd9398cf3a9453c46eba4e4ed23d8d2031522b6cb11bc8bb43c1'
	    'b9c02d63e9b47a838dbe67c05b9e9e4983d13b9d74794e1c30c73d341c3bc905c9edec3a72fa339ae8c0e06d97e69ac2ea23bf51336b77af14cab7ae67721a46'
	    '2d3835f7ac356805025cafedcad97faa48d0f5da386e6ac7b7451030059df8e2fdb0861ade07a576ebf9fb5b88a973585ab0437944b06aac9289d6898ba8586a'
            '1c9da9077edcebd46563bd9e47d330518e0b30061016650a759cfe051e9748fdad8932a472b1cca53a6adafed5f41656527271fc5f55ddfcefb558f0d83286b4'
            '005fdacd7e7b353b97b5c1305f973a90744e495d7bf5d407fc6c75d49323e13a743e6c3877c00ada2b3fc49a0f33e5c2dcd70b1f8fe8eb8f579af1bce3b3e6b9'
	    'ce7e23e750f2c6f7ff2e590cc8941caa18eaae2727c9ca31313ab72ab19278055bd9393d38b0b5b685594e2f04ee15cb83b3bbb25d09665fe7383d7f26bf2ae8')
noextract=("CoinMP-1.7.6.tgz"
	   "Firebird-3.0.0.32483-0.tar.bz2"
	   "QR-Code-generator-1.4.0.tar.gz"
	   "dtoa-20180411.tgz"
	   "pdfium-4306.tar.bz2"
           "serf-1.3.9.tar.bz2"
           "libatomic_ops-7.6.8.tar.gz"
           "libabw-0.1.3.tar.xz"
           "libcmis-0.5.2.tar.xz"
	   "skia-m85-e684c6daef6bfb774a325a069eda1f76ca6ac26c.tar.xz"
	   "0168229624cfac409e766913506961a8-ucpp-1.3.2.tar.gz"
	   "a7983f859eafb2677d7ff386a023bc40-xsltml_2.1.2.zip"
           "48d647fbd8ef8889e5a7f422c1bfda94-clucene-core-2.3.3.4.tar.gz"
           "mariadb-connector-c-3.1.8-src.tar.gz"
	   "884ed41809687c3e168fc7c19b16585149ff058eca79acbf3ee784f6630704cc-opens___.ttf")
validpgpkeys=('C2839ECAD9408FBE9531C3E9F434A1EFAFEEAEA3'  # LibreOffice Build Team <build@documentfoundation.org>
              'C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd ${_pkgname}-${_pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS="-p ab --no-timestamps --no-index"
    export QUILT_DIFF_ARGS="--no-timestamps"

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/apparmor-*.diff || true
    rm -v debian/patches/build-against-shared-lpsolve.diff || true
    rm -v debian/patches/debian-*.diff || true
    rm -v debian/patches/disableClassPathURLCheck.diff || true
    rm -v debian/patches/fix-bluez-external.diff || true
    rm -v debian/patches/fix-internal-hsqldb-build.diff || true
    rm -v debian/patches/fix-system-lpsolve-build.diff || true
    rm -v debian/patches/hide-math-desktop-file.diff || true
    rm -v debian/patches/hppa-is-32bit.diff || true
    rm -v debian/patches/install-fixes.diff || true
    rm -v debian/patches/javadoc-optional.diff || true
    rm -v debian/patches/jdbc-driver-classpaths.diff || true
    rm -v debian/patches/jurt-soffice-location.diff || true
    rm -v debian/patches/mention-java-common-package.diff || true
    rm -v debian/patches/no-openssl.diff || true
    rm -v debian/patches/pdfium-m68k.diff || true
    rm -v debian/patches/sensible-lomua.diff || true
    rm -v debian/patches/split-evoab.diff || true
    rm -v debian/patches/system-officeotron-and-odfvalidator || true
    rm -v debian/patches/unowinreg-static-libgcc.diff || true
    rm -v debian/patches/use-mariadb-java-instead-of-mysql-java.diff || true

    quilt push -av
  fi

  # link all non-extracted files to the external source directory
  install -dv ../ext_sources
  pushd ../ext_sources
  for source in ${noextract[@]}; do
    ln -sv ../${source} .
  done
  popd

  # apply patch fixing not upstreamable pyuno paths
  patch --verbose -p1 -i ../make-pyuno-work-with-system-wide-module-install.diff

  # use $CFLAGS variable but remove overridden ones from LibreOffice
  for i in ${CFLAGS}; do
    case "${i}" in
      -O?|-pipe|-Wall|-g|-fexceptions) continue;;
    esac
    ARCH_FLAGS="${ARCH_FLAGS} ${i}"
  done
}

build() {
  cd ${_pkgname}-${_pkgver}

  # strip -s from $MAKEFLAGS in case you use it to shorten build logs.
  _MAKEFLAGS=${MAKEFLAGS/-s/}

  # see https://icu.unicode.org/download/61#TOC-Migration-Issues
  CPPFLAGS+=' -DU_USING_ICU_NAMESPACE=1'
  ./autogen.sh \
    --with-vendor="Hyperbola" \
    --with-extra-buildid=${pkgver}-${pkgrel} \
    --prefix=/usr \
    --exec-prefix=/usr \
    --sysconfdir=/etc \
    --libdir=/usr/lib \
    --mandir=/usr/share/man \
    --enable-release-build \
    --enable-split-app-modules \
    --disable-fetch-external \
    --disable-dependency-tracking \
    --disable-report-builder \
    --with-external-tar=${srcdir}/ext_sources \
    --with-external-dict-dir=/usr/share/hunspell \
    --with-external-hyph-dir=/usr/share/hyphen \
    --with-external-thes-dir=/usr/share/mythes \
    --with-parallelism=${_MAKEFLAGS/-j/} \
    --enable-odk \
    --enable-qt5 \
    --enable-python=system \
    --enable-gio \
    --enable-openssl \
    --enable-scripting-javascript \
    --enable-ext-nlpsolver \
    --enable-ext-wiki-publisher \
    --disable-avahi \
    --disable-dbus \
    --disable-dconf \
    --disable-gtk3 \
    --with-system-boost \
    --with-system-cairo \
    --with-system-dicts \
    --with-system-gpgmepp \
    --with-system-icu \
    --with-system-headers \
    --with-system-libs \
    --with-system-mdds \
    --with-system-libcdr \
    --with-system-libtommath \
    --with-system-libvisio \
    --with-system-libwpg \
    --with-system-libwps \
    --with-system-libexttextcat \
    --with-system-libmwaw \
    --with-system-libnumbertext \
    --with-system-libqxp \
    --with-system-libstaroffice \
    --with-system-libzmf \
    --with-system-mythes \
    --with-system-redland \
    --with-help=html \
    --with-lang="" \
    --without-system-coinmp \
    --without-system-firebird \
    --without-system-qrcodegen \
    --without-system-clucene \
    --without-system-serf \
    --without-system-libatomic-ops \
    --without-system-libabw \
    --without-system-libcmis \
    --without-system-mariadb \
    --without-fonts \
    --without-java \
    --without-myspell-dicts

  touch src.downloaded
  make VERBOSE=1 build-nocheck
  install -dv ../fakeinstall
  make DESTDIR=${srcdir}/fakeinstall VERBOSE=1 distro-pack-install
}

package_libreoffice-sdk() {
  pkgdesc="Software Development Kit for LibreOffice"
  depends=('libreoffice' 'gcc-libs' 'zip')

  for dir in $(grep -h ^%dir ${_pkgname}-${_pkgver}/file-lists/sdk{,_doc}_list.txt); do
    install -dm755 ${pkgdir}/${dir/\%dir/}
  done
  for file in $(grep -h -v ^%dir ${_pkgname}-${_pkgver}/file-lists/sdk{,_doc}_list.txt); do
    dirname=$(dirname $file)
    [[ -d ${pkgdir}/${dirname} ]] || install -dm755 ${pkgdir}/${dirname}
    mv -v fakeinstall/${file} ${pkgdir}/${file}
  done

  # fix file permissions
  find ${pkgdir}/usr/lib/${_pkgname}/sdk/examples -type f | xargs chmod -v -x

  # install LibreOfficeKit headers
  install -Dm644 ${_pkgname}-${_pkgver}/include/LibreOfficeKit/* -t ${pkgdir}/usr/include/LibreOfficeKit

  # remove all source files related to Java due to freedom issues
  find ${pkgdir}/usr/lib/${_pkgname} -name java | xargs rm -frv

  # licenses
  install -Dm644 ${_pkgname}-${_pkgver}/COPYING{,.LGPL,.MPL} -t ${pkgdir}/usr/share/licenses/${pkgname}
}

package_libreoffice() {
  pkgdesc="A complete free and libre office productivity app suite"
  depends=('curl' 'desktop-file-utils' 'gcc-libs' 'gpgme' 'graphite' 'harfbuzz-icu'
           'hicolor-icon-theme' 'hunspell' 'hyphen' 'icu' 'lcms2' 'mesa-libgl' 'lpsolve' 'neon'
           'nspr' 'nss' 'pango' 'poppler' 'python' 'redland' 'shared-mime-info' 'xdg-utils'
           'xmlsec' 'libcdr' 'libcups' 'libe-book' 'libepoxy' 'libetonyek'
           'libfreehand' 'libjpeg-turbo' 'liblangtag' 'libmspub' 'libodfgen' 'liborcus'
           'libpagemaker' 'libtommath' 'libvisio' 'libwpd' 'libwps' 'libepubgen'
           'libexttextcat' 'libstaroffice' 'libmwaw' 'libnumbertext' 'libqxp'
           'libzmf' 'libxinerama' 'libxrandr' 'libxslt' 'qt-base' 'qt-x11extras')
  optdepends=('libmythes: for use in thesaurus'
              'libwpg: library for importing and converting graphics format'
              'sane: for scanner access'
              'unixodbc: adds ODBC database support'
              'gst-plugins-base-libs: for multimedia content, e.g. in Impress'
              'libpaper: takes care of paper size'
              'postgresql-libs: for postgresql-connector')
  backup=('etc/libreoffice/sofficerc'
          'etc/libreoffice/bootstraprc'
          'etc/libreoffice/psprint.conf'
          'etc/profile.d/libreoffice.sh'
          'etc/profile.d/libreoffice.csh')

  mv -v fakeinstall/* ${pkgdir}

  # remove the LibreOffice SDK directory
  rm -frv ${pkgdir}/usr/share/libreoffice/sdk

  # install config files to /etc/libreoffice directory and link them found by LibreOffice
  install -Dm644 ${pkgdir}/usr/lib/${_pkgname}/program/{bootstraprc,sofficerc} -t ${pkgdir}/etc/${_pkgname}
  install -Dm644 ${pkgdir}/usr/lib/${_pkgname}/share/psprint/psprint.conf -t ${pkgdir}/etc/${_pkgname}
  for cfgfile in {bootstrap,soffice}rc; do
    ln -fs {/etc/${_pkgname},${pkgdir}/usr/lib/${_pkgname}/program}/${cfgfile}
  done;
  ln -fs {/etc/${_pkgname},${pkgdir}/usr/lib/${_pkgname}/share/psprint}/psprint.conf

  # allow to preset desired VLC
  install -Dm644 ${_pkgname}.{sh,csh} -t ${pkgdir}/etc/profile.d

  # link pyuno to find its modules
  install -d ${pkgdir}/usr/lib/python3.8/site-packages
  for pyuno in uno{,helper}.py; do
    ln -fs {/usr/lib/${_pkgname}/program,${pkgdir}/usr/lib/python3.8/site-packages}/${pyuno}
  done;

  # make all language packages with help section ('1') available to
  # fix F1 key not opening translated offline help opening in browser
  install -Dm644 {help-${_pkgname}-,${pkgdir}/usr/lib/${_pkgname}/help/}languages.js

  # install shortcut and metadata
  install -d ${pkgdir}/usr/share/templates
  cat template-soffice.desktop.in | sed -e 's#@APP@#Calc#' | sed -e 's#@EXT@#ods#' | sed -e 's#@TYPE@#spreadsheet#' > ${pkgdir}/usr/share/templates/soffice.ods.desktop
  cat template-soffice.desktop.in | sed -e 's#@APP@#Draw#' | sed -e 's#@EXT@#odg#' | sed -e 's#@TYPE@#drawing#' > ${pkgdir}/usr/share/templates/soffice.odg.desktop
  cat template-soffice.desktop.in | sed -e 's#@APP@#Impress#' | sed -e 's#@EXT@#odp#' | sed -e 's#@TYPE@#presentation#' > ${pkgdir}/usr/share/templates/soffice.odp.desktop
  cat template-soffice.desktop.in | sed -e 's#@APP@#Writer#' | sed -e 's#@EXT@#odt#' | sed -e 's#@TYPE@#text#' > ${pkgdir}/usr/share/templates/soffice.odt.desktop
  install -Dm644 ${_pkgname}-${_pkgver}/sysui/desktop/appstream-appdata/*.xml -t ${pkgdir}/usr/share/metainfo

  # licenses
  install -Dm644 ${_pkgname}-${_pkgver}/COPYING{,.LGPL,.MPL} -t ${pkgdir}/usr/share/licenses/${pkgname}
}
