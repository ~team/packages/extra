# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libsbsms
pkgver=2.0.2
_debver=2.0.2
_debrel=2
pkgrel=1
pkgdesc='C++ library for high quality time stretching and pitch scaling of audio.'
arch=('i686' 'x86_64')
url='http://sbsms.sourceforge.net/'
license=('GPL-2')
depends=('gcc-libs' 'glibc')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/libs/libsbsms/${pkgname}_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/libs/libsbsms/libsbsms_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('361efc5e9712ad95b3d5c9e2cce717387776a6d948bb03b7983153d78d91d1cb01806f69c7992089823f69fd4273fee914fc5ced29de6f232c23272f6caa34fc'
            '1d7293647ffe578c9ba72bf2841cbee06726298e186c1c436d1d76671652796ff251f766286784155c57995fcca651abf65f7a7892f53b02aa92f8630ebc2566')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr \
              --libdir=/usr/lib
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
