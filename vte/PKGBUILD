# Maintainer: André Silva <emulatorman@hyperbola.info>

# Based on vte3 package

pkgbase=vte
pkgname=(vte vte-common)
pkgver=0.62.3
_debver=0.62.3
_debrel=1
pkgrel=1
pkgdesc="Virtual Terminal Emulator widget"
url="https://wiki.gnome.org/Apps/Terminal/VTE"
arch=(i686 x86_64)
license=(GPL-3)
depends=(gtk pcre2 gnutls fribidi)
makedepends=(gobject-introspection vala gperf meson quilt)
source=("https://gitlab.gnome.org/GNOME/$_pkgbase/-/archive/$pkgver/$pkgbase-$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/v/vte2.91/vte2.91_$_debver-$_debrel.debian.tar.xz")
sha512sums=('7a6c217bc7609fb49f973e993802c9b5c580a60a3f6ea248518b008a6b98c630f8b88c71c766f1376e52f659667f00532b36ac538a1fedf6f53f7b6c02566ca2'
            '1373ee8e75735fb3c8dd384f54806baa107e05eb119b4a15cbdd8c6f21d03f2330a68d718f6d092f9e99e9dccc68edf83385b7a7abacf675b5b2536ca00a64e9')

prepare() {
  cd $pkgbase-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  hyperbola-meson $pkgbase-$pkgver build -D docs=false -D b_lto=false -D _systemd=false
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package_vte() {
  depends+=(vte-common)
  replaces=(vte3)
  conflicts=(vte3)
  provides=(vte3 vte2.91)

  DESTDIR="$pkgdir" meson install -C build

  # split vte-common
  mkdir -p vte-common/{etc,usr/libexec}
  mv "$pkgdir"/etc/profile.d vte-common/etc
  mv "$pkgdir"/usr/libexec/vte-urlencode-cwd vte-common/usr/libexec
  rmdir -p --ignore-fail-on-non-empty "$pkgdir"/{etc,usr/libexec}

  # license
  install -Dm644 $pkgbase-$pkgver/COPYING.GPL3 -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_vte-common() {
  pkgdesc="Files shared by VTE libraries"
  depends=(sh)

  mv vte-common/* "$pkgdir"

  # license
  install -Dm644 $pkgbase-$pkgver/COPYING.GPL3 -t "$pkgdir"/usr/share/licenses/$pkgname
}

# vim:set sw=2 et:
