# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Alessio 'mOLOk' Bolognino <themolok@gmail.com>
# Contributor (Arch): Christoph 'delmonico' Neuroth <delmonico@gmx.net>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=librsync
pkgver=2.3.1
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Free and libre library for calculating and applying network deltas"
arch=('i686' 'x86_64')
url='http://librsync.sourcefrog.net/'
license=('LGPL-2.1')
depends=('popt' 'bzip2' 'zlib' 'libb2')
makedepends=('cmake' 'doxygen' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libr/librsync/librsync_$_debver-$_debrel.debian.tar.xz")
sha512sums=('89e5b5ad960b8036acce41df09f5e50601d7eb57d48a2bd21c4ee54a3a375f62ee514036b9a562277b5656735b84cadf6f54cbf48c364bbf0c04f2d95ae3b5a6'
            '9c9b77d36ffa5bfaf6d6d480a64e4c97c9d077718aa40348ba6dc0f7de0b69e40bd14f510311b4c17629da31a602c18e4368ed7889288ff8837c9e60d8e22cf2')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  cmake \
    -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib
  make -C build
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make -C build DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
