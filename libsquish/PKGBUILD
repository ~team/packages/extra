# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Davorin Učakar <davorin.ucakar@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libsquish
pkgver=1.15
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc="Free and libre DXT compression library"
arch=('i686' 'x86_64')
url='https://sourceforge.net/projects/libsquish'
license=('Expat')
makedepends=('cmake' 'ninja' 'doxygen' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/libs/libsquish/libsquish_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/libs/libsquish/libsquish_${_debver}-${_debrel}.debian.tar.xz"
        "${pkgname}-pkgconfig.pc")
sha512sums=('5b569b7023874c7a43063107e2e428ea19e6eb00de045a4a13fafe852ed5402093db4b65d540b5971ec2be0d21cb97dfad9161ebfe6cf6e5376174ff6c6c3e7a'
            '53b9934e152c6bfe15eba159b5d76c7f504cb9471405ba61772ad885e386b79b8ba88b64dfbc93906b0cebae610b3398c5a64cc78180ffce1ff25d4d1301fea6'
            'f71808820673e725cee112d1b139d4e4ab5110590b2f3a5bd3cac0cbcc90fc297496a442ffc9bc1c829d06e0ccad71baf9c2651eb9d0c2acb556a9ec85e72602')

prepare() {
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    quilt push -av
  fi
}

build() {
  cmake \
    -B build \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D BUILD_SHARED_LIBS=ON \
    -D BUILD_SQUISH_WITH_OPENMP=OFF \
    -G Ninja \
    -S .
  ninja -C build
}

package() {
  (cd build; cmake -D CMAKE_INSTALL_PREFIX="$pkgdir/usr" -P cmake_install.cmake)
  install -Dm644 CMakeModules/FindlibSquish.cmake "$pkgdir/usr/share/cmake/Modules/FindlibSquish.cmake"
  install -Dm644 LICENSE.txt -t "$pkgdir/usr/share/licenses/$pkgname"
  INSTALL_DIR="$pkgdir" LIB_PATH=usr/lib make uninstall install

  # cleanup and fixing
  rm -frv "$pkgdir/include"
  rm -rf "$pkgdir/usr/lib/pkgconfig"
  install -Dm644 "$srcdir/${pkgname}-pkgconfig.pc" "$pkgdir/usr/lib/pkgconfig/${pkgname}.pc"
}
