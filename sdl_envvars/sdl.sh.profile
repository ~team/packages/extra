#!/bin/sh

if [ -n "$XDG_CONFIG_HOME" ] && [ -r "$XDG_CONFIG_HOME/sdl.conf" ]; then
  . "$XDG_CONFIG_HOME/sdl.conf"
elif [ -n "$HOME" ] && [ -r "$HOME/.config/sdl.conf" ]; then
  . "$HOME/.config/sdl.conf"
elif [ -r /etc/sdl.conf ]; then
  . /etc/sdl.conf
fi

if [ $(uname -s) = Linux ]; then
  [ -n "$SDL_FBACCEL" ]               && export SDL_FBACCEL
  [ -n "$SDL_FBDEV" ]                 && export SDL_FBDEV
  [ -n "$SDL_JOYSTICK_DEVICE" ]       && export SDL_JOYSTICK_DEVICE
  [ -n "$SDL_LINUX_JOYSTICK" ]        && export SDL_LINUX_JOYSTICK
  [ -n "$SDL_MOUSEDEV" ]              && export SDL_MOUSEDEV
  [ -n "$SDL_MOUSEDEV_IMPS2" ]        && export SDL_MOUSEDEV_IMPS2
  [ -n "$SDL_MOUSEDRV" ]              && export SDL_MOUSEDRV
  [ -n "$SDL_NO_RAWKBD" ]             && export SDL_NO_RAWKBD
  [ -n "$SDL_NOMOUSE" ]               && export SDL_NOMOUSE
fi

[ -n "$AUDIODEV" ]                    && export AUDIODEV
[ -n "$SDL_AUDIODRIVER" ]             && export SDL_AUDIODRIVER
[ -n "$SDL_CDROM" ]                   && export SDL_CDROM
[ -n "$SDL_DEBUG" ]                   && export SDL_DEBUG
[ -n "$SDL_DISKAUDIODELAY" ]          && export SDL_DISKAUDIODELAY
[ -n "$SDL_DISKAUDIOFILE" ]           && export SDL_DISKAUDIOFILE
[ -n "$SDL_DSP_NOSELECT" ]            && export SDL_DSP_NOSELECT
[ -n "$SDL_MOUSE_RELATIVE" ]          && export SDL_MOUSE_RELATIVE
[ -n "$SDL_NO_LOCK_KEYS" ]            && export SDL_NO_LOCK_KEYS
[ -n "$SDL_PATH_DSP_NOSELECT" ]       && export SDL_PATH_DSP
[ -n "$SDL_VIDEO_ALLOW_SCREENSAVER" ] && export SDL_VIDEO_ALLOW_SCREENSAVER
[ -n "$SDL_VIDEO_CENTERED" ]          && export SDL_VIDEO_CENTERED
[ -n "$SDL_VIDEO_GL_DRIVER" ]         && export SDL_VIDEO_GL_DRIVER
[ -n "$SDL_VIDEO_X11_DGAMOUSE" ]      && export SDL_VIDEO_X11_DGAMOUSE
[ -n "$SDL_VIDEO_X11_MOUSEACCEL" ]    && export SDL_VIDEO_X11_MOUSEACCEL
[ -n "$SDL_VIDEO_X11_NODIRECTCOLOR" ] && export SDL_VIDEO_X11_NODIRECTCOLOR
[ -n "$SDL_VIDEO_X11_VISUALID" ]      && export SDL_VIDEO_X11_VISUALID
[ -n "$SDL_VIDEO_X11_YUV_DIRECT" ]    && export SDL_VIDEO_YUV_DIRECT
[ -n "$SDL_VIDEO_X11_YUV_HWACCEL" ]   && export SDL_VIDEO_YUV_HWACCEL
[ -n "$SDL_VIDEODRIVER" ]             && export SDL_VIDEODRIVER
[ -n "$SDL_WINDOWID" ]                && export SDL_WINDOWID
