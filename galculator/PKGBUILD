# Contributor (Arch): Martin Wimpress <code@flexion.org>
# Contributor (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Bartłomiej Piotrowski <barthalion@gmail.com>
# Contributor (Arch): SpepS <dreamspepser at yahoo dot it>
# Contributor (Arch): Alexander Fehr <pizzapunk gmail com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=galculator
pkgname=(galculator galculator-gtk2)
pkgver=2.1.4
_debver=2.1.4
_debrel=1.1
pkgrel=1
pkgdesc='GTK+ based scientific calculator'
arch=('i686' 'x86_64')
url='http://galculator.sourceforge.net/'
license=('GPL-2')
makedepends=('intltool' 'gtk' 'gtk2' 'quilt')
source=(http://galculator.mnim.org/downloads/${pkgbase}-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/g/galculator/galculator_$_debver-$_debrel.debian.tar.xz)
sha512sums=('ca5f373649d9bf26184e94ba6a501610efbb13e92a8723cda78b83aa495519e82e5b4fcd17f00f615eb702ed186598aecc70ae63a8238c32384b7f608cba4cfa'
            '01022c321d4cec09a5a440cac83ab0b10ec575920a6c63ce9ec8ec83f1f39294111be367b1f949e4227c561d9b6bc1314432e1ed815d5703d14d58bd83acbdec')

prepare() {
  cd "${srcdir}/${pkgbase}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  CFLAGS+=' -fcommon' # https://wiki.gentoo.org/wiki/Gcc_10_porting_notes/fno_common
  # GTK3 version
  cd "${srcdir}/${pkgbase}-${pkgver}"
  [ -d gtk3 ] || mkdir gtk3
  cd gtk3
  ../configure \
    --prefix=/usr
  make

  # GTK2 version
  cd "${srcdir}/${pkgbase}-${pkgver}"
  [ -d gtk2 ] || mkdir gtk2
  cd gtk2
  ../configure \
    --prefix=/usr \
    --disable-gtk3
  make
}

package_galculator() {
  depends=('gtk')

  cd "${pkgbase}-${pkgver}/gtk3"
  make DESTDIR="$pkgdir" install
  install -Dm644 "${srcdir}/${pkgbase}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_galculator-gtk2() {
  pkgdesc+=' (GTK2 version)'
  depends=('gtk2')
  conflicts=('galculator')

  cd "${pkgbase}-${pkgver}/gtk2"
  make DESTDIR="$pkgdir" install
  install -Dm644 "${srcdir}/${pkgbase}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
