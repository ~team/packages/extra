# Maintainer (Arch): David Runge <dave@sleepmap.de>
# Contributor (Arch): speps <speps at aur dot archlinux dot org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=zita-alsa-pcmi
pkgver=0.3.2
_debver=0.3.2
_debrel=2
pkgrel=1
pkgdesc="The successor of clalsadrv, provides easy access to ALSA PCM devices."
arch=('i686' 'x86_64')
url="https://kokkinizita.linuxaudio.org/linuxaudio/"
license=('GPL-3')
depends=('alsa-lib' 'gcc-libs' 'glibc')
makedepends=('quilt')
source=("https://kokkinizita.linuxaudio.org/linuxaudio/downloads/$pkgname-$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/z/zita-alsa-pcmi/zita-alsa-pcmi_$_debver-$_debrel.debian.tar.xz")
sha512sums=('9d6040d0ec636ebcf02a033cdc017105bbed8e0ddb99584890d8e027402a85791e0983a1a9cd677dc6f1da0ced530afb5e65a8e863329a5e785ef09e0b7124f0'
            '35331918655785cefbe3e6d0754e5d43ec51b32a320bde9b09a8822da690a0d76f1c0b48763ead3060b7d9202d9d58661d8e95632132eeadb56b63c85f1e928b')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # remove optimization
  # call ldconfig in local context
  sed -e '/native/d' \
      -e 's/ldconfig/& -N $(DESTDIR)\/$(LIBDIR)/' \
      -i source/Makefile
  # fix use of BINDIR without preceeding DESTDIR
  sed -e 's/\ $(BINDIR)/\ $(DESTDIR)$(BINDIR)/' \
      -i apps/Makefile
}

build() {
  cd "$pkgname-$pkgver"
  make -C source

  (
    cd source
    # create lib link for building apps
    ln -sv lib$pkgname.so.$pkgver lib$pkgname.so
  )

  CXXFLAGS+=" -I../source" \
  LDFLAGS+=" -L../source" \
  make -C apps

  rm -v source/lib$pkgname.so
}

package() {
  cd "$pkgname-$pkgver"
  # libs
  make -C source DESTDIR="$pkgdir/" PREFIX=/usr SUFFIX="" install
  install -vDm 644 {AUTHORS,README} -t "${pkgdir}/usr/share/doc/${pkgname}"
  # apps
  make -C apps DESTDIR="$pkgdir/" PREFIX=/usr install
  # license
  install -vDm 644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
# vim:set ts=2 sw=2 et:
