# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Chris Brannon <cmbrannon79@gmail.com>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@aur.archlinux.org>
# Contributor (Arch): Arvid Ephraim Picciani <aep@exys.org>
# Contributor (Arch): Michael Krauss <hippodriver@gmx.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

pkgname=python-pyparsing
pkgname=('python-pyparsing' 'tauthon-pyparsing')
pkgver=2.4.7
pkgrel=3
arch=('any')
license=('Expat')
url='https://github.com/pyparsing/pyparsing/'
makedepends=('python' 'tauthon')
source=("https://github.com/pyparsing/pyparsing/archive/pyparsing_$pkgver.tar.gz"
        "fix-build-without-setuptools.patch")
sha512sums=('c7a546729f86a2b5176e2482b566b9fd715b03e495aaef4d720b21307bb03f385dbc849247f8d266cb3d92be0a83c34ce4995b655ce85318355d5a0d42d6991e'
            '12d9a4d00e5e748147723f593e30ad03eba3351d6c6e031d315e880b63c81b87e6e39de1247855a0a2721fc68325db66464635128028fe515da7128e8aca8ac8')

prepare() {
  mv "pyparsing-pyparsing_$pkgver" "pyparsing-$pkgver"
  cd "$srcdir/pyparsing-$pkgver"
  patch -Np1 -i ${srcdir}/fix-build-without-setuptools.patch
  cp -a "$srcdir"/pyparsing-$pkgver{,-tauthon}
}

build() {
  cd "$srcdir/pyparsing-$pkgver"
  python setup.py build

  cd "$srcdir/pyparsing-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-pyparsing() {
  pkgdesc='General parsing module for Python'
  depends=('python')

  cd "$srcdir/pyparsing-$pkgver"

  python setup.py install --prefix=/usr --root="$pkgdir" --optimize=1
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_tauthon-pyparsing() {
  pkgdesc='General parsing module for Tauthon'
  depends=('tauthon')

  cd "$srcdir/pyparsing-$pkgver-tauthon"

  tauthon setup.py install --prefix=/usr --root="$pkgdir" --optimize=1
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
