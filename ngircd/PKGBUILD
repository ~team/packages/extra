# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Contributor (Arch): Alexander Rødseth <rodseth@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: André Silva <emulatorman@hyperbola.info>

pkgname=ngircd
pkgver=26
_debver=26.1
_debrel=1
pkgrel=2
pkgdesc='Next Generation IRC Daemon'
arch=('i686' 'x86_64')
backup=('etc/ngircd.conf')
url='https://ngircd.barton.de/'
license=('GPL-2')
depends=('libressl' 'libident' 'zlib')
makedepends=('quilt')
source=("https://ngircd.barton.de/pub/ngircd/ngircd-$pkgver.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/n/ngircd/ngircd_$_debver-$_debrel.debian.tar.xz"
        ngircd.initd
        ngircd.run)
sha512sums=('2502baf83e3bfca3e6b83c22fe660fee24732ee875b32a7071a489a8babcc08124738142215b55d4f9bd4e94bec3f2a41889ab18324f772b1674b02883cbfb91'
            'SKIP'
            'b0d02c3cbe75d0dbb472829be24346056356a0120b3b5d40ed60458137f101039defe86e267972af3d269fb8610904513e7c9f4fa66cd3f07deee80711c476e6'
            '016389f74d3bab61ca3a88c989a92fd4c30293395df2733343fdc91ab71d48a27769ffdbe79fa582ebb9a0d7da5194f7c58a201b62d0040f6c885c3bef48c8c8'
            '35909cb2706b09617365746486f9f9475ecf20f11298464e3e3680a755886ce7ed64bc93e87207adf082f686b19397f73b8952a978094bcacc325691bb963f97')
validpgpkeys=('F5B9F52ED90920D2520376A2C24A0F637E364856') # Alexander Barton <alex@barton.de>

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./configure --prefix=/usr \
        --sysconfdir=/etc \
        --mandir=/usr/share/man \
        --with-ident \
        --with-openssl \
        --with-pam=no \
        --enable-ipv6
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm755 "${srcdir}/ngircd.initd" "${pkgdir}/etc/init.d/ngircd"
  install -Dm755 "${srcdir}/ngircd.run" "${pkgdir}/etc/sv/ngircd/run"
}
