#!/bin/ksh

# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

# Based on aspell-en

_pkgname='aspell'
_pkglang='pl'
_srclang="${_pkglang}"
_srcsv=''
pkgname="${_pkgname}-i18n-dict-${_pkglang}"
# Example: _srcname='aspell6-en'
_srcname="${_pkgname}${_srcsv}-${_srclang}"
# The "0.50-2" and "6.0_20061121-0" package versions are undistributable,
# see: "_PKGBUILD" for "6.0_20061121-0" (the "0.50-2" does not contain license)
pkgver='0.51.0'
_srcver='0.51-0'
pkgrel='2'
pkgdesc='Polish dictionary for Aspell'
arch=('i686' 'x86_64')
url='https://ftp.gnu.org/gnu/aspell/dict/0index.html'
license=('CC-BY-SA-1.0')
groups=("${_pkgname}-i18n-dict" "${_pkgname}-dict"
        "g${_pkgname}-i18n-dict" "g${_pkgname}-dict"
        "gnu${_pkgname}-i18n-dict" "gnu${_pkgname}-dict"
        "gnu-${_pkgname}-i18n-dict" "gnu-${_pkgname}-dict")
depends=('aspell')
provides=("${_pkgname}-dict-${_pkglang}"
          "g${pkgname}" "g${_pkgname}-dict-${_pkglang}"
          "gnu${pkgname}" "gnu${_pkgname}-dict-${_pkglang}"
          "gnu-${pkgname}" "gnu-${_pkgname}-dict-${_pkglang}"
          "${_pkgname}-${_pkglang}")
conflicts=("${_pkgname}-${_pkglang}")
replaces=("${_pkgname}-${_pkglang}")
_source=("https://ftp.gnu.org/gnu/${_pkgname}/dict/${_srclang}/")
source=("${_source[0]}/${_srcname}-${_srcver}.tar.bz2"
        "${_source[0]}/${_srcname}-${_srcver}.tar.bz2.sig")
sha512sums=('494b93abb50dc40bb04c225b7294075132b1569bc013e9f1591b2c77c3ab085d7e1b149a9e6cc3f0672c020f637b0cf28c6e7de50429dba0a04b5f659262c292'
            'SKIP')
validpgpkeys=(# Kevin Atkinson
              '31A768B0808E4BA619B48F81B6D9D0CC38B327D7')
unset _pkgname _pkglang _srclang _srcsv _source

build() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  ./configure
  make 'V=1'
}

package() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  make "DESTDIR=${pkgdir}" 'install'

  for i in 'Copyright' 'doc/'*; do
    install -Dm '644' "${i}" -t "${pkgdir}/usr/share/licenses/${pkgname}"
  done
  unset i _srcname _srcver
}
