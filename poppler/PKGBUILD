# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgbase=poppler
pkgname=('poppler' 'poppler-glib' 'poppler-qt')
pkgver=20.09.0
pkgrel=4
arch=(i686 x86_64)
license=('GPL-3')
makedepends=('libjpeg-turbo' 'gcc-libs' 'cairo' 'fontconfig' 'openjpeg2' 'pkgconfig' 'lcms2' 
             'gobject-introspection' 'icu' 'qt-base' 'git' 'nss' 'curl' 'poppler-data'
             'cmake' 'python' 'boost')
options=('!emptydirs')
url="https://poppler.freedesktop.org/"
source=(https://poppler.freedesktop.org/${pkgbase}-${pkgver}.tar.xz{,.sig})
sha512sums=('fbd57dd0754279d4b0a20ccbdbfb06abc5a5d84e63915676353fd42d5b80318904cf7e6ae462db445ed6c6e011fc24675dccbb8bd3597fc6532657a4ea23406a'
            'SKIP')
validpgpkeys=('CA262C6C83DE4D2FB28A332A3A6A4DB839EAA6D7') # "Albert Astals Cid <aacid@kde.org>"

prepare() {
  git clone https://gitlab.freedesktop.org/poppler/test
}

build() {
  cmake -B build -S $pkgname-$pkgver \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
    -DENABLE_GTK_DOC=OFF \
    -DENABLE_ZLIB_UNCOMPRESS=ON \
    -DTESTDATADIR=$srcdir/test
  make -C build
}

check() {
  LANG=en_US.UTF8 make -C build test
}

package_poppler() {
  pkgdesc="PDF rendering library based on xpdf 3.0"
  depends=('libjpeg-turbo' 'gcc-libs' 'cairo' 'fontconfig' 'openjpeg2' 'lcms2' 'nss' 'curl')
  optdepends=('poppler-data: encoding data to display PDF documents containing CJK characters')
  conflicts=("poppler-qt3<${pkgver}" "poppler-qt4<${pkgver}")

  make DESTDIR="${pkgdir}" -C build install

  # license
  install -Dm644 ${srcdir}/${pkgbase}-${pkgver}/COPYING3 -t ${pkgdir}/usr/share/licenses/${pkgname}

  # cleanup for splitted build
  rm -vrf "${pkgdir}"/usr/include/poppler/{glib,qt5}
  rm -vf "${pkgdir}"//usr/lib/libpoppler-{glib,qt5}.*
  rm -vf "${pkgdir}"/usr/lib/pkgconfig/poppler-{glib,qt5}.pc
  rm -vrf "${pkgdir}"/usr/{lib,share}/gir*
}

package_poppler-glib() {
  pkgdesc="Poppler glib bindings"
  depends=("poppler=${pkgver}" 'glib2')

  make -C build/glib DESTDIR="${pkgdir}" install
  install -m755 -d "${pkgdir}/usr/lib/pkgconfig"
  install -m644 build/poppler-glib.pc "${pkgdir}/usr/lib/pkgconfig/"
  rm -vf "${pkgdir}"/usr/lib/libpoppler.*
  rm -vf "${pkgdir}/usr/bin/poppler-glib-demo"

  # license
  install -Dm644 ${srcdir}/${pkgbase}-${pkgver}/COPYING3 -t ${pkgdir}/usr/share/licenses/${pkgname}
}

package_poppler-qt() {
  pkgdesc="Poppler Qt bindings"
  depends=("poppler=${pkgver}" 'qt-base')

  make -C build/qt5 DESTDIR="${pkgdir}" install
  install -m755 -d "${pkgdir}/usr/lib/pkgconfig"
  install -m644 build/poppler-qt5.pc "${pkgdir}/usr/lib/pkgconfig/"

  # license
  install -Dm644 ${srcdir}/${pkgbase}-${pkgver}/COPYING3 -t ${pkgdir}/usr/share/licenses/${pkgname}
}

package_poppler-qt() {
  pkgdesc="Poppler Qt bindings"
  depends=("poppler=${pkgver}" 'qt-base')

  cd build
  make -C qt5 DESTDIR="${pkgdir}" install
  install -m755 -d "${pkgdir}/usr/lib/pkgconfig"
  install -m644 poppler-qt5.pc "${pkgdir}/usr/lib/pkgconfig/"

  # license
  install -Dm644 ${srcdir}/${pkgbase}-${pkgver}/COPYING3 ${pkgdir}/usr/share/licenses/${pkgname}/COPYING3
}
