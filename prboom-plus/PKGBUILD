# Maintainer (Arch): Alex Brinister <IrVQ55Gw9TZ7BW8e at tuta dot io>
# Contributor (Arch): Kingan <kingan201 at gmail dot com> 
# Contributor (Arch): Frederic Bezies <fredbezies at gmail dot com> 
# Contributor (Arch): Valsu [arch(at)hylia.de]
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=prboom-plus
pkgver=2.6um
pkgrel=1
pkgdesc="An advanced, vanilla-compatible Doom engine based on PrBoom"
url='https://sourceforge.net/projects/prboom-plus/'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('fluidsynth' 'glu' 'libmad' 'portmidi' 'sdl_image' 'sdl_mixer' 'sdl_net')
makedepends=('cmake')
groups=('games')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/coelckers/prboom-plus/archive/refs/tags/v$pkgver.tar.gz")
sha512sums=('c2f8c6895683ee22d729ad2c9dc10bd821e5bdb55d75b88b4c6db25b8f3604370d18285677660c82761912a159b0c1068a6fcf1c8333e20d849fa1087a74800f')

_rootdir="${pkgname}-${pkgver}/prboom2"

prepare() {
  cd "${srcdir}"  
  sed -i -E 's/mktemp/mkstemp/g' $(find "${_rootdir}" -type f -name r_demo.c)
}

build() {
  cd "${srcdir}"
  cmake \
    -B build \
    -S ${_rootdir} \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release
  make -C build
}

package() {
  cd "${srcdir}"
  make -C build DESTDIR="${pkgdir}" install

  # correct folder-structure
  install -d "${pkgdir}/usr/games"
  cp -R "${pkgdir}/usr/bin/"* "${pkgdir}/usr/games"
  rm -rf "${pkgdir}/usr/bin"

  install -Dm644 "${_rootdir}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
