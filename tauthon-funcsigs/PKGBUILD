# Contributor (Arch): Andrzej Giniewicz <gginiu@gmail.com>
# Maintainer (Arch): Jelle van der Waa <jelle@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=tauthon-funcsigs
pkgver=1.0.2
_debver=$pkgver
_debrel=5
pkgrel=2
pkgdesc="Tauthon function signatures from PEP362"
arch=('any')
url='https://pypi.python.org/pypi/funcsigs/'
license=('Apache-2.0')
depends=('tauthon')
makedepends=('tauthon-setuptools' 'quilt')
source=(https://pypi.python.org/packages/source/f/funcsigs/funcsigs-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/p/python-funcsigs/python-funcsigs_$_debver-$_debrel.debian.tar.xz)
sha512sums=('4e583bb7081bb1d6c0fe5a6935ca03032d562b93ef5c11b51a72ce9e7ac12902451cc2233c7e6f70440629d88d8e6e9625965ee408100b80b0024f3a6204afda'
            '07331d3e911ed2e33dab9cba34dae699367fc58c57227eed312726a0397060d3e68de5444ea6113aa67c8f9633b458ec1caaf169b5434d5afee5f17685d47593')

prepare() {
  cd "$srcdir/funcsigs-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package() {
  cd "$srcdir/funcsigs-$pkgver"
  tauthon setup.py install --root="${pkgdir}"  --optimize=1
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"
}
