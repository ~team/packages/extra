# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=seabios
pkgver=1.14.0
pkgrel=2
pkgdesc="A 16-bit x86 BIOS"
arch=('any')
url='https://seabios.org/SeaBIOS'
license=('GPL-3')
makedepends=('acpica' 'python')
options=('!makeflags' '!strip')
source=(https://www.seabios.org/downloads/$pkgname-$pkgver.tar.gz
        config.coreboot
        config.seabios-128k
        config.seabios-256k
        config.vga.isavga
        config.vga.stdvga
        config.csm
        config.vga.cirrus
        config.vga.qxl
        config.vga.bochs-display
        config.vga.ramfb
        config.vga.virtio
        reproducible-version.patch)
sha512sums=('215c42f59425f8abd062be7b11fc0e39c977cee5001a2381551b0f851ac337d0dd53c065267e6dee0e710ffd700fa635f9007b89da8dfce0f47122c984ee8146'
            'dc77f693e2426a8a9b084f22d607d9bf6dfd0776cb86373a55d6e02f154f546b6fd616bb981783e914be51eb843311652a90b111fb573e32b3a8207d66aea218'
            'e91fc068680a16439b76cb1910dee9088f703ca50abda405e7277b083ff9183cccdf6b428cc18ae42dd8464fffb567e195d39ed800c68c3b7c85f92166dd2488'
            '3d41739944da088edafb3ea298c0d3db59ed638b614c258209a30635caccf86a284f03492612694e3a56f40357743a0a36053e8ec11b7d93853b91ba9e5a502f'
            'aada61232f4834c1e9bec921b1e1365ce5ecb4adf42c659f34cdf051efb56f0ec2e62f0ccf66bb25d9bb0b8601e2df49b712265f19185068d45353c3aacf1cd9'
            '4a1b7fcc729d78dc8fd4e73d1cb6258ed9d49f8a91e6e00cc184e07c89a304f8d38ef5446d1c4ba5e8e929c82693d82c21526e42992ad6e1a008f39bb7c90448'
            'a2238723fbbb96184bb52b018633701aeb929bfae43f50659258dee854acaf4f1bdf2c201c65fb46d2712372d11ab345eac1c41068f82d6dcbef91ef9d1d39cd'
            'e9ef2d6bec9419e69bc90adf1a4bb7c174284cd722e53903deea0411f88074cc247069116e03e124715072ec82f153cf6014168febba41369a2569983d3265b6'
            '9ebcb6702cf28685daf1821be26bab8ddc791ef2c118217c984c03c5fb77c8b9691c0fa6931367a63b8d97d67c973cd4b620fe9ca9c76da51a9b2ab3b4b5653b'
            'c395975320ec9e4eabf0ef60c25122e141de0607c161e9c0ec507916297b5fc6bbf7874a1fd8c3d73ec82fb9d00b5ff8a60344cabd11c404f3de0b7b3f4ed6bf'
            '00bffe38865d210c2d3473e6a6913eda51235b89c03d2c35e04b606e19f1881857ac5cfed77bd127c1fe377065fdb435342111d311d7b48ce1d21b7b863e3bf3'
            '4d627be11d79f0b8bd814a49e608826375aba6b59a0189dcba9afe24a181347b92e6ab18e0d9199e2f7a78f8fb02f03dad84c63fbbc2ffe9af76777ef28c5f8a'
            '53f70350678f9557c39ce12fb7dc82dbb033414abb41566fc2245ec7000a21c8ab291a97205e371888abc3123a952e7e85ab5700a7586242a7289111ec24685a')

build_bios() {
  make clean distclean
  cp $1 .config
  make oldnoconfig V=1

  make V=1 $4

  cp out/$2 binaries/$3
}

prepare() {
  cd seabios-$pkgver

  patch -p1 < $srcdir/reproducible-version.patch

  rm -rf binaries
  mkdir binaries

  echo "Hyperbola GNU/Linux-libre ${pkgver}-${pkgrel}" > .version
}

build() {
  cd seabios-$pkgver
  # seabios
  build_bios "${srcdir}"/config.csm Csm16.bin bios-csm.bin
  build_bios "${srcdir}"/config.coreboot bios.bin.elf bios-coreboot.bin
  build_bios "${srcdir}"/config.seabios-128k bios.bin bios.bin
  build_bios "${srcdir}"/config.seabios-256k bios.bin bios-256k.bin
  # seavgabios
  for config in "${srcdir}"/{config.vga.isavga,config.vga.stdvga,config.vga.cirrus,config.vga.qxl,config.vga.ramfb,config.vga.virtio,config.vga.bochs-display}; do
    name=${config#*config.vga.}
    build_bios ${config} vgabios.bin vgabios-${name}.bin out/vgabios.bin
  done
}

package() {
  cd seabios-$pkgver
  install -Dm644 binaries/*.bin -t "${pkgdir}/usr/share/qemu"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
