# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=fltk
pkgver=1.3.8
pkgrel=2
pkgdesc="Graphical user interface toolkit for X"
arch=('i686' 'x86_64')
license=('LGPL-2' 'custom:FLTK')
url='https://www.fltk.org/'
depends=('alsa-lib' 'hicolor-icon-theme' 'libjpeg-turbo' 'libpng' 'libx11' 'libxcursor'
         'libxfixes' 'libxft' 'libxinerama' 'libxrender' 'mesa-libgl')
makedepends=('cmake' 'doxygen')
options=('staticlibs')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/${pkgname}/${pkgname}/archive/release-${pkgver}.tar.gz")
sha512sums=('197848d3b80a65cca936daf4f0b74609f0fe8332a4cd11af53385fb2aa45ad698b1e239a48732b118cd3cb189bc531711b72fb2eeeb85be887dc6c5a558fa4b3')

prepare() {
  mv -v ${pkgname}-{release-,}${pkgver}
}

build() {
  cmake \
    -S ${pkgname}-${pkgver} \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DFLTK_LIBDIR=/usr/lib \
    -DCMAKE_BUILD_TYPE='None' \
    -DOPTION_CREATE_LINKS=ON \
    -DOPTION_BUILD_SHARED_LIBS=ON \
    -DOPTION_BUILD_HTML_DOCUMENTATION=ON \
    -DOPTION_INSTALL_HTML_DOCUMENTATION=ON \
    -Wno-dev

  make -C build
  make -C build/documentation html
}

package_fltk() {
  make DESTDIR=${pkgdir} install -C build
  make DESTDIR=${pkgdir} install -C build/documentation html
  install -Dm644 ${srcdir}/${pkgname}-${pkgver}/COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
  rm -frv ${pkgdir}/usr/share/man/man6
}
