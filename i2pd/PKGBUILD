# Maintainer (Arch): robertfoster
# Contributor (Arch): kurych
# Contributor (Arch): redfish
# Contributor (Arch): atommixz
# Contributor (Arch): denn
# Contributor (Arch): post-factum
# Contributor (Arch): wrdcrrtmnstr
# Contributor (Arch): r4sas
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=i2pd
pkgver=2.52.0
pkgrel=1
pkgdesc="Simplified, free and libre C++ implementation of I2P client"
arch=('i686' 'x86_64')
url='https://i2pd.website/'
license=('Modified-BSD')
depends=('boost-libs' 'miniupnpc' 'libressl' 'zlib')
makedepends=('boost' 'cmake')
provides=('i2p-router')
install='i2pd.install'
backup=("etc/${pkgname}/${pkgname}.conf"
        "etc/${pkgname}/tunnels.conf"
        "etc/conf.d/${pkgname}")
source=("$pkgname-$pkgver.tar.gz::https://github.com/PurpleI2P/$pkgname/archive/$pkgver.tar.gz"
        "$pkgname.confd"
        "$pkgname.initd"
        "$pkgname.run")
sha512sums=('54272b23dcb62b8b7523038a9c1fc71b87bb9042f22bd12d6ebc62cfa47da07e2df47d88d706f285e9d88f9cace0564546632c10161f83f5c57b9ea17f5bde5f'
            '083f4c860d7556bd14f2765b098743c25f996ef16de3982430ff27ac7711051738d48709654441099ea8c755b6d9a6e25b52286f7e8c928d3f39f1207a9517a9'
            '99a7e4d94840b68ce093c22141da46d89b54c63c214fbf3c0eada4bc28d128da181fc7968a0571028f8772acc91a7d58a87f9f57bb65b748dd730bd57e6c64e1'
            '42ea21e73fdc32cd16e2e29140037450258e2585f3df8f394600d15de991b353e242c874edeeebf5b7473489b923b7fbbeab875e4cfc715e7e90de5dc6aea525')

build() {
  cmake \
    -S $pkgname-$pkgver/build \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_BINDIR=sbin \
    -DCMAKE_BUILD_TYPE=Release \
    -DWITH_UPNP=ON \
    -DWITH_LIBRARY=OFF \
    -DWITH_PCH=OFF
  make -C build
}

package(){
  make DESTDIR="$pkgdir" -C build install

  cd "$srcdir/$pkgname-$pkgver"
  install -Dm0644 contrib/$pkgname.conf $pkgdir/etc/$pkgname/$pkgname.conf
  install -Dm0644 contrib/tunnels.conf $pkgdir/etc/$pkgname/tunnels.conf
  install -Dm0644 contrib/subscriptions.txt $pkgdir/etc/$pkgname/subscriptions.txt

  install -d -m0750 $pkgdir/var/lib/$pkgname
  ln -s ../../../etc/$pkgname/$pkgname.conf $pkgdir/var/lib/$pkgname/$pkgname.conf
  ln -s ../../../etc/$pkgname/tunnels.conf $pkgdir/var/lib/$pkgname/tunnels.conf
  ln -s ../../../etc/$pkgname/subscriptions.txt $pkgdir/var/lib/$pkgname/subscriptions.txt

  pushd contrib
  find ./certificates -type d -exec install -d {} $pkgdir/usr/share/${pkgname}/{} \;
  find ./certificates -type f -exec install -Dm644 {} $pkgdir/usr/share/${pkgname}/{} \;
  ln -s ../../../usr/share/$pkgname/certificates $pkgdir/var/lib/$pkgname/certificates
  popd

  # license
  install -Dm644 LICENSE $pkgdir/usr/share/licenses/$pkgname/LICENSE

  # docs
  install -Dm644 README.md $pkgdir/usr/share/doc/$pkgname/README.md

  # openrc and runit
  install -Dm0644 "$srcdir/${pkgname}.confd" "${pkgdir}/etc/conf.d/${pkgname}"
  install -Dm0755 "$srcdir/${pkgname}.initd" "${pkgdir}/etc/init.d/${pkgname}"
  install -Dm0755 "$srcdir/${pkgname}.run" "${pkgdir}/etc/sv/${pkgname}/run"

  # man
  install -Dm644 debian/$pkgname.1 $pkgdir/usr/share/man/man1/$pkgname.1
  chmod -R o= $pkgdir/var/lib/$pkgname
}
