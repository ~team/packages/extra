# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): graysky <graysky AT archlinux DOT us>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Contributor (Arch): Army <uli[dot]armbruster[at]gmail[dot]com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ncmpcpp
pkgver=0.9.2
pkgrel=2
pkgdesc='Almost exact clone of ncmpc with some new features'
url='https://ncmpcpp.rybczak.net/'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('curl' 'libmpdclient' 'fftw' 'boost-libs' 'icu' 'glibc'
         'gcc-libs' 'ncurses' 'readline' 'taglib')
makedepends=('boost')
source=(https://rybczak.net/ncmpcpp/stable/${pkgname}-${pkgver}.tar.bz2)
sha512sums=('bbcb3b50233e363412af3c1cf1c0118122733dc376759cd35dee79950f42e098a0402ff670684df91847b990b26022630c9a71192e462bd46e70628e31489742')

prepare() {
  cd ${pkgname}-${pkgver}
  autoreconf -fiv
  sed -e 's/CXXFLAGS=/CXXFLAGS+=/' \
      -e 's/CPPFLAGS=/CPPFLAGS+=/' \
      -e 's/LDFLAGS=/LDFLAGS+=/' \
      -i extras/Makefile
}

build() {
  cd ${pkgname}-${pkgver}

  # http://site.icu-project.org/download/61#TOC-Migration-Issues
  export CPPFLAGS+=' -DU_USING_ICU_NAMESPACE=1'

  # Work around deprecated TagLib::String::null from TagLib 1.12
  CXXFLAGS+=' -Wno-error=deprecated-declarations'

  BOOST_LIB_SUFFIX='' ./configure \
    --prefix=/usr \
    --enable-clock \
    --enable-outputs \
    --enable-visualizer \
    --with-fftw \
    --with-taglib
  make
  make -C extras
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm 755 extras/artist_to_albumartist -t "${pkgdir}/usr/bin"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}