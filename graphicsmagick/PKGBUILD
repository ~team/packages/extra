# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Jaroslav Lichtblau <dragonlord@aur.archlinux.org>
# Contributor (Arch): Anton Leontiev <bunder@t-25.ru>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=graphicsmagick
pkgver=1.3.36
__debver=1.4+really
_debver=1.3.36+hg16481
_debrel=2
pkgrel=2
pkgdesc="Image processing system"
url='http://www.graphicsmagick.org/'
arch=('i686' 'x86_64')
license=('Expat' 'Simplified-BSD')
optdepends=('jasper: jp2 module'
            'libwmf: wmf module'
            'libxml2: msl, svg, url modules'
            'ghostscript: pdf, ps modules')
makedepends=('perl' 'jasper' 'libwmf' 'libxml2' 'quilt')
depends=('freetype2' 'libxext' 'libsm' 'lcms2' 'libltdl' 'bzip2' 'xz' 'libpng' 'libtiff')
replaces=('imagemagick')
conflicts=('imagemagick')
provides=('imagemagick')
source=("https://downloads.sourceforge.net/project/${pkgname}/${pkgname}/${pkgver}/GraphicsMagick-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/g/graphicsmagick/graphicsmagick_${__debver}${_debver}-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('27ed5fcbfb0303436bdcb0f11d18257a967d471251b51e633e2c655b0a7e5ed01f61a61f9702cc7d4c02714e005ac18ca9ba0f690c0580e41d07947a03c5cbb4'
            '55b008386029129856df9c79fbbf1272785a74562c4104d36604f84a6bcf799d7f68f14280861a6560fcf1cf107bc6f00a69db998fb2f6fb674106d7fb75cf63')
options=('!emptydirs' 'libtool')

prepare() {
	cd "${srcdir}/GraphicsMagick-${pkgver}"
	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		quilt push -av
	fi
	sed -e "s:freetype_config='':freetype_config='/usr/bin/pkg-config freetype2':g" -i configure
}

build() {
	cd "${srcdir}/GraphicsMagick-${pkgver}"
	./configure \
		--prefix=/usr \
		--enable-shared \
		--with-modules \
		--with-perl \
		--with-gs-font-dir=/usr/share/fonts/Type1 \
		--with-quantum-depth=16 \
		--with-threads
	make
}

package() {
	cd "${srcdir}/GraphicsMagick-${pkgver}"

	make DESTDIR="${pkgdir}" install

	# Install license
	install -Dm644 "Copyright.txt" -t "${pkgdir}/usr/share/licenses/${pkgname}"

	# Install perl bindings
	# The patching was introduced in order to build perl module without installing package itself and
	# not to introduce unnecessary path into LD_RUN_PATH
	cd PerlMagick
	sed -i -e "s:'LDDLFLAGS'  => \"\(.*\)\":'LDDLFLAGS'  => \"-L${pkgdir}/usr/lib \1\":" Makefile.PL
	perl Makefile.PL INSTALLDIRS=vendor PREFIX=/usr DESTDIR="${pkgdir}"
	sed -i -e "s/LDLOADLIBS =/LDLOADLIBS = -lGraphicsMagick/" Makefile
	make
	make install
}