# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Simon Parzer <simon.parzer@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=grafx2
pkgver=2.7
pkgrel=1
_recoil=5.0.0
pkgdesc='Pixelart-oriented painting program'
arch=('i686' 'x86_64')
url='http://grafx2.chez.com/'
license=('GPL-2')
depends=('fontconfig' 'hicolor-icon-theme' 'lua' 'sdl2_image' 'sdl2_ttf')
options=(!emptydirs)
source=("https://gitlab.com/GrafX2/grafX2/-/archive/v2.7/grafX2-v${pkgver}.tar.gz"
        "https://sourceforge.net/projects/recoil/files/recoil/$_recoil/recoil-$_recoil.tar.gz"
        "fix-appstream.patch")
sha512sums=('d2af107b5c3495c1c769446b3669d44c3b776ab6e906595b69a7142443f9f045daa594f7cb5ccbc6cd624e6a8047ede7824986d162ddc59c2432339b9b7efc67'
            'f6c7d782abcfb7ab09c09b64e2561d5ae752cd1657be6f440a5af118cba57842b064fa6eb9ffb3a3cdce4d2c6b9a76e0a1379e37851779ec2420a2acc430a01f'
            '14530b489b2c1b06cb193ebe7127994ba19ece286fb2233b9e2c4e396c349d81b25f4fbce7b55863ab71af4178409a95e9bc8b8b0a1db3f2e78a67e8e7ec3023')

prepare() {
  mv grafX2-v${pkgver} $pkgname
  sed 's/-liconv//g' -i "$pkgname/src/Makefile"
  mkdir -p grafx2/3rdparty/archives
  cp -uv recoil-$_recoil.tar.gz grafx2/3rdparty/archives/recoil-$_recoil.tar.gz

  cd $pkgname

  # Fix AppStream metadata
  # https://gitlab.com/GrafX2/grafX2/-/merge_requests/288
  patch -Np1 -i ../fix-appstream.patch
}

build() {
  CFLAGS="$CFLAGS -w" make -C $pkgname/src PREFIX=/usr API=sdl2
}

package() {
  install -d "$pkgdir/usr/share/grafx2/scripts/samples_2.4/picture/"{thomson/lib,others-8bit/lib}
  CFLAGS="$CFLAGS -w" make -C $pkgname/src PREFIX=/usr DESTDIR="$pkgdir" API=sdl2 install
  mv "$pkgdir/usr/bin/grafx2-sdl2" "$pkgdir/usr/bin/grafx2"
  install -Dm644 "$pkgname/doc/README.txt" \
    "$pkgdir/usr/share/doc/$pkgname/README"

  # install license
  install -Dm644 "$srcdir/$pkgname/LICENSE" -t "$pkgdir/usr/share/licenses/$pkgname"
}
