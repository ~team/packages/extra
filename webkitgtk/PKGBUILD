# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Luke R. <g4jc@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=webkitgtk
_pkgname=webkit2gtk
pkgver=2.32.1
_debver=$pkgver
_debrel=2
pkgrel=8
pkgdesc="Web content engine for GTK"
url='https://webkitgtk.org'
arch=(i686 x86_64)
# WebCore uses LGPL-2.1, LGPL-2 or Simplified-BSD
# JavaScriptCore uses LGPL-2
# WebDriver, WebKit and WebInspectorUI uses Simplified-BSD
# builtin angle library uses Clear-BSD
# builtin xdgmime library uses LGPL-2 or AFL-2.0
# other pieces of code uses Unicode, Modified-BSD, Simplified-BSD and Expat
# unused tools (like skeletons and m4sugar) uses GPL-3 and GPL-2
license=(LGPL-2.1)
# this package contains builtin libraries, like:
# angle and xdgmime, it require patch to separate it
# disabled builtin libraries, are: capstone, gtest and libwebrtc
depends=(enchant gperf gst-plugins-bad gst-plugins-base gtk harfbuzz-icu hyphen libxslt libxt libsoup sqlite)
optdepends=('gobject-introspection: gir bindings')
makedepends=(cmake gobject-introspection python quilt ruby ninja)
provides=($_pkgname)
conflicts=($_pkgname)
replaces=($_pkgname)
source=(https://webkitgtk.org/releases/webkitgtk-$pkgver.tar.xz{,.asc}
        https://repo.hyperbola.info:50000/sources/$_pkgname/${_pkgname}_$_debver-$_debrel.debian.tar.xz{,.sig}
        remove-libwebp-support.patch)
sha512sums=('ff49522c585d3c5c9b88c4176673aebfdf7f2a127d7bc4007008a66c493476ae6551189865e4f0f330a7b332a438b6c951bf1a4c21cd0acf69b7051903d24843'
            'SKIP'
            '5dbe43329e967517674837b751eeedfbee70aebaf73525d72f8a87190117a64318fc6978f06df1c288c1370781d60e05476a533a0c1d5a469f92a7c320dee96c'
            'SKIP'
            '63fb2771d0683aba736c65463881636c092934d132f12a87be09db58068e188b95cd2e12ff40ec3adf3d8f3bf49f30f1203034e90ce17814456d0697a108b967')
validpgpkeys=('D7FCF61CF9A2DEAB31D81BD3F3D322D0EC4582C3'  # Carlos Garcia Campos <cgarcia@igalia.com>
              '5AA3BC334FD7E3369E7C77B291C559DBE4C9123B'  # Adrián Pérez de Castro <aperez@igalia.com>
              'C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -Np1 -i ${srcdir}/remove-libwebp-support.patch
}

build() {
  CFLAGS+=' -g1'
  CXXFLAGS+=' -g1'

  cmake -S $pkgname-$pkgver -B build -G Ninja \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D CMAKE_INSTALL_LIBDIR=lib \
    -D CMAKE_INSTALL_LIBEXECDIR=libexec \
    -D CMAKE_SKIP_RPATH=ON \
    -D DEVELOPER_MODE=OFF \
    -D ENABLE_API_TESTS=OFF \
    -D ENABLE_BUBBLEWRAP_SANDBOX=OFF \
    -D ENABLE_GAMEPAD=OFF \
    -D ENABLE_GLES2=OFF \
    -D ENABLE_GTKDOC=OFF \
    -D ENABLE_MINIBROWSER=ON \
    -D ENABLE_SPELLCHECK=ON \
    -D PORT=GTK \
    -D USE_LIBNOTIFY=OFF \
    -D USE_LIBSECRET=OFF \
    -D USE_OPENGL_OR_ES=ON \
    -D USE_SYSTEMD=OFF \
    -D USE_WPE_RENDERER=OFF \
    -D USE_WOFF2=OFF

  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  install -Dm644 $pkgname-$pkgver/Source/WebCore/LICENSE-LGPL-2.1 -t $pkgdir/usr/share/licenses/$pkgname
}
