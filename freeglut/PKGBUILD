# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=freeglut
pkgver=3.2.1
pkgrel=1
pkgdesc="Provides functionality for small OpenGL programs"
arch=(i686 x86_64)
url="http://freeglut.sourceforge.net/"
license=(Expat)
depends=(libxi libxrandr mesa-libgl)
makedepends=(mesa glu libxxf86vm cmake)
replaces=(glut)
provides=(glut)
conflicts=(glut)
source=(https://downloads.sourceforge.net/freeglut/${pkgname}-${pkgver}.tar.gz)
sha512sums=('aced4bbcd36269ce6f4ee1982e0f9e3fffbf18c94f785d3215ac9f4809b992e166c7ada496ed6174e13d77c0f7ef3ca4c57d8a282e96cbbe6ff086339ade3b08')

build() {
  cmake -H$pkgname-$pkgver -Bbuild \
    -DFREEGLUT_BUILD_STATIC_LIBS=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DCMAKE_BUILD_TYPE=None
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --build build --target install
  ln -sr "$pkgdir"/usr/lib/pkgconfig/{glut,freeglut}.pc
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 $pkgname-$pkgver/COPYING
}

# vim: ts=2 sw=2 et:
