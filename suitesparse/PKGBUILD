# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=suitesparse
pkgver=5.8.1
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="A free and libre collection of sparse matrix libraries"
url='http://faculty.cse.tamu.edu/davis/suitesparse.html'
arch=('i686' 'x86_64')
conflicts=('umfpack')
provides=('umfpack')
replaces=('umfpack')
depends=('metis' 'lapack' 'tbb' 'mpfr')
makedepends=('gcc-fortran' 'cmake' 'chrpath' 'quilt')
license=('LGPL-2.1' 'Modified-BSD' 'GPL-2' 'Apache-2.0' 'Simplified-BSD' 'LGPL-3')
options=('staticlibs')
source=("$pkgname-$pkgver.tar.gz::https://github.com/DrTimothyAldenDavis/SuiteSparse/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/suitesparse/suitesparse_${_debver}+dfsg-${_debrel}.debian.tar.xz"
        "suitesparse-no-demo.patch")
sha512sums=('3d986157acd39968216880c2de30c54d72e0addd7318a04948c1b842ad9c876bcde32f292504d4540a4baa998a368dfc0db000606a2d00f61760a92d5b8c1cb8'
            'f7561a966c03c9de16303ce8c341204180a26d8fe4109bd81a14a793456fde20b45cff643442f2f46cb825caccef9daa45d2bbdb593b91e215833dd10a2cea59'
            'f7bfd7fd8d8defd1e7dc1afdb05ed9fe32ab144b82052a6863b96b8c423521aca9560d093c82aca02da493473d5cda90482fd084f528b90f33590fae2cf5e4d9')

prepare() {
  cd SuiteSparse-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/buildflags.patch || true
    rm -v debian/patches/graphblas-build.patch || true
    rm -v debian/patches/mongoose-build.patch || true

    quilt push -av
  fi
  patch -p1 -i ${srcdir}/suitesparse-no-demo.patch
}

build() {
  if [[ $CARCH = "i686" ]]; then
    export CXXFLAGS="-Og"
    export CFLAGS="-Og"
  fi
  cd SuiteSparse-$pkgver
  BLAS=-lblas LAPACK=-llapack TBB=-ltbb SPQR_CONFIG=-DHAVE_TBB MY_METIS_LIB=/usr/lib/libmetis.so make
}


package() {
  cd SuiteSparse-$pkgver
  install -dm755 "${pkgdir}"/usr/{include,lib}

  BLAS=-lblas LAPACK=-llapack TBB=-ltbb SPQR_CONFIG=-DHAVE_TBB MY_METIS_LIB=/usr/lib/libmetis.so \
  make INSTALL_LIB="${pkgdir}"/usr/lib INSTALL_INCLUDE="${pkgdir}"/usr/include install

  # fix RPATH
  chrpath -d "$pkgdir"/usr/lib/*

  install -Dm644 debian/copyright -t "${pkgdir}/usr/share/licenses/$pkgname"
}
