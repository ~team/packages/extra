# Maintainer (Arch): Eli Schwartz <eschwartz@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ncompress
pkgver=4.2.4.6
_debver=$pkgver
_debrel=6
pkgrel=1
pkgdesc="A fast, simple, free and libre LZW file compressor"
arch=('i686' 'x86_64')
url='https://github.com/vapier/ncompress'
license=('Unlicense')
provides=('compress')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/n/ncompress/ncompress_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/n/ncompress/ncompress_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('03dd9670c48548d6f7a028dd307758ce7ac61997297c1075d65457ac849e8f2d710bc38fa9eb42e4b2cac163c1b93b7cf7eb197e90cbd689a1274df3de2a3ed8'
            '71e4a439a7309707b3f892d575318e8d449c032dd64f0a21f117c3e133a05b7ac626dcf2d98ede0b72ca8359194d559b32ff0d58933f940f48d0de32374d9904')

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" PREFIX=/usr install_core
  rm -rf "${pkgdir}/usr/bin/"
  install -Dm755 compress -t "${pkgdir}/usr/bin/"
  ln -s compress "${pkgdir}/usr/bin/"uncompress.real

  install -Dm644 -t "${pkgdir}/usr/share/licenses/${pkgname}/" LZW.INFO UNLICENSE
}