# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): Eric Bélanger <eric@archlinux.org>
# Contributor (Arch): Lukas Jirkovsky <l.jirkovsky@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=geeqie
pkgver=1.6
_debver=$pkgver
_debrel=9
pkgrel=7
pkgdesc="Lightweight image viewer"
arch=('i686' 'x86_64')
url='http://www.geeqie.org/'
license=('GPL-2' 'FDL-1.2')
depends=('exiv2' 'gtk' 'ffmpegthumbnailer' 'djvulibre' 'poppler-glib')
makedepends=('intltool' 'python' 'librsvg-legacy' 'libwmf' 'gawk' 'perl-image-exiftool'
             'doxygen' 'yelp-tools' 'graphviz' 'quilt')
optdepends=('librsvg-legacy: SVG rendering'
            'gawk: to use the geo-decode function'
            'perl-image-exiftool: for the jpeg extraction plugin'
            'gphoto2: command-line tools for various (plugin) operations')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/g/geeqie/geeqie_${pkgver}.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/g/geeqie/geeqie_${_debver}-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('4acd05c6012c80581e9c594c7dc7d12265528aa8d3868afef9def36798945afb851da9376bbe5f205ac3c5e1e2362188cc0e2891b49f3ea4396bbc6974c143ef'
            '325c10f13284e8d4260a74b78a3cb7972d361414dba4532dd9b7d983263efb8cb78ed700506c8582379f961ceab3f885c52e1d235fc1d0edaa76396e37c599a6')

prepare() {
  cd "$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/ ./

    quilt push -av
  fi

  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd "$pkgname-$pkgver"
  ./configure \
    --prefix=/usr
  make
}

package(){
  cd "$pkgname-$pkgver"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING debian/copyright -t "${pkgdir}/usr/share/licenses/$pkgname"
}
