#!/bin/ksh

# Maintainer: Márcio Silva <coadde@hyperbola.info>

# Based of memtest86+ package

pkgname='pcmemtest'
pkgver='1.5'
pkgrel='1'
pkgdesc="Advanced memory diagnostic tool"
arch=('i686' 'x86_64')
url="https://github.com/martinwhitaker/${pkgname}"
license=('GPL-2')
depends=('efibootmgr' 'sh' 'util-linux')
optdepends=('grub: for Grub support')
provides=('memtest86+' 'memtest86')
conflicts=('memtest86+' 'memtest86')
replaces=('memtest86+' 'memtest86')
_source=("https://github.com/martinwhitaker/${pkgname}")
source=("${_source[0]}/archive/refs/tags/v${pkgver}.tar.gz"
	"60_${pkgname}" "${pkgname}-install")
sha512sums=('12db2504fef23d62b2246fb638e6f50cd2634ce54629467686b565512e866a51e0b6bcb4b0ad89b08e824b76841568b01ec151a86c50c6c14b4ef433c342068e'
            '5c00475f87ca5b6a588a6d864545d0277e1108a41e8525dbd360491c066e34bbece2ebf42111870e9dc3cc83de96bba80c22672083d4d0090b5db4292d583220'
            '02e7583e986756894bbe11200d583a2f80f711d3f6c1ca7b383c88e9ad46cb73df43b0c841c767470c560fdc9bb3ba9f0c6bd8e4110b216217837546f933bd2e')
if [ "${CARCH}" = 'i686' ]; then
  _arch='32'
  _uefi="ia${_arch}"
elif [ "${CARCH}" = 'x86_64' ]; then
  _arch='64'
  _uefi="x${_arch}"
fi

build() {
  cd "${pkgname}-${pkgver}/build${_arch}"
  make V='1'
}

package() {
  cd "${pkgname}-${pkgver}/build${_arch}"

  install -Dm '644' "${srcdir}/${pkgname}-install" -t "${pkgdir}/usr/sbin"

  install -Dm '644' "${pkgname/pc}.bin" -t "${pkgdir}/usr/share/${pkgname}"
  install -Dm '644' "${pkgname/pc}.efi" \
    "${pkgdir}/usr/share/${pkgname}/boot${_uefi}.efi"
  install -Dm '755' "${srcdir}/60_${pkgname}" -t "${pkgdir}/etc/grub.d"

  install -Dm '644' '../LICENSE' -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
