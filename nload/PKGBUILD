# Maintainer: Sébastien Luttringer <seblu@archlinux.org>
# Contributor (Arch): Andreas Hauser <andy-aur@splashground.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=nload
pkgver=0.7.4
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="Monitors network traffic and bandwidth usage"
url='http://www.roland-riegel.de/nload'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('gcc-libs' 'ncurses')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::http://deb.debian.org/debian/pool/main/n/nload/nload_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/n/nload/nload_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ca3e36d3f77c5a7994a3261042fb65a1458a29c769e6bebbe3b8e49b3045aca6f4d047065d11431b28368e0d881c24c92533fa24f0e54ac270f1f5bccfb5c0af'
            'a0f49ca6197215b7f5de83fb755b6f098d585713b16ffea3ca2ad193f23d5d0b86c34b405953769a2021cba725183bab02568ba1efddcd4a3ef1269f5d5d0d2a')

prepare() {
  cd $pkgname-$pkgver

  if [ ${pkgver%.*} = ${_debver%.*} ]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # make gzip reproducible by not embedding timestamps
  sed -i 's/gzip -f/gzip -nf'/ docs/Makefile.am docs/Makefile.in
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}