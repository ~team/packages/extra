# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=libx11
_openbsdver=6.9
pkgver=1.7.0
pkgrel=1
pkgdesc="X11 client-side library, provided by Xenocara"
arch=(i686 x86_64)
url="https://www.xenocara.org"
depends=('libxcb' 'xenocara-proto')
makedepends=('xenocara-util-macros' 'libxtrans')
license=('X11')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libx11-$pkgver.tar.lz{,.sig})
sha512sums=('aa4dd261ed67db481b67e02b0e2325713f701a6eee301356a8c90e609eb113b6c802adef301749f49681d2a95b09d6d419acad4911c8095a0e2011c7bff628d5'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libX11"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/libX11"
  ./configure --prefix=/usr --disable-static --disable-xf86bigfont
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/libX11"
  make check
}

package() {
  cd "xenocara-$_openbsdver/lib/libX11"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
