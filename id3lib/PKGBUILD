# Maintainer (Arch): Guillaume ALAUX <guillaume@archlinux.org>
# Contributor (Arch): Jochem Kossen <j.kossen@home.nl>
# Contributor (Arch): rtfreedman <rob.til.freedman@googlemail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=id3lib
pkgver=3.8.3
_debver=3.8.3
_debrel=16.3
pkgrel=17
pkgdesc='Library for reading, writing, and manipulating ID3v1 and ID3v2 tags'
arch=('i686' 'x86_64')
license=('LGPL-2')
url="http://id3lib.sourceforge.net/"
depends=('zlib' 'gcc-libs')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/sourceforge/${pkgname}/${pkgname}-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/i/id3lib3.8.3/id3lib3.8.3_$_debver-$_debrel.debian.tar.xz)
sha512sums=('3787e261f86933c1c2f2bff2c4b349b42f5d8636e489e4f39f9d75e6dfbdc79b87009a0f4ce4b786f2fb3dbc01ca9d56c4112095b46244f897e6c9a28573adaf'
            '7315b49881b40050d1c1c4a9a277270325ba0dbb82b230f82eebbceb1f10c25b8db0f3687014b3a0db18f69b0ed6bcb47e077793b0cafe0842c3b664d6baad45')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  libtoolize -fc
  aclocal
  autoconf
  automake --add-missing --copy
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  # fixing man-pages
  install -d "${pkgdir}"/usr/share/man/man1
  cp doc/man/* "${pkgdir}"/usr/share/man/man1

  # fixing license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
