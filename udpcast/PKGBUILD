# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=udpcast
pkgver=20120424
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="Network multicasting tool"
arch=('i686' 'x86_64')
url='http://udpcast.linux.lu/'
license=('Simplified-BSD')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/u/udpcast/udpcast_${pkgver}.orig.tar.gz"
	"https://deb.debian.org/debian/pool/main/u/udpcast/udpcast_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('b16a1d3215cbefba1e4d6bb0b38f4715e476c41ddc93fa1a38c5e707af786f4a97badd1eea494f7838f276f370af414d8379db04a14b9b3ff32a1318f16db33d'
            '61c494f30c6b85188fc57c9f77206186a298b7f275167a6e37ad86522dcb9bb6a1d664a133438efd6ad2c319194a3498d9a0fea797d9697beff5c5330e123e25')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure --prefix=/usr
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}