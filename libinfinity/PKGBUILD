# Maintainer (Arch): Baptiste Jonglez <arch at bitsofnetworks dot org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor (Arch): Gregory Haynes <greg@greghaynes.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libinfinity
pkgver=0.7.2
pkgrel=1
pkgdesc="A library to build collaborative text editors, including the infinoted server"
arch=('i686' 'x86_64')
url='https://gobby.github.io/'
license=('LGPL-2.1')
depends=('gnutls' 'gsasl' 'glib2' 'libxml2' 'gobject-introspection-runtime')
makedepends=('gettext-tiny' 'gobject-introspection' 'gtk' 'mesa-libgl')
optdepends=('gtk: gtk support')
source=("https://deb.debian.org/debian/pool/main/libi/libinfinity/libinfinity_${pkgver}.orig.tar.gz")
sha512sums=('02b76affc095546f858a315d9a6a485d4e893e18a897f84e5263f06fba9c9d66aef5840482820992f45acfc11a92a323721676f71aaed4cfcaade3099b67478f')

build() {
  cd "$srcdir"/${pkgname}-${pkgver}
  [ -x configure ] || ./autogen.sh
  ./configure \
    --prefix=/usr \
    --localstatedir=/var \
    --with-infgtk \
    --with-inftextgtk
  make
}

package() {
  cd "$srcdir"/${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install

  # remove unneeded parts
  rm -rf "$pkgdir"/var "$pkgdir"/usr/share/gtk-doc "$pkgdir"/usr/lib/infinoted-0.7/plugins/libinfinoted-plugin-dbus.{so,a}

  # license
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
