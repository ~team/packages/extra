# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Jan "heftig" Steffens <jan.steffens@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=clang
pkgver=11.0.0
pkgrel=3
pkgdesc="C language family frontend for LLVM"
arch=('i686' 'x86_64')
url="https://clang.llvm.org/"
license=('custom:Apache-2.0+LLVM-Exceptions')
depends=('llvm-libs' 'gcc' 'compiler-rt')
makedepends=('llvm' 'cmake' 'ninja' 'python-sphinx' 'tauthon')
optdepends=('python: for scan-view and git-clang-format'
            'llvm: referenced by some clang headers')
provides=("clang-analyzer=$pkgver" "clang-tools-extra=$pkgver")
conflicts=('clang-analyzer' 'clang-tools-extra')
replaces=('clang-analyzer' 'clang-tools-extra')
_source_base=https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver
source=($_source_base/$pkgname-$pkgver.src.tar.xz
        $_source_base/clang-tools-extra-$pkgver.src.tar.xz
        $_source_base/llvm-$pkgver.src.tar.xz
        enable-SSP-and-PIE-by-default.patch)
sha512sums=('5874d99d05aa6ac0a7f5131a8522440ca1fc332a63cbfbf92f844ecb03e7f698a1839106fe6d1c8efaa839d03a4547cda5ab40f445d9923b99907d8cf1988276'
            '48d3ae04556f2c3102462053e40e228ddfffb7d19f20552faa18acea763d8d568cf65232cbb4ae03505dcb8b626eb3516b5c5004f5ca1c556129510872e28fc1'
            'b3e92091ac48772edc0c30801218ce646ef374e1968baab91df9005f58e11c3ce149b2c4c655c7001f8554fd337caa02c08783bc736153bf58f35fe008e624a4'
            '6329b0b4544abbd3e6cafa703e6538d84fe758afc0b733a883c5fca262d1d7bc9ba71be5928705953da1fcef5c0a8b97cf28c198cc51bf89f1a911e0bf7cd1fa')

# Utilizing LLVM_DISTRIBUTION_COMPONENTS to avoid
# installing static libraries; inspired by Gentoo
_get_distribution_components() {
  local target
  ninja -t targets | grep -Po 'install-\K.*(?=-stripped:)' | while read -r target; do
    case $target in
      clang-libraries|distribution)
        continue
        ;;
      clang-tidy-headers)
        continue
        ;;
      clang|clangd|clang-*)
        ;;
      clang*|findAllSymbols)
        continue
        ;;
    esac
    echo $target
  done
}

prepare() {
  cd "$srcdir/$pkgname-$pkgver.src"
  mkdir build
  mv "$srcdir/clang-tools-extra-$pkgver.src" tools/extra
  patch -Np2 -i ../enable-SSP-and-PIE-by-default.patch
}

build() {
  cd "$srcdir/$pkgname-$pkgver.src/build"

  local cmake_args=(
    -G Ninja
    -DCMAKE_BUILD_TYPE=Release
    -DCMAKE_INSTALL_PREFIX=/usr
    -DPYTHON_EXECUTABLE=/usr/bin/python
    -DLLVM_LINK_LLVM_DYLIB=ON
    -DCLANG_LINK_CLANG_DYLIB=ON
    -DLLVM_ENABLE_RTTI=ON
    -DLLVM_BUILD_TESTS=ON
    -DLLVM_INCLUDE_DOCS=ON
    -DLLVM_BUILD_DOCS=ON
    -DLLVM_ENABLE_SPHINX=ON
    -DSPHINX_WARNINGS_AS_ERRORS=OFF
    -DLLVM_EXTERNAL_LIT=/usr/bin/lit
    -DLLVM_MAIN_SRC_DIR="$srcdir/llvm-$pkgver.src"
  )

  cmake .. "${cmake_args[@]}"
  local distribution_components=$(_get_distribution_components | paste -sd\;)
  test -n "$distribution_components"
  cmake_args+=(-DLLVM_DISTRIBUTION_COMPONENTS="$distribution_components")

  cmake .. "${cmake_args[@]}"
  ninja
}

_tauthon_optimize() {
  tauthon -m compileall "$@"
  tauthon -O -m compileall "$@"
}

_python_optimize() {
  python -m compileall "$@"
  python -O -m compileall "$@"
  python -OO -m compileall "$@"
}

package() {
  cd "$srcdir/$pkgname-$pkgver.src/build"

  DESTDIR="$pkgdir" ninja install-distribution
  install -Dm644 ../LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  # Remove documentation sources
  rm -r "$pkgdir"/usr/share/doc/clang{,-tools}/html/{_sources,.buildinfo}

  # Install Tauthon bindings
  install -d "$pkgdir/usr/lib/tauthon2.8/site-packages"
  cp -a ../bindings/python/clang "$pkgdir/usr/lib/tauthon2.8/site-packages/"
  _tauthon_optimize "$pkgdir/usr/lib/tauthon2.8"

  # Install Python bindings
  install -d "$pkgdir/usr/lib/python3.8/site-packages"
  cp -a ../bindings/python/clang "$pkgdir/usr/lib/python3.8/site-packages/"
  _python_optimize "$pkgdir/usr/lib/python3.8"

  # Fix shebang in Tauthon script
  sed -i '\|/usr/bin/env| s|python$|tauthon|' \
    "$pkgdir"/usr/share/$pkgname/run-find-all-symbols.py

  # Compile Tauthon scripts
  _tauthon_optimize "$pkgdir/usr/share/clang"

  # Compile Python scripts
  _python_optimize "$pkgdir/usr/share" -x 'clang-include-fixer|run-find-all-symbols'
}

# vim:set ts=2 sw=2 et:
