# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Arjan Timmerman <arjan.archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster.gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=imlib2
pkgver=1.7.1
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Library that does image file loading and saving as well as rendering, manipulation, arbitrary polygon support"
url='https://sourceforge.net/projects/enlightenment/'
arch=('i686' 'x86_64')
license=('Imlib2')
depends=('libtiff' 'giflib' 'bzip2' 'freetype2' 'libxext' 'libpng' 'libid3tag' 'libjpeg-turbo')
source=(https://downloads.sourceforge.net/enlightenment/$pkgname-$pkgver.tar.bz2
        https://deb.debian.org/debian/pool/main/i/imlib2/imlib2_$_debver-$_debrel.debian.tar.xz)
sha512sums=('36e457855da0f129c7e7ba3dfcb44162b6624b1d56df5f84448495a96fe9ad1ce3e6c628222630be7274476722c4feaa8e1648d1e463439ca758e66ce9645970'
            '0700a2a7cba25b3adaca975fb3b6e399f879b5bb4558334a52e0b443bafe003a2bd61c197b307edba6c03e33d8b1abd55b4245755b0d048e456296fba4448d13')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -i 's/@my_libs@//' imlib2-config.in
}

build() {
  cd "${pkgname}-${pkgver}"

  [ $CARCH = "i686" ] && EXTRAOPTS="--enable-mmx"
  [ $CARCH = "x86_64" ] && EXTRAOPTS="--enable-amd64"

  ./configure --prefix=/usr \
              --sysconfdir=/etc/imlib2 \
              --x-libraries=/usr/lib $EXTRAOPTS
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # Install License
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
