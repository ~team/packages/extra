# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Hugo Doria <hugo@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: rachad
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=nmap-legacy
_pkgname=nmap
pkgver=7.80
_debver=7.91
_debrel=2
pkgrel=5
pkgdesc="Utility for network discovery and security auditing (legacy version)"
url='https://nmap.org/'
arch=('i686' 'x86_64')
license=('custom:BSD-like-SVN' 'GPL-2' 'Modified-BSD' 'Original-BSD' 'Expat' 'ISC')
replaces=('nmap')
conflicts=('nmap')
provides=('nmap')
depends=('glibc' 'pcre' 'libressl' 'lua' 'libpcap' 'libssh2' 'zlib' 'gcc-libs' 'python-lxml')
makedepends=('quilt')
source=("https://nmap.org/dist/${_pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/n/nmap/nmap_${_debver}+dfsg1+really7.80+dfsg1-${_debrel}.debian.tar.xz")
sha512sums=('d4384d3ebf4f3abf3588eed5433f733874ecdceb9342a718dc36db19634b0cc819d73399974eb0a9a9c9dd9e5c88473e07644ec91db28b0c072552b54430be6b'
            '943f703b6758acd5ef20243569add268b980d2967f5846464cce0c34c6443790ad66945cfdae8f71bb553a84920cf14012c68884a2c5001bbc7ec88fbca181aa')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  # Debian patches
  export QUILT_PATCHES=debian/patches
  export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
  export QUILT_DIFF_ARGS='--no-timestamps'

  mv "$srcdir"/debian .

  # Doesn't apply
  rm -v debian/patches/0003-Link-against-lua-lpeg.patch || true

  quilt push -av
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  export PYTHON=python
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --with-libpcap=/usr \
    --with-libpcre=/usr \
    --with-zlib=/usr \
    --with-libssh2=/usr \
    --with-liblua=/usr \
    --with-liblinear \
    --without-ndiff \
    --without-zenmap \
    --enable-ipv6
  make -j1
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # adding ndiff
  cd ndiff
  python setup.py install --root="${pkgdir}"
  cd ..

  # cleanup package from unwanted data
  rm "${pkgdir}/usr/share/nmap/nselib/jdwp.lua"
  rm "${pkgdir}/usr/share/nmap/scripts/"jdwp-{exec,info,inject,version}.nse
  rm -rf "${pkgdir}/usr/share/nmap/nselib/data/jdwp-class"
  rm "${pkgdir}/usr/bin/uninstall_ndiff"

  # further documentation and licenses
  install -Dm644 README.md docs/nmap.usage.txt -t "${pkgdir}/usr/share/doc/${_pkgname}"
  install -Dm644 COPYING docs/3rd-party-licenses.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
