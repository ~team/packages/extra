# Maintainer (Arch): dracorp aka Piotr Rogoza <piotr.r.public at gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgname=automake
pkgname=automake-1.15
pkgver=1.15.1
_debver=1.15.1
_debrel=5
pkgrel=1
pkgdesc="A GNU tool for automatically creating Makefiles"
arch=('any')
license=('GPL-2')
url="https://www.gnu.org/software/automake"
depends=('perl' 'bash')
makedepends=('autoconf' 'quilt')
provides=("automake=1.15")
source=("https://ftp.gnu.org/gnu/${_pkgname}/${_pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/a/automake-1.15/automake-1.15_$_debver-$_debrel.debian.tar.xz")
sha512sums=('02f661b2676f1d44334ce1c7188f9913a6874bf46ba487708ad8090ad57905f14aead80fefed815e21effacfbb925e23b944ea7dd32563dca39c1a4174eda688'
            'SKIP'
            'c8591dba2b51aaa4895786ea566af2dba43d7ff72ff9d8697b63df5db221cd49fc06f5f12277558b9e838e8a63ccf382d3bf1b227e22b2e9b1959885cfdb8006')
validpgpkeys=('E1622F96D2BB4E58018EEF9860F906016E407573'   # Stefano Lattarini
              'F2A38D7EEB2B66405761070D0ADEE10094604D37'   # Mathieu Lirzin
              '155D3FC500C834486D1EEA677FD9FCCB000BEEEE')  # Jim Meyering

prepare() {
	cd "${srcdir}/${_pkgname}-${pkgver}"

	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		# Doesn't apply and seems unimportant
		rm -v debian/patches/0001-texi-rename.patch || true
		rm -v debian/patches/0004-Use-system-help2man-if-it-is-available.patch || true
		rm -v debian/patches/0005-Disable-t-check12.sh-and-t-check12-w.sh.patch || true

		quilt push -av
	fi
}

build() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	./configure --build=$CHOST --prefix=/usr
	make
}

package() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
	make DESTDIR="${pkgdir}" install
	install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"

	rm -f "${pkgdir}"/usr/bin/{automake,aclocal}
	rm -rf "${pkgdir}"/usr/share/aclocal
	rm -fv "${pkgdir}"/usr/share/man/man1/{automake,aclocal}.1*

	rm -rf "${pkgdir}"/usr/share/info
	rm -rf "${pkgdir}"/usr/share/doc
}
