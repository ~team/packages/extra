# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=macchanger
pkgver=1.7.0
_debver=1.7.0
_debrel=5.4
pkgrel=1
pkgdesc="A small utility to change your NIC's MAC address"
arch=('i686' 'x86_64')
url="https://github.com/alobbs/macchanger"
license=('GPL-3')
depends=('glibc')
makedepends=('quilt')
source=("https://github.com/alobbs/macchanger/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/macchanger/macchanger_$_debver-$_debrel.debian.tar.xz")
sha512sums=('55fbfe4f4e94f0822e7a71b8c7052ed6c3a05066859044498bd68d068c33902b5718e92c8e00b73af71b57ce825644e1f81eb4dd7ef11701f197b953d7853aff'
            'b50b52532fc987f443088f2a4d0f2327dac972e69bc4c0bc0a94de4583df76c7d32c40a143d24b7fa6e066fc36b89c4b160c02b26860c484aab5054a4032f0cc')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  ./autogen.sh
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr \
              --bindir=/sbin \
              --mandir=/usr/share/man \
              --infodir=/usr/share/info
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}