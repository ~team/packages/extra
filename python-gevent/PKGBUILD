# Maintainer (Arch): Massimiliano Torromeo <massimiliano.torromeo@gmail.com>
# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Ralf Schmitt <ralf@systemexit.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgbase=python-gevent
pkgname=('tauthon-gevent' 'python-gevent')
pkgver=20.9.0
_debver=$pkgver
_debrel=2
pkgrel=3
arch=('i686' 'x86_64')
license=('Apache-2.0' 'Expat' 'Modified-BSD' 'GPL-3')
url='https://www.gevent.org/'
makedepends=('python-cython' 'c-ares' 'libev' 'python-greenlet' 'python-setuptools'
             'python-zope-event' 'python-zope-interface' 'tauthon-greenlet' 'tauthon-setuptools'
             'tauthon-zope-event' 'tauthon-zope-interface' 'tauthon-cython' 'quilt')
source=("${pkgbase}-${pkgver}.tar.gz::https://github.com/gevent/gevent/archive/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/$pkgbase/${pkgbase}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('3a94cf3a3606cbf7f38930ec03112bcce430334bcea47c1cfb565715111f3ef4d9ca52d668eacb6ea99c911fc6f70cbb8ff8f313f4bf13e66b733385f675344f'
            '173af612ebc0d6b83baa270fef3782931d78d94e95116ca500908af1ead8f6023c1c7d3b4c088bea4e63aef70d994421985043d99fc50ae4096c4fe1e48736fa')

export EMBED=0

prepare() {
  cd "$srcdir/gevent-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cd "$srcdir"
  cp -rf "gevent-$pkgver" "gevent-$pkgver-tauthon"
}

build() {
  cd "$srcdir/gevent-$pkgver"

  CYTHON=cython \
  PYTHON=python \
  python setup.py build

  cd "$srcdir/gevent-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-gevent() {
  pkgdesc="Python network library that uses greenlet and libev for easy and scalable concurrency"
  depends=('c-ares' 'libev' 'python-greenlet' 'python-setuptools' 'python-zope-event' 'python-zope-interface')
  cd "$srcdir/gevent-$pkgver"
  python setup.py install -O1 --root="$pkgdir"
  install -Dm644 debian/copyright LICENSE -t $pkgdir/usr/share/licenses/$pkgname/
}

package_tauthon-gevent() {
  pkgdesc="Coroutine-based network library for Tauthon"
  depends=('tauthon-greenlet' 'tauthon-setuptools' 'tauthon-zope-event' 'tauthon-zope-interface')
  cd "$srcdir/gevent-$pkgver-tauthon"
  tauthon setup.py install -O1 --root="$pkgdir"
  install -Dm644 debian/copyright LICENSE -t $pkgdir/usr/share/licenses/$pkgname/
}
