# Maintainer (Arch): Andrew Crerar <crerar@archlinux.org>
# Maintainer (Arch): Morten Linderud <foxboron@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Contributor (Arch): Douglas Soares de Andrade <dsa@aur.archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-psycopg2
pkgname=('python-psycopg2' 'tauthon-psycopg2')
pkgver=2.8.6
pkgrel=1
arch=('i686' 'x86_64')
url="http://initd.org/psycopg/"
license=('LGPL-3')
makedepends=('tauthon' 'tauthon-setuptools'
             'python' 'python-setuptools' 'postgresql-libs')
source=(http://initd.org/psycopg/tarballs/PSYCOPG-2-8/psycopg2-$pkgver.tar.gz{,.asc})
sha512sums=('1e1d5d8755c6d1a153d84210bf29902afafe853659d709e13abc6bc5772def13779d2394690af1c544384c9c607edc0fe5cf2763244fb346febf9a9e0032b45f'
            'SKIP')
validpgpkeys=('8AD609956CF1899418E19A856013BD3AFCF957DE')

build(){
  cd "$srcdir/psycopg2-$pkgver"
  sed -i 's/,PSYCOPG_DEBUG$//' setup.cfg
  python setup.py build
  tauthon setup.py build
}

package_python-psycopg2() {
  pkgdesc="A PostgreSQL database adapter for Python."
  depends=('python' 'postgresql-libs')

  cd "$srcdir/psycopg2-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -Dm644 doc/COPYING.LESSER -t "$pkgdir/usr/share/licenses/$pkgname/"
}

package_tauthon-psycopg2() {
  pkgdesc="A PostgreSQL database adapter for Tauthon."
  depends=('tauthon' 'postgresql-libs')

  cd "$srcdir/psycopg2-$pkgver"
  tauthon setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -Dm644 doc/COPYING.LESSER -t "$pkgdir/usr/share/licenses/$pkgname/"
}
