# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-ati package

pkgname=xenocara-video-ati
_openbsdver=6.9
pkgver=19.1.0
pkgrel=7
pkgdesc="Xenocara ATI video driver for Mach, Rage and Radeon R100/R200/R300/R400/R500 series"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('X11')
depends=('libeudev' 'mesa')
makedepends=('xenocara-server-devel' 'eudev' 'X-ABI-VIDEODRV_VERSION=24.0' 'xenocara-util-macros')
provides=('xf86-video-ati')
conflicts=('xf86-video-ati' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25')
replaces=('xf86-video-ati')
groups=('xenocara-video-drivers' 'xenocara-video' 'xenocara-drivers'
        'xorg-video-drivers' 'xorg-video' 'xorg-drivers')
install=$pkgname.install
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-ati-$pkgver.tar.lz{,.sig})
sha512sums=('941f6bd3931378b95df83e6bbfd7a1418ec2f84fae7ea111351c80c7a5b2e1b0932aba43c5411c564018d1db6c5480d4bad5a0d8d362f41f10d01e63638d212c'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-ati"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-ati"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}

  ./configure --prefix=/usr
  make
}

check() {
  cd "xenocara-$_openbsdver/driver/xf86-video-ati"
  make check
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-ati"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
