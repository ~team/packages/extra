# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Dave Reisner <dreisner@archlinux.org>
# Contributor (Arch): Matthias Blaicher <matthias@blaicher.com>
# Contributor (Arch): Severen Redwood <severen@shrike.me>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=capnproto
pkgver=0.7.0
_debver=$pkgver
_debrel=7
pkgrel=1
pkgdesc="Cap'n Proto serialization/RPC system"
arch=('i686' 'x86_64')
url='https://capnproto.org/'
license=('Expat')
depends=('gcc-libs' 'glibc' 'zlib')
makedepends=('cmake' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/c/capnproto/capnproto_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/capnproto/capnproto_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('9f8fb5753155798fcf9377a87f984a54d9fc5157c41aa11cd94108a773ca22d6e6952657e2d8079c9806f7de06f316c94957329fa52dbab6207aaa3b52348f04'
            'a9b09b80cb02816d50de1ea5cd0d5a6999a12747fa92052b858400eda625318615c3e7c30bd9d502f9c0c858133a18f9109a1bfd3f3e5fdd8ac6aadeb5856e87')

prepare() {
  mv ${pkgname}-c++-${pkgver} ${pkgname}-${pkgver}
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/01_couldnt.patch || true

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  cmake -DCMAKE_INSTALL_PREFIX=/usr \
        -DBUILD_SHARED_LIBS=ON \
        -B build \
        -S .
  make VERBOSE=1 -C build
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install -C build
  install -vDm 644 LICENSE.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -vDm 644 README.txt -t "${pkgdir}/usr/share/doc/${pkgname}"
}
