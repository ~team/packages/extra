# Maintainer (Arch): Andreas Radke <andyrtr at archlinux.org>
# Contributor (Arch): Thomas Baechler <thomas.baechler@rwth-aachen.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=cups-pdf
pkgver=3.0.1
_debver=3.0.1
_debrel=13
pkgrel=1
pkgdesc="PDF printer for cups"
arch=(i686 x86_64)
depends=('cups' 'ghostscript')
makedepends=('quilt')
install=cups-pdf.install
url="https://www.cups-pdf.de/welcome.shtml"
license=('GPL-2')
backup=(etc/cups/cups-pdf.conf)
source=(https://www.cups-pdf.de/src/cups-pdf_$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/c/cups-pdf/cups-pdf_$_debver-$_debrel.debian.tar.xz)
sha512sums=('a06580f3056dddbb883c8db0c71b37b883482ae1ac8aa9bcfd85e679c4dcb44278006b7110d2988173d0eeb05b0f19fb39081fad49bf26839235e97fbe27a818'
            '58166b1367445e0bc615f8fae15b04fb5139796159819b47bde8c517f180713ca94e3e424221d460c4e741fa8aeafddbfa6bdd008a432039c77a585350be3cad')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .
    # Doesn't apply and seems unimportant
    #rm -v debian/patches/01_debian_changes_to_cups-pdf_conf.patch || true

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"/src
  [ -z "$CC" ] && CC=gcc
  $CC $CFLAGS $LDFLAGS -s cups-pdf.c -o cups-pdf -lcups 
}

package() {
  cd "$pkgname-$pkgver"/src
  install -D -m700 cups-pdf "$pkgdir"/usr/lib/cups/backend/cups-pdf

  # Install Postscript Color printer
  cd ../extra
  install -D -m644 CUPS-PDF_noopt.ppd "$pkgdir"/usr/share/cups/model/CUPS-PDF_noopt.ppd
  install -D -m644 CUPS-PDF_opt.ppd "$pkgdir"/usr/share/cups/model/CUPS-PDF_opt.ppd

  # Install config file
  install -D -m644 cups-pdf.conf "$pkgdir"/etc/cups/cups-pdf.conf

  # Install docs
  install -D -m 644 ../README "$pkgdir"/usr/share/doc/$pkgname/README
  
  # use cups group from cups pkg FS#57311/FS#56818/FS#57313
  # chgrp -R cups "${pkgdir}"/etc/cups #not working to be checked later
  sed -i "s:### Default\: lp:### Default\: cups:" "${pkgdir}"/etc/cups/cups-pdf.conf
  sed -i "s:#Grp lp:Grp cups:" "${pkgdir}"/etc/cups/cups-pdf.conf

  # Install license file
  install -D -m644 ../COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}
