# Maintainer (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Contributor (Arch): Paulius Palevicius <paulius@birzai.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=catdoc
pkgver=0.95
_debver=0.95
_debrel=5
pkgrel=2
pkgdesc="Extractor for Office- and RTF-files to text"
arch=('i686' 'x86_64')
url='https://www.wagner.pp.ru/~vitus/software/catdoc/'
license=('GPL-2')
depends=('glibc')
makedepends=('quilt')
optdepends=('tk: for using wordview')
options=('!makeflags')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/c/catdoc/catdoc_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/catdoc/catdoc_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('7567344662f402fae483e0ef99ffe0f6860a8d80fd3c79651b59be7298907781c3d8b09eddbf6840387ea9f866da0eba301227bfc0726ec4739000f4493e375a'
            'e267ef878b134bce22192829d996d1fe733c400a0a4bd43a548772ca8e48f9f18474b37321c0f8c5b5a89d253600f5f6cf5b6d610db4c9ec5073356cba53a23c')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure --prefix=/usr  --with-wish=/usr/bin/wish
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make installroot="$pkgdir" mandir=/usr/share/man/man1 install
  install -d "$pkgdir/usr/share/man/man1"
  install -m644 doc/*.1 "$pkgdir/usr/share/man/man1"
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
