#!/sbin/openrc-run
# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

name="unbound daemon"
extra_commands="checkconfig"
extra_started_commands="reload"
description="unbound is a Domain Name Server (DNS) that is used to resolve host names to IP address."
description_configtest="Run syntax tests for configuration files only."
description_reload="Kills all children and reloads the configuration."

UNBOUND_BINARY=${UNBOUND_BINARY:-/usr/sbin/unbound}
UNBOUND_CHECKCONF=${UNBOUND_CHECKCONF:-/usr/sbin/unbound-checkconf}
UNBOUND_CONFFILE=${UNBOUND_CONFFILE:-/etc/unbound/$RC_SVCNAME.conf}
UNBOUND_PIDFILE=${UNBOUND_PIDFILE:-/run/$RC_SVCNAME.pid}

depend() {
    need net
    use logger
    provide dns
    after auth-dns
}

checkconfig() {
    ebegin "Checking ${UNBOUND_CONFFILE}"
    /usr/sbin/unbound-checkconf -f "${UNBOUND_CONFFILE}" >/dev/null
    eend $?
}

start_pre() {
    checkconfig
}

start() {
    ebegin "Starting ${RC_SVCNAME}"
    start-stop-daemon --start --background --exec "${UNBOUND_BINARY}" --pidfile "${UNBOUND_PIDFILE}" \
                      -- -c "${UNBOUND_CONFFILE}"
    eend $?
}

stop() {
    start_pre || return $?
    ebegin "Stopping ${RC_SVCNAME}"
    start-stop-daemon --stop --pidfile "${UNBOUND_PIDFILE}"
    eend $?
}

reload() {
    start_pre || return $?
    ebegin "Reloading ${RC_SVCNAME}"
    start-stop-daemon --signal HUP --pidfile "${UNBOUND_PIDFILE}"
    eend $?
}
