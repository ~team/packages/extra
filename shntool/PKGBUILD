# Maintainer (Arch): schuay <jakob.gruber@gmail.com>
# Contributor (Arch): Michal Hybner <dta081@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgname=shntool-libre
pkgname=shntool
pkgver=3.0.10
_debver=3.0.10
_debrel=1
pkgrel=1
pkgdesc="A multi-purpose WAVE data processing and reporting utility"
arch=('i686' 'x86_64')
url="http://shnutils.freeshell.org/shntool/"
license=('GPL-2')
options=(!emptydirs)
depends=('glibc')
makedepends=('quilt')
optdepends=('flac: support for flac format'
            'wavpack: support for wv format')
mksource=("https://deb.debian.org/debian/pool/main/s/shntool/shntool_${pkgver}.orig.tar.gz")
source=("https://repo.hyperbola.info:50000/sources/${_pkgname}/${_pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/s/shntool/shntool_3.0.10-1.debian.tar.xz"
        "libre.patch")
mksha512sums=('2150d7123860abb54a56a1615bda991ed3713d73c338723f28b7d01a63c49a47809be16dc57b5b4edeee1567b003f9a4b54945c1cd08440f9503d22b91eaa06d')
sha512sums=('dc0a8593fb570679f00fa77db8ac7e0846492995a8a1e7826b3481688c4c7ccbb33cf00c9f486ca6cff4b17c07898db768ebdda791348e5f95cfe351ae30d1e2'
            'SKIP'
            '4b30c683dbe2ca202bf6673889052710e33ca5c5a74b1365cc5063f409641254021f55d55663aa15f71857c02173909ce9c60966cc696d87496db1dbd5634313'
            'e856faa83658c80199e702949211a8e2b8d1c15ea4d20ef69b13df8ea5994f44cbcbac2425a6641608c1d02e5039ba2837624bcab270cfe23c28af37de13f971')
validpgpkeys=('684D54A189305A9CC95446D36B888913DDB59515') # Márcio Silva

mksource() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  # remove unfree codecs for APE, OFR and LPAC
  rm "${srcdir}/${pkgname}-${pkgver}/src/"{format_ape.c,format_ofr.c,format_lpac.c}
  rm "${srcdir}/${pkgname}-${pkgver}/"configure
}

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # patch for libre version without unfree formats and codecs
  patch -Np1 -i "${srcdir}/libre.patch"
  autoreconf -vfi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
