# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-ati package

pkgname=xenocara-video-cirrus
_openbsdver=6.9
pkgver=1.5.3
pkgrel=2
pkgdesc="Xenocara Cirrus Logic video driver"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('X11')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'xenocara-util-macros')
provides=('xf86-video-cirrus')
conflicts=('xf86-video-cirrus' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25')
replaces=('xf86-video-cirrus')
groups=('xenocara-video-drivers' 'xenocara-video' 'xenocara-drivers'
        'xorg-video-drivers' 'xorg-video' 'xorg-drivers')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-cirrus-$pkgver.tar.lz{,.sig})
sha512sums=('57b884b86866bf2cc1991cd5f5f5d5d810e34a0ba56eb324266f8d1466f9b2b99bb8ece5c8ccde4b7fe20c65e3f0f1c9eb09231ac1847c731b0fd4390ab65cc8'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-cirrus"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-cirrus"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}

  ./configure --prefix=/usr
  make
}

check() {
  cd "xenocara-$_openbsdver/driver/xf86-video-cirrus"
  make check
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-cirrus"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
