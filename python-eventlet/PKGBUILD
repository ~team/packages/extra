# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=python-eventlet
_pyname=eventlet
pkgver=0.26.1
_debver=$pkgver
_debrel=7
pkgrel=3
pkgdesc="Highly concurrent networking library"
url='https://eventlet.net'
arch=('any')
license=('Expat')
depends=('python' 'python-greenlet' 'python-monotonic' 'python-dnspython')
makedepends=('python-setuptools' 'python-sphinx' 'quilt')
optdepends=('python-psycopg2: non-blocking PostgreSQL support'
            'python-pyopenssl: non-blocking SSL support'
            'python-httplib2: non-blocking HTTP support'
            'python-dnspython: non-blocking DNS support')
options=('!makeflags')
source=("https://github.com/eventlet/eventlet/archive/v${pkgver}/${_pyname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-eventlet/python-eventlet_${_debver}-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('78dddbbc51827bebc331afa5eaade27a65e1f95b0a68ec57c4ad4f83ab7c90bffea4bd866718f3d0bef45794ccae1883fc046b4c92f05cb320e3ffb172e0d9a1'
            '3958c5379a507d102f7c05e34ef2b9410248c1f6ff3e2b90549cbe700d83f2b802617de5cc390c319115e1023e67c9e5424107f931023671f6f640a27f45d31b')

prepare() {
  cd ${_pyname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -r 's|(check_idle_cpu_usage\(.*,) .*\)|\1 0.8\)|g' -i ./tests/*_test.py
}

build() {
  cd ${_pyname}-${pkgver}
  python setup.py build
  make -C doc text
}

package() {
  cd ${_pyname}-${pkgver}
  python setup.py install --prefix=/usr --root="${pkgdir}" -O1
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r doc/_build/text "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r examples "${pkgdir}/usr/share/doc/${pkgname}"
}
