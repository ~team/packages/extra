# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): svoufff <svoufff at gmail dot com>
# Contributor (Arch): Shinlun Hsieh <yngwiexx@yahoo.com.tw>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: rachad

pkgname=libgig
pkgver=4.2.0
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="C++ library for loading, modifying and creating GIG, KSF, KMP, SF2, and DLS files"
arch=('i686' 'x86_64')
url='https://www.linuxsampler.org/libgig/'
license=('GPL-2')
depends=('util-linux' 'glibc' 'libsndfile')
makedepends=('quilt')
source=("https://download.linuxsampler.org/packages/${pkgname}-${pkgver}.tar.bz2"
	"https://deb.debian.org/debian/pool/main/libg/${pkgname}/${pkgname}_${_debver}~ds1-${_debrel}.debian.tar.xz")
sha512sums=('52c8c749c7852e229c76c17211398dbf5dd5891a1ee9d8e8bc61d55b825c5676595b20b26014e739b9bfd387db36895152b380519609a928a8fa68f4a20279f6'
	    '423c5446383b13989c644aa235d0b58dd6fb1079ff9d16bba22f9730e5d91e985b17be1db76fdab736a48b79de691ae98638a16fbb93f26882b2f9da17973891')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv ${srcdir}/debian .

    # Doesn't apply
    rm -v debian/patches/01-Makefile.patch || true

    quilt push -av
  fi

  sed -ie 's#\sdebian\b##' Makefile.am
  sed -ie 'N;s#^\s*debian/Makefile \\\n##' configure{,.ac}
  autoreconf
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}/
  make DESTDIR=${pkgdir} install
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
