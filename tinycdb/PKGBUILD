# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=tinycdb
pkgver=0.78
pkgrel=1
pkgdesc='Very fast and simple package for creating and reading constant data bases'
arch=('i686' 'x86_64')
url='https://www.corpit.ru/mjt/tinycdb.html'
license=('Public-Domain')
depends=('glibc')
options=(!staticlibs)
source=("https://www.corpit.ru/mjt/tinycdb/${pkgname}-${pkgver}.tar.gz")
sha512sums=('8930086b8e7fddcd4dbd3354c5f5ee05171df68fde1cc222b6c402430042b6e761efbad7e5fa8de18e1d36390f1526cc3e605c5086fe1c363ba1df6c03201553')

build() {
  cd "${pkgname}-${pkgver}"
  make CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}" all shared
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" \
       prefix=/usr \
       mandir=/usr/share/man \
       INSTALLPROG=cdb-shared \
       install-{all,sharedlib}
  install -vDm 644 debian/copyright -t "${pkgdir}/usr/share/licenses/${pkgname}/"
  install -vDm 644 {NEWS,ChangeLog} -t "${pkgdir}/usr/share/doc/${pkgname}/"
}

# vim:set ts=2 sw=2 et:
