# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=gdk-pixbuf2
pkgver=2.42.2
_debver=2.42.2+dfsg
_debrel=1
pkgrel=2
pkgdesc="An image loading library"
arch=(i686 x86_64)
url="https://wiki.gnome.org/Projects/GdkPixbuf"
license=(LGPL-2.1)
depends=(glib2 libpng libtiff libjpeg-turbo libx11 shared-mime-info)
makedepends=(gobject-introspection meson quilt)
install=gdk-pixbuf2.install
source=(https://download.gnome.org/sources/gdk-pixbuf/${pkgver:0:4}/gdk-pixbuf-$pkgver.tar.xz
        https://deb.debian.org/debian/pool/main/g/gdk-pixbuf/gdk-pixbuf_$_debver-$_debrel.debian.tar.xz
        gdk-pixbuf-query-loaders.hook)
sha512sums=('f341d032ea410efed7a35f8ca6a7389bf988f663dae16e774d114d6f11611e9e182c835e90d752b71c258c905cc5c4c785ea697feed5e6921a2a676c9deaa5f2'
            '4e80f09cd7936a65fd734251d98c54756e6b1fcd13b6836938922a92e95e17486b9d6dfdd63e77b5a488a6681427e53b94495e86b25d4881ba8d156bef44b338'
            'a3d558fb92a0802307427f5c936ca4b86dd966acb96978f247f64cac54b434657cc9b952cece266b992d22655bb658129025718b29beb380a4a0429c34fdd417')

prepare() {
  cd gdk-pixbuf-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/tests-Don-t-check-whether-bmp-and-gif-loaders-are-enabled.patch || true

    quilt push -av
  fi
}

build() {
  hyperbola-meson gdk-pixbuf-$pkgver build \
    -D docs=false \
    -D installed_tests=false
  ninja -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  install -Dt "$pkgdir/usr/share/libalpm/hooks" -m644 *.hook
  cd gdk-pixbuf-$pkgver
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 COPYING
}

# vim:set ts=2 sw=2 et:
