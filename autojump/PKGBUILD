# Maintainer (Arch): graysky <therealgraysky AT protonmail DOT com>
# Contributor (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Geoffroy Carrier <geoffroy@archlinux.org>
# Contributor (Arch): Joël Schaerer <joel.schaerer@laposte.net>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=autojump
pkgver=22.5.1
_debver=$pkgver
_debrel=1.1
pkgrel=1
pkgdesc="A faster way to navigate your filesystem from the command line"
arch=('any')
url='https://github.com/wting/autojump'
license=('GPL-3')
depends=('python>=3.3')
makedepends=('quilt')
install=$pkgname.install
source=("$pkgname-$pkgver.tar.gz::https://github.com/wting/$pkgname/archive/release-v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/a/autojump/autojump_22.5.1-1.1.debian.tar.xz")
sha512sums=('ed1bb28b62d14a481d5c8ee8ebbfc286d21ca54c961904f81ac123c784e17810217dfe1689bdd4b50fd7a9d72509c0cb13583a9817074f6afe26dc3144f64243'
            '257c029653cf2143c7b537b84a17f49e507b659329d9a1352b145e89679814575f833de809a0f6f1c4f60f6921b3990a00acafe67768a1c74c3924d185eea74a')

prepare() {
  cd $pkgname-release-v$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package() {
  cd $pkgname-release-v$pkgver

  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"

  # https://wiki.archlinux.org/index.php/Python_package_guidelines#Using_site-packages
  local _site_packages=$(python -c "import site; print(site.getsitepackages()[0])")

  SHELL=/bin/bash ./install.py --destdir "${pkgdir}" \
    --prefix 'usr/' \
    --zshshare 'usr/share/zsh/site-functions'

  cd "${pkgdir}/usr/share/$pkgname"
  for i in $pkgname.*
    do ln -s "/usr/share/$pkgname/$i" "${pkgdir}/etc/profile.d/$i"
  done

  install -d "${pkgdir}/$_site_packages"
  mv ${pkgdir}/usr/bin/*.py "${pkgdir}/$_site_packages"
  python -m compileall -d /usr/lib "${pkgdir}/usr/lib"
  python -O -m compileall -d /usr/lib "${pkgdir}/usr/lib"

  install -d "${pkgdir}"/usr/share/fish/functions
  mv "${pkgdir}"/etc/profile.d/$pkgname.fish "${pkgdir}"/usr/share/fish/functions

  # https://github.com/joelthelion/autojump/pull/339
  sed -i "s:/usr/local/:/usr/:g" "${pkgdir}/etc/profile.d/$pkgname.sh"
  sed -i "s:/build/autojump/pkg/autojump/:/:g" "${pkgdir}/etc/profile.d/$pkgname.sh"

  sed -i '27,31d' "${pkgdir}/etc/profile.d/$pkgname.sh"
}
