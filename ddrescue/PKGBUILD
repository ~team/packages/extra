# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): Pierre Schmitz <pierre@archlinux.de>
# Contributor (Arch): Paul Mattal <paul@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname='ddrescue'
pkgver='1.26'
pkgrel='2'
pkgdesc='GNU data recovery tool'
arch=('i686' 'x86_64')
url='https://www.gnu.org/software/ddrescue/ddrescue.html'
license=('GPL-2')
depends=('gcc-libs')
optdepends=('man-db: manual pages support'
            'texinfo: software information support')
source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.lz"
        "https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.lz.sig")
sha512sums=('bbe414563d459a1ad1c4e55848214d195e01f8bc455104eddbeff7046623f5bc21d961457d27ace2605d13dff68c7948182706b8afe6755e5c363e8cad86a1cc'
            'SKIP')
validpgpkeys=('1D41C14B272A2219A739FA4F8FE99503132D7742') # Antonio Diaz Diaz

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  # fake configure script reinvents autotools and requires custom parameters to
  # set CPPFLAGS, CXXFLAGS and LDFLAGS
  ./configure --prefix='/usr' \
    CPPFLAGS="${CPPFLAGS}" CXXFLAGS="${CXXFLAGS}" LDFLAGS="${LDFLAGS}"
  make V='1'
}

package() {
  make DESTDIR="${pkgdir}" 'install' 'install-man' -C \
    "${srcdir}/${pkgname}-${pkgver}"

  for i in 'AUTHORS' 'ChangeLog' 'NEWS' 'README'; do
    install -Dm '644' "${srcdir}/${pkgname}-${pkgver}/${i}" -t \
      "${pkgdir}/usr/share/doc/${pkgname}"
  done
  unset i

  install -Dm '644' "${srcdir}/${pkgname}-${pkgver}/COPYING" -t \
    "${pkgdir}/usr/share/licenses/${pkgname}"
}
