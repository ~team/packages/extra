# Maintainer (Arch): Deposite Pirate <dpirate at metalpunks dot info>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=lbreakouthd
pkgver=1.1.9
pkgrel=1
pkgdesc="Scalable 16:9 remake of LBreakout2"
arch=('i686' 'x86_64')
url='https://lgames.sourceforge.io/LBreakoutHD/'
license=('GPL-3')
depends=('sdl2' 'sdl2_ttf' 'sdl2_image' 'sdl2_mixer')
makedepends=('automake')
groups=('games')
source=("https://downloads.sourceforge.net/project/lgames/${pkgname}/${pkgname}-${pkgver}.tar.gz")
sha512sums=('c3a51cd0c57c87133423912de2b4e9411bc8eff5220288d484851db30299cdfec8c6b044b90702896bbd374678da1cf6e663ab9a75555ff58323149b55169ec3')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  cp $(ls -td /usr/share/automake-* | head -n1)/config.guess .
  sed -i 's|-Wno-format||g' configure
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  CFLAGS="${CFLAGS}" ./configure \
    --prefix=/usr \
    --bindir=/usr/games \
    --datadir=/usr/share/games \
    --localstatedir=/var/games/lbreakouthd
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install
  mv "${pkgdir}/usr/share/games/"{applications,icons} "${pkgdir}/usr/share"

  chown root:games "${pkgdir}"/var/games
  chmod 775 "${pkgdir}"/var/games
  chown root:games "${pkgdir}"/var/games/lbreakouthd
  chmod 755 "${pkgdir}"/var/games/lbreakouthd
  chown root:games "${pkgdir}"/var/games/lbreakouthd/lbreakouthd.hscr
  chmod 775 "${pkgdir}"/var/games/lbreakouthd/lbreakouthd.hscr

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}