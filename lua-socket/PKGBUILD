# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Anders Bergh <anders1@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=luasocket
pkgname=('lua-socket' 'lua51-socket' 'lua52-socket')
pkgver=3.0~rc1+git+ac3201d
_debver=3.0~rc1+git+ac3201d
_debrel=4
pkgrel=1
pkgdesc='Networking support library for the Lua language'
arch=('i686' 'x86_64')
url='https://github.com/diegonehab/luasocket'
license=('Expat')
makedepends=('lua' 'lua51' 'lua52' 'quilt')
source=("https://deb.debian.org/debian/pool/main/l/luasocket/luasocket_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/l/luasocket/luasocket_${_debver}-${_debrel}.debian.tar.xz"
        "fix-library.patch")
sha512sums=('c34848f35c605cea700d67f2ebc4447deb58262090891651ff5793db6278152d6b902d6c1860f747d6380d3ecf365f1df6809f6b0e364257b93decbd3533462c'
            'a5c78dd5189086b431bd13f04980fd9e5c39524455b84817c5193a8cc8af0352b468a70912cbfe8998b7b6a8756056675b2fd262a001dcf2443d23a033370b0f'
            'b9bc7860dccaf6a5baf47e7b35a15071649c0b103f39ca83664631af4c811e5d381c02cfb3284c1c5619ba420126724609a4317161855381d3d969790bbdf0de')

prepare() {
  mv ${pkgbase}-${pkgver} ${pkgbase}
  cd ${pkgbase}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # additional fixes
  patch -Np1 -i ../fix-library.patch

  cd ..
  cp -a ${pkgbase} ${pkgbase}-51
  cp -a ${pkgbase} ${pkgbase}-52
}

build() {
  cd "$srcdir"/${pkgbase}-51
  MYCFLAGS="$CFLAGS" MYLDFLAGS=$LDFLAGS make LUAV=5.1 linux

  cd "$srcdir"/${pkgbase}-52
  MYCFLAGS="$CFLAGS" MYLDFLAGS=$LDFLAGS make LUAV=5.2 linux

  cd "$srcdir"/${pkgbase}
  MYCFLAGS="$CFLAGS" MYLDFLAGS=$LDFLAGS make LUAV=5.3 linux
}

package_lua51-socket() {
  replaces=('luasocket')
  conflicts=('luasocket')

  cd ${pkgbase}-51
  make DESTDIR="$pkgdir" LUAV=5.1 prefix=/usr install-unix
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_lua52-socket() {
  cd ${pkgbase}-52
  make DESTDIR="$pkgdir" LUAV=5.2 prefix=/usr install-unix
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_lua-socket() {
  cd ${pkgbase}
  make DESTDIR="$pkgdir" LUAV=5.3 prefix=/usr install-unix
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
