# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Jeremy "Ichimonji10" Audet <ichimonji10 at gmail dot com>
# Contributor (Arch): pumpkin <pumpkin at mailoo dot org>
# Contributor (Arch): Vsevolod Balashov <vsevolod at balashov dot name>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=gunicorn
pkgver=20.1.0
_debver=$pkgver
_debrel=6
pkgrel=2
pkgdesc="WSGI HTTP Server for UNIX"
arch=('any')
url='https://gunicorn.org/'
license=('Expat' 'GPL-3' 'Modified-BSD')
depends=('python-setuptools')
makedepends=('quilt')
optdepends=('python-eventlet: for asynchronous request handling with eventlet'
            'python-gevent: for asynchronous request handling with gevent')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/benoitc/${pkgname}/archive/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/g/$pkgname/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('4fd905f62adc30e044cf2a56a1a77e14bc633258267d6bfbd4f6a68494f93f377e9fb9ed94fab7f11f9d7813857a680974a88c4b6bf97d4f1b74792a81810111'
            'a2702b84961e7fec4f63de0488b06cdab807f6c8a0ef6de6b74d2c8530a091a4e60f02d0e6d9c6f6845679ab7c16987ccae40b58cd4b2c4ee253df1b57ff5e63')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -e 's/==/>=/' -e 's/,<.*$//' -i requirements_test.txt
}

build() {
  cd $pkgname-$pkgver
  python setup.py build
}

package() {
  cd $pkgname-$pkgver
  python setup.py install --root="$pkgdir" --prefix=/usr --optimize=1
  install -Dm 644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
