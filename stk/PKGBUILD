# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): Mateusz Herych <heniekk@gmail.com>
# Maintainer: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=stk
pkgver=4.6.1
pkgrel=1
pkgdesc='The Synthesis ToolKit in C++'
arch=('x86_64' 'i686')
url='https://ccrma.stanford.edu/software/stk/'
license=('Expat')
depends=('gcc-libs' 'glibc')
makedepends=('alsa-lib' 'doxygen' 'jack')
optdepends=('tk: run provided demos/samples')
source=("https://deb.debian.org/debian/pool/main/s/${pkgname}/${pkgname}_${pkgver}+dfsg.orig.tar.xz")
sha512sums=('c62d88055f31c0cd911cace0c30bc56535d438681144056effae70e25e8f7780ebdfb0031256c555a1e24f07730645236f900052a74dfc6f9393e9e3356c57c0')

prepare() {
	cd ${pkgname}-${pkgver}

	autoreconf -fiv
}

build() {
	cd ${pkgname}-${pkgver}

	export RAWWAVE_PATH=/usr/share/${pkgname}/rawwaves
	./configure --prefix=/usr/ --enable-shared --with-alsa --with-jack
	make VERBOSE=1
	make VERBOSE=1 -C projects/demo
	cd doc/doxygen
	doxygen
}

package() {
	cd ${pkgname}-${pkgver}

	make DESTDIR=${pkgdir} VERBOSE=1 install
	# In demo component.
	install -Dvm 755 projects/demo/stk-demo -t ${pkgdir}/usr/bin
	install -Dvm 644 projects/demo/tcl/*.tcl -t ${pkgdir}/usr/share/${pkgname}/tcl
	install -Dvm 644 projects/demo/scores/{README,*.ski} -t ${pkgdir}/usr/share/${pkgname}/scores
	install -Dvm 644 rawwaves/*.{raw,c} -t ${pkgdir}/usr/share/${pkgname}/rawwaves
	# In documentation component.
	install -Dvm 644 doc/README-Linux.txt ${pkgdir}/usr/share/doc/${pkgnanme}/README.txt
	install -Dvm 644 doc/{ReleaseNotes,SKINI,hierarchy}.txt -t ${pkgdir}/usr/share/doc/${pkgname}
	install -Dvm 644 doc/html/*.{html,png,js} -t ${pkgdir}/usr/share/doc/${pkgname}/html
	# Install licence file.
	install -Dvm 644 LICENSE -t ${pkgdir}/usr/share/licenses/${pkgname}
}
