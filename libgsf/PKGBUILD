# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Sarah Hay <sarahhay@mb.sympatico.ca>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libgsf
pkgver=1.14.50
_debver=$pkgver
_debrel=1
pkgrel=1
arch=('i686' 'x86_64')
pkgdesc="An extensible I/O abstraction library for dealing with structured file formats"
url='https://gitlab.gnome.org/GNOME/libgsf.git'
license=('GPL-2')
depends=('libxml2' 'gdk-pixbuf2' 'bzip2')
makedepends=('gobject-introspection' 'autoconf-archive' 'tauthon' 'quilt')
source=("https://download.gnome.org/sources/${pkgname}/1.14/${pkgname}-${pkgver}.tar.xz"
        "https://security.debian.org/debian-security/pool/updates/main/libg/libgsf/libgsf_${_debver}-${_debrel}+deb12u1.debian.tar.xz")
sha512sums=('e4a405d01440654bfe5059536524d494e5b66859235648bcf6031a74b59967504b50750f471bbe1607c4a5ef11cac98aa2ca9ebb31151cf5a19ffa340d421adb'
            'c8d906a78527706506d55d6c21c2cecbeb16dfa5d0dcdc61222daf587b5ef9755d2fd135bc29e64d13c5e4c06f474b43c5d8c2f3c7b9516dc8243fb5a07a0714')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  PYTHON=/usr/bin/tauthon ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-static \
    --enable-introspection \
    --disable-gtk-doc
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install

  # removing references towards gtk-doc
  rm -rf "${pkgdir}"/usr/share/gtk-doc

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
