# Contributor (Arch): damir <damir@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=vcdimager
pkgver=2.0.1
_debver=2.0.1+dfsg
_debrel=5
pkgrel=1
pkgdesc="A full-featured mastering suite for authoring disassembling and analyzing Video CD's and Super Video CD's"
url='https://www.gnu.org/software/vcdimager/'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('libcdio' 'libxml2' 'popt')
makedepends=('quilt')
source=(https://ftp.gnu.org/gnu/vcdimager/$pkgname-$pkgver.tar.gz{,.sig}
        https://deb.debian.org/debian/pool/main/v/vcdimager/vcdimager_$_debver-$_debrel.debian.tar.xz)
sha512sums=('55a9d235149a35fe26576703c623a2c9a3f7deedd2e42e01271fdf1e1fdf14c51ee040ee3b5d15fe1b5860fbd4cbeb437362b1a1f40187c8d4d691b6b89a2230'
            'SKIP'
            '70b4d5b8217d43acde600c14d99fe8f36eeaf33e2293a017ce481e24aa108220e8cd8527093f0114c088dba8fc2a54136e1ad7e4fa2c6ef822125a6d3b20747e')
validpgpkeys=(DAA63BC2582034A02B923D521A8DE5008275EC21) # R. Bernstein <rocky@panix.com>

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi
}


build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
