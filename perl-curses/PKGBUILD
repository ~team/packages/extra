# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Charles Mauch <cmauch@gmail.com>
# Contributor (Arch): Francois Charette <firmicus@gmx.net>

pkgname=perl-curses
pkgver=1.37
_debver=1.37
_debrel=1
pkgrel=1
pkgdesc='Character screen handling and windowing'
arch=('i686' 'x86_64')
url='https://search.cpan.org/dist/Curses'
license=('GPL-1')
depends=('perl' 'ncurses')
options=('!emptydirs')
source=(https://search.cpan.org/CPAN/authors/id/G/GI/GIRAFFED/Curses-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/libc/libcurses-perl/libcurses-perl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('10886e725b174fdabc0c9b86a9ef9b717949f0fb03561bfc315e8503be689be7945d1ceeea2f950628ce480c2cc8081736d3b8f28042ea677b252298a9eb4827'
            '2696627a6b97155d6ff4774d8c17573c107b78277c512ecd58301fbc0d6dbffae210774c2c18a06c4509e01260de1a993fe809a39b635132a5a7a9caa7c798a3')

prepare() {
  cd "$srcdir"/Curses-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/Curses-$pkgver
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd "$srcdir"/Curses-$pkgver
  make pure_install doc_install DESTDIR="$pkgdir"
  find "$pkgdir" -name '.packlist' -delete
  find "$pkgdir" -name '*.pod' -delete
  install -Dm644 Copying -t "${pkgdir}/usr/share/licenses/$pkgname"
}
