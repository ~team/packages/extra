# Maintainer (Arch): Pierre Neidhardt <ambrevar@gmail.com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Mike Sampson <mike at sambodata dot com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): froggie <sullivanva@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pstotext
pkgver=1.9
_debver=1.9
_debrel=6
pkgrel=1
pkgdesc='Converts Postscript to text'
arch=('i686' 'x86_64')
url='https://www.cs.wisc.edu/~ghost/doc/pstotext.htm'
license=('Modified-BSD')
depends=('ghostscript')
makedepends=('quilt')
source=(https://deb.debian.org/debian/pool/main/p/pstotext/pstotext_${pkgver}.orig.tar.gz
        https://deb.debian.org/debian/pool/main/p/pstotext/pstotext_${_debver}-${_debrel}.debian.tar.gz
        copyright)
sha512sums=('a8148e94d635544a7dfa8c96ff6071717388e4dc6c3db36f2c50939dce96c1f824ee423b1622af9111fffc60cb298215a6ee85d956c81873d71ceac804ee48ca'
            'cf06c5da84ffea9f21897787d453bd7dbbff4ac787f0a05c85eab1187f3e2f3d8d22baf11a4facb28b06041aaa23ed018edfe4e0e22b8096121d86c964f1e9a8'
            'd0bb3f7946f9dc8042d480afe3f333a873bf8a47a20f4eddf6285490819683b6040b77c727ac39ee13f26d14004dd538fbd802a47ba8f15a1b1612394d9b7ca9')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/doc-path.patch || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/$pkgname-$pkgver
  make
}

package() {
  cd "$srcdir"/$pkgname-$pkgver
  install -Dm755 pstotext "$pkgdir"/usr/bin/pstotext
  install -Dm644 pstotext.1 "$pkgdir"/usr/share/man/man1/pstotext.1
  install -Dm644 $srcdir/copyright -t "${pkgdir}"/usr/share/licenses/${pkgname}
}

