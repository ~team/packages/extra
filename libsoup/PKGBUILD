# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libsoup
pkgver=2.74.3
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="HTTP client/server library"
url='https://wiki.gnome.org/Projects/libsoup'
arch=('i686' 'x86_64')
license=('LGPL-2')
depends=('glib2' 'libxml2' 'glib-networking' 'sqlite' 'krb5' 'libpsl')
makedepends=('gobject-introspection' 'python' 'vala' 'meson' 'quilt')
source=("https://download.gnome.org/sources/${pkgname}/${pkgver:0:4}/${pkgname}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/libs/libsoup2.4/libsoup2.4_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('72f8a055df984cb35105fe67f4ca67d3fb110d40a9cacb660fccd89f93b06bc32e25d94375dcc76608a245f7c5e081d968d7aaf5952eb16013d81c741247cb4c'
            'a865db271a4a30264b4b53e019510abaca9b8274338284cae696b42ca94866231ebfa2d05ad6a0a1daf18417d42018503ef6106ca337c9f59523bfc74f905241')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  hyperbola-meson $pkgname-$pkgver build -D gtk_doc=false -D ntlm=disabled -D sysprof=disabled -D brotli=disabled
  ninja -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  install -Dm644 $pkgname-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
