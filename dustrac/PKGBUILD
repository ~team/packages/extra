# Maintainer (Arch): Frederic Bezies <fredbezies at gmail dot com>
# Contributor (Arch): Daniel Milde
# Maintainer: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=dustrac
_pkgname=DustRacing2D
pkgver=2.1.1
pkgrel=2
_debver=2.1.1
_debrel=1
pkgdesc='Dust Racing 2D is a traditional top-down car racing game including a level editor'
arch=('x86_64' 'i686')
url='https://juzzlin.github.io/DustRacing2D/'
license=('GPL-3' 'CC-BY-SA-3.0')
depends=('qt-tools' 'openal' 'libvorbis' 'glu')
makedepends=('cmake' 'quilt')
groups=('games')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/juzzlin/${_pkgname}/archive/${pkgver}.tar.gz"
	"https://deb.debian.org/debian/pool/main/d/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('9a49cd0302ab47b12ee587a33cc74c1df2bf6cf7925141cbf957fc90a5c1a34423d4420be28bcb6909a862daa12d6214b397fd0fc77e9e18fa649f3ca5c2e1ab'
	    '8824ae4dd0d281d945521262eb0a88927539ee335f475f3f42b5876bece2af32eaa1e4651b0be40d07cfd282a851c4a508229c90c23e02b40e5d3a7377399f57')

prepare() {
  cd ${_pkgname}-${pkgver}/

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian/ ./

    quilt push -av
  fi
}

build() {
  cd ${_pkgname}-${pkgver}/

  mkdir build && cd build
  cmake ../ \
    -DReleaseBuild=ON \
    -DCMAKE_INSTALL_PREFIX=/usr/ \
    -DBIN_PATH=games
  make
}

package() {
  cd ${_pkgname}-${pkgver}/build/

  make DESTDIR=${pkgdir} install
  # Install licence file.
  install -Dm644 ../COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}/
}
