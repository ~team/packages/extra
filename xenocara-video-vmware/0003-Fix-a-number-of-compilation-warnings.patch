From dc85a6da15768f7a9622677e79c618bfb35ce513 Mon Sep 17 00:00:00 2001
From: Thomas Hellstrom <thellstrom@vmware.com>
Date: Wed, 14 Feb 2018 10:14:20 +0100
Subject: [PATCH] Fix a number of compilation warnings

There are a number of compilation warnings caused by const char pointers
being either explicitly or implicitly cast to char pointers. There
are a number of ABI differences that have hindered this so far, but
make a new attempt using the common_compat.h defines.

Signed-off-by: Thomas Hellstrom <thellstrom@vmware.com>
Reviewed-by: Brian Paul <brianp@vmware.com>
Reviewed-by: Sinclair Yeh <syeh@vmware.com>
Reviewed-by: Deepak Rawat <drawat@vmware.com>
---
 src/common_compat.h       | 13 +++++++++++++
 src/vmware_bootstrap.c    | 16 +++++++++++-----
 src/vmwarevideo.c         | 14 ++++++++++----
 vmwgfx/vmwgfx_overlay.c   |  8 ++++++--
 vmwgfx/vmwgfx_tex_video.c | 22 +++++++++++++++-------
 5 files changed, 55 insertions(+), 18 deletions(-)

diff --git a/src/common_compat.h b/src/common_compat.h
index d7b4867..5c46a73 100644
--- a/src/common_compat.h
+++ b/src/common_compat.h
@@ -7,5 +7,18 @@
 #define CONST_ABI_18_0
 #endif
 
+#if (GET_ABI_MAJOR(ABI_VIDEODRV_VERSION) >= 16)
+#define CONST_ABI_16_0 const
+#else
+#define CONST_ABI_16_0
+#endif
+
+#if ((GET_ABI_MAJOR(ABI_VIDEODRV_VERSION) >= 16) && \
+     (GET_ABI_MAJOR(ABI_VIDEODRV_VERSION) < 19))
+#define CONST_ABI_16_TO_19 const
+#else
+#define CONST_ABI_16_TO_19
+#endif
+
 #endif
 
diff --git a/src/vmware_bootstrap.c b/src/vmware_bootstrap.c
index d7ab070..4f59e73 100644
--- a/src/vmware_bootstrap.c
+++ b/src/vmware_bootstrap.c
@@ -82,6 +82,10 @@
 
 #define VMWARE_NAME "vmware"
 #define VMWARE_DRIVER_NAME "vmware"
+
+static char vmware_name[] = VMWARE_NAME;
+static char vmware_driver_name[] = VMWARE_DRIVER_NAME;
+
 #define VMWARE_DRIVER_VERSION \
    (PACKAGE_VERSION_MAJOR * 65536 + PACKAGE_VERSION_MINOR * 256 + PACKAGE_VERSION_PATCHLEVEL)
 #define VMWARE_DRIVER_VERSION_STRING \
@@ -255,8 +259,10 @@ VMwarePreinitStub(ScrnInfoPtr pScrn, int flags)
     if (pciInfo == NULL)
         return FALSE;
 
-    pScrn->chipset = (char*)xf86TokenToString(VMWAREChipsets,
-					      DEVICE_ID(pciInfo));
+    pScrn->chipset = xstrdup(xf86TokenToString(VMWAREChipsets,
+					       DEVICE_ID(pciInfo)));
+    if (pScrn->chipset == NULL)
+	return FALSE;
 
     return (*pScrn->PreInit)(pScrn, flags);
 };
@@ -274,8 +280,8 @@ VMwarePciProbe (DriverPtr           drv,
                                NULL, NULL, NULL, NULL, NULL);
     if (scrn != NULL) {
         scrn->driverVersion = VMWARE_DRIVER_VERSION;
-        scrn->driverName = VMWARE_DRIVER_NAME;
-        scrn->name = VMWARE_NAME;
+        scrn->driverName = vmware_driver_name;
+        scrn->name = vmware_name;
         scrn->Probe = NULL;
     }
 
@@ -509,7 +515,7 @@ VMWareDriverFunc(ScrnInfoPtr pScrn,
 
 _X_EXPORT DriverRec vmware = {
     VMWARE_DRIVER_VERSION,
-    VMWARE_DRIVER_NAME,
+    vmware_driver_name,
     VMWAREIdentify,
 #if XSERVER_LIBPCIACCESS
     NULL,
diff --git a/src/vmwarevideo.c b/src/vmwarevideo.c
index 634af69..3ba1dcd 100644
--- a/src/vmwarevideo.c
+++ b/src/vmwarevideo.c
@@ -44,6 +44,7 @@
 #include "fourcc.h"
 #include "svga_escape.h"
 #include "svga_overlay.h"
+#include "common_compat.h"
 
 #include <X11/extensions/Xv.h>
 
@@ -52,6 +53,8 @@
 #include <xf86_libc.h>
 #endif
 
+static CONST_ABI_16_0 char xv_adapt_name[] = "VMWare Overlay Video Engine";
+static CONST_ABI_16_0 char xv_image_name[] = "XV_IMAGE";
 
 #define HAVE_FILLKEYHELPERDRAWABLE \
     ((GET_ABI_MAJOR(ABI_VIDEODRV_VERSION) >= 2) ||  \
@@ -90,7 +93,7 @@ static XF86VideoEncodingRec vmwareVideoEncodings[] =
 {
     {
        0,
-       "XV_IMAGE",
+       xv_image_name,
        VMWARE_VID_MAX_WIDTH, VMWARE_VID_MAX_HEIGHT,
        {1, 1}
     }
@@ -111,6 +114,9 @@ static XF86ImageRec vmwareVideoImages[] =
     XVIMAGE_UYVY
 };
 
+static CONST_ABI_16_TO_19 char xv_colorkey_name[] = "XV_COLORKEY";
+static CONST_ABI_16_TO_19 char xv_autopaint_name[] = "XV_AUTOPAINT_COLORKEY";
+
 #define VMWARE_VID_NUM_ATTRIBUTES 2
 static XF86AttributeRec vmwareVideoAttributes[] =
 {
@@ -118,13 +124,13 @@ static XF86AttributeRec vmwareVideoAttributes[] =
         XvGettable | XvSettable,
         0x000000,
         0xffffff,
-        "XV_COLORKEY"
+        xv_colorkey_name,
     },
     {
         XvGettable | XvSettable,
         0,
         1,
-        "XV_AUTOPAINT_COLORKEY"
+        xv_autopaint_name,
     }
 };
 
@@ -575,7 +581,7 @@ vmwareVideoSetup(ScrnInfoPtr pScrn)
 
     adaptor->type = XvInputMask | XvImageMask | XvWindowMask;
     adaptor->flags = VIDEO_OVERLAID_IMAGES | VIDEO_CLIP_TO_VIEWPORT;
-    adaptor->name = "VMware Video Engine";
+    adaptor->name = xv_adapt_name;
     adaptor->nEncodings = VMWARE_VID_NUM_ENCODINGS;
     adaptor->pEncodings = vmwareVideoEncodings;
     adaptor->nFormats = VMWARE_VID_NUM_FORMATS;
diff --git a/vmwgfx/vmwgfx_overlay.c b/vmwgfx/vmwgfx_overlay.c
index 49bad73..c35cebd 100644
--- a/vmwgfx/vmwgfx_overlay.c
+++ b/vmwgfx/vmwgfx_overlay.c
@@ -61,6 +61,7 @@ typedef uint8_t uint8;
 #include "../src/svga_reg.h"
 #include "../src/svga_escape.h"
 #include "../src/svga_overlay.h"
+#include "../src/common_compat.h"
 
 #include <X11/extensions/Xv.h>
 
@@ -114,6 +115,9 @@ static XF86ImageRec vmwareVideoImages[] =
     XVIMAGE_UYVY
 };
 
+static CONST_ABI_16_TO_19 char xv_colorkey_name[] = "XV_COLORKEY";
+static CONST_ABI_16_TO_19 char xv_autopaint_name[] = "XV_AUTOPAINT_COLORKEY";
+
 #define VMWARE_VID_NUM_ATTRIBUTES 2
 static XF86AttributeRec vmwareVideoAttributes[] =
 {
@@ -121,13 +125,13 @@ static XF86AttributeRec vmwareVideoAttributes[] =
         XvGettable | XvSettable,
         0x000000,
         0xffffff,
-        "XV_COLORKEY"
+        xv_colorkey_name,
     },
     {
         XvGettable | XvSettable,
         0,
         1,
-        "XV_AUTOPAINT_COLORKEY"
+        xv_autopaint_name,
     }
 };
 
diff --git a/vmwgfx/vmwgfx_tex_video.c b/vmwgfx/vmwgfx_tex_video.c
index f2cc813..a767d89 100644
--- a/vmwgfx/vmwgfx_tex_video.c
+++ b/vmwgfx/vmwgfx_tex_video.c
@@ -32,6 +32,7 @@
 #include "vmwgfx_driver.h"
 #include "vmwgfx_drmi.h"
 #include "vmwgfx_saa.h"
+#include "../src/common_compat.h"
 
 #include <xf86xv.h>
 #include <X11/extensions/Xv.h>
@@ -40,7 +41,9 @@
 #include <xa_context.h>
 #include <math.h>
 
-/*XXX get these from pipe's texture limits */
+static CONST_ABI_16_0 char xv_adapt_name[] = "XA G3D Textured Video";
+
+/*Xxx get these from pipe's texture limits */
 #define IMAGE_MAX_WIDTH		2048
 #define IMAGE_MAX_HEIGHT	2048
 
@@ -71,13 +74,18 @@ static const float bt_709[] = {
 };
 
 static Atom xvBrightness, xvContrast, xvSaturation, xvHue;
+static CONST_ABI_16_TO_19 char xv_brightness_name[] = "XV_BRIGHTNESS";
+static CONST_ABI_16_TO_19 char xv_contrast_name[] = "XV_CONTRAST";
+static CONST_ABI_16_TO_19 char xv_saturation_name[] = "XV_SATURATION";
+static CONST_ABI_16_TO_19 char xv_hue_name[] = "XV_HUE";
+static CONST_ABI_16_TO_19 char xv_image_name[] = "XV_IMAGE";
 
 #define NUM_TEXTURED_ATTRIBUTES 4
 static const XF86AttributeRec TexturedAttributes[NUM_TEXTURED_ATTRIBUTES] = {
-    {XvSettable | XvGettable, -1000, 1000, "XV_BRIGHTNESS"},
-    {XvSettable | XvGettable, -1000, 1000, "XV_CONTRAST"},
-    {XvSettable | XvGettable, -1000, 1000, "XV_SATURATION"},
-    {XvSettable | XvGettable, -1000, 1000, "XV_HUE"}
+    {XvSettable | XvGettable, -1000, 1000, xv_brightness_name},
+    {XvSettable | XvGettable, -1000, 1000, xv_contrast_name},
+    {XvSettable | XvGettable, -1000, 1000, xv_saturation_name},
+    {XvSettable | XvGettable, -1000, 1000, xv_hue_name}
 };
 
 #define NUM_FORMATS 3
@@ -88,7 +96,7 @@ static XF86VideoFormatRec Formats[NUM_FORMATS] = {
 static XF86VideoEncodingRec DummyEncoding[1] = {
    {
       0,
-      "XV_IMAGE",
+      xv_image_name,
       IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT,
       {1, 1}
    }
@@ -943,7 +951,7 @@ xorg_setup_textured_adapter(ScreenPtr pScreen)
 
    adapt->type = XvWindowMask | XvInputMask | XvImageMask;
    adapt->flags = 0;
-   adapt->name = "XA G3D Textured Video";
+   adapt->name = xv_adapt_name;
    adapt->nEncodings = 1;
    adapt->pEncodings = DummyEncoding;
    adapt->nFormats = NUM_FORMATS;
-- 
2.12.2

