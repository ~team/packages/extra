# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gtksourceview3
_pkgbasename=gtksourceview
pkgver=3.24.11
pkgrel=2
pkgdesc='A text widget adding syntax highlighting and more'
url='https://wiki.gnome.org/Projects/GtkSourceView'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
depends=('gtk' 'libxml2')
makedepends=('intltool' 'gobject-introspection' 'vala' 'autoconf-archive' 'yelp-tools' 'mesa-libgl')
source=("https://deb.debian.org/debian/pool/main/g/gtksourceview3/gtksourceview3_${pkgver}.orig.tar.xz")
sha512sums=('3490b34c6432a2e2ef292ad5bf982fdd22d33b1472434549b4ea8ddae0fc8808e128ef947e4d0dcb41e8de5e601b202f52532bbbbfa11d35050acfc42f9936b2')

build() {
  cd $_pkgbasename-$pkgver

  ./configure --prefix=/usr --sysconfdir=/etc \
      --localstatedir=/var --disable-static \
      --disable-glade-catalog --disable-gtk-doc --disable-Werror
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $_pkgbasename-$pkgver

  make DESTDIR="$pkgdir" install

  # we don't support gtk-doc
  rm -rf $pkgdir/usr/share/gtk-doc

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
