# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@koon.fr>
# Contributor (Arch): Dalius <dagis@takas.lt>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=wmctrl
pkgver=1.07
_debver=$pkgver
_debrel=7
pkgrel=3
pkgdesc="Control your EWMH compliant window manager from command line"
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('glib2' 'libxmu')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/w/wmctrl/wmctrl_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/w/wmctrl/wmctrl_${_debver}-${_debrel}.debian.tar.gz")
sha512sums=('4c77ad1e204e8d444f682ad1d05c0993bcab9097ac6d4b6a944556ab85acbe713f549dbaf443cd4d1226a162ce7d46fbd209c92652e87fc8e609feee74907daa'
            '2e5663f485801b871b388895d4b1eba90ae43dc353cdb0d6af962a51c1c45ea457523cb6c58fce1004998cbd500f553881e49c28eca776d34517d563efd7bbea')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure \
    --bindir=${pkgdir}/usr/bin \
    --mandir=${pkgdir}/usr/share/man
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
