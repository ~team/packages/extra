# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libopenmpt
pkgver=0.5.12
pkgrel=1
pkgdesc='A library to render tracker music to a PCM audio stream'
arch=('i686' 'x86_64')
url="https://lib.openmpt.org/libopenmpt/"
license=('Modified-BSD')
depends=('flac' 'gcc-libs' 'glibc' 'libogg' 'libsndfile' 'libvorbis' 'mpg123' 'portaudio')
makedepends=('autoconf-archive' 'doxygen' 'help2man')
provides=('openmpt123')
replaces=('openmpt123')
source=("$pkgname-$pkgver.tar.gz::https://lib.openmpt.org/files/libopenmpt/src/${pkgname}-${pkgver}+release.autotools.tar.gz")
sha512sums=('e22a79737a825a5753429271bba7af94fd195fb0ff770a3e2091b941c3856bbad770eeeb88cd86d37e01b0138e820199d3650f55546f2e33bb2e99b1b14d9925')

prepare() {
  mv -v "$pkgname-$pkgver+release.autotools" "$pkgname-$pkgver"
  cd "$pkgname-$pkgver"
  autoreconf -fiv
}

build() {
  cd "$pkgname-$pkgver"
  ./configure --prefix=/usr --with-pulseaudio=no --without-portaudiocpp
  # prevent excessive overlinking due to libtool
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install
  install -vDm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}
