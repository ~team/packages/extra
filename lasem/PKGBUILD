# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Adria Arrufat <adria.arrufat@protonmail.ch>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=lasem
pkgver=0.4.4
pkgrel=1
pkgdesc='SVG and Mathml rendering library'
arch=('i686' 'x86_64')
url='https://wiki.gnome.org/Projects/Lasem'
license=('GPL-2')
depends=('gtk')
makedepends=('intltool' 'python' 'gettext-tiny')
options=('!libtool')
source=(https://download.gnome.org/sources/$pkgname/${pkgver:0:3}/$pkgname-$pkgver.tar.xz
        CVE-2013-7447.patch)
sha512sums=('3f78f6892715b0aabc1c6b16b4ab45c00227fcbd155ebed6a1e84af907d1ce3934c0a9db3f0c25c5e52a092ab58442b9b18a027dbcaae15e5e6dbe8d0d085480'
            '25f771fe5455c032198125eebd8acf3847db5fe7ddbc6261bfab3b3b586210cb507c6085ef562239787730e3b56411f31020fba8d189d3b0bdf3820a296caa42')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i ../CVE-2013-7447.patch
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --disable-static
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  mv "$pkgdir/usr/doc" "$pkgdir/usr/share/doc"

  # removing references towards gtk-doc
  rm -rf "${pkgdir}"/usr/share/gtk-doc

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
