# Maintainer (Arch): Alad Wenter <alad@mailbox.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=cabextract
pkgver=1.9
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc="Cabinet file unpacker"
arch=('i686' 'x86_64')
license=('GPL-3')
depends=('glibc')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/c/cabextract/cabextract_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/cabextract/cabextract_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('d144c1df9ed8e8c339bcadb3d1be2eb2576bb066e3f75a3480df0d5e81407a633554e4674b3bb43716c0cf33a0f2327e6f798cd6d7825bceefa0ad839fcc3f64'
            '4c6bdf201381926b7b30dd6df6118a29db9510dea21e0c0b3b492bb9dd39ff0724d74088ea627abc936af13d527c64908bc0fd6e86a401b724ab12ecaa7f0fe9')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./configure --prefix=/usr \
              --sysconfdir=/etc \
              --mandir=/usr/share/man
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
