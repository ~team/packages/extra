# Maintainer (Arch): Vesa Kaihlavirta <vegai@iki.fi>
# Contributor (Arch): Markus Pargmann
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=quesoglc
pkgver=0.7.2
_debver=$pkgver
_debrel=6
pkgrel=2
pkgdesc="A free and libre implementation of the OpenGL Character Renderer (GLC)"
arch=('i686' 'x86_64')
url='https://quesoglc.sourceforge.net/'
license=('LGPL-2.1')
depends=('fontconfig' 'freeglut' 'fribidi' 'glew')
makedepends=('pkg-config' 'quilt')
source=("https://downloads.sourceforge.net/sourceforge/${pkgname}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/q/quesoglc/quesoglc_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('f53d4c2a8c9f0cda9eecdf95087589dd893974e5059ca208909c84fb82084627398f578d428fe612e6b2b6f7e428c71de4139e41a64df063129d16003c6d9fd8'
            '35fa08a81c581e7827d9eeae89edbdc47b64606a1c4ec9cf793904a0dd0a835acf144675d02216d0d3c5ce62122a306e3dfd3e825ba84f24d11005ede4a3d885')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/01_fix_glew_search.diff || true

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"

  ./configure --prefix=/usr
  make
}

package() {
  cd "${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
