# Maintainer (Arch): Florian Pritz <bluewind@xinu.at>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-file-tail
pkgver=1.3
_debver=$pkgver
_debrel=6
_ddir=File-Tail-$pkgver
pkgrel=1
pkgdesc="Perl extension for reading from continously updated files"
arch=('any')
license=('GPL-1')
url='https://metacpan.org/release/File-Tail'
options=(!emptydirs)
depends=('perl')
makedepends=('quilt')
source=("https://search.cpan.org/CPAN/authors/id/M/MG/MGRABNAR/File-Tail-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libf/libfile-tail-perl/libfile-tail-perl_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('942f87880d0d8b79c521248cdbc449daafbd117a2a3700e2dbcc7fe7a6046e5783e8616b6c32de513f85a56663b63c4dde217e9e124216ba6a605c645db2bde9'
            '681a614a8b6ced5dc1de591e912da04974d2aa2d2dedc3adbfbc30abbe3398ca189f28fec878af11dd222b87335b2f6ff474b2311cc090c3cad8c41702131241')

prepare() {
  cd "$srcdir/$_ddir"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() (
  cd "$srcdir/$_ddir"
  export PERL_MM_USE_DEFAULT=1 PERL_AUTOINSTALL=--skipdeps
  unset PERL5LIB PERL_MM_OPT
  /usr/bin/perl Makefile.PL
  make
)

package() (
  cd "$srcdir/$_ddir"
  make install INSTALLDIRS=vendor DESTDIR="$pkgdir"
  find "$pkgdir" -name .packlist -o -name perllocal.pod -delete
  install -Dm644 debian/copyright -t "${pkgdir}/usr/share/licenses/$pkgname"
)
