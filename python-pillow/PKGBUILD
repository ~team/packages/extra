# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Contributor (Arch): minder
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=python-pillow
pkgver=8.1.2
_debver=$pkgver
_debrel=0.3
_appname=Pillow
_py3basever=3.8
pkgrel=2
pkgdesc='Python Imaging Library (PIL) fork.'
arch=('i686' 'x86_64')
url="https://python-pillow.github.io/"
license=('Expat')
depends=('python' 'lcms2' 'libtiff' 'openjpeg2' 'libimagequant' 'libxcb')
optdepends=('freetype2: for the ImageFont module'
            'libraqm: for complex text scripts'
            'tk: for the ImageTK module')
makedepends=('python-setuptools' 'freetype2' 'libraqm' 'tk' 'quilt')
source=("https://files.pythonhosted.org/packages/source/P/$_appname/$_appname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/pillow/pillow_${_debver}+dfsg-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('7e1c14aed7446d53fb67a1f9de283aadc6be3b16e0b56361329af1d0e1b02e4110d1d1a4eb021aada90fbab2ee67d3e582e0c1829d90af39641c5562061c9270'
            '17afc0010886fadb691c6870f16af063ce8c567f34f732edaeaa3acd9800ba409adfed5b6e456de001e6dfd68a52f6149d07668fcccfb49f9a03128ec6c9f155')

prepare() {
  cd "$srcdir/$_appname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$_appname-$pkgver"
  python setup.py build
}

package() {
  cd "$srcdir/$_appname-$pkgver"
  python setup.py install --root="$pkgdir/" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"

  install -dm755 "$pkgdir/usr/include/python$_py3basever/"
  install -m644 -t "$pkgdir/usr/include/python$_py3basever/" src/libImaging/*.h
}