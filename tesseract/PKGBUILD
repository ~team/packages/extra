# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Andreas Hauser <andy-aur@splashground.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_langs=(afr amh ara asm aze aze_cyrl bel ben bod bos bre bul cat ceb ces
    chi_sim chi_tra chr cos cym dan dan_frak deu deu_frak div dzo ell eng enm epo
    equ est eus fao fas fil fin fra frk frm fry gla gle glg grc guj hat heb
    hin hrv hun hye iku ind isl ita ita_old jav jpn kan kat kat_old kaz khm
    kir kmr kor kor_vert lao lat lav lit ltz mal mar mkd mlt mon mri msa
    mya nep nld nor oci ori osd pan pol por pus que ron rus san sin slk
    slk_frak slv snd spa spa_old sqi srp srp_latn sun swa swe syr tam tat
    tel tgk tgl tha tir ton tur uig ukr urd uzb uzb_cyrl vie yid yor)

pkgname=tesseract
pkgver=4.1.1
_debver=4.1.1
_debrel=2.1
pkgrel=1
pkgdesc='An OCR program'
arch=('i686' 'x86_64')
url='https://github.com/tesseract-ocr/tesseract'
license=('Apache-2.0')
depends=('libpng' 'libtiff' 'libjpeg-turbo' 'zlib' 'giflib' 'gcc-libs' 'curl' 'libarchive' 'leptonica')
makedepends=('icu' 'cairo' 'pango' 'asciidoc' 'quilt')
install=tesseract.install
optdepends=('icu' 'cairo' 'pango'
	    $(for l in ${_langs[@]}; do echo tesseract-data-${l}; done))
source=($pkgname-$pkgver.tar.gz::https://github.com/tesseract-ocr/tesseract/archive/$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/t/tesseract/tesseract_$_debver-$_debrel.debian.tar.xz
        https://github.com/tesseract-ocr/tessdata/raw/bf82613055ebc6e63d9e3b438a5c234bfd638c93/osd.traineddata)
sha512sums=('017723a2268be789fe98978eed02fd294968cc8050dde376dee026f56f2b99df42db935049ae5e72c4519a920e263b40af1a6a40d9942e66608145b3131a71a2'
            '458f805cb13dce5538e9811ea0e243275e82d7bcb7aef8f468c7ed20a0e278bcee1ff4cc874ba1952a8d9d27b2f5f1eabaa4cad659668a38474d579e4334082d'
            'c54f481903187bed19cf14c69b24c44044b540f50814de66dff8d35e6987eea71ef4464492a8fae9242fcb22cccbe59e009f3a4dab6c36ad63f78c52ebe9628f')

prepare() {
  cd "$srcdir"/$pkgname-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/$pkgname-${pkgver}
  [ -x configure ] || ./autogen.sh
  [ -f Makefile ] || ./configure --prefix=/usr
  make
  make training
}

package() {
  cd "$srcdir"/$pkgname-${pkgver}
  make DESTDIR="$pkgdir" install
  make DESTDIR="$pkgdir" training-install
  mkdir -p "$pkgdir"/usr/share/tessdata
  install -Dm0644 "$srcdir"/osd.traineddata "$pkgdir"/usr/share/tessdata/osd.traineddata
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"
}
