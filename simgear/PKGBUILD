# Maintainer (Arch): Andrew Whatson <whatson@gmail.com>
# Contributor (Arch): Jake <aur@ja-ke.tech>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Contributor (Arch): Hans Janssen <hans@janserv.xs4all.nl>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=simgear
pkgver=2020.3.6
_pkgver=${pkgver%.*}
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="A set of free software libraries designed to be used as building blocks for quickly assembling 3d simulations, games, and visualization applications."
arch=('i686' 'x86_64')
url='https://home.flightgear.org/'
license=('LGPL-2')
depends=('glu' 'glut' 'freealut' 'plib' 'openscenegraph' 'expat' 'udns')
makedepends=('boost' 'cmake' 'mesa' 'quilt')
options=('staticlibs')
source=("https://downloads.sourceforge.net/project/flightgear/release-${_pkgver}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/s/simgear/simgear_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('eaa8e3663c5fb486ea0fdad26c490fa49472596157d8d19e60426d5a1d340dfda4e5e3d82f68542bb6d9689abb8e867dc33761ea578b9c8f6c07aeb4b942880e'
            'db9571ec7faea5d0e4866e68a7aa88fdded7484af648b221f7dc37ec1ddb801635c9bf461e6c59acb9b007f32bdf2d1beb134071f490ea15ce5a99f627e46cdc')

prepare() {
  cd "$srcdir"/simgear-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/kfreebsd.patch || true
    rm -v debian/patches/hurd.patch || true
    rm -v debian/patches/fix-ftbfs-on-armel-armhf.patch || true

    quilt push -av
  fi
}

build() {
  cmake \
    -S simgear-${pkgver} \
    -Bbuild \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr \
    -DSIMGEAR_SHARED=OFF \
    -DSYSTEM_EXPAT=ON \
    -DSYSTEM_UDNS=ON \
    -DENABLE_DNS=ON
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install
  install -Dm644 "simgear-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
