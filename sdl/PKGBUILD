# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Contributor (ConnochaetOS): Henry Jensen <hjensen@connochaetos.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=sdl
pkgver=1.2.15
_debver=1.2.15+dfsg2
_debrel=6
pkgrel=9
pkgdesc="A library for portable low-level access to a video framebuffer, audio output, mouse, and keyboard"
arch=('i686' 'x86_64')
url="https://www.libsdl.org/"
license=('LGPL-2.1')
depends=('glibc' 'libxext' 'libxrender' 'libx11' 'libsndio')
makedepends=('alsa-lib' 'mesa' 'glu' 'quilt')
optdepends=('alsa-lib: ALSA audio driver'
            'sndio: sndio audio driver')
options=('staticlibs')
mksource=(https://www.libsdl.org/release/SDL-${pkgver}.tar.gz)
source=(https://repo.hyperbola.info:50000/sources/${pkgname}-libre/SDL-libre-${pkgver}.tar.gz{,.sig}
        https://deb.debian.org/debian/pool/main/libs/libsdl1.2/libsdl1.2_$_debver-$_debrel.debian.tar.xz
        SDL-1.2.10-GrabNotViewable.patch
        SDL-1.2.15-SDL_EnableUNICODE_drops_keyboard_events.patch
        SDL-1.2.15-const_XData32.patch
        SDL-1.2.15-ignore_insane_joystick_axis.patch
        SDL-1.2.15-no-default-backing-store.patch
        SDL-1.2.15-x11-Bypass-SetGammaRamp-when-changing-gamma.patch
        sdl-1.2.14-fix-mouse-clicking.patch
        sdl-1.2.14-disable-mmx.patch
        libsdl-1.2.15-resizing.patch
        X11_KeyToUnicode.patch
        SDL-1.2.15-CVE-2019-13616-validate_image_size_when_loading_BMP_files.patch
        SDL-1.2.15-CVE-2019-7572-Fix-a-buffer-overread-in-IMA_ADPCM_nib.patch
        SDL-1.2.15-CVE-2019-7572-Fix-a-buffer-overwrite-in-IMA_ADPCM_de.patch
        SDL-1.2.15-CVE-2019-7573-CVE-2019-7576-Fix-buffer-overreads-in-.patch
        SDL-1.2.15-CVE-2019-7574-Fix-a-buffer-overread-in-IMA_ADPCM_dec.patch
        SDL-1.2.15-CVE-2019-7575-Fix-a-buffer-overwrite-in-MS_ADPCM_dec.patch
        SDL-1.2.15-CVE-2019-7577-Fix-a-buffer-overread-in-MS_ADPCM_deco.patch
        SDL-1.2.15-CVE-2019-7577-Fix-a-buffer-overread-in-MS_ADPCM_nibb.patch
        SDL-1.2.15-CVE-2019-7578-Fix-a-buffer-overread-in-InitIMA_ADPCM.patch
        SDL-1.2.15-CVE-2019-7635-Reject-BMP-images-with-pixel-colors-ou.patch
        SDL-1.2.15-CVE-2019-7637-Fix-in-integer-overflow-in-SDL_Calcula.patch
        SDL-1.2.15-CVE-2019-7638-CVE-2019-7636-Refuse-loading-BMP-image.patch
        SDL-1.2.15-Reject-2-3-5-6-7-bpp-BMP-images.patch
        sndio.patch
        libre.patch)
mksha512sums=('ac392d916e6953b0925a7cbb0f232affea33339ef69b47a0a7898492afb9784b93138986df53d6da6d3e2ad79af1e9482df565ecca30f89428be0ae6851b1adc')
sha512sums=('0f43a2d7905eb7bf4e2348f9999ee6b716d08159b8418fe0f179c235034c4d20874e38055d9eb2cdbcec612e396e1c2739a4d23d7181aecb018f24ca16e27b08'
            'SKIP'
            '6cca017439661e7f1a6eef2fa19ecd26de03cd4d81bf1a8117651bebd28b5ea1ae88c2149029ef27e664ec692d39d34c571811812824180a4693b94ff752da6f'
            '20049408d4c00d895c39a7901d889d1874ebcd382e93b2e8df38bd3726e2236f4e9a980720724cf176a35d05fb0db5dbcabd42089423adeb404f2dba16d52b7b'
            '8816d3c3767bb02007cc8617b62d21b79c5f224f77c80ebc6b2be6e817571f255ef6901ed4c7461f3d6b24f0ade315c0e07445c13acd8d8db6b01001be498ec2'
            'c414a088350e4b039edf46b109721bea01300ad959b84c313f34d5bc085cab97107abb55a71cb8343f092546e4a36c52febf029ffa7d5bacbd580aee43c07bf3'
            '6fc50981ef6ae1c737afcb597241d8e89379072ed495cfcc54ee969125c2de1b1455bc3bada8c0c8c3ec13ab2d1b01bc2f7c31e88fcbc399b21754878b5c325e'
            'd21850ae37faeea3a5c57e5695773d732b0ca4452e0b3a9d35af0ec73879f6f91e98b56d9137eb18f04b5886ded2de10df6f8574f3207800275af04982ebefd1'
            'e78153051c496ad37d55137d8d9cd8782269c9be201feaeb32ed57f547d628810f1fba3d9b6c69cc37b1f9941b155cc9d37ba78b275485d129f3c8bad69d5df7'
            '69ccd8122829530bbff2d056cbf2e0a172e1385c46ab666f869a2c11777a8ef458603aadd59f3a044aac429d1f1ca7b0339ea1b968347c44f784a9423aacea58'
            'b0f22c21afcc5942d5cca246875eca23ad5bf0af63b57986b70cefa297f7913dc12269e00e36b6be6b635802b86f2e1aa078c603a0f2d3531321d6a178195c31'
            'f037efd76547eb2bb02a869ca245ea682f0d3321a1dd83d4dadadbda7cd6208c31d977281361b4e683416510be117704038fa54aa9d898e6b6c1168d0b24ecbb'
            'cc9b7e6f775608caa004961805b8817e44a698aaefdc0a05a8410ce6d2e9249b7c4d4ee5c029738bf32486f3e9bb7bd18849542c830a0fb8dd4c587c6d5ac0e5'
            '538910b9f74923f89b664b69d710df06280d629c912303fcd9ccca9c8e6d43c7aefd6053ffc615ec0a38d3e0b86e828df7e2ce503b2b889b9c42179453112596'
            '3274f91e41b72cd98b6d7962013dd45289952b7af78cc7bc5fe99d4f143434243c8ef0743117d3ec6b090784dfcba8dd460679cc5b49f298ebd8b5afab78a108'
            'e713d0f3d24d73831d9f116d4e15e965c5f09e19b15634e8cbf92714612b0172f24a5c542b3fde09732d17b03d7dac3aaac0d8f4e359a45c1c538970413d6e7c'
            '3bf62a71988feff2329e298cee8ce48c636c65100959385b73953c95eea21cb069a7ed096165c252e5ef1db133330da5d095cf5ad145d9875b1197d3b5517b81'
            '8c287d6ffcc159f19d934d560e073a716325b6a62d9dea974b92b2d4a417defc4f8441769b4761c5a2600b10a45ff401b0afbab6823880e3d54eab09e22f9859'
            'abe54d9f29b5e6c1a91cba2bb44e0988b7ceb5a94c3f63569f436f49f282b80280cecd79ee48b9926fff458efbdf0fff019b0fdbf6530692a11a68dbec73e7ca'
            'f364161069ceb5d05d329ff04f6e72d2c52baff68d0d3f2203f8a7ee3ace1efe8fc63676ea7d097ccc8eb696dcc20c6b141319ddf0c2bb6efc4fd92cb1dba038'
            'd2f0664cc0388908ec621c84e7f889ef5abda31dc4e4d23e6e379e26475ed73863ad47b2f13d282c96ba269bdbc77e7effaf5f01032d0683ad991b506063ef19'
            'a31d5c685fafbca72fdc5336343b74b90b1bfd5af4b6f632b4d8271bb1a218ec6419a7994290f65e7a5fc36d921c2d3c1a25ddf0cdf29bffb7229229415eaa9f'
            '60d57952fb0190e15aac85d9d7e5b589e1a6e781870c35d75327aefc21c8285dffc48aee3b458caee0e8c7704f59e8c8d2596d7126f58944878f905688267b5e'
            'e0fcbfc500f654f0791ca2d240589275e9dd8d97e76e962d0de095e1135880d3f21f5b1a851ec7cac35245d1e92f53758f88cd673e823a975a6c7c3760def24e'
            'ac5f3b731b82a6aa1b89492e8eddf48b1e927b8ec52b24cd1618417cc0edc646e8de5b9d903e965a0e10dbd830fdc3964d753274995a59865a059ce594efb56f'
            '5c8210c830afc97e6401e0cda2c7372fcb3f20ed91470258c0ab168410c12664f76c5fc5e23f88c4b2c40dcee1e87e49ad41de4c8a0c409c16d269247ade105c'
            '20d7200147de20a0e3a9ce7205b169b4b1a26e05f57d7d44174564e8ce8f5d9ba005b9de9c538e72d48eaac046fc22a56440f4974827c2959a88fcdb6d8355ef'
            '0c8b6da6e1fb02b19062d9cde4b35ba55a522285041367d79cd3634ad2887683f27e0cb88f6e8cf044e24da41f0bdcf39d4b77758e7417c57bc4506671235ecb')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

mksource() {
  cd SDL-$pkgver
  rm -v src/video/fbcon/riva_mmio.h
}

prepare() {
  cd SDL-$pkgver

  if [[ ${pkgver} = ${_debver} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/replace-relicenced-SDL_qsort.patch || true

    quilt push -av
  else
    patch -Np1 -i ../SDL-1.2.15-no-default-backing-store.patch
    patch -Np1 -i ../libsdl-1.2.15-resizing.patch
    patch -Np1 -i ../X11_KeyToUnicode.patch
  fi

  patch -Np1 -i ../SDL-1.2.10-GrabNotViewable.patch
  patch -Np1 -i ../SDL-1.2.15-SDL_EnableUNICODE_drops_keyboard_events.patch
  patch -Np1 -i ../SDL-1.2.15-const_XData32.patch
  patch -Np1 -i ../SDL-1.2.15-ignore_insane_joystick_axis.patch

  # https://bugs.freedesktop.org/show_bug.cgi?id=27222
  patch -Np1 -i ../SDL-1.2.15-x11-Bypass-SetGammaRamp-when-changing-gamma.patch

  patch -Np1 -i ../sdl-1.2.14-fix-mouse-clicking.patch
  patch -Np1 -i ../sdl-1.2.14-disable-mmx.patch

  # bunch of CVE fixes from Fedora - Thanks!
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7577-Fix-a-buffer-overread-in-MS_ADPCM_deco.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7575-Fix-a-buffer-overwrite-in-MS_ADPCM_dec.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7574-Fix-a-buffer-overread-in-IMA_ADPCM_dec.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7572-Fix-a-buffer-overread-in-IMA_ADPCM_nib.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7572-Fix-a-buffer-overwrite-in-IMA_ADPCM_de.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7573-CVE-2019-7576-Fix-buffer-overreads-in-.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7578-Fix-a-buffer-overread-in-InitIMA_ADPCM.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7638-CVE-2019-7636-Refuse-loading-BMP-image.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7637-Fix-in-integer-overflow-in-SDL_Calcula.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7635-Reject-BMP-images-with-pixel-colors-ou.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-13616-validate_image_size_when_loading_BMP_files.patch
  patch -Np1 -i ../SDL-1.2.15-CVE-2019-7577-Fix-a-buffer-overread-in-MS_ADPCM_nibb.patch
  patch -Np1 -i ../SDL-1.2.15-Reject-2-3-5-6-7-bpp-BMP-images.patch

  patch -Np1 -i ../sndio.patch

  patch -Np1 -i ../libre.patch

  ./autogen.sh
}

build() {
  cd SDL-$pkgver
  ./configure --prefix=/usr --disable-nasm --enable-alsa \
              --with-x --disable-rpath --disable-static \
              --disable-pulseaudio --disable-pulseaudio-shared \
              --enable-sndio --enable-video-directfb
  make
}

package() {
  cd SDL-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
