# Maintainer (Arch): Kyle Keen <keenerd@gmail.com>
# Contributor (Arch): Mihai Militaru <mihai militaru at xmpp dot ro>
# Contributor (Arch): carstene1ns <arch carsten-teibes.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mbedtls
pkgver=2.16.9
pkgrel=1
pkgdesc='Portable cryptographic and SSL/TLS library, aka polarssl'
arch=('i686' 'x86_64')
url='https://tls.mbed.org'
license=('GPL-2')
depends=('glibc' 'sh')
checkdepends=('python')
makedepends=('cmake')
provides=('polarssl')
replaces=('polarssl')
conflicts=('polarssl')
options=('staticlibs')
source=(https://github.com/ARMmbed/mbedtls/archive/${pkgname}-${pkgver}.tar.gz)
sha512sums=('f72538851c7a24ac14b5c153220260a49a083bfff44a52e9c1e77c51109bac779b5b4caac21f995176fe8f9d27843f3495692d6c7e9dc733cbcec896823ff0e0')

prepare() {
  cd "$pkgname-$pkgname-$pkgver"
  # enable flags for non-embedded systems
  sed -i 's|//\(#define MBEDTLS_THREADING_C\)|\1|' include/mbedtls/config.h
  sed -i 's|//\(#define MBEDTLS_THREADING_PTHREAD\)|\1|' include/mbedtls/config.h
}

build() {
  cd "$pkgname-$pkgname-$pkgver"
  cmake \
    -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DENABLE_TESTING=OFF \
    -DUSE_SHARED_MBEDTLS_LIBRARY=ON \
	-DCMAKE_INSTALL_RPATH=
  make -C build
}

package() {
  cd "$pkgname-$pkgname-$pkgver"
  make -C build DESTDIR="$pkgdir" install

  # rename generic utils
  local _prog _baseprog
  for _prog in "$pkgdir"/usr/bin/*; do
	_baseprog=$(basename "$_prog")
    mv -v "$_prog" "${_prog//$_baseprog/mbedtls_$_baseprog}"
  done

  # fixup static lib permissions
  chmod 644 "$pkgdir"/usr/lib/*.a

  install -Dm644 "gpl-2.0.txt" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
