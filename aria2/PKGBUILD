# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Thomas Dziedzic < gostrc at gmail >
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Alexander Fehr <pizzapunk gmail com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=aria2
pkgver=1.36.0
pkgrel=1
pkgdesc="Download utility that supports HTTP(S), FTP, BitTorrent, and Metalink"
arch=('i686' 'x86_64')
url='https://aria2.github.io/'
license=('GPL-2')
depends=('gnutls' 'libxml2' 'sqlite' 'c-ares' 'ca-certificates' 'libssh2')
checkdepends=('cppunit')
source=("https://github.com/tatsuhiro-t/aria2/releases/download/release-${pkgver}/${pkgname}-${pkgver}.tar.xz")
sha512sums=('8203dbb75274455a78c50dd4f894e631de6931ac889f26896dceed78ec38c98cdbcf07e164744f308f2bfffeae1016beec1bfdbe8cad7f3280d11376aa0c2542')

build() {
  cd $pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --enable-libaria2 \
    --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt

  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  # add bash completion
  install -d "$pkgdir"/usr/share/bash-completion/completions
  install -m644 "$pkgdir"/usr/share/doc/aria2/bash_completion/aria2c \
    "$pkgdir"/usr/share/bash-completion/completions
  rm -rf "$pkgdir"/usr/share/doc/aria2/bash_completion

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
