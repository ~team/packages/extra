# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer (Arch): Bruno Pagani <archange@archlinux.org>
# Contributor (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Contributor (Arch): Douglas Soares de Andrade <dsa@aur.archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=python-matplotlib
pkgver=3.3.4
_debver=$pkgver
_debrel=1
pkgrel=3
pkgdesc="A python plotting library, making publication quality plots"
arch=('i686' 'x86_64')
url='https://matplotlib.org'
license=('custom:matplotlib' 'custom:Bitstream-Vera' 'Modified-BSD' 'Expat' 'CC-BY-4.0')
depends=('freetype2' 'python-cycler' 'python-dateutil' 'python-kiwisolver' 'python-numpy'
         'python-pillow' 'python-pyparsing' 'qhull')
optdepends=('tk: Tk backends'
            'python-pyqt: Qt5 backends'
            'python-pygobject: for GTK3 backend'
            'python-wxpython: WX backend'
            'python-pycairo: {GTK3,Qt5,Tk,WX}Cairo backends'
            'ffmpeg: for saving movies'
            'ghostscript: usetex dependencies'
            'python-certifi: https support')
makedepends=('rsync' 'python-setuptools' 'tk' 'python-pyqt' 'python-pygobject'
             'python-wxpython' 'python-pycairo' 'python-certifi'
             'ghostscript' 'quilt')
source=("https://github.com/matplotlib/matplotlib/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/matplotlib/matplotlib_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e29bcd17ea2b65c26c75421a319d3f21c5b0148c9e61d55d719c191c07342ed19032f97cde269647bd545ed45b5e142483e50caf29ffd421acde5b0b3bfa238b'
            '21a4e08c974a004db2530c40af1c502ea97bebda5bc790b81bde6410b698ec8b250d57662bb8494c7e616c847afa592873566baa934f5c540d1ba5202022a032')

prepare() {
  cd matplotlib-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/20_matplotlibrc_path_search_fix.patch || true

    quilt push -av
  fi

  # use system freetype and qhull
  sed -e 's|#system_freetype = False|system_freetype = True|' -e 's|#system_qhull = False|system_qhull = True|' setup.cfg.template > setup.cfg
}

build() {
  cd matplotlib-${pkgver}
  python setup.py build
}

package_python-matplotlib() {
  cd matplotlib-${pkgver}
  python setup.py install --root "${pkgdir}" --prefix=/usr --optimize=1 --skip-build
  install -Dm644 doc/users/license.rst debian/copyright -t "${pkgdir}"/usr/share/licenses/${pkgname}/
  # needed since https://github.com/matplotlib/matplotlib/pull/14170
  rm -r "${pkgdir}"$(python -c "import site; print(site.getsitepackages()[0])")/{matplotlib,mpl_toolkits}/tests/
}
