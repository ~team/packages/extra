# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Contributor (Arch): Thomas Dziedzic <gostrc@gmail.com>
# Contributor (Arch): Jan "heftig" Steffens <jan.steffens@gmail.com>
# Contributor (Arch): tobias [ tobias at archlinux org ]
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Arch): Eli Schwartz <eschwartz@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=vim
pkgname=('vim' 'gvim' 'vim-runtime')
pkgver=9.1.0707
_versiondir=91
pkgrel=1
pkgdesc="Vi Improved, a highly configurable, improved version of the Vi text editor"
url='https://www.vim.org'
arch=(i686 x86_64)
license=('custom:Vim')
makedepends=('glibc' 'libgcrypt' 'gpm' 'tauthon' 'python' 'ruby' 'libxt' 'gtk' 'lua'
             'gawk' 'tcl' 'pcre' 'zlib' 'libffi' 'gettext-tiny')
source=(https://github.com/vim/vim/archive/v${pkgver}/${pkgbase}-${pkgver}.tar.gz
        vimrc
        hyperbola.vim
        vimdoc.hook
        fix-build.patch)
sha512sums=('694d9b8d08cd9a980c4ea9c8d6510181f1978ee72125bcde02add255057f2f36d8b14f54180e5ad715d8a09911496f79993a612128bed9f2deff3578d4f98660'
            'a81a132d182d1e5cc405bc72777c819b3f4597a36a29228dee78e7fb312eb5da396de23947bdc08f43f8b157211cfdfb528c2cc4831d1a31e9f8a0100bf1ea95'
            '7e3bdfa923664125855b212208d68c76861e962cf6b0d8c2a4d8ed773d9a8cc5a73acd1b7a1e439b4cc0b263a7529067e852d2d53887dfc45f9f66240075c45b'
	    '1e06e981691b17662fd0fddac5c00c87c920d1b4a1cbb6191c42d57cc40b00af12710e26b22fcfc0901bb8142b15f6a04aa65cec2d9b3bb9d5a06cb650d3ab9c'
            '095caacf0a5a464941368c738eca136d5f1e7f5b1cfb274661b95b4648ed4562a897e350fdd691d2a4a887510e9359c45cfab2ec7ee2ffc39a188b1a41546b22')

prepare() {
  cd "${srcdir}"/vim-$pkgver

  # additional fixes for languages
  patch -Np1 -i $srcdir/fix-build.patch

  cd src

  # define the place for the global (g)vimrc file (set to /etc/vimrc)
  sed -E 's|^.*(#define SYS_.*VIMRC_FILE.*").*$|\1|g' -i feature.h
  sed -E 's|^.*(#define VIMRC_FILE.*").*$|\1|g' -i feature.h

  autoconf

  cd "$srcdir"
  cp -a vim-$pkgver gvim-$pkgver
}

build() {
  cd "${srcdir}"/vim-$pkgver
  ./configure \
    --prefix=/usr \
    --localstatedir=/var/lib/vim \
    --with-features=huge \
    --with-compiledby='Hyperbola GNU/Linux-libre' \
    --enable-gpm \
    --enable-acl \
    --with-x=no \
    --disable-gui \
    --enable-multibyte \
    --enable-cscope \
    --enable-netbeans \
    --enable-perlinterp=no \
    --enable-pythoninterp \
    --with-python-command=tauthon2.8 \
    --enable-python3interp=dynamic \
    --enable-rubyinterp=dynamic \
    --enable-luainterp=dynamic \
    --enable-tclinterp=dynamici \
    --disable-canberra
  make

  cd "${srcdir}"/gvim-$pkgver
  ./configure \
    --prefix=/usr \
    --localstatedir=/var/lib/vim \
    --with-features=huge \
    --with-compiledby='Hyperbola GNU/Linux-libre' \
    --enable-gpm \
    --enable-acl \
    --with-x=yes \
    --enable-gui=gtk3 \
    --enable-multibyte \
    --enable-cscope \
    --enable-netbeans \
    --enable-perlinterp=no \
    --enable-pythoninterp \
    --with-python-command=tauthon2.8 \
    --enable-python3interp=dynamic \
    --enable-rubyinterp=dynamic \
    --enable-luainterp=dynamic \
    --enable-tclinterp=dynamic \
    --disable-canberra
  make
}

package_vim-runtime() {
  pkgdesc+=' (shared runtime)'
  optdepends=('sh: support for some tools and macros'
              'python: demoserver example tool'
              'gawk: mve tools upport')
  backup=('etc/vimrc')

  cd "${srcdir}"/vim-$pkgver

  make -j1 VIMRCLOC=/etc DESTDIR="${pkgdir}" install
  # man and bin files belong to 'vim'
  rm -r "${pkgdir}"/usr/share/man/ "${pkgdir}"/usr/bin/

  # Don't forget logtalk.dict
  install -Dm644 runtime/ftplugin/logtalk.dict \
    "${pkgdir}"/usr/share/vim/vim${_versiondir}/ftplugin/logtalk.dict

  # rc files
  install -Dm644 "${srcdir}"/vimrc "${pkgdir}"/etc/vimrc
  install -Dm644 "${srcdir}"/hyperbola.vim \
    "${pkgdir}"/usr/share/vim/vimfiles/hyperbola.vim

  # no desktop files and icons
  rm -r "${pkgdir}"/usr/share/{applications,icons}

  # license
  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}

  # pacman hook for documentation helptags
  install -Dm644 "${srcdir}"/vimdoc.hook "${pkgdir}"/usr/share/libalpm/hooks/vimdoc.hook
}

package_vim() {
  depends=("vim-runtime=${pkgver}-${pkgrel}" 'gpm' 'acl' 'glibc' 'libgcrypt' 'pcre'
           'zlib' 'libffi')
  optdepends=('tauthon: Tauthon language support'
              'python: Python language support'
              'ruby: Ruby language support'
              'lua: Lua language support'
              'tcl: Tcl language support')
  conflicts=('gvim' 'vim-minimal' 'vim-python3')
  provides=('xxd' 'vim-minimal' 'vim-python3' 'vim-plugin-runtime')
  replaces=('vim-python3' 'vim-minimal')

  cd "${srcdir}"/vim-$pkgver
  make -j1 VIMRCLOC=/etc DESTDIR="${pkgdir}" install

  # provided by (n)vi in core
  rm "${pkgdir}"/usr/bin/{ex,view}

  # delete some manpages
  find "${pkgdir}"/usr/share/man -type d -name 'man1' 2>/dev/null | \
    while read _mandir; do
    cd ${_mandir}
    rm -f ex.1 view.1 # provided by (n)vi
    rm -f evim.1    # this does not make sense if we have no GUI
  done

  # Runtime provided by runtime package
  rm -r "${pkgdir}"/usr/share/vim

  # no desktop files and icons
  rm -r "${pkgdir}"/usr/share/{applications,icons}

  # license
  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}
}

package_gvim() {
  pkgdesc+=' (with advanced features, such as a GUI)'
  depends=("vim-runtime=${pkgver}-${pkgrel}" 'gpm' 'libxt' 'gtk' 'glibc' 'libgcrypt' 'pcre'
           'zlib' 'libffi')
  optdepends=('tauthon: Tauthon language support'
              'python: Python language support'
              'ruby: Ruby language support'
              'lua: Lua language support'
              'tcl: Tcl language support')
  provides=("vim=${pkgver}-${pkgrel}" "xxd" 'vim-plugin-runtime')
  conflicts=('vim-minimal' 'vim')
  replaces=('gvim-python3')

  cd "${srcdir}"/gvim-$pkgver
  make -j1 VIMRCLOC=/etc DESTDIR="${pkgdir}" install

  # provided by (n)vi in core
  rm "${pkgdir}"/usr/bin/{ex,view}

  # delete some manpages
  find "${pkgdir}"/usr/share/man -type d -name 'man1' 2>/dev/null | \
    while read _mandir; do
    cd ${_mandir}
    rm -f ex.1 view.1 # provided by (n)vi
  done

  # need to remove since this is provided by vim-runtime
  rm -r "${pkgdir}"/usr/share/vim

  # keep gvim desktop file only, remove vim
  rm "${pkgdir}"/usr/share/applications/vim.desktop

  # license
  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
