# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Arkamririon <Arkamririon@gmx.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=freealut
pkgver=1.1.0
_debver=1.1.0
_debrel=6
pkgrel=1
pkgdesc='OpenAL Utility Toolkit (ALUT)'
url="https://www.openal.org"
arch=('i686' 'x86_64')
license=('LGPL-2')
depends=('openal')
makedepends=('quilt')
source=(https://deb.debian.org/debian/pool/main/f/freealut/freealut_$pkgver.orig.tar.gz
        https://deb.debian.org/debian/pool/main/f/freealut/freealut_$_debver-$_debrel.debian.tar.xz)
sha512sums=('270f74972548b4ac6b98c52c51787ed43c20cf79979063d073bbee7bd08ac4f34c2b579fbf15c09c4e606a5ed38dcd0252f5c46fb3cfe43b727b6b53cf747933'
            '2eb01903a35e52d55e37b0006f3168f320015292713512242e8717c2095f7524c2ffc406ac2c42693a368cc61fd196c8de239891969c2f19a223541d00b50feb')

prepare() {
  cd $srcdir/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/001_freebsd_pthread.diff || true
    rm -v debian/patches/005_am_maintainer_mode.diff || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --disable-static
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
