# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Gustavo Alvarez <sl1pkn07@gmail.com>
# Contributor (Arch): Panagiotis Papadopoulos pano_90 AT gmx DOT net
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libbs2b
pkgver=3.1.0
_debver=3.1.0+dfsg
_debrel=2.2
pkgrel=1
pkgdesc="Bauer stereophonic-to-binaural DSP effect library"
url="http://bs2b.sourceforge.net"
arch=(i686 x86_64)
license=('Expat')
depends=(libsndfile gcc-libs)
makedepends=(quilt)
source=("https://downloads.sourceforge.net/sourceforge/bs2b/$pkgname-$pkgver.tar.lzma"
        "https://deb.debian.org/debian/pool/main/libb/libbs2b/libbs2b_$_debver-$_debrel.debian.tar.xz")
sha512sums=('0192f37344763c582fbe6c6a5349a5694886543b7da8773c90500457b9123c0302fda16f7bbe0d532b5b8b5991a5e71492b1ba01ac6601bd78a355516a34a769'
            '8852a9374ae758f88209021ffddf8cb38db2775be224127a141770ad8214261b6a1bc986405bb3a331f161c57aff9d67dcd456c507f72f5e7c12c20197aa2be7')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -i 's/dist-lzma/dist-xz/g' configure.ac
  autoreconf -fvi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
