# Maintainer (Arch): kleintux <reg-archlinux AT klein DOT tuxli DOT ch> 
# Contributor (Arch): Michael Straube <michael.straubej@gmail.com>
# Contributor (Arch): Loui Chang <louipc dot ist at no spam gmail company>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=tomatoes
pkgver=1.55
_debver=$pkgver
_debrel=10
pkgrel=1
pkgdesc="I Have No Tomatoes - free and libre tomatoes smashing game"
url='https://tomatoes.sourceforge.net'
license=('zlib' 'GPL-2')
arch=('i686' 'x86_64')
depends=('sdl_image' 'sdl_mixer' 'glu')
makedepends=('quilt')
groups=('games')
source=("https://deb.debian.org/debian/pool/main/t/tomatoes/tomatoes_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/t/tomatoes/tomatoes_${_debver}-${_debrel}.debian.tar.xz"
        "${pkgname}.desktop"
        "makefile-compile-flags.patch")
sha512sums=('b16fdfc6ba2777edc53d5b235811f7b25f5f498ddb1dcb148aae0a7e9a15af575ba76e3e0b1e10d9441c830b557f646cee48b80c53d3c95257093e332e5d56e7'
            '3a4e955753be1f76d3fd19625528aab00b66951818ce8e30b42307a4b3ee518403b2dbac5ede2951a7ccaf7cd595053a80eb8d82c1dd7b3ae81260db42263cf1'
            'aad74b21648f1ecc78d4aec1e9d54bc578609f28032b464733a51bcfda2ed8dbfbd36ad1b65df6a08c87b7e1927614fe04a4f941b65f06e9ba51830d0945a85a'
            'e1abac2428864b55d00e7902e7fd026167c1587b29384e08921ea30d34b7478587dedb2a9fe5dcf89f6ee7333f80f024e26e72a09935e850885c8fa056d40085')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/02_version.diff || true
    rm -v debian/patches/08_flags.diff || true

    quilt push -av
  fi

  # additional fixes
  patch -p1 -i $srcdir/makefile-compile-flags.patch
}

build() {
  cd $pkgname-$pkgver

  make \
    MPKDIR=/usr/share/games/$pkgname/ \
    MUSICDIR=/usr/share/games/$pkgname/music/ \
    CONFIGDIR=/usr/share/games/$pkgname/ \
    OVERRIDEDIR=./
}

package() {
  cd $pkgname-$pkgver

  install -d "$pkgdir"/usr/share/games/$pkgname
  cp $pkgname.mpk config.cfg "$pkgdir"/usr/share/games/$pkgname
  install -d "$pkgdir"/usr/share/games/$pkgname/music
  cp music/IHaveNoTomatoes.it "$pkgdir"/usr/share/games/$pkgname/music

  install -Dm755 $pkgname "$pkgdir"/usr/games/$pkgname
  install -Dm644 icon.png "$pkgdir"/usr/share/pixmaps/$pkgname.png
  install -Dm644 "$srcdir"/$pkgname.desktop -t "$pkgdir"/usr/share/applications
  install -Dm644 debian/copyright -t "$pkgdir"/usr/share/licenses/$pkgname
}
