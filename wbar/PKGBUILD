# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=wbar
pkgver=2.3.4
_debver=$pkgver
_debrel=13
pkgrel=2
pkgdesc="Free software light and fast launch bar"
arch=('i686' 'x86_64')
url='https://github.com/rodolf0/wbar'
license=('GPL-3')
depends=('libx11' 'imlib2' 'gcc-libs' 'gtk2' 'libglade')
makedepends=('quilt' 'intltool')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/w/${pkgname}/${pkgname}_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/w/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('07f3484bd071761d57ef1172c56b30980eef9ce2ee65d1fc21ea20c631a1da1d88c1b8a9e1a1fca0e5f18e7bf1c5697c349a543d72966ce71ff53d7e921147b3'
            '7509998879a72c7854ec3481ab125c8d57cda1f1542bd1941a7804b0712215b54cde604e2de577839102a38180eedd105c231b93a7c7ec4951481b75c59efec4')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/01_change_initial_wbar_config || true
    rm -v debian/patches/autoconf-2.70.patch || true
    rm -v debian/patches/configure.ac.patch || true

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd "$pkgname-$pkgver"
  CFLAGS=-Wno-error CXXFLAGS=-Wno-error ./configure \
    --prefix=/usr \
    --sysconfdir=/etc
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="${pkgdir}" PREFIX="/usr" install

  # correct package-structure
  rm -rf "${pkgdir}"/etc/bash_completion.d
  mkdir -p "${pkgdir}"/usr/share/applications/
  mv "${pkgdir}"/etc/wbar.d/wbar.desktop "${pkgdir}"/usr/share/applications/

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
