# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Douglas Soares de Andrade <dsa@aur.archlinux.org>
# Contributor (Arch): Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=python-numpy
pkgver=1.19.4
_debver=1.19.4
_debrel=1
pkgrel=1
pkgdesc="Scientific tools for Python"
arch=('i686' 'x86_64')
license=('Modified-BSD')
url="https://www.numpy.org/"
depends=('cblas' 'lapack' 'python')
optdepends=('python-nose: testsuite'
            'openblas: faster linear algebra')
makedepends=('python-setuptools' 'tauthon-setuptools' 'gcc-fortran' 'python-nose' 'python-cython' 'tauthon-cython')
options=('staticlibs')
source=("$pkgname-$pkgver.tar.gz::https://github.com/numpy/numpy/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/n/numpy/numpy_$_debver-$_debrel.debian.tar.xz")
sha512sums=('369790f3a2dc91156c15f3b6502615ef40a2f0973a80ffdadfcd28a6e7971997d6ba5c146299e76ee0ec1776799a03c6fa185e64b38abea9475c452aebe1e07a'
            '994f40ed6045c50bc0d1a37afa47c8253c9a01b4efddc6a20a5fe667d9b0e984d72d527a3d829eb1f0c8efc3fe67b655629066ee8e52634dda5738bd4fceb27c')

prepare() {
  cd numpy-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0005-Dont-fail-if-we-cant-import-mingw32.patch || true
    rm -v debian/patches/0006-disable-asserts-on-ppc-with-broken-malloc-only-longd.patch || true
    rm -v debian/patches/adapt_swig_docs_to_debian.patch || true
    rm -v debian/patches/debian_skip_tests.patch || true
    rm -v debian/patches/python3-soabi.patch || true
    rm -v debian/patches/sphinx_no_dashW.patch || true
    rm -v debian/patches/use_local_object.inv_files.patch || true

    quilt push -av
  fi
}

build() {
  cd numpy-$pkgver
  python setup.py build
}

package() {
  cd numpy-$pkgver
  python setup.py install --prefix=/usr --root="$pkgdir" --optimize=1

  install -Dm644 LICENSE.txt $pkgdir/usr/share/licenses/$pkgname/LICENSE.txt
}
