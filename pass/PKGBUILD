# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Simon Zimmermann <simon@insmo.com>
# Contributor (Arch): Daniel M. Capella <polyzen@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=pass
pkgver=1.7.4
_debver=$pkgver
_debrel=6
pkgrel=1
pkgdesc="Stores, retrieves, generates and synchronizes passwords securely"
arch=('i686' 'x86_64')
url='https://www.passwordstore.org/'
license=('GPL-2')
depends=('bash' 'gnupg-stable' 'tree')
makedepends=('quilt')
optdepends=('git: for Git support'
            'dmenu: for passmenu'
            'xdotool: to type passwords with passmenu'
            'qrencode: for QR code support'
            'vim-plugin-runtime: for redact_pass.vim'
            'xclip: for clipboard support on X11')
source=("https://git.zx2c4.com/password-store/snapshot/password-store-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/p/password-store/password-store_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('1fe108895b224451b54e545d399b6a97040e094049448509f92ae33164a5cf9044a91f52edfc705fcf333f6526df1a478deeebc4df109764d58100f9e3e22681'
            'a8f89a7116a402f18ddc42e26df8b0f5ca3bb448b53093cf319a4db074c2d6b05c1c087d0531b20a7562c8451b9cb599dd46e32a7436dd7a7f10a4711db563f2')

prepare() {
  cd "${srcdir}/password-store-$pkgver/"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

package() {
  cd "${srcdir}/password-store-$pkgver/"
  make DESTDIR="${pkgdir}" WITH_ALLCOMP=yes install

  install -Dm755 -t "${pkgdir}/usr/bin" contrib/dmenu/passmenu
  install -Dm644 -t "${pkgdir}/usr/share/vim/vimfiles/plugin" contrib/vim/redact_pass.vim

  install -Dm644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname/
}