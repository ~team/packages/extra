# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Gordian Edenhofer <gordian.edenhofer@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=routersploit
pkgver=3.4.0
pkgrel=1
pkgdesc="Free and libre exploitation framework dedicated to embedded devices"
url='https://github.com/threat9/routersploit'
arch=('any')
license=('Modified-BSD')
depends=('python' 'python-future' 'python-requests' 'python-paramiko' 'python-pysnmp' 'python-pycryptodome')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/threat9/routersploit/archive/v${pkgver}.tar.gz"
        "${pkgname}-py38.patch")
sha512sums=('9182598105c4d1c971dc63ac24059469dd1862af204f59994cfc85012091663a85faee4c7c04cce573eef1fa91525b44a3963b8c24460d3b31595c4ef6a6c4e5'
            'b1f81e6bd8bba0a90b3a5bfc57b5ebb690d83e1e404adc49659dffa23c7dbac902815b3a75195902fcf6a4fb7acb1db627f29533d0e8fc60caaaa76f766fb3cb')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i "${srcdir}/${pkgname}-py38.patch"
}

package() {
  cd ${pkgname}-${pkgver}

  python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1
  rm -rf "${pkgdir}"/usr/lib/python*/site-packages/tests
  mv "${pkgdir}/usr/bin/rsf"{.py,}

  install -Dm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r docs/* "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}