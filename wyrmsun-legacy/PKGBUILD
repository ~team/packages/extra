# Mantainer (Arch): MCMic <come@chilliet.eu>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=wyrmsun-legacy
_realpkgname=wyrmsun
pkgver=3.5.4
pkgrel=1
pkgdesc="Real-time strategy game based on history, mythology and fiction (legacy version)"
arch=('i686' 'x86_64')
url='http://andrettin.github.io/'
license=('GPL-2' 'CC0-1.0' 'CC-BY-4.0' 'CC-BY-3.0' 'CC-BY-SA-3.0')
provides=('wyrmsun')
conflicts=('wyrmsun')
depends=('sdl' 'tolua++' 'libvorbis' 'libpng')
makedepends=('cmake' 'boost')
groups=('games')
source=("wyrmsun-${pkgver}.tar.gz::https://github.com/Andrettin/Wyrmsun/archive/v${pkgver}.tar.gz" 
        "wyrmgus-${pkgver}.tar.gz::https://github.com/Andrettin/Wyrmgus/archive/v${pkgver}.tar.gz"
        "oaml-1.0.tar.gz::https://github.com/marcelofg55/oaml/archive/v1.0.tar.gz")
sha512sums=('6041684eb26798a452ec801249768ad8517ecb8a398e5aae9f32a0632908d1285ae379ab877c71f5c6a0f17b39f957960bb6e5a2068221e2686dae041a057614'
            '2cb97152e713edc2eb99e3c21bf7bf399792b87de314a4695985e04227e9ff3e98475dfd1e2e6cdcd24872887fa6a56dbb4a5ccda0ed29a7517039b7dd565bb2'
	    'e8c5631198ac5695880a6e0c5877b0a8447886122f47ac10c6f05c0d16acf61b9e665018dcf00bec70d453f5f0575671b7ddfb554ad1514aacc8b6ef4191c2d9')
_name='Wyrmsun'
_categories='Game;StrategyGame'

prepare() {
  cp -a ${srcdir}/oaml-1.0/* ${srcdir}/Wyrmgus-${pkgver}/src/oaml/
  cd ${srcdir}/Wyrmgus-${pkgver}/src/oaml
  cmake .
  make

  # fixing paths for being fhs-compliant
  sed -i -e 's/BINDIR "bin"/BINDIR "games"/g' ${srcdir}/Wyrmgus-${pkgver}/CMakeLists.txt
  sed -i -e 's/DATADIR share\/wyrmsun/DATADIR share\/games\/wyrmsun/g' \
         -e 's/DESTINATION bin/DESTINATION games/g' \
         ${srcdir}/Wyrmsun-${pkgver}/CMakeLists.txt
}

build() {
  cd ${srcdir}/Wyrmgus-${pkgver}
  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DENABLE_USEGAMEDIR=ON .
  make

  cd ${srcdir}/Wyrmsun-${pkgver}
  cmake \
    -DCMAKE_INSTALL_PREFIX=/usr .
  make
}

package() {
  cd ${srcdir}/
  mkdir -p ${pkgdir}/usr/games/

  cd ${srcdir}/Wyrmgus-${pkgver}
  make DESTDIR="$pkgdir" install
  mv ${pkgdir}/usr/games/stratagus ${pkgdir}/usr/games/wyrmgus

  cd ${srcdir}/Wyrmsun-${pkgver}
  make DESTDIR="$pkgdir" install

  cd ${srcdir}/Wyrmgus-${pkgver}
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
  cd ${srcdir}/Wyrmsun-${pkgver}
  install -Dm644 ./graphics/credits.txt "${pkgdir}/usr/share/licenses/$pkgname/graphics_credits.txt"
  install -Dm644 ./music/credits.txt "${pkgdir}/usr/share/licenses/$pkgname/music_credits.txt"
  install -Dm644 ./sounds/credits.txt "${pkgdir}/usr/share/licenses/$pkgname/sounds_credits.txt"
}
