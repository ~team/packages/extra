# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-fbdev package

pkgname=xorg-video-fbdev-debug
pkgver=0.5.0
pkgrel=3
pkgdesc="X.Org framebuffer video driver"
arch=(i686 x86_64)
license=('X11')
url="https://xorg.freedesktop.org/"
depends=('glibc')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'xenocara-util-macros')
provides=('xf86-video-fbdev')
conflicts=('xf86-video-fbdev' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25')
replaces=('xf86-video-fbdev' 'xf86-video-amdgpu')
groups=('xenocara-video-drivers-debug' 'xenocara-video-debug' 'xenocara-drivers-debug'
        'xorg-video-drivers-debug' 'xorg-video-debug' 'xorg-drivers-debug')
source=(https://xorg.freedesktop.org/releases/individual/driver/xf86-video-fbdev-$pkgver.tar.bz2
        gcc8.patch)
options=(!strip) # It's required for debug packages
sha512sums=('c1217b943bbe3301b3c2a8649ed1004c3c67b02607bd56bbc14f6dfa05e7f0184332c81a6a19595514745501ed88526aee932e555779b7c3a8233646b0979448'
            '82c53039d1f3aa1898f7fb4849a7e9af4ffb518334a5c179f48f90c7bab82138fb0be59d0e41b712e499e176b3ab62f25996b5a6569bd9ddfc94ade567cfcb4a')

prepare() {
  cd "xf86-video-fbdev-$pkgver"
  patch -p0 -i "$srcdir/gcc8.patch"
  autoreconf -vfi
}

build() {
  cd "xf86-video-fbdev-$pkgver"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}
  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

package() {
  cd "xf86-video-fbdev-$pkgver"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
