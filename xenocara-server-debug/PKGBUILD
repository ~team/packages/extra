# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xorg-server package

pkgbase=xenocara-server
pkgname=("${pkgbase}-debug" "${pkgbase}-xephyr-debug" "${pkgbase}-xdmx-debug"
         "${pkgbase}-xvfb-debug" "${pkgbase}-xnest-debug")
_openbsdver=6.9
pkgver=1.20.10
pkgrel=7
arch=('i686' 'x86_64')
license=('X11')
groups=('xenocara-debug' 'xorg-debug')
url="https://www.xenocara.org"
makedepends=('xenocara-proto' 'pixman' 'libx11' 'mesa' 'mesa-libgl' 'libxtrans'
             'libxkbfile' 'libxfont2' 'libpciaccess' 'libxv'
             'libxmu' 'libxrender' 'libxi' 'libxaw' 'libdmx' 'libxtst' 'libxres'
             'xenocara-xkbcomp' 'xenocara-util-macros' 'xenocara-font-util' 'libgcrypt' 'libepoxy'
             'xcb-util' 'xcb-util-image' 'xcb-util-renderutil' 'xcb-util-wm' 'xcb-util-keysyms'
             'libxshmfence' 'libunwind' 'eudev')
options=(!strip) # It's required for debug packages
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/xserver-$pkgver.tar.lz{,.sig}
        xserver-autobind-hotplug.patch
        remove-forced-openbsd-getdtablecount-function-support.patch
        remove-forced-openbsd-mmap-flag-support.patch
        remove-forced-mno-sse-support.patch
        remove-forced-bison-yyparse-function-support.patch
        xvfb-run
        xvfb-run.1
        10-xorg.conf.example)
sha512sums=('a544cebe022891f65e91d1c49f146e0dad5de75018c6261a0f68152c9db9c2f346afefb5a4d97c17c5fb385308d44766380aad77904f7a893cae174777caae61'
            'SKIP'
            'd84f4d63a502b7af76ea49944d1b21e2030dfd250ac1e82878935cf631973310ac9ba1f0dfedf10980ec6c7431d61b7daa4b7bbaae9ee477b2c19812c1661a22'
            'ffefe558d7ab64b4d066872964fbcf474b4b8c454da072d7f65475d44d1d72d6cd879805db3bb72eb27c3ec123d094e5f64fe4cb6af80c0205c1ffa45df568ea'
            '51e37248d1a4967e055219d52957bcc9702dd243da1f937f76524019d4b2c00604e451b50420239ff7e2c51ce523eb5b61d643c8b2c797667672053abd5489eb'
            'a101d5be7c2e774fb1aba9087d7a4583c6ae8900d40ba079fc7ce8592846c20ce24e5834f4c398dca6008f7ada6099c299ab6982c7f036667bc07b17fcf25708'
            'dae6fa8635f8156aa79ce1d8385651eba3f61990ed2fb2c67493fe9d5079e80aaa0f272e31cc587e10eef15dc3be9ad97b862b5ea0897396e05a04068e2c94ac'
            '73c8ead9fba6815dabfec0a55b3a53f01169f6f2d14ac4a431e53b2d96028672dbd6b50a3314568847b37b1e54ea4fc02bdf677feabb3b2697af55e2e5331810'
            'de5e2cb3c6825e6cf1f07ca0d52423e17f34d70ec7935e9dd24be5fb9883bf1e03b50ff584931bd3b41095c510ab2aa44d2573fd5feaebdcb59363b65607ff22'
            '47a60f37f6cb77c588537db750fd340d50d6cb998ab6d4ff3383a5871989a7c18c836c3e4fa1400659006cae4923763b7348d55ea1d28789432fdd693f2a98e2')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/xserver"

  # Patch from Fedora, not yet merged
  patch -Np1 -i "$srcdir/xserver-autobind-hotplug.patch"
  # Remove forced OpenBSD getdtablecount function support
  patch -Np1 -i "$srcdir/remove-forced-openbsd-getdtablecount-function-support.patch"
  # Remove forced OpenBSD mmap flag (__MAP_NOFAULT) support
  patch -Np1 -i "$srcdir/remove-forced-openbsd-mmap-flag-support.patch"
  # Remove forced -mno-sse (SSE extensions disabling) support
  patch -Np1 -i "$srcdir/remove-forced-mno-sse-support.patch"
  # Remove forced Bison yyparse function support
  patch -Np1 -i "$srcdir/remove-forced-bison-yyparse-function-support.patch"

  autoreconf -vfi
}

build() {
  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}
  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  cd "xenocara-$_openbsdver/xserver"
  ./configure --prefix=/usr \
      --enable-ipv6 \
      --enable-dri \
      --enable-dmx \
      --enable-xvfb \
      --enable-xnest \
      --enable-composite \
      --enable-xcsecurity \
      --enable-libunwind \
      --enable-xorg \
      --enable-xephyr \
      --enable-glamor \
      --enable-debug \
      --disable-xwayland \
      --enable-kdrive \
      --enable-config-udev \
      --disable-systemd-logind \
      --disable-suid-wrapper \
      --enable-install-setuid \
      --enable-record \
      --disable-static \
      --libexecdir=/usr/libexec/xorg \
      --sysconfdir=/etc \
      --localstatedir=/var \
      --with-xkb-path=/usr/share/X11/xkb \
      --with-xkb-output=/var/lib/xkb \
      --with-fontrootdir=/usr/share/fonts \
      --with-sha1=libgcrypt \
      --without-systemd-daemon

  make

  # Disable subdirs for make install rule to make splitting easier
  sed -e 's/^DMX_SUBDIRS =.*/DMX_SUBDIRS =/' \
      -e 's/^XVFB_SUBDIRS =.*/XVFB_SUBDIRS =/' \
      -e 's/^XNEST_SUBDIRS =.*/XNEST_SUBDIRS = /' \
      -e 's/^KDRIVE_SUBDIRS =.*/KDRIVE_SUBDIRS =/' \
      -i hw/Makefile
}

package_xenocara-server-debug() {
  pkgdesc="Xenocara X server"
  depends=(libepoxy libxfont2 libbsd pixman xenocara-server-common libunwind mesa-libgl xorg-input-evdev
           libpciaccess libdrm libxshmfence) # FS#52949

  # see xenocara/hw/xfree86/common/xf86Module.h for ABI versions - we provide major numbers that drivers can depend on
  # and /usr/lib/pkgconfig/xorg-server.pc in xenocara-server-devel pkg
  provides=('xenocara-server' 'xorg-server' 'X-ABI-VIDEODRV_VERSION=24.0' 'X-ABI-XINPUT_VERSION=24.1' 'X-ABI-EXTENSION_VERSION=10.0' 'x-server')
  conflicts=('xenocara-server' 'xorg-server' 'nvidia-utils<=331.20' 'glamor-egl' 'xf86-video-modesetting')
  replaces=('xorg-server' 'glamor-egl' 'xf86-video-modesetting' 'xf86-video-amdgpu')
  install=$pkgbase.install

  cd "xenocara-$_openbsdver/xserver"
  make DESTDIR="$pkgdir" install

  # distro specific files must be installed in /usr/share/X11/xorg.conf.d
  install -m755 -d "$pkgdir/etc/X11/xorg.conf.d"
  cp $srcdir/10-xorg.conf.example "$pkgdir/etc/X11/xorg.conf.d"

  rm -rf "$pkgdir/var"

  # remove files, these files are part of "-common" package
  rm -f "$pkgdir/usr/share/man/man1/Xserver.1"
  rm -f "$pkgdir/usr/lib/xorg/protocol.txt"

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # remove files, these files are part of "-devel" package
  rm -rf "$pkgdir/usr/lib/pkgconfig"
  rm -rf "$pkgdir/usr/include"
  rm -rf "$pkgdir/usr/share/aclocal"
}

package_xenocara-server-xephyr-debug() {
  pkgdesc="A nested X server that runs as an X application, provided by Xenocara"
  depends=(libxfont2 mesa-libgl libepoxy libunwind libeudev libxv pixman xenocara-server-common xcb-util-image
           xcb-util-renderutil xcb-util-wm xcb-util-keysyms)
  provides=('xenocara-server-xephyr' 'xorg-server-xephyr')
  conflicts=('xenocara-server-xephyr' 'xorg-server-xephyr')
  replaces=('xorg-server-xephyr')

  cd "xenocara-$_openbsdver/xserver/hw/kdrive"
  make DESTDIR="$pkgdir" install

  install -Dm644 ../../COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

package_xenocara-server-xvfb-debug() {
  pkgdesc="Virtual framebuffer X server, provided by Xenocara"
  depends=(libxfont2 libunwind libeudev pixman xenocara-server-common xenocara-xauth mesa-libgl)
  provides=('xenocara-server-xvfb' 'xorg-server-xvfb')
  conflicts=('xenocara-server-xvfb' 'xorg-server-xvfb')
  replaces=('xorg-server-xvfb')

  cd "xenocara-$_openbsdver/xserver/hw/vfb"
  make DESTDIR="$pkgdir" install

  install -m755 "$srcdir/xvfb-run" "$pkgdir/usr/bin/"
  install -m644 "$srcdir/xvfb-run.1" "$pkgdir/usr/share/man/man1/"

  install -Dm644 ../../COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

package_xenocara-server-xnest-debug() {
  pkgdesc="A nested X server that runs as an X application, provided by Xenocara"
  depends=(libxfont2 libxext libunwind pixman xenocara-server-common libeudev)
  provides=('xenocara-server-xnest' 'xorg-server-xnest')
  conflicts=('xenocara-server-xnest' 'xorg-server-xnest')
  replaces=('xorg-server-xnest')

  cd "xenocara-$_openbsdver/xserver/hw/xnest"
  make DESTDIR="$pkgdir" install

  install -Dm644 ../../COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

package_xenocara-server-xdmx-debug() {
  pkgdesc="Distributed Multihead X Server and utilities, provided by Xenocara"
  depends=(libxfont2 libxi libxaw libxrender libdmx libxfixes libunwind pixman xenocara-server-common)
  provides=('xenocara-server-xdmx' 'xorg-server-xdmx')
  conflicts=('xenocara-server-xdmx' 'xorg-server-xdmx')
  replaces=('xorg-server-xdmx')

  cd "xenocara-$_openbsdver/xserver/hw/dmx"
  make DESTDIR="$pkgdir" install

  install -Dm644 ../../COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
