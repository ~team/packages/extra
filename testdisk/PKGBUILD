# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Michal Krenek <mikos@sg1.cz>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=testdisk
pkgver=7.1
_debver=$pkgver
_debrel=5
pkgrel=2
pkgdesc="Checks and undeletes partitions + PhotoRec, signature based recovery tool"
arch=('i686' 'x86_64')
url="https://www.cgsecurity.org/wiki/TestDisk"
license=('GPL-2')
depends=('libjpeg-turbo' 'libressl' 'reiserfsprogs' 'ntfs-3g' 'qt-base')
makedepends=('quilt' 'qt-tools')
source=("https://www.cgsecurity.org/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/t/testdisk/testdisk_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ad6531eec45c1ed8d4a0ce6132692bb609c4c269fbca57f788ce808214e0b00b5fb1187745a859c5da8a3cb8de18b29904792d3246b15cedfa24bf24cbfe3df5'
            '5bfd89935dc928fd5a8cd992659b79e97f2f78cb5e923b2cd08a9f8dff26e9163673c839fd14e03fa72c5c1ea44f9a5ebd5b419c7258cd7e7dea4059a975c8b8')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  ./configure --prefix=/usr \
    --without-ewf
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
