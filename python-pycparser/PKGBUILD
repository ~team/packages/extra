# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Justin Dray <justin@dray.be>
# Contributor (Arch): Alexander Rødseth <rodseth@gmail.com>
# Contributor (Arch): lang2 <wenzhi.liang@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-pycparser
pkgname=('python-pycparser' 'tauthon-pycparser')
pkgver=2.20
_debver=$pkgver
_debrel=3
pkgrel=3
url='https://github.com/eliben/pycparser'
makedepends=('python-ply' 'tauthon-ply' 'python-setuptools' 'tauthon-setuptools' 'quilt')
arch=('any')
license=('Modified-BSD')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/eliben/pycparser/archive/release_v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/pycparser/pycparser_$_debver-$_debrel.debian.tar.xz")
sha512sums=('d2dc48727750ec0a80d5e3b0c9fba390c3647416a3e18cad59429edb9f55d043c7695c52301b037c93af5d96ffc85fcba7cd54d376abdb89e973759c59fa189d'
            '984a8f14a48303ac140e6ad81c54cd09ef3cf4b40542605021b0278fb02110b832188955efa15431ffec674c074a036042caab975fdee6f624247dbc6d1a2b56')

prepare() {
  cd pycparser-release_v$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cd $srcdir
  cp -a pycparser-release_v$pkgver{,-tauthon}
}

build() {
  cd "$srcdir"/pycparser-release_v$pkgver
  python setup.py build
  cd pycparser
  python _build_tables.py

  cd "$srcdir"/pycparser-release_v$pkgver-tauthon
  tauthon setup.py build
  cd pycparser
  tauthon _build_tables.py
}

package_python-pycparser() {
  pkgdesc='C parser and AST generator written in Python'
  depends=('python-ply')

  cd pycparser-release_v$pkgver

  python setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_tauthon-pycparser() {
  pkgdesc='C parser and AST generator written in Tauthon'
  depends=('tauthon-ply')

  cd pycparser-release_v$pkgver-tauthon

  tauthon setup.py install --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
