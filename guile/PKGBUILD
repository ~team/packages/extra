# Maintainer (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=guile
pkgver=2.2.7
_debver=$pkgver
_debrel=9
pkgrel=2
pkgdesc="Portable, embeddable Scheme implementation written in C"
url='https://www.gnu.org/software/guile/'
arch=('i686' 'x86_64')
license=('GPL-3' 'LGPL-3')
depends=('gmp' 'libtool' 'ncurses' 'texinfo' 'libunistring' 'gc' 'libffi')
makedepends=('quilt')
source=("https://ftp.gnu.org/pub/gnu/$pkgname/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/g/guile-2.2/guile-2.2_${_debver}+1-${_debrel}.debian.tar.xz")
sha512sums=('ad11885ffeb7655ef6c8543e67233992dc37bdcf91ed82188e6a144169c6b7d4e31cf7a6d01509c573d00904cb002719b851f71cdf1359a86de401daf613d773'
            '9f28fe3a380205e5a176523b0206278a35ae39d6a9bb3ef7ed8d4cc0463d82ed3b184d2ef0720a45aad08b1dfc42326a35c34b63af094374223c0209e225bf2e')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0007-Mark-test-out-of-memory-as-an-expected-failure-for-n.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --disable-static  \
    --disable-error-on-warning
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  rm "$pkgdir"/usr/lib/libguile-2.?.so.*-gdb.scm
  for i in COPYING{,.LESSER}; do
    install -Dm644 $i $pkgdir/usr/share/licenses/$pkgname/$i
  done
}
