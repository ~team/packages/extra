# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=swig
pkgver=4.0.2
_debver=4.0.2
_debrel=1
pkgrel=0
pkgdesc="Generate scripting interfaces to C/C++ code"
arch=('i686' 'x86_64')
url="http://www.swig.org/"
license=('GPL-3')
depends=('pcre' 'gcc-libs')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/s/swig/swig_$_debver-$_debrel.debian.tar.xz
        remove-csharp-and-java-support.patch)
sha512sums=('05e7da70ce6d9a733b96c0bcfa3c1b82765bd859f48c74759bbf4bb1467acb1809caa310cba5e2b3280cd704fca249eaa0624821dffae1d2a75097c7f55d14ed'
            'eccea9167f36e692da1b605594c81ef03d4a65602cf50db91128379facd8d3b1ce148f4d5be7b4c68b81c043fe780c9b03815c3f7ba83b3274d468a0e6b7bb43'
            'b4a9bf5c727acb7646abbcdf9de23520ab8a57bda3f07c689c3a15eff1ce54d57fbcbcb552418d3df00b8da349bbcf968fa1e860e9d229a7821a91c6e528c887')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i ../remove-csharp-and-java-support.patch
  ./autogen.sh
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  for i in LICENSE{,-GPL,-UNIVERSITIES}; do
    install -D -m644 ${i} "${pkgdir}"/usr/share/licenses/${pkgname}/${i}
  done
}
