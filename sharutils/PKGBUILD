# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Kevin Piche <kevin@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=sharutils
pkgver=4.15.2
_debver=$pkgver
_debrel=5
pkgrel=1
pkgdesc="Makes so-called shell archives out of many files"
url='https://www.gnu.org/software/sharutils/'
license=('GPL-3')
arch=('i686' 'x86_64')
depends=('perl' 'glibc' 'texinfo')
makedepends=('quilt')
source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/s/sharutils/sharutils_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('80d0b804a0617e11e5c23dc0d59b218bbf93e40aaf5e9a5401a18ef9cb700390aab711e2b2e2f26c8fd5b8ef99a91d3405e01d02cadabcba7639979314e59f8d'
            'SKIP'
            '9194761e95aa7a72ace68a0a045bfa198eef2bf7195b6b99c1c852b62dec706f34a5d78ffd7777446f0b43678a478c61e734ee73dc5f19a297c03fb7e8e32581')
validpgpkeys=('1F967B15DEB2349CACDF3D71D9204CB5BFBF0221') # Bruce Korb <bkorb@gnu.org>

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed 's/FUNC_FFLUSH_STDIN/-1/g' -i lib/fseeko.c
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  CFLAGS+=' -fcommon'
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --infodir=/usr/share/info
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
