# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): kmaster <kmaster12@sasktel.net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=uudeview
pkgver=0.5.20
_debver=$pkgver
_debrel=12
pkgrel=1
pkgdesc="UUDeview helps transmitting & receiving binary files using mail or newsgroups"
arch=('i686' 'x86_64')
url='http://www.fpx.de/fp/Software/UUDeview/'
license=('GPL-2')
makedepends=('quilt')
optdepends=('tk: for GUI')
options=('staticlibs')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/u/uudeview/uudeview_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/u/uudeview/uudeview_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('6c355d01ed2f9b842d348da8630da7e63834edb835861a8cbbf642c73d2600a10a065a626da5bc3993595e3b91dfd8410aba6e85c5daa9c27cf3b612105eed5c'
            'daaceb1ba106c9dc889a396d8c0fe5ee84d58bb85bd762efd39e6489994015d01cc3d168d81eedb1df8245a19ff3d24183dfa9e02360f79cd07361312c4e04e2')

prepare() {
  mv "$pkgname-$pkgver.orig" "$pkgname-$pkgver"
  cd "$srcdir"/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/040_add_shared_lib.diff || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/uudeview-$pkgver/uulib
  autoreconf
  ./configure \
    --prefix=/usr
  make

  cd "$srcdir"/$pkgname-$pkgver
  autoreconf
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man CFLAGS="-ffat-lto-objects"
  make
}

package() {
  install -d "$pkgdir"/usr/{bin,lib/uudeview,include/uudeview}

  cd "$srcdir"/$pkgname-$pkgver
  make prefix="$pkgdir"/usr MANDIR="$pkgdir"/usr/share/man install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"

  cd "$srcdir"/uudeview-$pkgver/uulib
  cp -v *.o "$pkgdir"/usr/lib/uudeview
  cp -v *.a "$pkgdir"/usr/lib/uudeview
  cp -v *.h "$pkgdir"/usr/include/uudeview
}
