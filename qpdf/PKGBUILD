# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): Miguel Revilla <yo at miguelrevilla.com>
# Contributor (Arch): David Sotelo <dvsotelo at gmail.com>
# Contributor (Arch): Nuno Araujo <nuno.araujo@russo79.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=qpdf
pkgver=10.1.0
pkgrel=2
pkgdesc="A PDF Transformation System"
arch=('i686' 'x86_64')
url="https://github.com/qpdf/qpdf"
license=('Artistic-2.0' 'Apache-2.0')
depends=('libjpeg-turbo' 'zlib' 'libressl')
source=(https://github.com/qpdf/qpdf/releases/download/release-${pkgname}-${pkgver}/${pkgname}-${pkgver}.tar.gz{,.asc})
sha512sums=('f9b22d89fc62e430e6232c840ad74d220ac9eb09d7f2dfe2c42c1c943d35b5fd25e2abec9970ac19ad7ad25dc3d95c74e68e50bad395abf815cf71c432233106'
            'SKIP')
validpgpkeys=('C2C96B10011FE009E6D1DF828A75D10998012C7E') # "Jay Berkenbilt <ejb@ql.org>"

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --disable-static \
    --disable-crypto-gnutls \
    --enable-crypto-openssl \
    --disable-implicit-crypto \
    --enable-show-failed-test-output
  make
}

check() {
  cd ${pkgname}-${pkgver}
  make check # passes all
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}/" install

  install -Dm644 completions/bash/qpdf "${pkgdir}/usr/share/bash-completion/completions/qpdf"
  install -Dm644 completions/zsh/_qpdf "${pkgdir}/usr/share/zsh/site-functions/_qpdf"

  for i in Artistic-2.0 LICENSE.txt; do
    install -Dm644 ${i} "${pkgdir}"/usr/share/licenses/${pkgname}/${i}
  done
}
