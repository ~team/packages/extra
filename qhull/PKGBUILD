# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Ronald van Haren <ronald@archlinux.org>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Jason Taylor <jftaylor21@gmail.com>
# Contributor (Arch): Darwin Bautista <djclue917@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=qhull
pkgver=2020.2
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="A general dimension code for computing convex hulls and related structures"
arch=('i686' 'x86_64')
url='http://www.qhull.org/'
license=('custom:Qhull')
depends=('glibc')
makedepends=('cmake' 'quilt')
source=("https://deb.debian.org/debian/pool/main/q/qhull/qhull_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/q/qhull/qhull_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('63d42442f1444ddb09a09680bfdb4d4ffd54295add31f19ec977c1a5bd02a74665f57fdb8503c882720a317cf741c8511b458dcc1ae0930cabe08069ba1496fd'
            '14da9a6580bb2abc7e16cce7249f63290fd023cb93928c687ad7ded4cc0c2e9495dce51802fa5a80d5cb50f21e9d5272068144995e5b96103eb106ffff87393b')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake -B build -S $pkgname-$pkgver \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_C_FLAGS="$CFLAGS -ffat-lto-objects" \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS -ffat-lto-objects"
  cmake --build build
  cmake --build build --target libqhull # deprecated but nevertheless needed
}

package() { 
  DESTDIR="$pkgdir" cmake --install build
  cp -P build/libqhull.so* "$pkgdir"/usr/lib/

  # license
  install -Dm644 "$srcdir"/$pkgname-$pkgver/COPYING.txt -t "$pkgdir"/usr/share/licenses/$pkgname/
}
