# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Ionut Biru  <ibiru@archlinux.ro>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Contributor (Arch): Allan McRae <mcrae_allan@hotmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=geany
pkgver=1.38
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Fast and lightweight IDE"
arch=('i686' 'x86_64')
url='https://www.geany.org/'
license=('GPL-2')
depends=('gtk')
makedepends=('doxygen' 'intltool' 'python-lxml' 'quilt')
optdepends=('geany-plugins: additional functionality'
            'vte: embedded terminal support')
source=("https://download.geany.org/$pkgname-$pkgver.tar.bz2"
        "https://deb.debian.org/debian/pool/main/g/geany/geany_$_debver-$_debrel.debian.tar.xz")
sha512sums=('5aff0643d0d9e667d0fb526e9c8426cb80c89decf92110683086c2a7a18e0d3d55ab514fdd68aa0c4431c98ae0c4685607b49c620525184c89e21b26fca06699'
            'cb3fdb501cb5f1b900ea7262dddedebdb464666dd8ef80c15051579b814d264089c020fc9503399eae05cd918579d57fa6208398199871ef945aae90a0d0c965')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/20_change_gpl_location.patch || true
    rm -v debian/patches/20_add_debian_specific_filetypes.patch || true
    rm -v debian/patches/20_debian_control_tags.patch || true
    rm -v debian/patches/20_use_sensible_browser.patch || true
    rm -v debian/patches/20_load_plugins_from_all_paths.patch || true
    rm -v debian/patches/20_use_x_terminal_emulator.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --enable-gtk3 --enable-gtkdoc-header --prefix=/usr
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  make -C $pkgname-$pkgver DESTDIR="$pkgdir" install
  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
