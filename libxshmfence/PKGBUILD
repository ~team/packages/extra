# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=libxshmfence
_openbsdver=6.9
pkgver=1.3
pkgrel=3
pkgdesc="a library that exposes an event API on top of Linux kernel futexes, provided by Xenocara"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('Expat')
depends=('glibc')
makedepends=('xenocara-util-macros' 'xenocara-proto')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxshmfence-$pkgver.tar.lz{,.sig}
        remove-forced-openbsd-futex-support.patch)
sha512sums=('5b717aafa3b2443f60c75d20d534abf6b52a10a8de7520e04290eede6ad41cede44872ddf23739f464b9e30029341454e7c5c8e717859899cbe94cb9a66f186c'
            'SKIP'
            'f32ed726960475fba1c2c253f2f9d9e8d65bc562410c9db26c79f4645f70a32eeb1c5f5e6a0f270364cb1e79819cca99a9ffac6383e4c86fc298081442defc81')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  patch -p1 -i "$srcdir/remove-forced-openbsd-futex-support.patch"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  ./configure --prefix=/usr
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  make -k check
}

package() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
