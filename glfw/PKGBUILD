# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor (Arch): Mihai Militaru <mihai.militaru@ephemeros.org>
# Contributor (Arch): Christoph Siegenthaler <csi@gmx.ch>
# Contributor (Arch): lindquist <tomas@famolsen.dk>
# Contributor (Arch): philefou <tuxication AT gmail DOT com>
# Contributor (Arch): SpepS <dreamspepser at yahoo dot it>
# Maintainer: Jayvee Enaguas <harvettfox96@dismail.de>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=glfw
pkgname=('glfw' 'glfw-doc')
_pkgname=${pkgbase}
pkgver=3.3.2
pkgrel=2
arch=('x86_64' 'i686')
url='https://www.glfw.org/'
license=('zlib')
makedepends=('cmake' 'doxygen' 'extra-cmake-modules' 'libxcursor' 'libxi' 'libxinerama' 'libxkbcommon' 'libxrandr')
source=("${_pkgname}-${pkgver}.tar.gz::https://github.com/${_pkgname}/${_pkgname}/archive/${pkgver}.tar.gz")
sha512sums=('f5af749d33b5b900ccf07988ad0bf51bd766a18e4cf2bc2a76020c88e98a2528ff1b965224184fe0d290cfe34b1af1e6f633600660d81194fe354078e2f35c56')

prepare() {
  cd ${_pkgname}-${pkgver}

  # remove the date from the documentation footer.
  sed -i 's#Last update on \$date for##' docs/footer.html
  # remove the line of "Requires.private" from the package config file.
  sed -i '10d' src/${pkgbase}3.pc.in
}

build() {
  cmake \
    -S ${_pkgname}-${pkgver} \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DBUILD_SHARED_LIBS=ON
  make -C build VERBOSE=1 docs
}

package_glfw() {
  pkgdesc="A portable, free and libre framework for graphical application development"
  depends=('mesa-libgl' 'libxcursor' 'libxi' 'libxinerama' 'libxrandr')

  make -C build DESTDIR=${pkgdir} VERBOSE=1 install
  install -Dvm644 ${srcdir}/${_pkgname}-${pkgver}/LICENSE.md -t ${pkgdir}/usr/share/licenses/${pkgname}
}

package_glfw-doc() {
  pkgdesc="Documentation files for GLFW"
  depends=('glfw')

  install -dv ${pkgdir}/usr/share/doc
  cp -rv build/docs/html ${pkgdir}/usr/share/doc/${_pkgname}
  install -Dvm644 ${srcdir}/${_pkgname}-${pkgver}/LICENSE.md -t ${pkgdir}/usr/share/licenses/${pkgname}
}