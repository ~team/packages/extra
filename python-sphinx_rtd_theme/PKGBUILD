# Maintainer (Arch): Johannes Löthberg <johannes@kyriasis.com>
# Contributor (Arch): eolianoe <eolianoe [at] gmail [DoT] com>
# Contributor (Arch): Romain Schmitz <slopjong .at. slopjong .dot. de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=python-sphinx_rtd_theme
pkgver=0.5.0
_debver=0.5.0+dfsg
_debrel=1
pkgrel=1
pkgdesc="Python Sphinx Read The Docs Theme"
url='https://github.com/rtfd/sphinx_rtd_theme/'
arch=('any')
license=('Expat')
makedepends=('python-setuptools' 'quilt')
source=("https://pypi.org/packages/source/s/sphinx_rtd_theme/sphinx_rtd_theme-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/sphinx-rtd-theme/sphinx-rtd-theme_$_debver-$_debrel.debian.tar.xz")
sha512sums=('73b32e8402be59dafb86deb679628a5b8bd0295a95dc8b67ec1990faef1faad64b67b44101c13ca058480beb0ca5c10f859ecc8ab166eabe891eee09df2fe805'
            'a67f823406f6fb54e9f3b0fbac8386eaecf20fd2f3347865d62c10a0a8fd01bdc72c396de52a4082047c313a375b1b327d70094221910bdf68e57230d54873b1')

prepare() {
  cd sphinx_rtd_theme-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/fonts-only-ttf-woff2.patch || true
    rm -v debian/patches/replace-webpack-imports.patch || true

    quilt push -av
  fi
}

build() {
  cd sphinx_rtd_theme-$pkgver
  python setup.py build
}

package() {
  cd sphinx_rtd_theme-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1

  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
