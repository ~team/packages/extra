# Maintainer (Arch): Andrew Sun <adsun701 at gmail dot com>
# Contributor (Arch): Florian Pritz <bluewind at xinu dot at>
# Contributor (Arch): Jelle van der Waa <jelle@ at vdwaa dot nl>
# Contributor (Arch): Hugo Osvaldo Barrera <hugo at barrera dot io>
# Contributor (Arch): Matthias Maennich <arch at maennich dot net>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=shiboken
pkgname=('python-shiboken' 'tauthon-shiboken' 'shiboken')
pkgver=1.2.4
pkgrel=1
arch=('i686' 'x86_64')
license=('GPL-2' 'LGPL-2.1')
url='http://www.pyside.org'
makedepends=('cmake' 'tauthon' 'python' 'qt4' 'libxslt')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/PySide/Shiboken/archive/$pkgver.tar.gz"
        "support-new-python.diff")
sha512sums=('daa3fadf3daffaec52f199c0285a37431a4b6b0d450a43a035f49e5505a35b666a1cb0b334c7267af7530841dadbf0b97296812141c93de3b7cd07c7d9016a2a'
            'e14854a6119bed1824d7e18daf235c4d000a1519abd0912fa68cd06560a60cc9b3b2c4877ace7ce6ac32f301383ae2cc047a24ad3c190aae2e2de1c01405ad46')

prepare() {
  cd "$srcdir/Shiboken-$pkgver"
  patch -p1 -i "$srcdir/support-new-python.diff"
}

build() {
  cd "$srcdir/Shiboken-$pkgver"

  # build tauthon
  mkdir -p build-py2 && cd build-py2
  _ver2=$(tauthon -c "import platform; print(platform.python_version())")
  cmake ../ -DCMAKE_INSTALL_PREFIX=/usr \
            -DBUILD_TESTS=OFF \
            -DPYTHON_EXECUTABLE=/usr/bin/tauthon \
            -DPYTHON_INCLUDE_DIR=$(tauthon -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
            -DPYTHON_LIBRARY=$(tauthon -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") \
            -DQT_QMAKE_EXECUTABLE=qmake-qt4
  make

  # build python
  cd "$srcdir/Shiboken-$pkgver"
  mkdir -p build-py3 && cd build-py3

  _ver3=$(python -c "import platform; print(platform.python_version())")
  declare -a _extra_library_opt
  declare -a _extra_include_opt
  if [[ ${_ver3//./} -lt 380 ]]; then
    _includedir=/usr/include/python${_ver3%.*}m
    _library=/usr/lib/libpython${_ver3%.*}m.so
    _extra_library_opt+=("-DPYTHON3_LIBRARIES=${_library}")
    _extra_include_opt+=("-DPYTHON3_INCLUDE_DIRS=${_includedir}")
  else
    _includedir=/usr/include/python${_ver3%.*}
    _library=/usr/lib/libpython${_ver3%.*}.so
    _extra_library_opt+=("-DPYTHON3_LIBRARY=${_library}")
    _extra_include_opt+=("-DPYTHON3_INCLUDE_DIR=${_includedir}")
  fi

  cmake ../ -DCMAKE_INSTALL_PREFIX=/usr \
            -DBUILD_TESTS=OFF \
            -DUSE_PYTHON3=yes \
             "${_extra_library_opt[@]}" \
             "${_extra_include_opt[@]}" \
             -DQT_QMAKE_EXECUTABLE=qmake-qt4
  make
}

package_shiboken() {
  pkgdesc="CPython bindings generator for C++ libraries"
  depends=('python' 'qt4' 'libxslt')
  optdepends=('tauthon-shiboken: for compilation against tauthon'
              'python-shiboken: for compilation against python')

  # Header files, /usr/bin/shiboken, pkgconfig, man page
  cd "$srcdir/Shiboken-$pkgver/build-py3"
  make DESTDIR="$pkgdir" install

  cd "$srcdir/Shiboken-$pkgver/build-py2"
  cd data
  install -Dm 644 ShibokenConfig-python2.8.cmake "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/"
  install -Dm 644 shiboken.pc "$pkgdir/usr/lib/pkgconfig/shiboken-py2.pc"

  rm -rf "$pkgdir/usr/lib/python"*
  rm -rf "$pkgdir/usr/lib/libshiboken"*
  rm -rf "$pkgdir/usr/lib/pkgconfig/"
  rm "$pkgdir"/usr/lib/cmake/Shiboken-$pkgver/ShibokenConfig*python*.cmake

  install -Dm644 "$srcdir/Shiboken-$pkgver/"COPYING{,.libsample,.libshiboken} -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_tauthon-shiboken() {
  pkgdesc="Support library for Tauthon bindings"
  depends=("qt4>=4.8" 'libxslt' 'tauthon' 'shiboken')

  cd "$srcdir/Shiboken-$pkgver/build-py2"
  make DESTDIR="$pkgdir" install

  cd "$srcdir/Shiboken-$pkgver/build-py2"
  cd data
  install -Dm 644 ShibokenConfig-python2.8.cmake "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/"
  mv "$pkgdir"/usr/lib/pkgconfig/shiboken{,-py2}.pc

  rm -rf "$pkgdir"/usr/{include,bin,share}
  rm "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/ShibokenConfigVersion.cmake"
  rm "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/ShibokenConfig.cmake"

  install -Dm644 "$srcdir/Shiboken-$pkgver/"COPYING{,.libsample,.libshiboken} -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_python-shiboken() {
  pkgdesc="Support library for Python bindings"
  depends=("qt4>=4.8" 'libxslt' 'python' 'shiboken')

  cd "$srcdir/Shiboken-$pkgver/build-py3"
  make DESTDIR="$pkgdir" install

  rm -rf "$pkgdir"/usr/{include,bin,share}
  rm "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/ShibokenConfigVersion.cmake"
  rm "$pkgdir/usr/lib/cmake/Shiboken-$pkgver/ShibokenConfig.cmake"

  install -Dm644 "$srcdir/Shiboken-$pkgver/"COPYING{,.libsample,.libshiboken} -t "${pkgdir}/usr/share/licenses/$pkgname"
}
