# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Tobias Kieslich <tobias@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=obconf
pkgver=2.0.4
pkgrel=1
pkgdesc="A GTK+ based configuration tool for Openbox"
arch=('x86_64' 'i686')
url="http://openbox.org/wiki/ObConf:About"
license=('GPL-2')
depends=('gtk' 'openbox')
makedepends=('intltool' 'gettext-tiny')
source=("http://openbox.org/dist/${pkgname}/${pkgname}-${pkgver}.tar.gz"{,.asc}
	"${pkgname}.1"
	"02-update-russian-translate.patch"
	"add-missing-keywords-and-translations.patch"
	"stop-using-libglade.patch"
	"switch-to-gtk3.patch")
sha512sums=('b8cf6f363fa8c2f1d21142a3f79687a72f683f564285b05751c822ddf7a421b09c3440cbc5b08eb09d151601c6ada835ff5cc940bf5abeec2210cca26b552294'
	    'SKIP'
	    'ad4676f8e5e4918680c20634bc8e3918f764cbc1eeb051dd638ca6e03cda95a00cbc284929c9875f2e85b0014b877d1bb186a8418896037c511f91f4bcefeadd'
	    'fb99344da7ff3f424167d46a505f953147c2077371bbd1f8e81e247df4731ef8ec63da2faf755fd8147807b5eed9b75b7b986be60bd24649627344ca12cf184b'
	    'a5e349ab368c58b4fc512840417719ff0a6554bdf762f02b7a40280ad030e23eabedf50a79f2fad38d6dff5a09ff3fb41abf3ae2b8dd664e957a13544ec70cb6'
	    'ea181c2be8873794ed6cb6d4b8834100c4b81f5c5bf7cbb53aa83a91f2135ad30174c0bcd6aea3c2269aa655a13f2ffb2a010f9d70fc2ab8394dd79f759e9b33'
	    '5371f622aa8e1ed21454873bc1b055534edaa289c98303a4090cc90407413ed0ed394167758b2a0c7bf309b15480d2ccd6f1d683778a5a562095e224f892c9b4')
validpgpkeys=('1FEECBB03AB7D3216B5E0AC9274E52855D203EC3')	# Dana Jansens <danakj@orodu.net>

prepare() {
  cd ${pkgname}-${pkgver}

  # Stop using libglade, and switch to GTK3.
  patch --verbose -p1 -i ../stop-using-libglade.patch
  patch --verbose -p1 -i ../switch-to-gtk3.patch
  # Apply patch adding missing keywords and translated names and
  # comments into the app shortcut.
  patch --verbose -p1 -i ../add-missing-keywords-and-translations.patch
  # Apply patch updating Russian translation.
  patch --verbose -p1 -i ../02-update-russian-translate.patch
  # Apply patch fixing minor typo.
  sed -i 's#occured#occurred#' src/tree.c
  autoreconf -fiv
}

build() {
  cd ${pkgname}-${pkgver}

  ./configure --prefix=/usr
  make VERBOSE=1
  # Compress manual file into gzip.
  cp -v ../${pkgname}.1 . && gzip -v9 ${pkgname}.1
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR=${pkgdir} VERBOSE=1 install
  install -Dvm644 ${pkgname}.1.gz -t ${pkgdir}/usr/share/man/man1
  install -Dvm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
