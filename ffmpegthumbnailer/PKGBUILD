# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): boromil@gmail.com
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ffmpegthumbnailer
pkgver=2.1.1
_debver=$pkgver
_debrel=0.2
pkgrel=3
pkgdesc="Lightweight video thumbnailer that can be used by file managers and other applications"
url='https://github.com/dirkvdb/ffmpegthumbnailer'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('ffmpeg' 'libjpeg-turbo' 'libpng')
makedepends=('cmake' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/dirkvdb/$pkgname/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/f/ffmpegthumbnailer/ffmpegthumbnailer_${_debver}-${_debrel}.debian.tar.xz"
        "ffmpegthumbnailer.thumbnailer")
sha512sums=('9c16663884ae068776992ad62e76be443b4914b34870b0cea256c12b15f56f7c26b1e9a99c1ab91b207d18c7c17bb02fbdccba8ed99e843f02fae7ecdd4ac9bd'
            '79ffb9c2d2140eee6d1c1f2818d3d5851f57260781188fc0b763ef4f3bcfc7478b84f9fc4442e0767cefb97ea306946afaad060e00a60be1fc49c82366b3d112'
            'a5087a618fc583923b082bcf412b50cf2ea006e5decf0f65831c34acfb5ce5e0b44deb41ad4ae6d50c2d042776a1c0577120b70bf7b950b563a503d287955305')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  cmake -B build \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_INSTALL_LIBDIR=lib \
	-DENABLE_GIO=OFF \
	-DENABLE_THUMBNAILER=ON
  make -C build
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make -C build DESTDIR="${pkgdir}" install

  # using modified version for more supported formats
  rm "${pkgdir}"/usr/share/thumbnailers/ffmpegthumbnailer.thumbnailer
  cp "${srcdir}"/ffmpegthumbnailer.thumbnailer "${pkgdir}"/usr/share/thumbnailers

  # license
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
