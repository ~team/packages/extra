# Maintainer (Arch): Kr1ss <kr1ss+x-yandex+com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=getmail6
pkgver=6.18.11
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="POP3 mail retriever with reliable Maildir and command delivery"
arch=('any')
url='https://getmail6.org/'
license=('GPL-2')
conflicts=('getmail')
makedepends=('python' 'python-setuptools' 'quilt')
depends=('python-chardet')
source=("$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/g/getmail6/getmail6_$_debver-$_debrel.debian.tar.xz")
sha512sums=('67fa7db20ab28b9e744922c96e05eee934c33e02bcfdff1ae621231a7017c427fbffe209c0d28f3004a5d2ad212c9ccaeada09f57a20bd98fbd5c4c9808a18bf'
            '344b74cb11697f77feff41389bee78762cb2422a82e76b38cc8730ad9876c9f0b19762c74c4a60d0dd4c81aa58718663936e54c9e1f67cedd59e120ceed5b708')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"
  python setup.py build
}

package() {
  cd "$pkgname-$pkgver"
  PYTHONHASHSEED=0 python setup.py install --skip-build --optimize=1 --root="$pkgdir"
  install -Dm644 "./docs/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
