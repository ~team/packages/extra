# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Maintainer (Arch): Bruno Pagani <archange@archlinux.org>
# Contributor (Arch): Sébastien Luttringer
# Contributor (Arch): parchd <parchd@archlinux.info>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=opensmtpd
pkgver=6.8.0p2
pkgrel=3
pkgdesc="Free and libre implementation of the server-side SMTP protocol"
arch=('i686' 'x86_64')
url='https://www.opensmtpd.org/'
license=('Original-BSD' 'Modified-BSD' 'Simplified-BSD' 'ISC' 'Public-Domain')
depends=('libevent' 'libressl' 'libxcrypt' 'zlib')
optdepends=('opensmtpd-filter-rspamd: rspamd integration'
            'opensmtpd-filter-senderscore: senderscore integration')
provides=('smtp-server' 'smtp-forwarder')
conflicts=('smtp-server' 'smtp-forwarder')
install=${pkgname}.install
backup=('etc/smtpd/smtpd.conf'
        'etc/smtpd/aliases')
options=(emptydirs)
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/o/opensmtpd/${pkgname}_${pkgver}.orig.tar.gz"
        "smtpd.initd"
        "smtpd.run")
sha512sums=('48f152b75575146fdd09bdf47123041ea62fefb6e5de33a69826bf91a2126a918f8db1caffadb2f142a1a21de8126d492de88cb65bdf169e61c0b22d3e78d290'
            'be4f146ba35d5a362d946a5e96817675316a2f5f8b80ee88ca77cc239c8d9c9c11839c88dae41eee9fd32b6b3813e90feb90727fb2b1650b005ea53c6adf3658'
            'a360b44d60538c3c73fad575cb6cbbc23720f402c97071da443c6a6c0d780290f594584fa59576473aa5bf77dc01b3e91835345e62bb68f3894beeb742efb4bd')

prepare() {
  sed -ri 's,/etc/mail,/etc/smtpd,g' ${pkgname}-${pkgver}/usr.sbin/smtpd/smtpd.conf
}

build() {
  cd ${pkgname}-${pkgver}

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc/smtpd \
    --libexecdir=/usr/libexec/smtpd \
    --with-path-mbox=/var/mail \
    --with-path-empty=/var/empty \
    --with-path-socket=/run \
    --with-path-CAfile=/etc/ssl/certs/ca-certificates.crt \
    --with-user-smtpd=smtpd \
    --with-user-queue=smtpq \
    --with-group-queue=smtpq \
    --with-cflags='-I/usr/include/libressl -L/usr/lib/libressl -Wl,-rpath=/usr/lib/libressl'

  make
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install SMTPD_QUEUE_USER=92

  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/sendmail
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/mailq
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/newaliases
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/makemap

  install -Dm644 etc/aliases -t  "${pkgdir}"/etc/smtpd/

  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}/

  install -Dm 755 "${srcdir}/smtpd.initd" "${pkgdir}/etc/init.d/smtpd"
  install -Dm 755 "${srcdir}/smtpd.run" "${pkgdir}/etc/sv/smtpd/run"
}
