# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# Based on xf86-input-wacom package

pkgname=xorg-input-wacom
pkgver=0.39.0
pkgrel=1
pkgdesc="X.Org Wacom tablet driver"
arch=(i686 x86_64)
url="https://linuxwacom.github.io/"
license=(GPL-2)
depends=(libxi libxinerama libxrandr libeudev)
makedepends=('xenocara-server-devel' 'X-ABI-XINPUT_VERSION=24.1' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-input-wacom')
conflicts=('xf86-input-wacom' 'xenocara-server<1.20' 'X-ABI-XINPUT_VERSION<24.1' 'X-ABI-XINPUT_VERSION>=25')
replaces=('xf86-input-wacom')
source=(https://github.com/linuxwacom/xf86-input-wacom/releases/download/xf86-input-wacom-$pkgver/xf86-input-wacom-$pkgver.tar.bz2{,.sig})
sha512sums=('9ad92c86c4ba3587d68e2107057c89dfe8628c0a2ec882f5a424ab4983c18ff6048489d7f6d3a8de87403744f74de1982de25327fc955bb5c21346a242e0aaa3'
            'SKIP')
validpgpkeys=('9A12ECCC5383CA2AF5B42CDCA6DC66911B2127D5') # Jason Gerecke <killertofu@gmail.com>
validpgpkeys+=('3C2C43D9447D5938EF4551EBE23B7E70B467F0BF') #  "Peter Hutterer (Who-T) <office@who-t.net>"
validpgpkeys+=('5222AA87620F928D2C16F62BDB4ABF7C3424190B') # "Aaron Armstrong Skomra <skomra@gmail.com>"
validpgpkeys+=('FBE078781106933D3DDCF93E5B4EA609784983CA') # "Jason Gerecke <jason.gerecke@wacom.com>"

prepare() {
  cd "xf86-input-wacom-$pkgver"
  autoreconf -vfi
}

build() {
  cd "xf86-input-wacom-$pkgver"
  ./configure --prefix=/usr --with-udev-rules-dir=/lib/udev/rules.d
  make
}

package() {
  cd "xf86-input-wacom-$pkgver"
  make DESTDIR="$pkgdir" install

  install -Dm644 GPL "$pkgdir/usr/share/licenses/$pkgname/GPL"
}
