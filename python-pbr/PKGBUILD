# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Daniel Wallace <danielwallace at gtmanfred dot com>
# Contributor (Arch): Limao Luo <luolimao+AUR@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-pbr
pkgname=('python-pbr' 'tauthon-pbr')
pkgver=5.5.0
_debver=$pkgver
_debrel=2
pkgrel=2
arch=('any')
url='https://pypi.python.org/pypi/pbr'
license=('Apache-2.0')
makedepends=('tauthon-setuptools' 'python-setuptools' 'git' 'quilt')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/openstack-dev/pbr/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-pbr/python-pbr_$_debver-$_debrel.debian.tar.xz")
sha512sums=('26ac1e67341e1e4388e93a64fbb3c7f4d94c700ae95c265a34542e867be38c91cb07db311df637dfc6f63a660a5b49cb8ef3c1f527f851ea76127416e7224979'
            'ae4e1d7a6e1b8c96b91b8490304ef3518a96b0ff37e1f440d92a903ded875f81d9433490ca8359118e8c8a4beb93b4c15abf27c805e3e7d2cce80dac9b96de78')

export PBR_VERSION=$pkgver

prepare() {
  cd pbr-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cd $srcdir
  cp -a pbr-$pkgver{,-tauthon}

  find pbr-$pkgver-tauthon -name \*.py -exec sed -i '1s/python$/tauthon/' {} +
}

build() {
  cd "$srcdir"/pbr-$pkgver
  python setup.py build

  cd "$srcdir"/pbr-$pkgver-tauthon
  tauthon setup.py build
}

package_python-pbr() {
  pkgdesc="Python Build Reasonableness"
  depends=('python-setuptools')

  cd pbr-$pkgver
  python setup.py install -O1 --root="$pkgdir"
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}

package_tauthon-pbr() {
  pkgdesc="Tauthon Build Reasonableness"
  depends=('tauthon-setuptools')

  cd pbr-$pkgver-tauthon
  tauthon setup.py install -O1 --root="$pkgdir"
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname

  mv "$pkgdir"/usr/bin/pbr{,-tauthon}
}
