# Maintainer (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): detto <detto-brumm@freenet.de>
# Contributor (Arch): Roman Kyrylych <Roman.Kyrylych@gmail.com>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jayvee Enaguas <harvettfox96@dismail.de>

pkgname=gtk2-engine-murrine
_pkgname=${pkgname: -7}
pkgver=0.98.2
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc="GTK2 engine to make your desktop look like a 'murrina', an italian word meaning the art glass works done by Venicians glass blowers"
arch=('i686' 'x86_64')
url='https://www.gnome-look.org/p/1080239/'
license=('LGPL-2.1')
depends=('gtk2')
makedepends=('intltool' 'gettext-tiny' 'quilt')
provides=('gtk-engine-murrine')
conflicts=('gtk-engine-murrine')
source=("https://ftp.gnome.org/pub/GNOME/sources/${_pkgname}/${pkgver::-2}/${_pkgname}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/g/gtk2-engines-murrine/gtk2-engines-murrine_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('5ed6b499b957c33d301c7ac508937517e8f9753fabb84e0cf977f67b10b09968cee5a10bdeae64a05f64606b5769e7313ededbb3b0c3ae9f78e34f893aa96667'
            '021cd306f539e7d8c74556d8f7351c6e15678b0e7a68263017b96fb793564cafa488ee326880910cf6a89199cb2815d2d65f8e1b2a2a9561874e764da9f8b6a7')

prepare() {
  mv {${_pkgname},${pkgname}}-${pkgver} && cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -i 's#libmurrine_la_LIBADD =.*\$(pixman_LIBS)#& -lm#' Makefile.am
}

build() {
  cd ${pkgname}-${pkgver}

  ./configure \
    --prefix=/usr \
    --enable-animation \
    --enable-animationrtl
  sed -i 's#\\$CC -shared#& -Wl,-O1,--as-needed#' libtool
  make
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR=${pkgdir} install
  install -Dm644 COPYING.2.1 -t ${pkgdir}/usr/share/licenses/${pkgname}
}
