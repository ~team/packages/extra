# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Jens Adam <jra@byte.cx>
# Contributor (Arch): Arch3y <arch3y[at]archstrike[dot]org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=zmap
pkgver=2.1.1
_debver=$pkgver
_debrel=2
pkgrel=4
pkgdesc="Fast network scanner designed for Internet-wide network surveys"
url='https://zmap.io/'
arch=('i686' 'x86_64')
license=('Apache-2.0')
depends=('gmp' 'json-c' 'libpcap')
makedepends=('byacc' 'cmake' 'pkg-config' 'gengetopt' 'quilt')
backup=('etc/zmap/blacklist.conf'
        'etc/zmap/zmap.conf')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/z/zmap/zmap_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/z/zmap/zmap_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('9d62dceff77fa20ff5945516b0766340366175947130bb844f19c11dac7676e658bcdedd6be29ffc945706aa3cdc0e4ac207bd127b618592769a4d3e75892857'
            'a31ef53676df3e4038ae6bbd9b3c16b9e12f41fc327e8ebea1e1c19eae02b482382de695a41d63d5d07d4276d623261ca3c7bbda6659882fb884e463c80a4f0c')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  cmake \
    -Bbuild \
    -DWITH_JSON=ON \
    -DWITH_REDIS=OFF \
    -DWITH_MONGO=OFF
  make -C build
}

package() {
  cd "${pkgname}-${pkgver}"
  install -Dm 755 build/src/{zmap,ztee,zblacklist} -t "${pkgdir}/usr/bin"
  install -Dm 644 src/*.1 -t "${pkgdir}/usr/share/man/man1"
  install -Dm 644 conf/* -t "${pkgdir}/etc/zmap"
  install -Dm 644 AUTHORS CHANGELOG.md INSTALL.md README.md -t "${pkgdir}/usr/share/doc/zmap"
  cp -a examples "${pkgdir}/usr/share/doc/zmap"
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"
}
