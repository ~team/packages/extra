# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Douglas Soares de Andrade <dsandrade@gmail.com>
# Contributor (Arch): Michael Fellinger <m.fellinger@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=sqlitebrowser
pkgver=3.12.1
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="SQLite Database browser is a light GUI editor for SQLite databases, built on top of Qt"
url='https://sqlitebrowser.org/'
arch=('i686' 'x86_64')
license=('GPL-3')
depends=('qt-base' 'hicolor-icon-theme' 'desktop-file-utils' 'qscintilla')
makedepends=('quilt' 'cmake' 'ninja' 'qt-tools')
source=("https://deb.debian.org/debian/pool/main/s/sqlitebrowser/sqlitebrowser_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/sqlitebrowser/sqlitebrowser_${_debver}-${_debrel}.debian.tar.xz"
        "libre.patch")
sha512sums=('f0a62958008693606d9ab04b0413fa10705c7b45fb970c218d8f1adfe8ca01895a6470f369ded6cb7a9338c27385838b8d57c89b5855b4050ce9bec0d69626d1'
            'e55a0b6cbe888ae755477b63ece8359a98d3b2f0442b9fa78d2f9c61b979673638e542c570fd4e4e65a80b50ae29131eaa5913eefa79fc9be4023b8d7d5d4da7'
            'c47ade301121be51f496a67236868a30647f30859baa61f0fba2c97c7be26416a12158bcfc20cac9cdf002bdcc1f630bd06f28cfc8265a61915f149671322bcd')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # additional patches
  patch -Np1 -i ${srcdir}/libre.patch
}

build() {
  cd ${pkgname}-${pkgver}
  cmake -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=None \
    -DENABLE_TESTING=OFF \
    -Wno-dev \
    -G Ninja
  ninja -C build
}

package() {
  cd ${pkgname}-${pkgver}
  DESTDIR="${pkgdir}" ninja -C build install
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"
}