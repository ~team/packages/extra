# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Paulo Matias <matias.archlinux-br.org>
# Contributor (Arch): Leslie P. Polzer <polzer.gnu.org>
# Contributor (Arch): erm67 <erm67.yahoo.it>
# Contributor (Arch): Daniel J Griffiths
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Maintainer: aloniv
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=djview
pkgver=4.12
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="Viewer for DjVu documents"
arch=('i686' 'x86_64')
url='https://djvu.sourceforge.net/'
license=('GPL-2' 'Expat')
depends=('qt-base' 'djvulibre')
makedepends=('qt-tools' 'quilt')
source=("https://downloads.sourceforge.net/djvu/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/djview4/djview4_${_debver}-${_debrel}.debian.tar.xz"
        "djvulibre-djview4.appdata.xml")
sha512sums=('0314c409b5474bb5bbe81aa2dc44b24507dac6f7cfde8d275a9d3bb1c2046b6ce2173543618ab7cbb702dbe94d7b2d85d3e19df09fafea73bb8662ee2ba89a01'
            '27ffef6cbe8a9f1cba6737e2b5238a1c848599c044a4a057c555c56899410c056bbe7f270df34182ee3c898f7e1b1891a4fff8bce0327ffb0fd7f548719770eb'
            'b4f24258008e4181f6588f2f5e3770f789d0154ddfd6a0d2f3585928ba019f7ec8b7f9e42b08c194eaec39bf93d67419adddb21c50f8913a8bab891097b9f6e6')

prepare() {
  cd ${pkgname}4-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi
  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd ${pkgname}4-$pkgver
  ./configure \
    --prefix=/usr \
    --disable-nsdejavu
  make
}

package() {
  cd ${pkgname}4-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 ../djvulibre-djview4.appdata.xml "$pkgdir/usr/share/metainfo/djvulibre-djview4.appdata.xml"
  ln -s djview "$pkgdir/usr/bin/djview4"
  install -Dm644 COPYING COPYRIGHT -t "$pkgdir/usr/share/licenses/$pkgname"
}