# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Alex Anthony <alex.anthony28991@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-markupsafe
pkgname=('python-markupsafe' 'tauthon-markupsafe')
pkgver=1.1.1
pkgrel=2
arch=('i686' 'x86_64')
url='https://pypi.python.org/pypi/MarkupSafe'
license=('Modified-BSD')
makedepends=('python-setuptools' 'tauthon-setuptools')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/pallets/markupsafe/archive/$pkgver.tar.gz")
sha512sums=('c2a1072c5dd0918e47f9cfbd30b79b0690a43635f7cfc3fdd27f3df52f8e20406d7e3cfadd29df8ab1d1110f50e23eb7c7272707e0739d862d4b1edb59bbc241')

prepare() {
  cp -a "$srcdir/markupsafe-$pkgver"{,-tauthon}
}

build() {
  cd "$srcdir/markupsafe-$pkgver"
  python setup.py build

  cd "$srcdir/markupsafe-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-markupsafe() {
  pkgdesc="Implements a XML/HTML/XHTML Markup safe string for Python"
  depends=('python')

  cd "$srcdir/markupsafe-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1

  install -D -m644 LICENSE.rst "$pkgdir"/usr/share/licenses/python-markupsafe/LICENSE.rst
}

package_tauthon-markupsafe() {
  pkgdesc="Implements a XML/HTML/XHTML Markup safe string for Tauthon"
  depends=('tauthon')

  cd "$srcdir/markupsafe-$pkgver-tauthon"
  tauthon setup.py install --root="$pkgdir" --optimize=1

  install -D -m644 LICENSE.rst "$pkgdir"/usr/share/licenses/tauthon-markupsafe/LICENSE.rst
}
