# Maintainer (Arch): Jonathan Steel <jsteel at archlinux.org>
# Contributor (Arch): megadriver <megadriver at gmx dot com>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Daenyth <Daenyth+Arch [at] gmail [dot] com>
# Contributor (Arch): kagan <juanynie@mi.madritel.es>
# Contributor (Arch): Hans-Kristian Arntzen <maister@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mednafen
pkgver=1.29.0
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="Command-line multi-system gaming emulator"
url='https://mednafen.github.io'
license=('GPL-2')
arch=('i686' 'x86_64')
depends=('libsndfile' 'sdl2' 'zlib' 'alsa-lib')
makedepends=('mesa' 'glu' 'quilt')
source=("https://mednafen.github.io/releases/files/${pkgname}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/m/mednafen/mednafen_${_debver}+dfsg-${_debrel}.debian.tar.xz")
sha512sums=('375f0fb4f12907faf56b4e9ecdc44c1655b0660a3773240fe3118f2b261e171479b8eb254299f899fc442b54041c941eab2a5f343aec559d9731c3e92cde0569'
            '60c70984f8225267b3938558de46c2929c383a9d247518a288378c0b40e47b3c6ac22710999ea8670e03d72dee3125a92bb8bb0500492e966e3a4dc3fe789e9b')

prepare() {
  cd $pkgname

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname

  ./configure --prefix=/usr

  make
}

package() {
  cd $pkgname

  make DESTDIR="$pkgdir" install

  for i in ABOUT-NLS AUTHORS ChangeLog COPYING INSTALL NEWS \
           README{,.DOS,.PORTING} TODO; do
    install -Dm644 $i "$pkgdir"/usr/share/doc/$pkgname/$i
  done
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"  
}
