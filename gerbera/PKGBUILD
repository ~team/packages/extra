# Maintainer (Arch): Sergej Pupykin <arch+pub@sergej.pp.ru>
# Contributor (Arch): sulaweyo <sledge.sulaweyo@gmail.com>
# Contributor (Arch): Francois Menning <f.menning@pm.me>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gerbera
pkgver=2.0.0
pkgrel=1
pkgdesc="Free and libre UPnP Media Server"
arch=('i686' 'x86_64')
url='https://github.com/gerbera/gerbera'
license=('GPL-2')
depends=('taglib' 'curl' 'sqlite' 'file' 'gcc-libs' 'libupnp' 'duktape' 'libexif' 'fmt'
         'pugixml' 'spdlog' 'ffmpeg' 'ffmpegthumbnailer' 'libmatroska' 'libebml' 'exiv2')
makedepends=('cmake')
install=$pkgname.install
options=('emptydirs')
source=("$pkgname-$pkgver.tar.gz::https://github.com/gerbera/gerbera/archive/v$pkgver.tar.gz"
        "libre.patch"
        "$pkgname.initd"
        "$pkgname.confd"
        "$pkgname.run")
sha512sums=('7324c838ceec00add8792ba09a32d705a48515d91a02bce625fb99c1af90ee4794e9738cb9020406a036ee423fbcd09c05734ac1b089ea8a7e87793c1c3960c8'
            'af1edaf920c81b48da8a24898eb21ef2b1a5e27b6c1e12f77a47af48cbd5bc5d869cbf40fa0d3164ef99f2402511897ef27988835ab55f582ca587a252b1e958'
            '7e4e4b826946a1159a76fd26b378ad7d804bf4555a9032c2cd2198c1ead154879184dd71e277329063ad717f8b9a225e6bbd7a8628dc6052c2140c272924045e'
            '392d7e964b0328778847f871e88ffd475ddac99b6cf8c8ded4825eb2f970e084db692552790a1b30ff96f59eabcd9fff50164d9dedf328badab95a1cec833c02'
            '122fb01fc2a2b69e3a928d449e2c1563b8d1545d8e553496e59fc1862b83cc4ace8f7975a2a1631276bfaac6842ea74510b2d460f5d923f13ef6d67b14eac21d')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  patch -Np1 -i ${srcdir}/libre.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  cmake \
    -Wno-dev \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DWITH_MAGIC=1 \
    -DWITH_CURL=1 \
    -DWITH_JS=1 \
    -DWITH_AVCODEC=1 \
    -DWITH_FFMPEGTHUMBNAILER=1 \
    -DWITH_EXIF=1 \
    -DWITH_EXIV2=1 \
    -DWITH_MATROSKA=1 \
    -DWITH_MYSQL=0 \
    -DWITH_SYSTEMD=0 \
    -DWITH_LASTFM=0 \
    -DWITH_DEBUG=0 \
    -DWITH_TESTS=0
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install

  # create initial configuration
  ./gerbera --create-config > config.xml
  sed -i 's#<home>.*#<home>/var/lib/gerbera</home>#' config.xml
  install -Dm644 config.xml -t "${pkgdir}"/etc/$pkgname

  # additional folders
  install -dm755 "${pkgdir}"/var/lib/$pkgname
  install -dm755 "${pkgdir}"/var/log/$pkgname

  # services
  install -Dm755 "${srcdir}"/$pkgname.initd "${pkgdir}"/etc/init.d/$pkgname
  install -Dm644 "${srcdir}"/$pkgname.confd "${pkgdir}"/etc/conf.d/$pkgname
  install -Dm755 "${srcdir}"/$pkgname.run "${pkgdir}"/etc/sv/$pkgname/run

  # license
  install -Dm644 LICENSE.md -t "${pkgdir}"/usr/share/licenses/$pkgname
}
