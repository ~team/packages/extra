# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Hans Janssen <janserv@gmail.com>
# Contributor (Arch): my64 <packages@obordes.com>
# Contributor (Arch): Colin Pitrat <colin.pitrat@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=openscenegraph
pkgver=3.6.5
_debver=$pkgver
_debrel=7
pkgrel=3
pkgdesc="Free software high performance real-time graphics toolkit"
url='http://www.openscenegraph.org'
arch=('i686' 'x86_64')
license=('custom:OSGPL' 'wxWindows-Library-3.1' 'LGPL-2.1')
depends=('fontconfig' 'mesa-libgl' 'libxinerama')
makedepends=('cmake' 'ffmpeg' 'gst-plugins-base-libs' 'jasper' 'librsvg-legacy' 'libvncserver' 'poppler-glib' 'quilt')
optdepends=('ffmpeg' 'gst-plugins-base-libs' 'jasper' 'librsvg-legacy' 'libvncserver' 'poppler-glib')
source=("https://github.com/openscenegraph/OpenSceneGraph/archive/OpenSceneGraph-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/o/openscenegraph/openscenegraph_${_debver}+dfsg1-${_debrel}.debian.tar.xz")
sha512sums=('7002fa30a3bcf6551d2e1050b4ca75a3736013fd190e4f50953717406864da1952deb09f530bc8c5ddf6e4b90204baec7dbc283f497829846d46d561f66feb4b'
            'bf327b992897034fc0697d56118ec82ccf89ccc6dff8ad23718d9c47b70b7d3e87c28536b5e6e6b1919ac2ec563eceac4952c4a1775b00107b6b64594c3dbbb8')

prepare() {
  mv "OpenSceneGraph-OpenSceneGraph-${pkgver}" "${pkgname}-${pkgver}"
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake \
    -S ${pkgname}-${pkgver} \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_LIBDIR=lib
  make -C build
}

package() {
  make -C build DESTDIR="${pkgdir}" install
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/LICENSE.txt" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
