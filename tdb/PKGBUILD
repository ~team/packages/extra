# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): eric <eric@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=tdb
pkgver=1.4.3
_debver=1.4.3
_debrel=1
pkgrel=1
pkgdesc='Trivial Database similar to GDBM but allows simultaneous commits'
arch=('i686' 'x86_64')
license=('GPL-3' 'LGPL-3' 'Modified-BSD' 'Expat')
url='https://tdb.samba.org/'
depends=('glibc')
makedepends=('python' 'libxslt' 'docbook-xsl' 'quilt')
optdepends=('python: for python bindings')
source=(https://samba.org/ftp/${pkgname}/${pkgname}-${pkgver}.tar{.gz,.asc}
        https://deb.debian.org/debian/pool/main/t/tdb/tdb_${_debver}-${_debrel}.debian.tar.xz
        LICENSE)
validpgpkeys=('9147A339719518EE9011BCB54793916113084025') # Samba Library Distribution Key <samba-bugs@samba.org>
sha512sums=('99488839e7da396f04df60412d21a7d3e09efeab52772d6cb5e9470a3dfd585d73ef2422c51cd0d8ccc123a65d455de400d5d6b24a21a2a50d3da60d9a70e67a'
            'SKIP'
            '21289db0e27f10fa0b4d93e702b142f58347b17bfddd3db7d52c77b815746aec347c89b6d3022d78fc8545b9775521b988c30bc4161db168451001ba9aa79dd4'
            '438bb58048c062d795c5f975b30d0fda8cb4b50badae12ac8038f7a3e0a32e0a6627d8359ef6b82f1882dc0bff27263b453007e90f13a5efdd6c993ff8f3881f')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
              --localstatedir=/var \
              --sysconfdir=/etc/samba
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 "${srcdir}/LICENSE" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
