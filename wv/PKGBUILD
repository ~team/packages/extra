# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=wv
pkgver=1.2.9
_debver=$pkgver
_debrel=5
pkgrel=2
pkgdesc="Free software library for loding several document file formats"
arch=('i686' 'x86_64')
url='https://sourceforge.net/projects/wvware'
license=('GPL-2')
depends=('libgsf' 'libpng' 'sh')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::http://deb.debian.org/debian/pool/main/w/wv/wv_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/w/wv/wv_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ae5c762012595ce8922bfda8749ad3e9cb055edab30152c3a5feaaf3ca6601f357d1e301b179e9a5860cd99452dd6e63e5f44f5bcc12f8db148bdd334e0200a5'
            '9a3448650ca5e4d06499e3ef8a934bd4fb153849aad5afafa35aa9dbf691bfcc0806c58f1d150c3f4232b09964ec1f1dccecc3fddec1eaa40190a48c98548935')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver

  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --disable-static
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install

  # license
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
