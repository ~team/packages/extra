# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor (Arch): Marcus Schulderinsky <mmaacceess at gmail dot com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=lxmusic
pkgname=('lxmusic' 'lxmusic-gtk2')
pkgver=0.4.7
pkgrel=1
pkgdesc='Lightweight XMMS2 client'
arch=('i686' 'x86_64')
url="https://lxde.org/"
license=('Expat' 'GPL-2')
depends=('gtk2' 'gtk' 'xmms2')
makedepends=('intltool' 'gettext-tiny')
source=(https://downloads.sourceforge.net/lxde/$pkgbase-$pkgver.tar.xz)
sha512sums=('e1e6a7e2306b24dfd5864df88db47c07656d43a1901212b1d6b6edec8addf866b31ebae46bca572e88801620016c6fab9860f5ed44d6086f24494e2eb2737216')

build() {
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure --prefix=/usr
  make

  cd "$srcdir"
  # GTK+ 3 version
  [ -d gtk3 ] || cp -r $pkgbase-$pkgver gtk3
  cd gtk3
  ./configure --prefix=/usr --enable-gtk3
  make
}

package_lxmusic-gtk2() {
  depends=('gtk2' 'xmms2')

  cd gtk2
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_lxmusic() {
  pkgdesc+=' (GTK+ 3 version)'
  depends=('gtk' 'xmms2')
  conflicts=('lxmusic')

  cd gtk3
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
