# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): damir <damir@archlinux.org>
# Contributor (Arch): Paul Mattal <paul@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=x264
pkgver=0.160.3011+gitcde9a93
_debver=$pkgver
_debrel=2.1
pkgrel=2
pkgdesc="Free and libre H264/AVC video encoder"
arch=('i686' 'x86_64')
url='https://www.videolan.org/developers/x264.html'
license=('GPL-2')
depends=('l-smash')
makedepends=('nasm' 'quilt')
provides=('libx264')
conflicts=('libx264' 'libx264-10bit' 'libx264-all')
replaces=('libx264' 'libx264-10bit' 'libx264-all')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/x/x264/x264_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/x264/x264_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('4c3521bb19978dd6df311f56d2799de2b268403d982955882519e7baef1ba430a66d555f7ffb291ef8b57bf3a7d4697162e9b175e8fded6afafce3a95a00478b'
            '60923789705d36a1f46231c12cfac7c1cf0a50c3e3e0e769725080a2815abe59050576b00a3a93f22cf299a4521e11793f2841799bb9b5e81c64f61ddfad4f91')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --enable-shared \
    --enable-pic \
    --enable-lto \
    --disable-avs
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install-cli install-lib-shared
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
