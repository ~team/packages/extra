# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=portmidi
pkgver=217
_debver=217
_debrel=6
pkgrel=2
pkgdesc="Platform independent library for real-time MIDI input/output"
arch=('i686' 'x86_64')
url="http://portmedia.sourceforge.net/"
license=('Expat')
depends=('alsa-lib' 'libsndio')
makedepends=('cmake' 'quilt')
optdepends=('sndio: audio and MIDI server')
options=('!makeflags')
source=(https://deb.debian.org/debian/pool/main/p/portmidi/portmidi_$pkgver.orig.tar.gz
        https://deb.debian.org/debian/pool/main/p/portmidi/portmidi_$_debver-$_debrel.debian.tar.xz
        remove-java-support.patch
        sndio.patch)
sha512sums=('ae534b5e9128b586044ce4ac2625d1763fa5abacacac7eb36ba050309c282de453ab403499ca13fd02e1e2fff18f1a3932b78063bc28c37c69f2b45905b4c78e'
            '0fedc8115e42954eff090ba545903070188adbb882e7d3ef9c4918ca8daef19292419f970cb35e52530b79cea1e00a73a99d9ed0a78dcd673307511be4891f48'
            '75219a938ecabc03999af4639bce4b8ded77d267cbb08fa308f74d684047445b27e1c30e1fd96038df1ad6768ad36530b15415bf41603bb9f92921ae1bb05122'
            'a6ec49a1dd472d3cbfe00a5606744e4c2a7d05c0b17f65af6724bd303acbe405b8db38c51fb95feb459de839f6584c97e9c24536f1e01472fff35836764c7bfc')

prepare() {
  cd ${pkgname}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/13-disablejni.patch || true

    quilt push -av
  fi
  patch -p1 -i ../remove-java-support.patch
  patch -p1 -i ../sndio.patch
}

build() {
  cd ${pkgname}
  cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY=Release \
    -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=Release \
    -DCMAKE_RUNTIME_OUTPUT_DIRECTORY=Release
  make
}

package() {
  cd ${pkgname}
  make DESTDIR="${pkgdir}" install

  install -Dm644 license.txt "${pkgdir}/usr/lib/licenses/${pkgname}/license.txt"
}
