# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

pkgname=python-configobj
pkgver=5.0.6
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc="Config file reading, writing and validation."
arch=('any')
url='https://github.com/DiffSK/configobj'
license=('Modified-BSD')
depends=('python' 'python-six')
makedepends=('python-setuptools' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/DiffSK/configobj/archive/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/c/configobj/configobj_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('326eb86e362f281ebf07abcb1cf7616abb270c482eafe842371cda8708245ca5e8262f1644b7164664ecc10e9004ed061c9de18cd233a657d4697dbc3ba3c59d'
            'e13cedfc9c016ead9de97a44890cdc51cfc85662fae4e05b35f5a68b1d9ec420930771eb8b4f7528b09c274b551802389faad642207786382e2d5633cd50f8ed')

prepare() {
  cd configobj-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "${srcdir}"/debian .

    quilt push -av
  fi
}

build() {
  cd configobj-${pkgver}
  python setup.py build
}

package() {
  cd configobj-${pkgver}
  python setup.py install --prefix=/usr --root="${pkgdir}"
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
