# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=qtemu
pkgver=2.1
pkgrel=1
pkgdesc="Graphical user interface for QEMU written in Qt"
arch=('i686' 'x86_64')
url='https://qtemu.org'
license=('GPL-2')
depends=('qemu' 'qt-base')
source=("${pkgname}-${pkgver}.tar.bz2::https://gitlab.com/qtemu/gui/-/archive/${pkgver}/gui-${pkgver}.tar.bz2"
        "libre.patch")
sha512sums=('e96e5299c5c8281c4c1fc0ef08a8ca55082cfb877ea1bc1b8947bb61df78e80963f70c4e3a3ca34931a181d4daa090e9392737ee0917f6650ba87bb6dd0ea9ef'
            'c749331e87d4583c0e7e59e1fb28695abc13d3a448dd290bb76326715232acb6abde705276db51f02c586bc9c1be545d3cfe906ca2ebc642816fd2da8beb7cc3')

prepare() {
  mv "gui-${pkgver}" "${pkgname}-${pkgver}"
  cd "${pkgname}-${pkgver}"
  patch -Np1 -i ${srcdir}/libre.patch

  if ! [ -d build ]
  then
    mkdir build
  fi
}

build() {
  cd "${pkgname}-${pkgver}/build"
  qmake-qt5  \
    QMAKE_CFLAGS="${CFLAGS}" \
    QMAKE_CXXFLAGS="${CXXFLAGS}" \
    ../qtemu.pro
  make
}

package() {
  cd "${pkgname}-${pkgver}/build"
  install -Dm755 qtemu -t "${pkgdir}/usr/bin"
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/qtemu.desktop" -t "${pkgdir}/usr/share/applications"
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/qtemu.png" -t "${pkgdir}/usr/share/pixmaps"
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/LICENSE.md" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}