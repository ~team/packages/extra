# Maintainer (Arch): Brian Bidulock <bidulock@openss7.org>
# Contributor (Arch): Philippe DUCRETTET <ecolinux@gmx.fr>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=fvwm3
pkgver=1.0.6a
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="A highly customizable virtual desktop window manager with small memory footprint."
arch=('i686' 'x86_64')
url='https://www.fvwm.org'
license=('GPL-2')
provides=('fvwm')
depends=('libevent' 'libx11' 'libxft' 'libxrender' 'libxt' 'python' 'libxrandr' )
makedepends=('libxslt' 'asciidoctor' 'quilt')
optdepends=('fontconfig' 'freetype2' 'fribidi' 'ncurses' 'libpng' 'readline'
            'librsvg-legacy' 'libsm' 'libxcursor' 'libxext' 'libxi' 'libxpm'
            'sharutils' )
options=('!emptydirs' '!makeflags')
source=("https://github.com/fvwmorg/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/f/fvwm3/fvwm3_${_debver}+ds-${_debrel}.debian.tar.xz")
sha512sums=('7577e0dde1f7c5c54024207cf48ab5b816c87fc163735e44eb72e66561c67c08f6a06e7fbf5c89fa6180b692ce25e5121845476d0d1dcfb8273b0aa7321d28f9'
            '9ca7baff34dea84b5b26e01b332a915e36e8309052a4b7191c52559664dc0025b83df31a7582cbc76d7e2729255ec3a05ec09d4a8cd96f99c503009fe27b03a1')

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${pkgname}-${pkgver}"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --enable-mandoc
  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" install
  install -Dm644 debian/desktop/$pkgname.desktop -t "${pkgdir}/usr/share/xsessions"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}