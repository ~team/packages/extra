# Maintainer (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Pierre Neidhardt <ambrevar@gmail.com>
# Contributor (Arch): Chris Brannon <cmbrannon79@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Adam Vogt <vogt.adam@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dtach
pkgver=0.9
_debver=0.9
_debrel=5
pkgrel=1
pkgdesc="Emulates the detach feature of screen"
arch=('i686' 'x86_64')
url="http://dtach.sourceforge.net/"
license=('GPL-2')
depends=('glibc')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/sourceforge/dtach/$pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/d/dtach/dtach_$_debver-$_debrel.debian.tar.xz)
sha512sums=('28c13dc8c96c16b9c6153a3a11fdeb4a4bc72e84e1f2575043b047cd8e600a47232f29544ffd87d82e2575623ec2e999a26e23e9eac25ec81c7722bdf98cfb18'
            '27590f09f241f7715c0523aeefe2c912e7cdc6579002391205c01bd586d35519a6ee041b42533be2a69c90e8d8e7cd48f2a4f57984f270b14133eb741d54751e')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver
  install -Dm755 dtach -t "$pkgdir"/usr/bin
  install -Dm644 dtach.1 -t "$pkgdir"/usr/share/man/man1
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
