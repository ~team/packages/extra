# Maintainer (Arch): Eli Schwartz <eschwartz@archlinux.org>
# Contributor (Arch): Daniel Müllner <muellner@math.stanford.edu>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=autoconf-archive
pkgver=2022.09.03
_debver=20220903
_debrel=3
pkgrel=1
epoch=1
pkgdesc="A collection of free, libre and re-usable Autoconf macros"
arch=('any')
url='https://www.gnu.org/software/autoconf-archive/'
license=('GPL-3')
depends=('autoconf')
makedepends=('quilt')
optdepends=('automake: macros for use with it')
source=("https://ftpmirror.gnu.org/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/a/autoconf-archive/autoconf-archive_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('157b5b6a979d5ec5bfab6ddf34422da620fec1e95f4c901821abbb7361544af77747b4a449029b84750d75679d6130a591e98da8772de2c121ecdea163f0340b'
            'SKIP'
            '4069d2d8f008e0df29c313224764b4fa4d6c4c893c6b031f0356607c9a7d6d61e4f312b2f823ef014d96254cc57a2f1a1b629a43157e7c6dc714d8f87fce4c84')
validpgpkeys=('1A4F63A13A4649B632F65EE141BC28FE99089D72') # Peter Simons <simons@cryp.to>

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  if [[ ${pkgver//./} = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
