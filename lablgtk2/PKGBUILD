# Maintainer (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=lablgtk2
pkgver=2.18.11
pkgrel=1
pkgdesc='An Objective Caml interface to gtk2'
arch=('i686' 'x86_64')
license=('LGPL-2.1')
url='http://lablgtk.forge.ocamlcore.org/'
depends=('gtk2' 'librsvg-legacy')
makedepends=('ocaml-compiler-libs' 'camlp5')
optdepends=('ocaml: for using the tools')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/garrigue/lablgtk/archive/${pkgver}.tar.gz")
sha512sums=('adc840df68aa5f9541db44f888f344b313e40c7d8f5cd35f2a45f1bc84e7136dde4b39f873de011db5d5fdfd904c0a097cd9d105458ea9f483cdab3954a2ee05')
options=(!makeflags staticlibs)

build() {
  cd "${srcdir}/lablgtk-${pkgver}"
  ./configure --prefix=/usr \
    --without-gnomeui \
    --without-gnomecanvas \
    --without-panel \
    --without-gtkspell \
    --without-gtksourceview \
    --without-gtksourceview2 \
    --without-glade
  sed -e "s|-O|$CFLAGS|" \
      -e "s|-shared|& -ccopt \"$LDFLAGS\"|" \
      -e "s|(CAMLMKLIB)|& -ldopt \"$LDFLAGS\"|" \
      -i src/Makefile
  make world
  make opt
}

package() {
  cd "${srcdir}/lablgtk-${pkgver}"
  make DESTDIR="${pkgdir}" old-install
  install -m644 META "${pkgdir}/usr/lib/ocaml/lablgtk2/"
  install -Dm644 LGPL -t "${pkgdir}/usr/share/licenses/$pkgname"
}
