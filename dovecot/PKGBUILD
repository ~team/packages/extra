# Maintainer (Arch): Johannes Löthberg <johannes@kyriasis.com>
# Maintainer (Arch): Thore Bödecker <foxxx0@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Paul Mattal <paul@mattal.com>
# Contributor (Arch): Federico Quagliata (quaqo) <quaqo@despammed.com>
# Contributor (Arch): GARETTE Emmanuel <gnunux at laposte dot net>
# Contributor (Arch): Phillip Schichtel <phillip@schich.tel>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# --->>> remember to rebuild/bump pigeonhole in one step <<<---

pkgname=dovecot
pkgver=2.3.13
_debver=$pkgver
_debrel=2
pkgrel=3
pkgdesc="A free and libre IMAP and POP3 server written with security primarily in mind"
url='https://dovecot.org/'
arch=('i686' 'x86_64')
license=('LGPL-2.1' 'Expat')
depends=('krb5' 'libressl' 'sqlite' 'libsodium' 'postgresql-libs' 'bzip2' 'lz4' 'expat' 'curl')
makedepends=('libcap' 'libldap' 'libsodium' 'lua' 'quilt')
optdepends=('libldap: ldap plugin'
            'xz: imap zlib  plugin'
            'lua: LUA auth and push support')
provides=('imap-server' 'pop3-server')
options=('!emptydirs')
install=dovecot.install
source=("https://dovecot.org/releases/2.3/${pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/d/dovecot/dovecot_${_debver}+dfsg1-${_debrel}+deb11u1.debian.tar.xz"
        "dovecot.ld.so.conf"
        "dovecot.initd"
        "dovecot.run")
sha512sums=('758a169fba8925637ed18fa7522a6f06c9fe01a1707b1ca0d0a4d8757c578a8e117c91733e8314403839f9a484bbcac71ce3532c82379eb583b480756d556a95'
            'SKIP'
            'd9db746b355e135aa44405f8c500dba387af32660b4e1ecd16e15753e224c10815b94b3e92be24bca5551273ce6e5a9339cb3782e92ecaeafef81e6f4c64b2e8'
            'c3c34db6179f43f13b198670e7d162af06cf69fe163be50202d1246dc1025ef182ac169597bef10c003bc545439651fd031e41acf91b76654e21c09228654d6e'
            '488a1213514f066fdc7b97e69a1a8336eb4c83054d138d94a63bc733a2163d0b316a6485bf1dde6c4caae92c7dc8d9bd58a4c723b6c0e81d4b130ebd91875baa'
            '5bf3eb1d92bcee2fb6679180871e9b7da9d22f809a59009c0acd95c85e64d13fb03f8b88a516d38c4414a919e41af616ef9b986c4285300107c45ff16755780d')
validpgpkeys=('E643F0BDFDCD04D9FFCB6279C948525140558AC9'  # Timo Sirainen <tss@iki.fi>
              '2BE74AAB3EE754DFB9C80D3318A348AEED409DA1') # Dovecot Community Edition

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/skip-rfc-subdir.patch || true
    rm -v debian/patches/Correct-misspellings.patch || true
    rm -v debian/patches/dovecot_name.patch || true
    rm -v debian/patches/exampledir.patch || true
    rm -v debian/patches/mboxlocking.patch || true
    rm -v debian/patches/split-protocols.patch || true
    rm -v debian/patches/ssl-cert-location.patch || true
    rm -v debian/patches/systemd-service-fixes.patch || true
    rm -v debian/patches/systemd-sd-notify-support.patch || true

    quilt push -av
  fi

  # fix path in helper script
  sed -i 's:OPENSSLCONFIG=${OPENSSLCONFIG-dovecot-openssl.cnf}:OPENSSLCONFIG=${OPENSSLCONFIG- /etc/ssl/dovecot-openssl.cnf}:' doc/mkcert.sh
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr --sysconfdir=/etc \
    --sbindir=/usr/sbin \
    --localstatedir=/var \
    --libexecdir=/usr/libexec \
    --disable-static \
    --without-systemdsystemunitdir \
    --without-pam \
    --without-lucene \
    --without-mysql \
    --with-rundir=/run/dovecot \
    --with-moduledir=/usr/lib/dovecot/modules \
    --with-nss \
    --with-pgsql \
    --with-sqlite \
    --with-ssl=openssl \
    --with-ssldir=/etc/ssl \
    --with-gssapi \
    --with-ldap=plugin \
    --with-lua=plugin \
    --with-zlib \
    --with-bzlib \
    --with-lzma \
    --with-lz4 \
    --with-libcap \
    --with-solr \
    --with-sodium \
    --with-docs

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="$pkgdir" install

  install -d -m755 "${pkgdir}/etc/dovecot/conf.d"
  rm -f "${pkgdir}/etc/dovecot/README"

  install -Dm644 COPYING* -t "$pkgdir"/usr/share/licenses/$pkgname
  install -Dm755 "$srcdir"/dovecot.initd "$pkgdir"/etc/init.d/dovecot
  install -Dm755 "$srcdir/dovecot.run" "$pkgdir/etc/sv/dovecot/run"

  # install mkcert helper script
  install -m 755  doc/mkcert.sh "${pkgdir}/usr/lib/dovecot/mkcert.sh"

  # add dovecot libdir
  install -Dm644 "${srcdir}/dovecot.ld.so.conf" "${pkgdir}/etc/ld.so.conf.d/dovecot.conf"
}
