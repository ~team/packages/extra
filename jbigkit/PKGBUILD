# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Contributor (Arch): Frank Ickstadt (frank dot ickstadt at gmail dot com)
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=jbigkit
pkgver=2.1
_debver=2.1
_debrel=3.1
pkgrel=3
pkgdesc="Data compression library/utilities for bi-level high-resolution images"
arch=('i686' 'x86_64')
url="http://www.cl.cam.ac.uk/~mgk25/jbigkit/"
license=('GPL-2')
makedepends=('quilt')
options=('staticlibs')
source=(https://www.cl.cam.ac.uk/~mgk25/download/jbigkit-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/j/jbigkit/jbigkit_$_debver-$_debrel.debian.tar.xz)
sha512sums=('c4127480470ef90db1ef3bd2caa444df10b50ed8df0bc9997db7612cb48b49278baf44965028f1807a21028eb965d677e015466306b44683c4ec75a23e1922cf'
            'ba3738605b2677c84a7136981d9afd431b7b14dd82f14c643d3ba334421a97c25ce1ee7000dca9ba866f64264b2c35935674422e1aae04ee4320c7adf4fab3b7')

prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/$pkgname-$pkgver
  unset CFLAGS CPPFLAGS LDFLAGS
  [ "$CARCH" == "x86_64" ] && export CFLAGS="$CFLAGS -fPIC"
  make CFLAGS="$CFLAGS"
}

package() {
  cd "$srcdir"/$pkgname-$pkgver

  install -D -m644 libjbig/libjbig.a "$pkgdir"/usr/lib/libjbig.a
  install -D -m644 libjbig/libjbig85.a "$pkgdir"/usr/lib/libjbig85.a
  install -D -m644 libjbig/jbig.h "$pkgdir"/usr/include/jbig.h
  install -D -m644 libjbig/jbig_ar.h "$pkgdir"/usr/include/jbig_ar.h
  install -D -m644 libjbig/jbig85.h "$pkgdir"/usr/include/jbig85.h

  install -d -m755 "$pkgdir"/usr/share/man/man1
#  install -d -m755 "$pkgdir"/usr/share/man/man5
  install -m644 pbmtools/*.1* "$pkgdir"/usr/share/man/man1
#  install -m644 pbmtools/*.5* "$pkgdir"/usr/share/man/man5

  install -D -m755 pbmtools/jbgtopbm "$pkgdir"/usr/bin/jbgtopbm
  install -D -m755 pbmtools/pbmtojbg "$pkgdir"/usr/bin/pbmtojbg
  install -D -m755 pbmtools/jbgtopbm85 "$pkgdir"/usr/bin/jbgtopbm85
  install -D -m755 pbmtools/pbmtojbg85 "$pkgdir"/usr/bin/pbmtojbg85

  rm -f "$pkgdir"/usr/share/man/man5/p{b,g}m.5*

  install -D -m644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}
