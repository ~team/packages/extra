# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Jaroslaw Swierczynski <swiergot@aur.archlinux.org>
# Contributor (Arch): Sven Kauber, <celeon@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=encfs
pkgver=1.9.5
pkgrel=1
pkgdesc="Encrypted filesystem in user-space"
arch=('i686' 'x86_64')
url='https://vgough.github.io/encfs/'
license=('LGPL-3')
depends=('libressl' 'fuse2' 'tinyxml2')
makedepends=('cmake' 'gettext-tiny')
source=("https://github.com/vgough/${pkgname}/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.gz"{,.asc})
sha512sums=('036e08ca9bc13b44742aebdee49bf7029d0c6b7e59cd6dedc9a09da2af99482859f6a79eddf07e3db296edaf45aafc48fe08488840e765682e9b192dd6ae4c46'
            'SKIP')
validpgpkeys=('FFF3E01444FED7C316A3545A895F5BC123A02740') # Jakob Unterwurzacher <jakobunt@gmail.com>

build() {
  cmake \
    -B build \
    -S $pkgname-$pkgver \
    -DUSE_INTERNAL_TINYXML=OFF \
    -DINSTALL_LIBENCFS=ON \
    -DBUILD_SHARED_LIBS=ON \
    -DCMAKE_INSTALL_PREFIX=/usr
  cmake --build build
}

package() {
  DESTDIR="${pkgdir}" cmake --install build
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING.LGPL" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}