# Maintainer (Arch):  Christian Hesse <mail@eworm.de>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): kfgz <kfgz at interia dot pl>
# Contributor (Arch): pootzko <pootzko at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad @ hyperbola . info>

pkgname=haveged
pkgver=1.9.14
pkgrel=2
pkgdesc='Entropy harvesting daemon using CPU timings'
arch=('i686' 'x86_64')
url='https://github.com/jirka-h/haveged'
license=('GPL-3')
depends=('glibc')
source=("$pkgname-$pkgver.tar.gz::https://github.com/jirka-h/$pkgname/archive/v$pkgver.tar.gz"
        "haveged.initd"
        "haveged.confd"
        "haveged.run")
sha512sums=('bdb6d9de667298d32b474bcbdd5f90c12b870b154b86f8817948de787d378b428bf823234f20129666bd1abced2f154643b5999e43975969f6bba87124650924'
            'b5ed26c8a856c6b6f7a35ee9326f3692c577e3abf9714d53b143e4899e6ee3bce7dfa6c6c707166e62d386295637bebcc9b47fc735bcb7deb3201169223bdf5f'
            '955dfe69c9f4d892afc4cd5a8e7371d2ffdc4446df5b6174e9e32f5520a44eaf435f50c47271c98ccb19d2e3fcc0a0bf2424d5c110f5436865d3c88acdfa49e2'
            '9147a642ebc7141215deb9b3ae11d764234c58cf6a196a22f5ac8d69249b1c4514b97d15254d8560a90fc19d73ba0de36c6a273d50c050684283387cdba0d755')

prepare() {
  cd $pkgname-$pkgver
  autoreconf -vif
}

build() {
  cd $pkgname-$pkgver

  ./configure \
      --prefix=/usr \
      --sysconfdir=/etc \
      --disable-static
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR="$pkgdir" install

  rm "$pkgdir"/usr/lib/libhavege.la

  # services
  install -Dm644 "$srcdir"/haveged.confd "$pkgdir"/etc/conf.d/haveged
  install -Dm755 "$srcdir"/haveged.initd "$pkgdir"/etc/init.d/haveged
  install -Dm 755 "$srcdir/haveged.run" "$pkgdir/etc/sv/haveged/run"

  # license
  install -Dm 644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
