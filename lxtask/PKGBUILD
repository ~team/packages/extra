# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@koon.fr>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=lxtask
pkgname=('lxtask' 'lxtask-gtk2')
pkgver=0.1.10
pkgrel=3
pkgdesc="Graphical Taskmanager"
arch=('i686' 'x86_64')
license=('GPL-2')
url='https://lxde.org/'
depends=('gtk2' 'gtk')
makedepends=('intltool' 'gettext-tiny' 'mesa-libgl')
source=("https://downloads.sourceforge.net/lxde/$pkgbase-$pkgver.tar.xz"
        "$pkgbase.desktop"
        "$pkgbase.png")
sha512sums=('21f3a145939b53136be11892d183f3549b1a736070bb0c69e0932009d46abfd4956d1ec1d904e58bd6fca0a30fa0cca0cce897a83113c3fe1591300cfddae99a'
            'e90af3af94bbf4e1da3be6caf501260ab8baf0ff73f4001225f66832227113f7b32092a348570275233eccd497191b682e697097ece583ba1dea3680a788ebaf'
            '35bf1aeffb41c2372fe7ce6afcb09040efe2731e7f0c1358ffb52ccbb4021cc4a1eefe36fd32a1fd9d58066088541c014b0b531545574852f47cd1339e39b437')

build() {
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure --sysconfdir=/etc --prefix=/usr
  make

  cd "$srcdir"
  # GTK+ 3 version
  [ -d gtk3 ] || cp -r $pkgbase-$pkgver gtk3
  cd gtk3
  ./configure --sysconfdir=/etc --prefix=/usr --enable-gtk3
  make
}

package_lxtask-gtk2() {
  depends=('gtk2')
  conflicts=('lxtask')

  cd gtk2
  make DESTDIR="$pkgdir" install

  # Correct desktop-file
  rm "${pkgdir}/usr/share/applications/$pkgbase.desktop"
  install -Dm644 "${srcdir}/$pkgbase.desktop" "${pkgdir}/usr/share/applications/$pkgbase.desktop"
  install -Dm644 "${srcdir}/$pkgbase.png" "${pkgdir}/usr/share/pixmaps/$pkgbase.png"

  # License
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}

package_lxtask() {
  pkgdesc+=' (GTK+ 3 version)'
  depends=('gtk')
  conflicts=('lxtask-gtk2')

  cd gtk3
  make DESTDIR="$pkgdir" install

  # Correct desktop-file
  rm "${pkgdir}/usr/share/applications/$pkgbase.desktop"
  install -Dm644 "${srcdir}/$pkgbase.desktop" "${pkgdir}/usr/share/applications/$pkgbase.desktop"
  install -Dm644 "${srcdir}/$pkgbase.png" "${pkgdir}/usr/share/pixmaps/$pkgbase.png"

  # License
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"

  # Ignore package by AppStream to avoid duplicated IDs
  echo 'X-AppStream-Ignore=true' >> "${pkgdir}/usr/share/applications/$pkgbase.desktop"
}
