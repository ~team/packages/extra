# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=python-commonmark
pkgver=0.9.1
pkgrel=1
pkgdesc='Python parser for the CommonMark Markdown spec'
url='https://pypi.org/project/commonmark/'
arch=('any')
license=('Modified-BSD')
depends=('python-docutils' 'python-future')
makedepends=('python-setuptools' 'python-sphinx' 'python-sphinx_rtd_theme')
options=('!makeflags')
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/readthedocs/commonmark.py/archive/${pkgver}.tar.gz)
sha512sums=('a6de0b384247ff79fe326012bd1fc71135e6ae5bc7b6a15d5402969a94f81fdd802f1a9d557c504eb74bfb3fea68f55355a70a6e0112a620cc285cf9039892e4')

build() {
  cd commonmark.py-${pkgver}
  python setup.py build
  make -C docs man text SPHINXBUILD=sphinx-build
}

package() {
  cd commonmark.py-${pkgver}
  python setup.py install --root="${pkgdir}" -O1 --skip-build
  install -Dm 644 README.rst CHANGELOG.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -Dm 644 docs/_build/text/* -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 docs/_build/man/commonmark-py.1 "${pkgdir}/usr/share/man/man1/${pkgname}.1"
  mv "${pkgdir}/usr/bin/cmark"{,-py3}
}