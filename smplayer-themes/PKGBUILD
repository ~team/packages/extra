# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Alessio 'mOLOk' Bolognino <themolok@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=smplayer-themes
pkgver=20.11.0
pkgrel=1
epoch=1
pkgdesc='Themes for SMPlayer'
url='https://www.smplayer.info/'
arch=('any')
license=('GPL-2' 'LGPL-3' 'CC-BY-2.5' 'CC-BY-SA-2.5' 'CC-BY-SA-3.0')
depends=('smplayer')
makedepends=('optipng')
source=(https://downloads.sourceforge.net/smplayer/${pkgname}-${pkgver}.tar.bz2)
sha512sums=('4e64c3f44fc3529d980751618cd30fcc395a21701ba7481ad0a6dffc1fa770c13a3414d0853f94cd6163738702a0a75060a92e5c1c426e79902a68d8f8336575')

prepare() {
  cd ${pkgname}-${pkgver}
  # Fix invalid PNG icons to work with libpng 1.6
  find -name '*.png' -exec optipng -quiet -force -fix {} +
}

build() {
  cd ${pkgname}-${pkgver}
  make PREFIX=/usr
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" PREFIX=/usr install
  install -Dm644 COPYING_CC-BY-2.5.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 COPYING_CC-BY-SA-2.5.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 COPYING_CC-BY-SA-3.0.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 COPYING_GPL.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 COPYING_LGPL.txt -t "${pkgdir}/usr/share/licenses/$pkgname"
}
