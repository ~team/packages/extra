# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=xsel
pkgver=1.2.0+git9bfc13d.20180109
_gitver=1.2.0~git9bfc13d.20180109
_debver=1.2.0+git9bfc13d.20180109
_debrel=3
pkgrel=1
pkgdesc='XSel is a command-line program for getting and setting the contents of the X selection'
arch=('i686' 'x86_64')
url="http://www.vergenet.net/~conrad/software/xsel/"
license=('Expat')
depends=('libx11')
makedepends=('libxt' 'quilt')
source=("https://deb.debian.org/debian/pool/main/x/xsel/xsel_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/xsel/xsel_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('9361d5c964b3424c839b96c003a726902eca5e96505a0d3a1a4750aec3778d859157cb0567455a62185b39731bbb9d5d0dc9c374a75ba8f7792e0e2c72022b73'
            'ebc2b4a8a21c731f8820651162756cb143656e9d179917a259b9e19d0760ffa2d016f052d3330c6895b5245140119fff414f2e7700c3c8486ad3a26165044e49')

prepare() {
  cd $pkgname-$_gitver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$_gitver
  touch README
  [ -x configure ] && ./configure --prefix=/usr || ./autogen.sh --prefix=/usr
  make
}

package() {
  cd $pkgname-$_gitver
  make DESTDIR="$pkgdir" install
  mkdir -p "$pkgdir"/usr/share/licenses/xsel/
  install -Dm644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname
}
