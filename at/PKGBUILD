# Maintainer (Arch): Alexander F Rødseth <xyproto@archlinux.org>
# Contributor (Arch): Nathan Baum <n@p12a.org.uk>
# Contributor (Arch): Judd Vinet <jvinet@zeroflux.org>
# Contributor (Arch): Todd Musall <tmusall@comcast.net>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=at
pkgver=3.1.23
pkgrel=1
pkgdesc="AT and batch delayed command scheduling utility and daemon"
arch=('i686' 'x86_64')
url='http://blog.calhariz.com/tag/at'
license=('GPL-2')
depends=('flex')
makedepends=('smtp-forwarder' 'git' 'quilt')
optdepends=('logger: message logging support')
backup=('etc/at.deny'
        'var/spool/atd/.SEQ'
        'etc/conf.d/atd')
options=('!makeflags')
source=("https://deb.debian.org/debian/pool/main/a/at/at_${pkgver}.orig.tar.gz"
        "atd.confd"
        "atd.initd"
        "atd.run")
sha512sums=('ee5cf5abf32cf1e89746e427d1cc20005ef49fad47db55512c90042a77e86b2c15f5de029c79573bc86ce4aead6ed2d561b89812510aadbc5763f9288b467cfd'
            '12f1423e2d0841a98ce506ebbf24280bc634b76a2b15cb1c6e12285fc7fd859428380841754ab2e3a0b7c3d7cc9957b53d4e018651ef097ca1af83f92cfa130a'
            'a327486feb23388809d5ead9c08d3e5905a41e9cfc026b54b308b71a9d692f72bbe637707a43729ba579a44ddf9c2f46c87d99a46e4b6ff00c8a9ba7005128fb'
            '11fc830778d65aaefe1b4172f4b1d03b00e50c44ff2e536dedc11121fa2de1cc1fe356b0093ff6b1282f41efff7869e670a8d0280fa58415de3d4ff5d774ab6d')

build() {
  cd "$pkgname-$pkgver"

  ./configure \
    --prefix=/usr \
    --with-loadavg_mx=1.5 \
    --with-jobdir=/var/spool/atd \
    --with-atspool=/var/spool/atd \
    SENDMAIL=/usr/sbin/sendmail

  CFLAGS="$CFLAGS -w" make
}

package() {
  make -C "$pkgname-$pkgver" IROOT="$pkgdir" docdir=/usr/share/doc install

  install -Dm644 "$srcdir"/atd.confd "$pkgdir"/etc/conf.d/atd
  install -Dm755 "$srcdir"/atd.initd "$pkgdir"/etc/init.d/atd
  install -Dm755 "$srcdir"/atd.run "$pkgdir"/etc/sv/atd/run

  install -Dm644 "$pkgname-$pkgver"/COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
