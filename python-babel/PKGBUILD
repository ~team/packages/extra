# Maintainer (Arch): Morten Linderud <foxboron@archlinux.no>
# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Giedrius Slavinskas <giedrius25@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-babel
pkgname=('python-babel' 'tauthon-babel')
pkgver=2.8.0
_core=36
_debver=$pkgver
_debrel=7
pkgrel=2
url='https://babel.pocoo.org/'
license=('Modified-BSD')
arch=('any')
makedepends=('python' 'tauthon' 'python-setuptools' 'tauthon-setuptools' 'python-pytz' 'tauthon-pytz' 'quilt')
noextract=("cldr-core-$_core.zip")
source=("$pkgbase-$pkgver.tar.gz::https://github.com/python-babel/babel/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-babel/python-babel_$_debver+dfsg.1-$_debrel.debian.tar.xz"
        "cldr-core-$_core.zip::https://unicode.org/Public/cldr/$_core/core.zip")
sha512sums=('e4ad388dde6e388d8ac0772f1931724fd37a371f440595aeac0a2809f4753387775774ecc7c402fb3636e1765fe1297e3629e7a9162506d8f433ada06795b181'
            '2d5fb5b14fdc43a7ae52aa9fdaf13ed4c6829030fac74c32a5e988a1c79c17c2817627fc2127cd8d426a57250e3cbc4df9d957d659ff339974c5ba5a34aaf3e7'
            '773aa51fe86485a92208ba0ce348761cc31f800f2d5d559799c4a9a2069086ef7796bca3936229d4263e56acee749a29eb6f535e35a68c73436a237c6d1b163e')

prepare() {
  cd "$srcdir/babel-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cp -a "$srcdir"/babel-$pkgver{,-tauthon}
  cp "$srcdir"/cldr-core-$_core.zip "$srcdir"/babel-$pkgver-tauthon/cldr/cldr-core-$_core.zip
  cp "$srcdir"/cldr-core-$_core.zip "$srcdir"/babel-$pkgver/cldr/cldr-core-$_core.zip
}

build(){
  cd "$srcdir/babel-$pkgver"
  python setup.py import_cldr
  python setup.py build

  cd "$srcdir/babel-$pkgver-tauthon"
  tauthon setup.py import_cldr
  tauthon setup.py build
}

package_python-babel() {
  pkgdesc="A collection of tools for internationalizing Python applications"
  depends=('python' 'python-pytz')

  cd "$srcdir/babel-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -D -m0644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_tauthon-babel() {
  pkgdesc="A collection of tools for internationalizing Tauthon applications"
  depends=('tauthon' 'tauthon-pytz')

  cd "$srcdir/babel-$pkgver-tauthon"
  tauthon setup.py install --root="$pkgdir" --optimize=1 --skip-build
  mv "$pkgdir"/usr/bin/pybabel "$pkgdir"/usr/bin/pybabel-tauthon
  install -D -m0644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
