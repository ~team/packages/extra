# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Aaron Schaefer <aaron@elasticdog.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=optipng
pkgver=0.7.7
_debver=0.7.7
_debrel=1
pkgrel=1
pkgdesc='Compresses PNG files to a smaller size, without losing any information.'
arch=('i686' 'x86_64')
url='http://optipng.sourceforge.net/'
license=('zlib')
depends=('glibc' 'libpng' 'zlib')
source=("https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/o/optipng/optipng_$_debver-$_debrel.debian.tar.xz")
sha512sums=('c33b7650143ab39944d2c066e07e10273c37024c5bfe7c00475bfb513af96afa081ff388164845d426d3bce624c0282dee574fa150e963f18d3683d821030280'
            'b3f13fb81e3bc26e77c1024436996ac5da8b8a62069e8d0458a2e05c90cf9f25df5fffd2269901a78065cdd06022f33c7104b852c60d31d7d59a4164f5ee76f6')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" configure
}


build() {
  cd "$srcdir/$pkgname-$pkgver"

  ./configure --prefix=/usr --mandir=/usr/share/man --with-system-libs
  make
}
package() {
  cd "$srcdir/$pkgname-$pkgver"

  make prefix="$pkgdir/usr" mandir="$pkgdir/usr/share/man" install

  # install license
  install -Dm0644 LICENSE.txt -t "$pkgdir/usr/share/licenses/$pkgname" 
}

