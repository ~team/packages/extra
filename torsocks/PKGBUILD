# Maintainer (Arch): Daniel Micay <danielmicay@gmail.com>
# Contributor (Arch): Nicolas Pouillard <nicolas.pouillard@gmail.com>
# Contributor (Arch): Rorschach <r0rschach@lavabit.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=torsocks
pkgver=2.3.0
_debver=2.3.0
_debrel=3
pkgrel=1
pkgdesc='Wrapper to safely torify applications'
arch=('i686' 'x86_64')
url='https://gitweb.torproject.org/torsocks.git/'
license=('GPL-2')
depends=('tor')
makedepends=('gettext-tiny' 'quilt')
backup=("etc/tor/${pkgname}.conf")
source=("https://people.torproject.org/~dgoulet/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "https://deb.debian.org/debian/pool/main/t/torsocks/torsocks_${_debver}-${_debrel}.debian.tar.xz")
validpgpkeys=('B74417EDDF22AC9F9E90F49142E86A2A11F48D36')
sha512sums=('4888caaffdcfebf3673e14f3491eae6aa84ca0a4a2a812ba7bdac2abb471307e89a3c5cffe7691fb6f190c7bd9ea455ee9a223d909a39152be8524f590be2031'
            'SKIP'
            'd2c51c6fa0932f5cd1e58ecf5806e36a3e8b6e50d356761b8157227aec347dca3a4cf7f08aa9f5475e3c2321a893b573693797d74cbca7cde3a045f621fcb04b')

prepare() {
  cd "${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd "${pkgname}-${pkgver}"

  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

check() {
  cd "${pkgname}-${pkgver}"

  make -j1 -k check
}

package() {
  cd "${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  install -Dm644 gpl-2.0.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim:set ts=2 sw=2 et:
