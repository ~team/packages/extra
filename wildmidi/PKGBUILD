# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Jaroslav Lichtblau <dragonlord@aur.archlinux.org>
# Contributor (Arch): Panagiotis Papadopoulos
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=wildmidi
pkgver=0.4.3
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Free software MIDI Synthesizer"
arch=('i686' 'x86_64')
url='https://www.mindwerks.net/projects/wildmidi/'
license=('GPL-3')
depends=('alsa-lib')
makedepends=('cmake')
optdepends=('freepats: additional files for MIDI audio synthesis')
source=("https://github.com/psi29a/${pkgname}/archive/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/w/wildmidi/wildmidi_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('7e86e998ee97cdf57328e4cf5ef52a64926fd01999879c0eae5b6c823be4e6d116f7026230bd15d209e6616fbc7ba1c29ebd1f3be04735e341ce5c83298f956f'
            'dcbd1af0e32167e4632a3186c41170be27a54c8616ede58d47ec01f845408ad4f774dc9e7bb4b1a6cbcb08288aa24c21393ffd91b369ee34f541f603d5deba2c')

prepare() {
  mv "${pkgname}-${pkgname}-${pkgver}" "${pkgname}-${pkgver}"
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .
  fi
}

build() {
  cmake \
    -B build \
    -S "${pkgname}-${pkgver}" \
    -DCMAKE_INSTALL_PREFIX=/usr
  make -C build
}

package() {
  make DESTDIR="${pkgdir}" -C build install
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/debian/$pkgname.cfg" -t "${pkgdir}/etc/$pkgname"
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
  for i in GPLv3.txt LGPLv3.txt; do
    install -Dm644 "${srcdir}/${pkgname}-${pkgver}/docs/license/$i" -t "${pkgdir}/usr/share/licenses/$pkgname"
  done
}