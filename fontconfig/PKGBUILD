# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=fontconfig
pkgver=2.13.1
_debver=$pkgver
_debrel=4.2
pkgrel=3
pkgdesc="A library for configuring and customizing font access"
arch=(i686 x86_64)
url='https://www.freedesktop.org/wiki/Software/fontconfig/'
license=(Expat)
depends=(expat freetype2)
makedepends=(gperf python quilt)
install=$pkgname.install
backup=(etc/fonts/fonts.conf)
source=(https://www.freedesktop.org/software/fontconfig/release/$pkgname-$pkgver.tar.bz2
        https://deb.debian.org/debian/pool/main/f/fontconfig/fontconfig_$_debver-$_debrel.debian.tar.xz
        fontconfig.hook)
sha512sums=('f97f2a9db294fd72d416a7d76dd7db5934ade2cf76903764b09e7decc33e0e2eed1a1d35c5f1c7fd9ea39e2c7653b9e65365f0c6205e047e95e38ba5000dd100'
            '50de46b55b71c5455fb2015c328f5219722ec27b541e6a16839401c840fe03e506cedb74d35a66b8dbf9764726e0d3da6f252dd4f9ff4697705d33f37bf1a2bb'
            '5ec6d8c52273a0edd90e588a3c5079f7bcce63ec8f7032b8f716d698c95d7ac604d9dbee706f7ce1148a8821dda7b44ea787a4f8f5b6c858b583884ee6e024f4')

# a nice page to test font matching:
# http://zipcon.net/~swhite/docs/computers/browsers/fonttest.html
# http://getemoji.com/

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/skip-dpkg-tmp-files.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-templatedir=/etc/fonts/conf.avail \
    --localstatedir=/var \
    --disable-static \
    --with-default-fonts=/usr/share/fonts \
    --with-add-fonts=/usr/local/share/fonts
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 $srcdir/fontconfig.hook -t "$pkgdir/usr/share/libalpm/hooks"
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
