# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=normalize
pkgver=0.7.7
_debver=$pkgver
_debrel=17
pkgrel=1
pkgdesc="A tool for adjusting the volume of audio-files to a standard level"
arch=('i686' 'x86_64')
url='https://normalize.nongnu.org'
license=('GPL-2')
depends=('audiofile' 'libmad' 'vorbis-tools' 'lame' 'flac' 'mpg123')
makedepends=('pkg-config' 'intltool' 'quilt')
source=("https://download.savannah.gnu.org/releases/${pkgname}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/n/normalize-audio/normalize-audio_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('5fb2265eac93b243682cedb955d2ddfe05f55cafdeb1d757aaef0afb36bc9907ea3321b8554e128f6228bdeeaeef96e3e9ba000d0ad84282cc76a50b3e460ce4'
            '5ae26ebcb58e29acef21ff5df1cb21384550d6fd65728d0cc66d4798aab8e7610c8b1d72eaa6380a862884a99928c402bd917b6dba763a0ae22468af1e03c335')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  touch AUTHORS NEWS ChangeLog
  autoreconf -vi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --with-audiofile \
    --with-mad
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
