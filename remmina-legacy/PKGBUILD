# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=remmina-legacy
_realpkgname=remmina
pkgver=1.4.11
_debver=$pkgver
_debrel=3
pkgrel=2
pkgdesc="Free and libre remote desktop client written in GTK+ (legacy version)"
arch=(i686 x86_64)
url="https://www.remmina.org/"
license=('custom:GPL-2+OpenSSL-Linking-Exception')
conflicts=('remmina')
provides=('remmina')
depends=('libgcrypt' 'libssh' 'vte' 'libsodium' 'libxkbfile')
makedepends=('cmake' 'freerdp' 'libvncserver' 'spice-gtk' 'spice-protocol' 'xdg-utils'
             'harfbuzz' 'xenocara-proto' 'gobject-introspection' 'webkitgtk' 'gtk-vnc' 'quilt')
optdepends=('gtk-vnc: GVNC plugin'
            'libvncserver: VNC plugin'
            'spice-gtk: Spice plugin'
            'webkit2gtk: WWW plugin'
            'xterm: external tools')
source=("$_realpkgname-$pkgver.tar.bz2::https://gitlab.com/Remmina/Remmina/-/archive/v${pkgver/rc/-rc}/Remmina-v${pkgver/rc/-rc}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/${_realpkgname::1}/$_realpkgname/${_realpkgname}_$_debver+dfsg-$_debrel.debian.tar.xz")
sha512sums=('25ca2d7ce1ae2955a4d59ce05d332894ee715aefc9bd9cb03758f45aea7edda31e7bec30bbd5ce5a893974c2d04624a209d9a7afb6e95ac9abc6b3cc71cffa31'
            '6c0f306a0f5f2b9411724a8f16f1382f9d7f5aeafa9c20c9274b74e355758901476832164216e89af4f50fbc20218b49125c4cc7c6e058e8de88cacde3f97881')

prepare() {
  cd Remmina-v${pkgver/rc/-rc}

  if [ ${pkgver%.*} = ${_debver%.*} ]; then
    # Debian patches
    export QUILT_PATCHES='debian/patches'
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir/debian" .

    quilt push -av
  fi
}

build() {
  cmake -S Remmina-v${pkgver/rc/-rc} -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DWITH_AVAHI=OFF \
    -DWITH_APPINDICATOR=OFF \
    -DWITH_FREERDP=ON \
    -DWITH_KF5WALLET=OFF \
    -DWITH_NEWS=OFF \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  for i in LICENSE LICENSE.OpenSSL; do
    install -Dvm644 "$srcdir/Remmina-v${pkgver/rc/-rc}/$i" \
      -t "$pkgdir/usr/share/licenses/$pkgname"
  done
}
