# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Maintainer (Arch): Jaroslav Lichtblau <svetlemodry@archlinux.org>
# Contributor (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Contributor (Arch): Mark Schneider <queueRAM@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gnucash
pkgver=5.6
_debver=$pkgver
_debrel=1
_gtestver=1.11.0
pkgrel=2
pkgdesc="Free and libre financial-accounting application"
arch=('i686' 'x86_64')
license=('custom:GPL-2+OpenSSL-Linking-Exception' 'Public-Domain' 'FDL-1.1')
url='https://gnucash.org/'
depends=('aqbanking' 'gwenhywfar' 'boost-libs' 'guile' 'libdbi' 'webkitgtk' 'libofx' 'yelp')
makedepends=('boost' 'cmake' 'libdbi-drivers' 'postgresql-libs' 'swig' 'quilt')
optdepends=('libdbi-drivers: open and save data into SQL database')
options=(!emptydirs)
source=("https://downloads.sourceforge.net/${pkgname}/gnucash-${pkgver}.tar.bz2"
        "https://downloads.sourceforge.net/${pkgname}/gnucash-docs-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/g/gnucash/gnucash_${_debver}-${_debrel}~bpo12+1.debian.tar.xz"
        "https://github.com/google/googletest/archive/refs/tags/release-${_gtestver}.tar.gz")
sha512sums=('5ae0994a3f358bbf5ee6aeffc7a7237f6869e72c34cd551e5243e0c966dbbccfe8cdea65b7d31e016657b97175d6e5d94f5797b5adec5f94bc0b627c7a4dd435'
            '2d2c2d2e209e8cf2d926aa34bd09da302e4472d835b348780ca18c0da8ddfb3f4296674473c66f0b412b43be11973c05a97a04c8919bce82ba046bcbfa499d19'
            '5f995aac99148c41cc1b4932642a298d58789379a828f3415155ef1b19186adf52641565c67508886e374974dd4d2df056ca931b772bc04d4a1c99aa27f06ffe'
            '6fcc7827e4c4d95e3ae643dd65e6c4fc0e3d04e1778b84f6e06e390410fe3d18026c131d828d949d2f20dde6327d30ecee24dcd3ef919e21c91e010d149f3a28')

prepare() {
  cd gnucash-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  # We don't directly support gtest and won't do that
  # so using a workaround by setting that temporary.
  cd googletest-release-$_gtestver
  
  cmake -B build -DBUILD_GMOCK=ON
  make -C build

  cd $srcdir/gnucash-$pkgver

  # Do not treat warnings as errors
  sed -e 's|-Werror||g' -i CMakeLists.txt

  export GTEST_ROOT=$srcdir/googletest-release-$_gtestver
  cmake	-DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=/usr/lib \
        -DWITH_PYTHON=ON \
        -DCOMPILE_GSCHEMAS=OFF
  make

  cd $srcdir/gnucash-docs-$pkgver

  cmake \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DWITH_PDF=OFF \
    -DWITH_HTML=OFF .
  make -C build
}

package() {
  cd $srcdir/gnucash-$pkgver
  make DESTDIR="$pkgdir" install

  # Delete the gnucash-valgrind executable because the source files
  # are not included with the package and the executable is hardlinked
  # to the location that it was built at.
  rm -f "$pkgdir/usr/bin/gnucash-valgrind"

  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"

  cd $srcdir/gnucash-docs-$pkgver
  make DESTDIR="${pkgdir}" -C build install

  install -Dm644 COPYING{,-DOCS} -t "${pkgdir}/usr/share/licenses/$pkgname"
}
