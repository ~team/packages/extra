# Maintainer (Arch): Jonathan Steel <jsteel at archlinux.org>
# Contributor (Arch): thorsten w. <p@thorsten-wissmann.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=herbstluftwm
pkgver=0.9.5
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="Manual tiling window manager for X"
arch=('i686' 'x86_64')
url='https://herbstluftwm.org'
license=('Simplified-BSD')
depends=('glib2' 'libxinerama' 'libxrandr' 'libxfixes' 'libxft')
makedepends=('cmake' 'asciidoc' 'quilt')
optdepends=('xenocara-xsetroot: to set wallpaper color in default autostart'
            'xterm: used as the terminal in default autostart'
            'dzen2: used in the default panel.sh'
            'dmenu: used in some example scripts')
source=("https://herbstluftwm.org/tarballs/${pkgname}-${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/h/herbstluftwm/herbstluftwm_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('a85fd1ab16b873f512d6965cc4a93524e3ed4a15079d0d4ee54012e217bd41db3b54729c1b30ee361c34b38799f95bc537099056ba7a5f791bf2be6724bc92c3'
            'SKIP'
            'a44006422f8c3ce5b5bd6bb5a8a7c6a13a255ed85c7f2ee4d2ab1027297f22814e3ef7acd2fa4f471297936e6601a667fa69fa45951073f37235418e48fd8c29')
validpgpkeys=('72B6C05CDFF309C6396167D464EF02821CAFF810') # Thorsten Wißmann <edu@thorsten-wissmann.de>

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake \
    -S $pkgname-$pkgver \
    -B build \
    -DBASHCOMPLETIONDIR=/usr/share/bash-completion/completions \
    -DZSHCOMPLETIONDIR=/usr/share/zsh/functions/Completion/X \
    -DCMAKE_INSTALL_PREFIX="/usr"
  make -C build
}

package() {
  make -C build DESTDIR="$pkgdir" install
  install -d "$pkgdir"/usr/share/licenses/$pkgname/
  install -Dm644 "$pkgdir"/usr/share/doc/$pkgname/LICENSE "$pkgdir"/usr/share/licenses/$pkgname
}