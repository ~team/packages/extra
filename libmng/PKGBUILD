# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=libmng
pkgver=1.0.10
pkgrel=1
_debver=1.0.10+dfsg
_debrel=3.1
pkgdesc="A collection of routines used to create and manipulate MNG format graphics files"
arch=(i686 x86_64)
url="https://www.$pkgdir.com/"
license=('custom:MNG-Library')
depends=(lcms2)
makedepends=(quilt)
source=(https://downloads.sourceforge.net/sourceforge/${pkgname}/${pkgname}-${pkgver}.tar.bz2
        https://ftp.debian.org/debian/pool/main/libm/$pkgname/${pkgname}_$_debver-$_debrel.debian.tar.xz)
sha512sums=('e920a0a93064ceae495d7f231fbb7f41a8b8c18dccbedefb8a00c7af95c9e92695c52f48386f2c80c0bd7820c1e4a0b0d607e66cdbf8d38f8df785d7524f6e8c'
            '012c6dc026639bfa2f21feb8efd92e647305c79841efe76d0c64042d94b79b4501419f9ff142e5dcf157cb34de45e95043001fd6efb2ffc5f855e64e39d8e649')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # prepare 1.x version to build it
  sed -i 's/AM_C_PROTOTYPES//' makefiles/configure.in
  ln -s makefiles/configure.in .
  ln -s makefiles/Makefile.am .
  autoreconf --force --install

  # fix FILE data type error
  sed -i 's|[#]define[ ][_]libmng[_]h[_]|#define _libmng_h_\n\n#include <stdio.h>                     /* needs FILE data type */|' $pkgname.h
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -D -m644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
