# Maintainer (Arch): Daniel Isenmann <daniel@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgname=babl
pkgver=0.1.82
_debver=0.1.82
_debrel=1
pkgrel=1
pkgdesc='Dynamic, any to any, pixel format conversion library'
arch=(i686 x86_64)
url='http://www.gegl.org/babl/'
license=(LGPL-3)
depends=(lcms2)
optdepends=('gobject-introspection: gir bindings'
            'vala: vala bindings')
makedepends=(gobject-introspection meson quilt vala)
options=(!makeflags)
source=(https://download.gimp.org/pub/babl/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz
        https://deb.debian.org/debian/pool/main/b/babl/babl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('f6eabc53a8b9a2f6002ec75ec9175aa854e54a6d1c14e8723506d44f7fc3230dbfb19b4595d73d5922c705a97a000230aa6ed33f4dc0de0d9399dc939fe61c8c'
            'c260ae079fb20d77e5228c950ea887cb7179e1f7abc83040cc10d8e4828190f622a3737a7cfc4d2a22557d02be0751a12105ba3f98b996ed962a12381268747a')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  if [ $CARCH = i686 ]; then
    export AVX2=false
    export F16C=false
    export MMX=true
    export SSE=true
    export SSE2=true
    export SSE4_1=false
  elif [ $CARCH = x86_64 ]; then
    export AVX2=true
    export F16C=true
    export MMX=true
    export SSE=true
    export SSE2=true
    export SSE4_1=true
  fi

  hyperbola-meson $pkgname-$pkgver build \
    -D enable-avx2=$AVX2 \
    -D enable-f16c=$F16C \
    -D enable-mmx=$MMX \
    -D enable-sse=$SSE \
    -D enable-sse2=$SSE2 \
    -D enable-sse4_1=$SSE4_1
  ninja -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  install -Dm644 $pkgname-$pkgver/COPYING -t $pkgdir/usr/share/licenses/$pkgname
}

# vim:set sw=2 et:
