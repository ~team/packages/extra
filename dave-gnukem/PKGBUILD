# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dave-gnukem
pkgver=1.0.3
pkgrel=1
pkgdesc="Free and libre 2D scrolling platform shooter"
arch=('i686' 'x86_64')
url='https://github.com/davidjoffe/dave_gnukem'
license=('GPL-2' 'Expat')
depends=('hicolor-icon-theme' 'sdl2_mixer')
makedepends=('graphicsmagick')
groups=('games')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/davidjoffe/dave_gnukem/archive/${pkgver}.tar.gz"
        "${pkgname}-data-${pkgver}.tar.gz::https://github.com/davidjoffe/gnukem_data/archive/${pkgver}.tar.gz"
        "${pkgname}"
        "${pkgname}.desktop"
        "${pkgname}.png"
        "libre.patch")
sha512sums=('b1b4ce3212ff3e6e4b0a9bf33c04bfe0df9e096e9faa8cd06cb50ac226e79cb0042b7150c0568172e531f3d90c3fb758365edaa8e25de141a8ba837264a7eadf'
            'fd628974f09de9e6f0e0416dfd48704766e2351a9e1e086cc0fe7fd3ad908d289ccccc5b67f8f8851a9e4a58f8caab1424e506341d928ba564679d3305061bf5'
            '2bcddd90cee07a4870188e1929d317a882e4d676fbb25f01663bd3d5e433aefde0b7bf8819e383a7d17cafd2aba5727ca59655ecd5b4f69f6a222a05b870f395'
            'b633d69ffa5e6c3bf58f07f203bfe9c2aac173a451674eddce0cab87cf3c73fb2d9a6ca7b80ed26db00fdf59a8b1a2eb698a3671de110fd66d16c95a80112ff8'
            '96e47115aa57f36094ecba554b090247a60fb86c466a94c796bf402c4df48c2fe3bb7da9a63ce8b40b6ff134e19425ccb6bb6f5304092f706e33b0c7e9b432f3'
            '26cc3f6790dd2e2593d7b2043502163b2390e7ee655698ae9699e4313feb9bc8ec134e7cd8ef8f404a27562891bc02cc9611028066126a99675347e3c7c7193c')

prepare() {
  # remove -DDEBUG flag in makefile
  sed -i 's/-DDEBUG//' "${pkgname/-/_}-${pkgver}/Makefile"

  cd "${pkgname/-/_}-${pkgver}"
  patch -Np1 -i "${srcdir}/libre.patch"
}

build() {
  make -C "${pkgname/-/_}-${pkgver}"
}

package() {
  install -Dvm755 "${pkgname}" -t "${pkgdir}/usr/games"
  install -Dvm644 "${pkgname}.desktop" -t "${pkgdir}/usr/share/applications"
  for i in 16 32 64 128; do
    gm convert "${pkgname}.png" -resize "${i}x${i}" "icon${i}.png"
    install -Dvm644 "icon${i}.png" "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
  done

  install -dv "${pkgdir}/usr/share/games/${pkgname}/data"
  cp -rfv "${pkgname#*-}_data-${pkgver}/"* "${pkgdir}/usr/share/games/${pkgname}/data"

  cd "${pkgname/-/_}-${pkgver}"
  install -Dm755 "${pkgname/-/}" "${pkgdir}/usr/share/games/${pkgname}/${pkgname}"
  install -Dm644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm644 COPYING MIT-LICENSE.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
