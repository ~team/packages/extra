# Maintainer (Arch): Jelle van der Waa <jelle@vdwaa.nl>
# Maintainer (Arch): Levente Polyak <anthraxx@archlinux.org>
# Contributor (Arch): Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor (Arch): Ingo Bürk <ingo.buerk@airblader.de>
# Contributor (Arch): Daniel M. Capella <polyzen@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>

pkgname=i3status
pkgver=2.13
pkgrel=2
pkgdesc="Generates status bar to use for example with i3bar or dzen2"
arch=('i686' 'x86_64')
url='https://i3wm.org/i3status/'
license=('Modified-BSD')
groups=('i3')
depends=('sndio' 'confuse' 'libnl' 'yajl')
makedepends=('asciidoc' 'xmlto')
backup=('etc/i3status.conf')
options=('docs')
source=("https://i3wm.org/i3status/$pkgname-$pkgver.tar.bz2"
        "build-without-pulse.patch"
        "add-sndio-backend.patch")
sha512sums=('6dadff19e53499d169ba4f491e1f821014b4f92fc3c93d7947c85cbbbdeaba538d02bd8ab98fe266a8f80756a287fd5803ec77a8cd874d50082b5cad309875c2'
            '3efd2ca3ef71a37689d5e6bbf0acb37571fa5dcf20608bad78f03a23386c4ece09519f7defe8d5ffe6d566b7864f82492316de558e5fdf5c9c2e0c3267ca263f'
            'd44093627fabc712b6464c1d045b5592995ab8b64b8227e9ee14bef384dc2226557239cbd7c3c67d39ff3fdfa065f507e75832ec2bc470e8a4aedd5d50b2830c')

prepare() {
  cd $pkgname-$pkgver

  patch -Np1 -i $srcdir/build-without-pulse.patch
  patch -Np1 -i $srcdir/add-sndio-backend.patch
  autoreconf -vi
}

build() {
  cd $pkgname-$pkgver
  mkdir build && cd build

  ../configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --disable-sanitizers \
    --disable-pulseaudio
  make CPPFLAGS+="-U_FORTIFY_SOURCE"
}

package() {
  cd $pkgname-$pkgver/build
  make DESTDIR="$pkgdir" install
  install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname ../LICENSE
}
