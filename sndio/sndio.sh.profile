#!/bin/sh

if [ -n "$XDG_CONFIG_HOME" ] && [ -r "$XDG_CONFIG_HOME/sndio.conf" ]; then
  . "$XDG_CONFIG_HOME/sndio.conf"
elif [ -n "$HOME" ] && [ -r "$HOME/.config/sndio.conf" ]; then
  . "$HOME/.config/sndio.conf"
elif [ -r /etc/sndio.conf ]; then
  . /etc/sndio.conf
fi

[ -n "$AUDIODEVICE" ] && export AUDIODEVICE
[ -n "$MIDIDEVICE" ]  && export MIDIDEVICE
