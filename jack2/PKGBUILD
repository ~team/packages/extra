#!/bin/ksh

# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): Daniele Paolella <danielepaolella@email.it>
# Contributor (Arch): Philipp Überbacher <hollunder at gmx dot at>
# Contributor (Arch): Thomas Bahn <thomas-bahn at gmx dot net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=jack
_pkgbase=jack2
pkgname=('jack2' 'libjack')
pkgver=1.9.17
pkgrel=3
_debname=jackd2
_debver=$pkgver
_debrel=1
arch=('i686' 'x86_64')
url='https://jackaudio.org/'
makedepends=('celt' 'db' 'libsamplerate' 'opus' 'readline' 'waf' 'quilt')
_url=("https://deb.debian.org/debian/pool/main/${_debname::1}/${_debname}")
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/jackaudio/${pkgname}/archive/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/j/jackd2/jackd2_${_debver}~dfsg-${_debrel}.debian.tar.xz"
        '40-audio-privileges.rules'
        'jackd.sh'
        'jackd.confd'
        'jackd.initd')
sha512sums=('dff611273e23cea6fe258114c65e193f01dfa0366ddd4e6a026a3d676a32002ee744bc70085fb8568cb85395399072beec2c7f7eed8d9ec912332a2a56623521'
            'b33163af66ecde21e4e158221b3f1963c6d66b1a207bd51aede689fd1195b88accffd219931ec7e73760e3e22085d94a4631e6ac91aa07e289053157bde9ecec'
            '7a8383fe2820b89159c4532a11eca6103a9d3001f20bb73d84f56512a885ac756deeca918627d88902f864ac96abeb2843b6bc9692b4d94150c08b1ebfa6cffe'
            '00350094e1be304d6ebf9c850a942d84a94a06040c0f892caebeb604e426000e77ba8a566ce5f06156b5b3589cef3c761a8aca294f95b687cd968bdc41126c14'
            '0478d821a280a74c2e7f58147cbe981201e7d06ae19e24c8cebacc43e802c0e3ba0330589b8a2a399269e6392188beec542da519be8b96a48105c5303057e77a'
            '7761988c17e61cc485bbe0e720836ddeefe14dc7e8d473d44db165e9fb6d7be3aca152171f11baa8f1294d01f76e79abd164aa5b1c9107c4edd878ace81be6c2')

prepare() {
  cd "${srcdir}/${_pkgbase}-${pkgver}"

  if [ "${pkgver%.*}" = "${_debver%.*}" ]; then
    # Debian patches
    export QUILT_PATCHES='debian/patches'
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "${srcdir}/debian" '.'

    # Doesn't apply
    rm -v 'debian/patches/reproducible-build.patch' || true

    quilt 'push' -av
  fi
  unset QUILT_PATCHES QUILT_REFRESH_ARGS QUILT_DIFF_ARGS

  # copying relevant custom waf scripts and loading them specifically using
  # wscript
  (
    touch '__init__.py'
    mkdir -vp 'tools'
    cp -v 'waflib/extras/xcode'*'.py' 'tools'
    rm -rv 'waflib'
    sed -e "s/load('xcode'/load('xcode', tooldir='tools'/g" \
        -e "s/load('xcode6'/load('xcode6', tooldir='tools'/g" \
        -i 'wscript'
  )

  # use sun in HyperbolaBSD and alsa in Hyperbola GNU/Linux
  cp "${srcdir}/jackd.confd" "${srcdir}/_jackd.confd"
  if [ "$(uname -s)" = 'HyperbolaBSD' ]; then
    sed -i 's|%SYSAUDIO%|-d sun -C /dev/audio0 -P /dev/audio0|' \
      "${srcdir}/_jackd.confd"
  elif [ "$(uname -o)" = 'GNU/Linux' ]; then
    sed -i 's|%SYSAUDIO%|-d alsa -d hw:0|' \
      "${srcdir}/_jackd.confd"
  fi
}

build() {
  cd "${srcdir}/${_pkgbase}-${pkgver}"

  export PYTHONPATH="${PWD}:${PYTHONPATH}"
  waf 'configure' --prefix='/usr' \
                  --htmldir="/usr/share/doc/${pkgbase}/" \
                  --autostart='none' \
                  --classic
  waf 'build'
}

package_jack2() {
  license=('GPL-2')
  pkgdesc="A low-latency audio server"
  backup=('etc/conf.d/jackd')
  depends=('libjack' 'readline')
  provides=('jack')
  conflicts=('jack')

  cd "${srcdir}/${_pkgbase}-${pkgver}"

  export PYTHONPATH="${PWD}:${PYTHONPATH}"
  waf 'install' --destdir="${pkgdir}"

  # move jackd binary file to correct directory and
  # fix RT priority in GNU/Linux
  if [ "$(uname -s)" = 'HyperbolaBSD' ]; then
    install -dm '750' -g '4' "${pkgdir}/usr/sbin"
    mv "${pkgdir}/usr/bin/jackd" "${pkgdir}/usr/sbin"
  elif [ "$(uname -o)" = 'GNU/Linux' ]; then
    install -dm '755' "${pkgdir}/usr/libexec/jack"
    mv "${pkgdir}/usr/bin/jackd" "${pkgdir}/usr/libexec/jack"
    install -dm '750' -g '4' "${pkgdir}/usr/sbin"
    install -Dm '755' "${srcdir}/jackd.sh" "${pkgdir}/usr/sbin/jackd"
  fi

  # move jackd manual file to correct directory
  install -dm '750' -g '4' "${pkgdir}/usr/share/man/man8"
  mv "${pkgdir}/usr/share/man/man1/jackd.1" \
    "${pkgdir}/usr/share/man/man8/jackd.8"

  # remove libjack library files, because these are files from libjack package
  rm "${pkgdir}/usr/lib/libjack"*".so"*

  # remove files, because these are files from libjack package
  for i in 'usr/lib/pkgconfig' 'usr/include'; do
    rm -r "${pkgdir}/${i}"
  done
  unset i

  # install udev rules
  install -Dm '644' "${srcdir}/40-audio-privileges.rules" \
    -t "${pkgdir}/lib/udev/rules.d"

  # install OpenRC
  install -Dm '644' "${srcdir}/_jackd.confd" "${pkgdir}/etc/conf.d/jackd"
  install -Dm '755' "${srcdir}/jackd.initd" "${pkgdir}/etc/init.d/jackd"

  # install license
  install -Dm644 "COPYING" \
    -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

package_libjack() {
  license=('GPL-2')
  pkgdesc="A low-latency audio server libraries"
  depends=('celt' 'db' 'libsamplerate' 'opus')
  makedepends=('waf' 'quilt')

  cd "${srcdir}/${_pkgbase}-${pkgver}"

  export PYTHONPATH="${PWD}:${PYTHONPATH}"
  waf 'install' --destdir="${pkgdir}"

  # remove files, because these are files from jack2 package
  for i in 'usr/bin' 'usr/lib/jack' 'usr/share'; do
    rm -r "${pkgdir}/${i}"
  done
  unset i

  # install license
  install -Dm644 "COPYING" \
    -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
