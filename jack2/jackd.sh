#!/bin/sh
prlimit --rtprio='99' '/usr/libexec/jack/jackd' "${@}"
prlimit --memlock='unlimited' --pid "${?}"
