# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Mirco Tischler <mt-ml at gmx dot de>
# Contributor (Arch): Keshav Amburay <(the ddoott ridikulus ddoott rat) (aatt) (gemmaeiil) (ddoott) (ccoomm)>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=efitools
pkgver=1.9.2
_debver=1.9.2
_debrel=1
pkgrel=1
pkgdesc="Tools for manipulating UEFI secure boot platforms"
url='http://git.kernel.org/cgit/linux/kernel/git/jejb/efitools.git'
arch=('i686' 'x86_64')
replaces=('prebootloader')
license=('custom:GPL-2+OpenSSL-Linking-Exception' 'LGPL-2.1')
options=('!strip' '!makeflags')
depends=('libressl')
makedepends=('gnu-efi-libs' 'help2man' 'sbsigntools' 'perl-file-slurp' 'quilt')
source=("https://git.kernel.org/pub/scm/linux/kernel/git/jejb/efitools.git/snapshot/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/e/efitools/efitools_${_debver}-${_debrel}.debian.tar.xz"
        "libressl.patch")
sha512sums=('77e0ad7e865814ed388ff6daabe0f4b49ba51672bf2cbb98b7905e209cbd28f9ede2f73213ce45af8a978c1e67dba24ec88a1188661317cc22317b47e575cde8'
            '91b935de3e045fd0ade635a3095077599647d5dd3300e03099a53560ac3524d6acd06c09791784ba2d72fced082c4b8d652aba2de87aad92e14b5508cc60c315'
            '27a5331f00fc3d8f18187120940b6e46c6267c0478663378a28c5beb80a761422162e002836a4f0db096a6386cbb2e2c5e328e9355638aab82b9556c992b9e0f')

prepare() {
  cd "${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  patch -p1 -i "${srcdir}/libressl.patch"
}

build() {
  cd "${pkgname}-${pkgver}"
  # fix PreLoader.efi building on x86_64
  export ARCH="${CARCH}"
  make
}

package() {
  # http://www.rodsbooks.com/efi-bootloaders/secureboot.html#prebootloader
  # http://blog.hansenpartnership.com/uefi-secure-boot/
  # http://blog.hansenpartnership.com/linux-foundation-secure-boot-system-released
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
