# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): dorphell <dorphell@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: dikasp2

pkgname=dillo
pkgver=3.0.5
_debver=$pkgver
_debrel=7
pkgrel=3
pkgdesc="A small, fast, free and libre graphical web browser"
arch=('i686' 'x86_64')
url='https://dillo-browser.github.io/'
license=('GPL-3')
depends=('fltk' 'libjpeg-turbo' 'libpng' 'libressl' 'perl' 'zlib')
makedepends=('quilt')
backup=(etc/dillo/{dillorc,domainrc,dpidrc,keysrc})
source=("${pkgname}-${pkgver}.tar.bz2::https://deb.debian.org/debian/pool/main/d/${pkgname}/${pkgname}_${pkgver}.orig.tar.bz2"
        "https://deb.debian.org/debian/pool/main/d/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz"
        "libre.patch"
        "$pkgname.desktop"
        "$pkgname.png")
sha512sums=('d0e86f57e50da762debf592b6deb94de5ee58f84e0cae1159890262540a463aea8143d2b99c634021c1f273f91c2fe0918f30d72c3eaf91fdb541e741469b155'
            '394ea20c345cce7492bf16270805af21d4a0a3f9e626aa2550f576dea0043ea41c2c51d361ef2d0aea8b36131250030130a750b5279fa08f95086ff03a5e06fc'
            '758dc86543ae1ac0d1102e23c4a37846ffa1e663ff64198e458a28535665ab93c81c3e4a3d5faec7840f62aeae46d797711aae7b6d3e19fb8ece43178af60d9e'
            'a8632b1bb06d46781cc8dbbe3521a9da53a540eb841f523b36313b72ebe4ad24386d1ddd348bcb46556d3b569228e418258fc1493ebf1c0f89905d2df6cea816'
            'd10f20ac3402be8a2c25da63d305eb66d553e47403128706aa50ccc122b2d519b13936b8f15990cd2bad0ca36e1b6c5e576e6b794f0ad36c2bd8ee93d9780b1e')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # additional fixes as package should not contain non-free parts
  patch -Np1 -i ${srcdir}/libre.patch

  autoreconf -vfi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  CFLAGS+=' -fcommon'
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --enable-cookies \
    --enable-ssl
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir/$pkgname.desktop" "$pkgdir/usr/share/applications/$pkgname.desktop"
  install -Dm644 "$srcdir/$pkgname.png" "$pkgdir/usr/share/pixmaps/$pkgname.png"

  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}