# Maintainer (Arch): Angel Velasquez <angvp@archlinux.org>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

_pkgname=pycairo
pkgname=python-$_pkgname
pkgver=1.16.2
_debver=$pkgver
_debrel=4
pkgrel=3
pkgdesc='Python bindings for the cairo graphics library'
arch=(i686 x86_64)
url='https://pycairo.readthedocs.io/en/latest/'
license=('LGPL-2.1')
depends=('cairo' 'python')
makedepends=('quilt')
provides=('python-cairo')
conflicts=('python-cairo')
replaces=('python-cairo')
source=(https://github.com/pygobject/$_pkgname/releases/download/v$pkgver/$_pkgname-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/p/$_pkgname/${_pkgname}_$_debver-$_debrel.debian.tar.xz)
sha512sums=('a5ac07f96f6607b1b40912b000c8b3b2114e75a6b8b314f1b68d2f5bb408dd7823c464d096c8cc26164219a961b448a4ea90b5a76683fb1807180f83cbc3b300'
            '1831bf4afd8277265e42593675833ab8da507f702be7a2da06df92d503592287d489849fbb118b65789293b115ca0012433c001717266c02444c4e9b670f5842')

prepare() {
  cd $_pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $_pkgname-$pkgver
  python setup.py build
}

package() {
  cd $_pkgname-$pkgver
  python setup.py install --skip-build --root="${pkgdir}" --optimize='1'
  install -Dm644 COPYING-LGPL-2.1 -t $pkgdir/usr/share/licenses/$pkgname
}
