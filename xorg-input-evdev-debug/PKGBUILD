# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-input-evdev package

pkgname=xorg-input-evdev-debug
pkgver=2.10.6
pkgrel=4
pkgdesc="X.Org evdev input driver"
arch=(i686 x86_64)
url="https://xorg.freedesktop.org/"
license=('Expat')
depends=('libeudev' 'mtdev' 'libevdev')
makedepends=('xenocara-server-devel' 'X-ABI-XINPUT_VERSION=24.1' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-input-evdev' 'xenocara-input-evdev')
conflicts=('xf86-input-evdev' 'xenocara-server<1.20' 'X-ABI-XINPUT_VERSION<24.1' 'X-ABI-XINPUT_VERSION>=25')
replaces=('xf86-input-evdev' 'xf86-input-libinput')
options=('!makeflags')
groups=('xenocara-drivers-debug' 'xorg-drivers-debug')
source=(https://xorg.freedesktop.org/releases/individual/driver/xf86-input-evdev-$pkgver.tar.bz2{,.sig})
options=(!strip) # It's required for debug packages
sha512sums=('560b0a6491d50a46913a5890a35c0367e59f550670993493bd9712d712a9747ddaa6fe5086daabf2fcafa24b0159383787eb273da4a2a60c089bfc0a77ad2ad1'
            'SKIP')
validpgpkeys=('3C2C43D9447D5938EF4551EBE23B7E70B467F0BF') # Peter Hutterer (Who-T)

prepare() {
  cd "xf86-input-evdev-$pkgver"
  autoreconf -vfi
}

build() {
  cd "xf86-input-evdev-$pkgver"

  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

package() {
  cd "xf86-input-evdev-$pkgver"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
