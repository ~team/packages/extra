# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Abhishek Dasgupta <abhidg@gmail.com>
# Contributor (Arch): Gabor Nyekhelyi (n0gabor) <n0gabor@vipmail.hu>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pitivi
pkgver=2022.06
pkgrel=1
pkgdesc="Free and libre editor for audio / video projects using the multimedia graph framework"
arch=('i686' 'x86_64')
url='https://pitivi.org/'
license=('LGPL-2.1')
depends=('gst-editing-services' 'gst-plugin-gtk' 'gst-plugins-bad' 'gst-plugins-good' 'gst-python'
         'gtk' 'python-pycairo' 'python-pygobject' 'python-matplotlib' 'python-numpy')
makedepends=('intltool' 'itstool' 'meson' 'quilt')
optdepends=('gst-libav: additional multimedia codecs'
            'gst-plugins-ugly: additional multimedia codecs')
source=("https://download.gnome.org/sources/$pkgname/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
        "remove-warning-soft-deps.patch")
sha512sums=('649c8e61d9545cbf35a0a0c8f648490c14c8bfb6c6637794887c03bffd6998c63cb2cc22102cd2f7a59974f79488a13f29beceadab29ddd1c1c01abff33a85fa'
            '87be36c0ed0048848a8522c033f9154cbf7941fcd6e41671483eee8cc77fab3be08d2834c789482745ab56489d721264200b2f8217fb8dc10655af88fa1f667e')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i "$srcdir"/remove-warning-soft-deps.patch
}

build() {
  hyperbola-meson $pkgname-$pkgver build
  meson compile -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build
  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING" -t "$pkgdir/usr/share/licenses/$pkgname"
}
