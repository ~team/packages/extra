# Maintainer (Arch): Pierre Schmitz <pierre@archlinux.de>
# Contributor (Arch): Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lynx
pkgver=2.9.0
_relver=${pkgver}dev.6
_debver=2.9.0dev.6
_debrel=1
pkgrel=1
pkgdesc="A text browser for the World Wide Web"
url="https://lynx.browser.org/"
arch=('i686' 'x86_64')
license=('custom:GPL-2+Lynx-Special-Exception')
depends=('libressl' 'libidn')
makedepends=('quilt')
backup=('etc/lynx.cfg')
source=(https://invisible-mirror.net/archives/lynx/tarballs/${pkgname}${_relver}.tar.bz2{,.asc}
        https://deb.debian.org/debian/pool/main/l/lynx/lynx_$_debver-$_debrel.debian.tar.xz)
sha512sums=('0c15e00a8e36e43671a093182c69593d50bc5c51a4acd92faa59416055bf4a1fd9ecde903a0209963f1f55d1fd85136e8448ca7867e198100ff749c53e1e1531'
            'SKIP'
            'b3742e155d6107925fdb6b06c45dc1a54de9f043721f641cf11ccfbac46812c61c3cf33a516edad68a0f1ae74e256bfeed83bdd0797fcadf707ca5236f3b3f9c')
validpgpkeys=('C52048C0C0748FEE227D47A2702353E0F7E48EDB')

prepare() {
  cd ${srcdir}/${pkgname}${_relver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/lynxcfg.patch || true
    rm -v debian/patches/nested_tables.patch || true

    quilt push -av
  fi
}

build() {
  cd ${srcdir}/${pkgname}${_relver}
  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --with-ssl=/usr \
    --enable-nls \
    --enable-ipv6 \
    --mandir=/usr/share/man
  make
}

package() {
  cd ${srcdir}/${pkgname}${_relver}
  make DESTDIR=${pkgdir} install

  # FS#20404 - points to local help
  sed -i -e "s|^HELPFILE.*$|HELPFILE:file:///usr/share/doc/lynx/lynx_help/lynx_help_main.html|" ${pkgdir}/etc/lynx.cfg

  install -d ${pkgdir}/usr/share/doc/lynx
  cp -rf lynx_help ${pkgdir}/usr/share/doc/lynx

  for i in COPYHEADER COPYING; do
  install -Dm644 ${i} ${pkgdir}/usr/share/licenses/${pkgname}/${i}
  done
}
