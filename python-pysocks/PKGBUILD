# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Michael Schubert <mschu.dev at gmail>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-pysocks
pkgname=(python-pysocks tauthon-pysocks)
pkgver=1.7.0
pkgrel=2
arch=('any')
license=('Modified-BSD')
url='https://github.com/Anorov/PySocks'
makedepends=('python-setuptools' 'tauthon-setuptools')
source=("$pkgname-$pkgver.tar.gz::https://github.com/Anorov/PySocks/archive/$pkgver.tar.gz")
sha512sums=('280c9d1f147cd665a9989dc143e0b1f43bbc0575dc0b6b92472bc4b8cf2315f37dba74c5e77ff0b3c3ede49ee5c1e53d6a330bcb3c8d822023378c1cfb8cac0f')

prepare() {
  cp -a PySocks-$pkgver{,-tauthon}
}

build() {
  cd "$srcdir"/PySocks-$pkgver
  python setup.py build

  cd "$srcdir"/PySocks-$pkgver-tauthon
  tauthon setup.py build
}

package_python-pysocks() {
  pkgdesc="SOCKS4, SOCKS5 or HTTP proxy (Anorov fork PySocks replaces socksipy) in Python"
  depends=('python')

  cd PySocks-$pkgver
  python setup.py install --prefix=/usr --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname/
}

package_tauthon-pysocks() {
  pkgdesc="SOCKS4, SOCKS5 or HTTP proxy (Anorov fork PySocks replaces socksipy) in Tauthon"
  depends=('tauthon')

  cd PySocks-$pkgver-tauthon
  tauthon setup.py install --prefix=/usr --root="$pkgdir" --optimize=1
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname/
}
