# Maintainer (Arch): Baptiste Jonglez <archlinux at bitsofnetworks dot org>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Simon Legner <Simon.Legner@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pmount
pkgver=0.9.23
_debver=0.9.23
_debrel=6
pkgrel=2
pkgdesc="Mount removable devices as normal user"
arch=('i686' 'x86_64')
license=('GPL-2')
url="http://pmount.alioth.debian.org/"
backup=('etc/pmount.allow')
depends=('sysfsutils' 'util-linux')
makedepends=('intltool' 'gettext-tiny' 'quilt')
provides=('pmount')
conflicts=('pmount')
source=("https://alioth-archive.debian.org/releases/${pkgname}/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/p/pmount/pmount_${_debver}-${_debrel}.debian.tar.xz"
        "pmount-bash-completion" # From pmount-debian
        "pmount_fix-a-careless-segfault-in-debug-mode.patch"
        "pmount_exfat.patch")
sha512sums=('a9d762fb9bd3f525d52b29a33c09c10a09f90fabdeed8f3658cb3fe8cbdbf237b2c9165b7c93a7170b9de5c65c513c1629b1fca638563a66d091c518782db92d'
            '97cfd97fded1ffa1d88973b2fe40899ed0659910a30f7b04cac8eb739ac549976582f6cddc555e61dad1f0267aa0c60502c80ab1015960a6ccd8599ad8a67680'
            'a78b2432c9e629856ff6eec0c396f7affd9837c4deabe3058d2caaf09179dc4c670f691ace460ffb1708b8a6233990c51e0425ac998551f5cd6343ebf65513cc'
            '90f49eaf2efef522b3a634ccc99776bbf3e9a953115153714903de85a1a6b06554bfbec62665dade986f7837a83301525f81b522a7001eada98044f67a546cc0'
            'a582ef694cde15f737e968dcbaa5efe75870de3feccd53e48e25e0f3edcf6aca6f2b4fc01399f88cbb681fb7811bf9e06b926fae0f3c0c7c766627840f5bf403')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 < "${srcdir}/pmount_fix-a-careless-segfault-in-debug-mode.patch"
  patch -p1 < "${srcdir}/pmount_exfat.patch"
}

build() {
  # commented out in order to avoid *** Error in `/usr/bin/ld': corrupted double-linked list: 0x09e43ce8 ***
  #export CFLAGS="${CFLAGS} -fPIE -pie"
  #export CXXFLAGS="${CXXFLAGS} -fPIE -pie"

  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr --disable-hal \
    --with-cryptsetup-prog=/usr/bin/cryptsetup
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 ${srcdir}/pmount-bash-completion ${pkgdir}/usr/share/bash-completion/completions/pmount
  mkdir -p ${pkgdir}/media
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
