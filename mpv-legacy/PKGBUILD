# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Eivind Uggedal <eivind@uggedal.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

_realpkgname=mpv
pkgname=$_realpkgname-legacy
pkgver=0.32.0
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="A free and libre cross-platform media player (legacy version)"
arch=('i686' 'x86_64')
license=('GPL-2' 'LGPL-2.1')
url='https://mpv.io/'
depends=('alsa-utils' 'jack' 'desktop-file-utils' 'ffmpeg' 'glibc'
         'hicolor-icon-theme' 'lcms2' 'libarchive' 'libass'
         'libcaca' 'libcdio' 'libcdio-paranoia' 'libdrm' 'libdvdnav' 'libdvdread' 'mesa'
         'libjpeg-turbo' 'libplacebo' 'libx11' 'libxext' 'libxinerama' 'libxkbcommon' 'libxrandr' 'libxss'
         'libxv' 'lua' 'lua52' 'uchardet' 'xdg-utils' 'zlib' 'sdl2')
makedepends=('mesa' 'python-docutils' 'ladspa' 'waf' 'quilt' 'libsndio'
             'lua52' 'alsa-lib' 'libjack')
optdepends=('sndio: sndio audio driver')
provides=('mpv')
options=('!emptydirs')
source=($_realpkgname-$pkgver.tar.gz::https://github.com/mpv-player/mpv/archive/refs/tags/v${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/m/mpv/mpv_$_debver-$_debrel.debian.tar.xz
        remove-ytdl.patch)
sha512sums=('f6426c0254ec0bf2f120e2196904f1e15fe17032b06764abca1d5e074f0cabb452eaf1cd09f8fd9b25b591accee7b881bfc3b06c19d5c98980305c4712486bd6'
            '513821281e28a07e8915172752577d387a95623cb97476156ef7a762ce0b97a21a122b5b994920a3be2af33bcf35e9e54d8bb2f2a2df1988b89126bfd141490a'
            'e54066ddbd3aba0bef45392a7ac591789341c7866aa1b2639072fb13e1c96c369dbcff571d5fc2ac8e3871db0d13af6e0eac03e4414586339a116a46c4f7502b')

prepare() {
  cd $_realpkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/03_waf.patch || true

    quilt push -av
  fi

  # remove ytdl complete
  patch -Np1 -i $srcdir/remove-ytdl.patch
}

build() {
  cd $_realpkgname-$pkgver

  waf configure --prefix=/usr \
                --confdir=/etc/mpv \
                --enable-cdda \
                --enable-dvb \
                --enable-dvdnav \
                --enable-libarchive \
                --enable-libmpv-shared \
                --enable-sdl2 \
                --enable-sndio \
                --enable-jack \
                --enable-alsa \
                --enable-lua \
                --disable-javascript \
                --disable-libsmbclient \
                --disable-rubberband \
                --disable-pulse \
                --disable-vulkan \
                --disable-build-date

  waf build
}

package() {
  cd $_realpkgname-$pkgver

  waf install --destdir="$pkgdir"

  install -Dm644 DOCS/{encoding.rst,tech-overview.txt} "$pkgdir"/usr/share/doc/mpv
  install -Dm644 TOOLS/lua/* -t "$pkgdir"/usr/share/mpv/scripts
  install -Dm644 LICENSE.{LGPL,GPL} -t "${pkgdir}/usr/share/licenses/$pkgname"
}
