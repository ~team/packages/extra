# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): mutantmonkey <mutantmonkey@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libmilter
_pkgname=sendmail
pkgver=8.15.2
_debver=8.15.2
_debrel=22
pkgrel=1
pkgdesc='Implementation of the sendmail Mail Filter API'
url='https://www.proofpoint.com/us/sendmail-open-source'
arch=('i686' 'x86_64')
options=('staticlibs')
license=('custom:Sendmail')
makedepends=('quilt')
validpgpkeys=('30BCA74705FA415455731D7BAAF5B5DE05BDCC53')
source=("https://ftp.sendmail.org/${_pkgname}.${pkgver}.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/s/sendmail/sendmail_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('04feb37316c13b66b1518596507a7da7c16cb0bf1abf10367f7fd888a428fadb093a9efa55342fa55b936c3f0cbdc63b9e2505cd99201a69a0c05b8ad65f49f9'
            'SKIP'
            '3ed2f2bc21acf80161a02b5348594023fcef11f9608dc98e56cc1fceeea9447e0417b6ab73c423f883ad8fd42b96918f411b0ca054bb49675a6c8763e6ada65b')


prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/shared_libmilter.patch || true

    quilt push -av
  fi

  # From http://www.j-chkmail.org/wiki/doku.php/doc/installation/start#libmilter
  cat >> devtools/Site/site.config.m4 <<EOF
dnl Include our flags
APPENDDEF(\`conf_libmilter_ENVDEF',\`${CPPFLAGS} ${CFLAGS}')
dnl Enable libmilter with a pool of workers
APPENDDEF(\`conf_libmilter_ENVDEF',\`-D_FFR_WORKERS_POOL=1 -DMIN_WORKERS=4')
dnl Use poll instead of select
APPENDDEF(\`conf_libmilter_ENVDEF',\`-DSM_CONF_POLL=1')
dnl Enable IPv6
APPENDDEF(\`conf_libmilter_ENVDEF',\`-DNETINET6=1')
dnl Add -fPIC
APPENDDEF(\`conf_libmilter_ENVDEF',\`-fPIC')
dnl Permissions
APPENDDEF(\`confINCGRP',\`root')
APPENDDEF(\`confLIBGRP',\`root')
EOF
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}/${pkgname}"
  ./Build
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}/${pkgname}"
  install -d "${pkgdir}/usr/lib"
  ./Build DESTDIR="${pkgdir}" install
  install -Dm644 ../LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
