# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Thomas Dziedzic <gostrc at gmail>
# Contributor (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Contributor (Arch): Tobias Kieslich <tobias@justdreams.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=gts
pkgver=0.7.6+darcs121130
_snap=gts-snapshot-121130
_debver=0.7.6+darcs121130
_debrel=4
pkgrel=2
pkgdesc='Provides useful functions to deal with 3D surfaces meshed with interconnected triangles'
url='http://gts.sourceforge.net/'
license=('LGPL-2')
arch=('i686' 'x86_64')
depends=('glib2' 'netpbm')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/g/gts/gts_$pkgver.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/g/gts/gts_$_debver-$_debrel.debian.tar.bz2")
sha512sums=('84c38dc345830eea75755d9d55235b6d76786a84c3b9c3b7e057437bf395a9f2687596bbf037afd601b9f31a485d425a371ca5e60680265f10cb414400db4142'
            'f7e0f52955b8e0a58a38c84d09ae5a737dc492d6792c5bc1356b98e314b0447a12226ab6ee312c7ec6fd28a0433929b0e02288856bcc8a33c3bcfcb7827e58f7')

prepare() {
	cd "${srcdir}/${_snap}"
	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian/patches debian

		quilt push -av
	fi
}

build() {
	cd "${srcdir}/${_snap}"
	./configure --prefix=/usr
	make
}

package() {
	cd "${srcdir}/${_snap}"
	make DESTDIR="${pkgdir}" install
	install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
