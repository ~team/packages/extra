# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=ipxe
_pkgver=1.0.0+git-20190125.36a4c85
pkgver=1.0.0+git20190125.36a4c85
_debver=$_pkgver
_debrel=5.1
pkgrel=2
pkgdesc="iPXE free and libre boot firmware"
arch=('i686' 'x86_64')
url='http://www.ipxe.org/'
license=('GPL-2')
depends=('bash')
makedepends=('libisoburn' 'mtools' 'syslinux' 'xz' 'quilt')
install=$pkgname.install
source=("${pkgname}-${_pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/i/ipxe/ipxe_${_pkgver}.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/i/ipxe/ipxe_${_debver}-${_debrel}.debian.tar.xz"
	"ipxe-0002-banner.patch"
	"ipxe-0003-efi-iso.patch"
	"grub"
	"chain-default.ipxe"
	"chain-default-3928.ipxe")
sha512sums=('8503fbc92a29a52368ac3b4dec0bc122767793a87493a83881eaf8b7e4a6dd8dd1211e354d66824e736114e90e1c26b81e89cdee3683d447071344df6ccf1e65'
            '0a62e1d3d6dade60c14e4aa1980674d69011fd42e392e8f900236608acf6c3246b8a90d21c0a11357695318477e1f34c5f2da8cce2bce881ef8f303af5399a0e'
            '6bd7041fc86308e1f9fdbfe168cdf4b5a429810739c070b4bca56855862773d72d52481bf4fd651f998c5b0988e96c3fd1bb5758cd50cd685c5fc1eddfb6e8dd'
            '2a6bf5978845e17c35fece6a626e2983c38f4b3f073f17bf7822be0aa552f4df5e42426e5358c7da4c5554a505123b696d9ab60b07cd5dc1b06e908868dd20f3'
            '4c820bf56a277cf230b2a057ddabca27578a24e6491a0616847b64852feabd48fa4f54e11c91dd79ddf9544820e21e363769c601d7fab1145036d57b9307dd26'
            '9162f528cd0080b9231785795f08d3229c52ce3c18ca5a6efcfbea5028e103a294ddef79a0f28ab64b8d0cdcb9e6cdd7fee797766ad2c3d1dbc3891ddeb4b553'
            '2208f9de509f07c0ae661ec54c8acb751c72ba7d7bb794fc6a8909118cdc06620c5423cf3ae75692af18130f641df6180839c7f09cb98c35925d7147e910d34f')

prepare() {
  cd $pkgname-$_pkgver/

  if [[ ${pkgver%.*.*} = ${_debver%.*.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  cd src/

  # Hyperbola branding
  patch -Np2 < "${srcdir}/ipxe-0002-banner.patch"

  # ISO image with EFI support
  patch -Np2 < "${srcdir}/ipxe-0003-efi-iso.patch"

  # change menu colors
  sed -i "/COLOR_[A-Z]*_BG/s/COLOR_BLUE/COLOR_BLACK/" config/colour.h

  # enable neighbour command
  sed -i "/NEIGHBOUR_CMD/c #define NEIGHBOUR_CMD" config/general.h

  # enable ping command
  sed -i "/PING_CMD/c #define PING_CMD" config/general.h

  # enable HTTPS
  sed -i "/DOWNLOAD_PROTO_HTTPS/c #define DOWNLOAD_PROTO_HTTPS" config/general.h

  # enable reboot and poweroff
  sed -i -e "/REBOOT_CMD/c #define REBOOT_CMD" \
         -e "/POWEROFF_CMD/c #define POWEROFF_CMD" config/general.h

  # disable WEP, WPA and WPA2
  sed -i -e "/CRYPTO_80211_/s/^#define/#undef/" \
         -e "/IWMGMT_CMD/c #undef IWMGMT_CMD" config/general.h

  # Fix compatibility for x86 architectures
  sed -i 's|SIZEOF_LOW_4GB[ ][/][ ]SIZEOF_2MB_PAGE|2048|' arch/x86/transitions/librm.S
}

build() {
  cd $pkgname-$_pkgver/src/

  # default targets (ipxe.{lkrn,dsk,iso,usb}, undionly.kpxe)
  make all

  # this includes drivers, but is bigger
  # build targets with embedded scripts first and rename
  make bin/ipxe.pxe EMBED=${srcdir}/chain-default.ipxe
  mv bin/ipxe.pxe bin/ipxe-default.pxe

  make bin/ipxe.pxe EMBED=${srcdir}/chain-default-3928.ipxe
  mv bin/ipxe.pxe bin/ipxe-default-3928.pxe

  make bin/ipxe.pxe

  if [ $CARCH = x86_64 ]; then
    # EFI
    make bin-i386-efi/ipxe.efi bin-x86_64-efi/ipxe.efi

    # ipxe.liso and ipxe.eiso
    # build after EFI!
    make bin/ipxe.liso bin/ipxe.eiso
  elif [ $CARCH = i686 ]; then
    # EFI
    make bin-i386-efi/ipxe.efi

    # ipxe.liso
    # build after EFI!
    make bin/ipxe.liso
  fi
}

package() {
  cd $pkgname-$_pkgver/

  install -D -m0755 ${srcdir}/grub ${pkgdir}/etc/grub.d/35_ipxe

  install -D -m0644 COPYING ${pkgdir}/usr/share/licenses/ipxe/COPYING
  install -D -m0644 COPYING.GPLv2 ${pkgdir}/usr/share/licenses/ipxe/COPYING.GPLv2
  install -D -m0644 COPYING.UBDL ${pkgdir}/usr/share/licenses/ipxe/COPYING.UBDL

  cd src/

  # iPXE kernel bzImage
  install -D -m0644 bin/ipxe.lkrn ${pkgdir}/usr/lib/ipxe/ipxe.lkrn

  # default targets, legacy and EFI ISO
  install -D -m0644 bin/ipxe.dsk ${pkgdir}/usr/share/ipxe/ipxe.dsk
  install -D -m0644 bin/ipxe.usb ${pkgdir}/usr/share/ipxe/ipxe.usb
  install -D -m0644 bin/ipxe.iso ${pkgdir}/usr/share/ipxe/ipxe.iso
  install -D -m0644 bin/ipxe.liso ${pkgdir}/usr/share/ipxe/ipxe-legacy.iso
  if [ $CARCH = x86_64 ]; then
    install -D -m0644 bin/ipxe.eiso ${pkgdir}/usr/share/ipxe/ipxe-efi.iso
  fi

  # iPXE UNDI-only targets
  install -D -m0644 bin/undionly.kpxe ${pkgdir}/usr/lib/ipxe/undi.kpxe

  # iPXE targets with full driver support
  install -D -m0644 bin/ipxe.pxe ${pkgdir}/usr/lib/ipxe/ipxe.pxe
  install -D -m0644 bin/ipxe-default.pxe ${pkgdir}/usr/lib/ipxe/ipxe-default.pxe
  install -D -m0644 bin/ipxe-default-3928.pxe ${pkgdir}/usr/lib/ipxe/ipxe-default-3928.pxe

  # iPXE EFI targets
  install -D -m0644 bin-i386-efi/ipxe.efi ${pkgdir}/usr/lib/ipxe/efi-i386.efi
  if [ $CARCH = x86_64 ]; then
    install -D -m0644 bin-x86_64-efi/ipxe.efi ${pkgdir}/usr/lib/ipxe/efi-x86_64.efi
  fi
}
