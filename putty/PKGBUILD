# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=putty
pkgver=0.81
pkgrel=1
pkgdesc="A terminal integrated, free and libre SSH client"
arch=('i686' 'x86_64')
url='https://www.chiark.greenend.org.uk/~sgtatham/putty/'
license=('Expat')
depends=('gtk')
makedepends=('cmake')
source=("https://the.earth.li/~sgtatham/putty/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "putty-${pkgver}.tar.gz.sig::https://the.earth.li/~sgtatham/putty/${pkgver}/putty-${pkgver}.tar.gz.gpg"
        "putty.desktop"
        "putty.png"
        "putty.xpm")
sha512sums=('d86f2fd0e126b18275d58cf64334b3b27c450899a1c2be2502de9faa2ef58f7fc8efc5d45f25c8395623f1e21917aa02407343bb2fee44c4c00b9f81267d5ecd'
            'SKIP'
            'fba973f8969ef21b371bfbd773a1992e8b24c047104d10d52e3c246e734fff2e70764e59aa07339671ba3ff513c710f12c6bde3e38a23f153300e521fc827c39'
            '9ba8f29da3f1933b7c2be56dae56ae6d7b0c922cd4eacc21e6408edf3630158868b7dd26e5f74b3dac2e5ea568bb7e4258b3a0582ec372f537cf6162e19ce592'
            'd9e01f3c13c82b1700c048010e7102602b9943426f5890c0a30c8cf410255e44ec87cf513388f096a58dbb41eeaacc3740a1e670a807261a3e07b0ff625ff912')
validpgpkeys=('F412BA3AA30FDC0E77B4E3871993D21BCAD1AA77') # PuTTY Releases <putty@projects.tartarus.org>

build() {
  cmake -B build -S ${pkgname}-${pkgver} \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  install -D -m644 "${srcdir}"/${pkgname}-${pkgver}/LICENCE -t "${pkgdir}"/usr/share/licenses/${pkgname}
  install -D -m0644 "${srcdir}"/putty.desktop "${pkgdir}"/usr/share/applications/putty.desktop
  install -D -m0644 "${srcdir}"/putty.png "${pkgdir}"/usr/share/pixmaps/putty.png
  install -D -m0644 "${srcdir}"/putty.xpm "${pkgdir}"/usr/share/pixmaps/putty.xpm
}
