# Maintainer (Arch): Sébastien Luttringer
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

pkgbase=docutils
pkgname=('python-docutils' 'tauthon-docutils')
pkgver=0.16
_debver=0.16+dfsg
_debrel=4
pkgrel=2
pkgdesc='Set of tools for processing plaintext docs into formats such as HTML or XML'
arch=('any')
url='https://docutils.sourceforge.io/'
license=('GPL-3')
makedepends=('python' 'tauthon' 'quilt')
source=("https://downloads.sourceforge.net/$pkgbase/$pkgbase-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/$pkgname/${pkgname}_$_debver-$_debrel.debian.tar.xz")
sha512sums=('1a4e1bdedc79388e73dd438ae6d4d2ba67a005c745613452e027c0f6b378058b528f3329d650454fc0d3dd5253a1eb95dc52b35846cbfb4d8618d9bd4c3ad934'
            'f90aaaeec788b60dfbb41b5df24491f8950e2eef7499a16ded2714b6b5a7d9ea21f49e95b5ccbbc632901b281b6b8c2dbeacf988bcbb5f0dd747cf9670530541')

prepare() {
  cd $pkgbase-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/doc-debian-paths.diff || true
    rm -v debian/patches/no-local-roman.diff || true

    quilt push -av
  fi
}

build() {
  cd $pkgbase-$pkgver
  msg2 python
  python setup.py build --build-lib=build/python
  find build/python -type f -exec \
    sed -i '1s,^#! \?/usr/bin/\(env \|\)python$,#!/usr/bin/python,' {} \;
  msg2 tauthon
  tauthon setup.py build --build-lib=build/tauthon
  find build/tauthon -type f -exec \
    sed -i '1s,^#! \?/usr/bin/\(env \|\)python$,#!/usr/bin/tauthon,' {} \;
}

check() {
  cd $pkgbase-$pkgver
  # we need utf locale to valid utf8 tests
  export LANG=en_US.UTF-8
  # Disable python check
  #msg2 'python checks'
  #PYTHONPATH="$PWD/build/python/" python test3/alltests.py
  msg2 'tauthon checks'
  PYTHONPATH="$PWD/build/tauthon/" tauthon test/alltests.py
}

package_python-docutils() {
  depends=('python')

  cd $pkgbase-$pkgver
  python setup.py build --build-lib=build/python \
                  install --root="$pkgdir" --optimize=1
  # symlink without .py
  for f in "$pkgdir"/usr/bin/*.py; do
      ln -s "$(basename $f)" "$pkgdir/usr/bin/$(basename $f .py)"
  done
  # setup license
  install -D -m644 COPYING.txt "$pkgdir/usr/share/licenses/$pkgname/COPYING.txt"
  install -D -m644 licenses/python* "$pkgdir/usr/share/licenses/$pkgname/"
}

package_tauthon-docutils() {
  depends=('tauthon')
  provides=("$pkgbase=$pkgver")
  replaces=("pkgbase")
  install=tauthon-$pkgbase.install

  cd $pkgbase-$pkgver
  tauthon setup.py build --build-lib=build/tauthon \
                   install --root="$pkgdir" --optimize=1
  # fix python-docutils conflict
  for _f in "$pkgdir"/usr/bin/*.py; do
      mv -v "$_f" "${_f%.py}tauthon.py"
  done
  # symlink without .py
  for _f in "$pkgdir"/usr/bin/*.py; do
      ln -s "$(basename $_f)" "$pkgdir/usr/bin/$(basename $_f .py)"
  done
  # setup license
  install -D -m644 COPYING.txt "$pkgdir/usr/share/licenses/$pkgname/COPYING.txt"
  install -D -m644 licenses/python* "$pkgdir/usr/share/licenses/$pkgname/"
}

# vim:set ts=2 sw=2 et:
