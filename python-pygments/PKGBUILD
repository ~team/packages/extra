# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Timm Preetz <timm@preetz.us>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=python-pygments
pkgname=('python-pygments' 'tauthon-pygments')
pkgver=2.3.1
_debver=$pkgver
_debrel=1
pkgrel=3
arch=('any')
url='https://pygments.org/'
license=('Simplified-BSD')
makedepends=('python-setuptools' 'tauthon-setuptools' 'quilt')	
source=(https://pypi.org/packages/source/P/Pygments/Pygments-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/p/pygments/pygments_$_debver+dfsg-$_debrel+deb10u2.debian.tar.xz)
sha512sums=('2c36cb42f8dd62e04b7664b5e87f951a8428ccbb6dbe5b5b43d8c7e6923ada0ab55a231bb8e9ed79eb5a85344ed64d3acc8e7bc991ab1f49c58eb612b8002c1e'
            '45c2582d0800cad3f51e7dc5776c9b2cb619c8d629e73ccb9d63a9bde108cf84f2d09d9b3cc4c41ed87fa71a9d2a9a212c6cc762a01a904477510821e2d379ca')

prepare() {
  cd "$srcdir/Pygments-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0003-docs-moved-to-python-pygments-doc-binary-package.patch || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir/Pygments-$pkgver"
  make -C doc
}

package_python-pygments() {
  pkgdesc="Python syntax highlighter"
  depends=('python-setuptools')
  replaces=('pygmentize')
  conflicts=('pygmentize')
  provides=('pygmentize')

  cd "$srcdir/Pygments-$pkgver"

  python setup.py install --root="$pkgdir" -O1
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"

  mkdir -p "$pkgdir/usr/share/doc"
  cp -rT doc/_build/html "$pkgdir/usr/share/doc/$pkgname"
  install -Dm644 doc/pygmentize.1 -t "$pkgdir/usr/share/man/man1"
  install -Dm644 external/pygments.bashcomp "$pkgdir/usr/share/bash-completion/completions/pygmentize"
}

package_tauthon-pygments() {
  pkgdesc="Tauthon syntax highlighter"
  depends=('tauthon-setuptools')

  cd "$srcdir/Pygments-$pkgver"

  tauthon setup.py install --root="$pkgdir" -O1
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"

  # pygmentize is included in python-pygments
  rm "$pkgdir/usr/bin/pygmentize"
  rmdir "$pkgdir/usr/bin"
}
