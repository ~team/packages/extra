# Maintainer (Arch): Steve Engledow <steve@offend.me.uk>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=heroes
pkgver=0.21
_debver=$pkgver
_debrel=18
pkgrel=2
pkgdesc="An arena fighting game."
url='https://heroes.sourceforge.net'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('sdl' 'sdl_mixer')
makedepends=('quilt')
groups=('games')
source=("https://downloads.sourceforge.net/heroes/$pkgname-$pkgver.tar.gz"
        "https://downloads.sourceforge.net/heroes/$pkgname-data-1.5.tar.gz"
        "https://downloads.sourceforge.net/heroes/$pkgname-sound-tracks-1.0.tar.gz"
        "https://downloads.sourceforge.net/heroes/$pkgname-sound-effects-1.0.tar.gz"
        "https://downloads.sourceforge.net/heroes/$pkgname-hq-sound-tracks-1.0.tar.gz"
        "https://deb.debian.org/debian/pool/main/h/heroes/heroes_$_debver-$_debrel.debian.tar.xz")
sha512sums=('8a28f7c0af8f194fd3557293dd65d555f4cf40e236f776ed1ef7b75f032fa511bae750d9312ba62d183129f8a529c515178ecd40f719c85583c519d531191d30'
            'ec001cda314b43b4e98f784e86bfd29da1cbac75ae981cf9ba738de0e366b47a112a21f0382a8c02737968d295f292e42451250ff9fdaee068662d0f221821f1'
            'eefa299a12b9d0a72eb73b978eab3cce5a6bcafb28acf52981762b64b7d74b7128c8a7e8517b6dcbb5281be0481c4d2fdd01de8706aefe08263ba5de4f29efae'
            '7ac2d0353122839a1b3266c6bb91c8279beade9d41f87031e4116c0a27fc7979d7a8d64fe1eab77b39a4da5869179f0ce7b8a137134df70159d745b7a63156cf'
            '565229fb00ac06d1a3ea37335138ffe87564317eab4d96b069ecd70e276f92141d9c746bc0b2d2f0abdd47e60ca580e063a2cb265d068624b4f306ce9c37f7fc'
            'c558bf06f76e5e9c74f86d14ec36d351395b2b69de7eb5c4c3bc3e18e3911585f9eca7a0f46e6b6076dafe4abca972302eeb6f4173853bd5e946eb0785eea98e')

prepare() {
  cd "$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"
  LDFLAGS=-lm ./configure \
    --prefix=/usr \
    --bin=/usr/games \
    --datadir=/usr/share/games
  make

  cd "$srcdir/$pkgname-data-1.5"
  ./configure \
    --prefix=/usr \
    --datadir=/usr/share/games
  make

  cd "$srcdir/$pkgname-sound-tracks-1.0"
  ./configure \
    --prefix=/usr \
    --datadir=/usr/share/games
  make

  cd "$srcdir/$pkgname-sound-effects-1.0"
  ./configure \
    --prefix=/usr \
    --datadir=/usr/share/games
  make

  cd "$srcdir/$pkgname-hq-sound-tracks-1.0"
  ./configure \
    --prefix=/usr \
    --datadir=/usr/share/games
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install
  mv "$pkgdir/"usr/share/games/locale "$pkgdir/"usr/share
  install -Dm644 debian/$pkgname.desktop "$pkgdir"/usr/share/applications/$pkgname.desktop
  install -Dm644 debian/$pkgname.xpm "$pkgdir"/usr/share/pixmaps/$pkgname.xpm
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"

  cd "$srcdir/$pkgname-data-1.5"
  make DESTDIR="$pkgdir/" install

  cd "$srcdir/$pkgname-sound-tracks-1.0"
  make DESTDIR="$pkgdir/" install

  cd "$srcdir/$pkgname-sound-effects-1.0"
  make DESTDIR="$pkgdir/" install

  cd "$srcdir/$pkgname-hq-sound-tracks-1.0"
  make DESTDIR="$pkgdir/" install
}
