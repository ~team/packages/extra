# Maintainer (Arch):  Dimitris Kiziridis <ragouel at outlook dot com>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Sarah Hay <sarahhay@mb.sympatico.ca>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=tauthon-pygtk
_pkgname=pygtk
pkgver=2.24.0
pkgrel=3
pkgdesc="Tauthon bindings for the GTK widget set"
arch=('i686' 'x86_64')
license=('LGPL-2.1')
depends=('libglade' 'tauthon-pycairo' 'tauthon-pygobject2' 'tauthon-numpy' 'tauthon')
source=("${pkgname}-${pkgver}.tar.bz2::https://download.gnome.org/sources/${_pkgname}/${pkgver%.*}/${_pkgname}-${pkgver}.tar.bz2"
        "drop-pangofont.patch"
        "python27.patch"
        "fix-leaks-of-pango-objects.patch")
sha512sums=('64f4344fcf7636e0b2016ffd5310250b5c02a1bf87e44aef39b5d4cf4a5fc50d27cb4f030d4c6802cff61fffb88dee7752821e3d8a4cd1c34dc3745d9ff2f0da'
            'b278297857346e34dc0d8269a75cb8447b69fd4619cf668c960ead2e60ebea0de57288bd26b9c0d7577ceaf4a606f66a0b83d1e432f42100f1240eafa25dd560'
            '044115bc08b78b9e656f47e77bd4e0d39b197242e5277fb2c4a03728faa58599090a388ff551ebc5ea9e5af4c979c154079c9efbacd221df2ef3c438014aa311'
            'bc385060c0b0cfee406d245c26bf7895be43db36160cf976c4def6416d7c06284a4d1eea0332fbe10e4065a1a3f1ca49ef3286a557e027fb953910a2aaa8359c')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  # https://bugzilla.gnome.org/show_bug.cgi?id=623965
  patch -Np1 -i "${srcdir}/python27.patch"

  # https://bugzilla.gnome.org/show_bug.cgi?id=660216
  patch -Np1 -i "${srcdir}/fix-leaks-of-pango-objects.patch"

  # fix build with new pango
  # https://gitlab.gnome.org/Archive/pygtk/-/merge_requests/1
  patch -p1 -i "${srcdir}/drop-pangofont.patch"

  # fix environment
  sed -i -e 's#env python$#env tauthon#' examples/pygtk-demo/{,demos/}*.py

  # no docs
  sed -i '/^SUBDIRS =/s/docs//' Makefile.in
}

build() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  PYTHON=/usr/bin/tauthon ./configure --prefix=/usr --build=unknown-unknown-linux --disable-docs
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -m644 gtk/gtk-extrafuncs.defs "${pkgdir}/usr/share/pygtk/2.0/defs/"
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
