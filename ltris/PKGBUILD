# Maintainer (Arch): Jaroslav Lichtblau <dragonlord@aur.archlinux.org>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): SmackleFunky <smacklefunky@optusnet.com.au>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=ltris
pkgver=1.3.2
pkgrel=2
pkgdesc="A puzzle game where you have a bowl with blocks falling down"
arch=('i686' 'x86_64')
url='https://lgames.sourceforge.net/index.php?project=LTris'
license=('GPL-2')
depends=('sdl_mixer')
makedepends=('gettext-tiny')
backup=('var/games/ltris.hscr')
install=$pkgname.install
groups=('games')
source=("https://downloads.sourceforge.net/lgames/${pkgname}-${pkgver}.tar.gz"
        "$pkgname.desktop")
sha512sums=('537d75f34b6cb19f340277f3cc0b4a9a8ad39cd8704ac82c31a768d8c16331846513856421a1e1f1fc82a0b2b83ba6bb096bc5269a8b2eca891faaf9a07c9fef'
            '90a207067270c341d47fcb861fa880d1a1a336d84089e1a918e4cac45e7b74ab408c63dd94f7c79f9c85a57e626f20bfb4a3b62f7347fc737234e20bab39bba2')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  autoreconf -vfi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure \
    --prefix=/usr \
    --bindir=/usr/games \
    --datarootdir=/usr/share/games \
    --localstatedir=/var/games
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR=${pkgdir} install

  mv "${pkgdir}"/usr/share/games/{applications,icons,locale} "${pkgdir}"/usr/share
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/$pkgname

  rm "${pkgdir}"/usr/share/applications/*
  install -Dm644 "${srcdir}"/$pkgname.desktop "${pkgdir}"/usr/share/applications/$pkgname.desktop

  rm -rf "${pkgdir}"/usr/share/icons
  install -d ${pkgdir}/usr/share/pixmaps
  install -m644 icons/ltris48.xpm ${pkgdir}/usr/share/pixmaps/${pkgname}.xpm
  chmod 775 ${pkgdir}/var/games
}