# Maintainer (Arch): Jiachen Yang <farseerfc@gmail.com>
# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Håvard Pettersson <mail@haavard.me>
# Contributor (Arch): naxuroqa <naxuroqa at gmail.com>
# Contributor (Arch): Boohbah <boohbah at gmail.com>
# Contributor (Arch): Kevin MacMartin <prurigro at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=toxcore
epoch=1
pkgver=0.2.19
pkgrel=2
pkgdesc="Secure, configuration-free, P2P communication backend"
arch=('i686' 'x86_64')
url='https://tox.chat'
license=('GPL-3')
depends=('libconfig' 'libsodium')
makedepends=('check' 'cmake')
conflicts=('tox')
provides=('tox')
backup=('etc/tox-bootstrapd.conf'
        'etc/conf.d/tox-bootstrapd')
install=$pkgname.install
source=("https://repo.hyperbola.info:50000/sources/${pkgname}/${pkgname}-${pkgver}.tar.gz"{,.sig}
        "tox-bootstrapd.confd"
        "tox-bootstrapd.initd"
        "tox-bootstrapd.run"
        "tox-bootstrapd.conf")
sha512sums=('23daa1557dce7b52e856914d763de425d7cd751b8c2ac8002c907c1e26e17732ee168a3a757e23aa5bd29bce890f4ded6071bc946d8d18657845322cba75bba8'
            'SKIP'
            '86c7245e8910ee34e8c07af465e9dd1dfe60a608d7b684d572511da84a5e7a7739bacb2cf7deb1f059640d937aed844f4b0b1f89bc86d6b623eb49276e2a7048'
            '01e564d9e056b55e68586630cb5af5875c9ad02cde74610d85ea2c23673a4b9fd2b6fe206f25e9c625d63dc41fa56f6360de9191ba46851b88d090cce0ea34ad'
            'cc5ed3e46c2a1691ffd5147e2009c61df5e6de045c029e9a55b9e937ff4c4e5ce03ce19d1f4ae7cbb38fb1d5e36af920cbb0266f7715064ee631d08bdf94934e'
            '86c7245e8910ee34e8c07af465e9dd1dfe60a608d7b684d572511da84a5e7a7739bacb2cf7deb1f059640d937aed844f4b0b1f89bc86d6b623eb49276e2a7048')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd $pkgname-$pkgver
  sed -i "/Rpath/d;/RPATH/d" CMakeLists.txt
}

build() {
  cmake \
    -S $pkgname-$pkgver \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DDHT_BOOTSTRAP=on \
    -DBOOTSTRAP_DAEMON=on \
    -DBUILD_TOXAV=off
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install

  install -Dm644 "$srcdir/tox-bootstrapd.confd" "$pkgdir/etc/conf.d/tox-bootstrapd"
  install -Dm755 "$srcdir/tox-bootstrapd.initd" "$pkgdir/etc/init.d/tox-bootstrapd"
  install -Dm755 "$srcdir/tox-bootstrapd.run" "$pkgdir/etc/sv/tox-bootstrapd/run"
  install -Dm644 "$srcdir/tox-bootstrapd.conf" "$pkgdir/etc/sv/tox-bootstrapd/conf"

  install -Dm644 "$srcdir/$pkgname-$pkgver/other/bootstrap_daemon/tox-bootstrapd.conf" "$pkgdir/etc/tox-bootstrapd.conf"
  install -Dm644 "$srcdir/$pkgname-$pkgver/LICENSE" -t "$pkgdir/usr/share/licenses/$pkgname"
}
