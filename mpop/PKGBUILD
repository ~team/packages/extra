# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Israel Herraiz <isra@herraiz.org>
# Contributor (Arch): Roberto Alsina <ralsina@kde.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mpop
pkgver=1.4.18
pkgrel=1
pkgdesc="A small, fast POP3 client suitable as a fetchmail replacement"
arch=('i686' 'x86_64')
url='https://marlam.de/mpop/'
license=('GPL-3')
depends=('gnutls' 'libidn')
install=$pkgname.install
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/m/mpop/mpop_${pkgver}.orig.tar.xz")
sha512sums=('4f20d3653c99361269011162df6a5d2b2f73d7e3e4bb62fc24c301a5d12f0e911ff432b2e786d2f49088cb96c273a4961ee514ba48465c0c455937559d2b2cff')

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
    --prefix=/usr
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install
  install -Dm644 scripts/vim/mpop.vim -t "$pkgdir/usr/share/vim/vimfiles/syntax"
  install -Dm644 doc/mpoprc.example -t "$pkgdir/usr/share/doc/mpop"
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}