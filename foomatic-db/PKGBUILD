# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgbase=foomatic-db
pkgname=('foomatic-db' 'foomatic-db-ppds')
arch=('any')
pkgver=20200820
_debpkgver=20200825
pkgrel=2
epoch=1
makedepends=('cups' 'perl' 'libxml2' 'enscript' 'perl' 'net-snmp' 'bash')
url='https://openprinting.github.io/projects/02-foomatic/'
options=('!emptydirs')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/f/foomatic-db/foomatic-db_${pkgver}.orig.tar.xz")
sha512sums=('654f792a19e78a3c8b9a88537ca3f433188e22dd62471e784dab646ae7a8bd2b90bac2ec8e4a4c6e2d941ce399a9211adc7ece96d5273e3d2250e500b3f0ee49')

prepare(){
  cp -a foomatic-db-${_debpkgver} foomatic-db-ppds-${_debpkgver}
}

build() {
  # foomatic-db
  pushd foomatic-db-${_debpkgver}
  ./make_configure
  ./configure --prefix=/usr \
    --disable-gzip-ppds \
    --disable-ppds-to-cups \
    --with-drivers=NOOBSOLETES,NOEMPTYCMDLINE
  popd
  
  # foomatic-db-ppds
  pushd foomatic-db-ppds-${_debpkgver}
  ./make_configure
  ./configure --prefix=/usr
  popd
}

package_foomatic-db() {
  pkgdesc="Foomatic - The collected knowledge about printers, drivers, and driver options in XML files, used by foomatic-db-engine to generate PPD files."
  license=('GPL-2' 'Expat')
  optdepends=('foomatic-db-ppds: PostScript PPD files')
  replaces=('foomatic-db-foo2zjs') # AUR pkg no more conflicting and dropped, formerly required by foo2zjs-utils
  conflicts=('foomatic-db-foo2zjs')
  provides=('foomatic-db-foo2zjs')

  cd "${srcdir}"/foomatic-db-${_debpkgver}
  make DESTDIR="${pkgdir}" install
   
  # add some docs
  install -Dm644 USAGE "${pkgdir}"/usr/share/doc/${pkgname}/USAGE
  install -Dm644 README "${pkgdir}"/usr/share/doc/${pkgname}/README
  install -v -Dm644 "${srcdir}"/foomatic-db-${_debpkgver}/COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
  install -Dm644 "${pkgdir}"/usr/share/foomatic/db/source/PPD/Kyocera/ReadMe.htm "${pkgdir}"/usr/share/doc/${pkgname}/Kyocera/ReadMe.htm

  # remove files from foomatic-db-ppds pkg
  rm -rf "${pkgdir}"/usr/share/cups/model/foomatic-db-ppds
  rm -rf "${pkgdir}"/usr/share/foomatic/db/source/PPD
}

package_foomatic-db-ppds() {
  pkgdesc="Foomatic - PPDs from printer manufacturers"
  license=('GPL-2' 'Expat')

  cd "${srcdir}"/foomatic-db-ppds-${_debpkgver}
  make DESTDIR="${pkgdir}" install

  # remove files from foomatic-db pkg
  rm -rf "${pkgdir}"/usr/share/foomatic/db/oldprinterids
  rm -rf "${pkgdir}"/usr/share/foomatic/db/source/{driver,opt,printer}
  rm -rf "${pkgdir}"/usr/share/foomatic/xmlschema
  rm -f "${pkgdir}"/usr/share/foomatic/db/source/PPD/Kyocera/ReadMe.htm

  install -v -Dm644 "${srcdir}"/foomatic-db-ppds-${_debpkgver}/COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
