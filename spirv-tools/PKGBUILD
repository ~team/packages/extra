# Maintainer (Arch): Laurent Carlier <lordheavym@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=spirv-tools
pkgver=2020.6
_hver=1.5.4.raytracing.fixed
pkgrel=2
pkgdesc="API and commands for processing SPIR-V modules"
arch=('i686' 'x86_64')
url='https://github.com/KhronosGroup/SPIRV-Tools'
license=(Apache-2.0)
depends=(gcc-libs)
makedepends=(cmake python ninja)
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/KhronosGroup/SPIRV-Tools/archive/v${pkgver}.tar.gz"
        "${pkgname}-${_hver}.tar.gz::https://github.com/KhronosGroup/SPIRV-Headers/archive/${_hver}.tar.gz")
sha512sums=('b6d3667581101536e1f4d405e9c1c64e64a0aba174cc2f41d31e080f44d23bcfaed9642fd31f1d44a56c3df3e894526b643c572832cb96218b16d748b1ac6fe2'
            '9d8c5ed58ebff603f0cffd1d6156ebafd3a0558e054937d8486bdc9267ad5de5dfd20d9a6f308bfbab77d391094bbc7119f1b05faf72bed41e6aa6fb35a04f5e')

build() {
  cd SPIRV-Tools-$pkgver

  rm -rf build && mkdir build && cd build
  cmake .. \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DCMAKE_INSTALL_LIBDIR=lib \
      -DCMAKE_BUILD_TYPE=Release \
      -DSPIRV_WERROR=Off \
      -DBUILD_SHARED_LIBS=ON \
      -DSPIRV-Headers_SOURCE_DIR=$srcdir/SPIRV-Headers-$_hver
  ninja
}

package() {
  cd SPIRV-Tools-$pkgver/build

  DESTDIR=$pkgdir ninja install

  install -Dm644 ../LICENSE -t $pkgdir/usr/share/licenses/$pkgname
}