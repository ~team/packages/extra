# Contributor (Arch): eric <eric@archlinux.org>
# Contributor (Arch): Markus Meissner <markus@meissna.de>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=tidy
pkgver=5.6.0
_debver=5.6.0
_debrel=11
pkgrel=1
pkgdesc='A tool to tidy down your HTML code to a clean style'
arch=('i686' 'x86_64')
url='http://www.html-tidy.org/'
license=('Modified-BSD')
depends=('glibc')
makedepends=('cmake' 'libxslt' 'quilt')
conflicts=('tidyhtml')
provides=('tidyhtml')
replaces=('tidyhtml')
source=("https://github.com/htacg/tidy-html5/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/t/tidy-html5/tidy-html5_$_debver-$_debrel.debian.tar.xz")
sha512sums=('179088a6dbd29bb0e4f0219222f755b186145495f7414f6d0e178803ab67140391283d35352d946f9790c6b1b5b462ee6e24f1cc84f19391cb9b65e73979ffd1'
            '109347ae71990eb3114373e4b823b54fa6948a7331b7338d143c98e1ab883a646343afa050e10bdec707c7f4134ba7f0bc9494f3d08e784e0dc211b7cc60ef20')

prepare() {
  cd tidy-html5-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake \
    -S tidy-html5-$pkgver \
    -Bbuild \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=Release
  make -C build
}

package() {
  make -C build DESTDIR="$pkgdir" install

  # Compatibility symlinks until everything is ported
  ln -s tidybuffio.h "$pkgdir"/usr/include/buffio.h
  ln -s tidyplatform.h "$pkgdir"/usr/include/platform.h

  install -Dm644 "$srcdir"/$pkgname-html5-$pkgver/README/LICENSE.txt -t "$pkgdir"/usr/share/licenses/$pkgname
}
