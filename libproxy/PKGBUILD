# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libproxy
pkgver=0.4.17
_debver=0.4.17
_debrel=1
pkgrel=1
pkgdesc="Automatic proxy configuration management library"
url="https://libproxy.github.io/libproxy/"
arch=(i686 x86_64)
license=(LGPL-2.1)
depends=(gcc-libs glib2)
makedepends=(cmake python tauthon perl quilt)
optdepends=('perl: Perl bindings'
            'tauthon: Tauthon bindings'
            'python: Python bindings')
source=(https://github.com/libproxy/libproxy/releases/download/$pkgver/libproxy-$pkgver.tar.xz
        https://deb.debian.org/debian/pool/main/libp/libproxy/libproxy_$_debver-$_debrel.debian.tar.xz)
sha512sums=('900bd7e44d968f6adf856d34dbc9d6558217143cae7a4eb6cb64766a26bee53f26e08f7feba3d5ad2b6d4db7409a35155c1dc34e1b9be42515b00b09d8c362b7'
            '4c989373d9709199bd2c120a9a7f1e8970952413519490c5746e1eafaec40ac547fb51d0e3ee258098aaaa54fe695cae4e984990f52f966fb21c5ecdb4b9cd18')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake -H$pkgname-$pkgver -Bbuild \
    -DBIPR=0 \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
    -DCMAKE_C_FLAGS="$CFLAGS" \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_SKIP_RPATH=ON \
    -DLIBEXEC_INSTALL_DIR=/usr/lib \
    -DPERL_VENDORINSTALL=yes \
    -DPYTHON2_EXECUTABLE:FILEPATH=/usr/bin/tauthon \
    -DWITH_MOZJS=OFF \
    -DWITH_WEBKIT=OFF \
    -DWITH_WEBKIT3=OFF
  cmake --build build
}

check() {
  cmake --build build --target test
}

package() {
  DESTDIR="$pkgdir" cmake --build build --target install
  install -Dm644 $pkgname-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
