# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

pkgbase=python-packaging
pkgname=('python-packaging' 'tauthon-packaging')
pkgver=20.4
pkgrel=3
arch=('any')
url='https://github.com/pypa/packaging'
license=('Apache-2.0')
makedepends=('python-pyparsing' 'tauthon-pyparsing')
source=("https://pypi.io/packages/source/p/packaging/packaging-$pkgver.tar.gz")
sha512sums=('d53912041a9950efb5d221fc968adc328c2ef1e54ec9806d2158fd6db1b170e37afb05213f5750b10c59927504083ca3781c958caa0c802b1c7c0fe1ac1682a4')

prepare() {
  cp -a "$srcdir"/packaging-$pkgver{,-tauthon}
}

build() {
  cd "$srcdir/packaging-$pkgver"
  python setup.py build

  cd "$srcdir/packaging-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-packaging() {
  pkgdesc="Core utilities for Python packages"
  depends=('python-pyparsing' 'python-six')

  cd "$srcdir/packaging-$pkgver"
  python setup.py install --root "$pkgdir"

  # license
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

package_tauthon-packaging() {
  pkgdesc="Core utilities for Tauthon packages"
  depends=('tauthon-pyparsing' 'tauthon-six')

  cd "$srcdir/packaging-$pkgver-tauthon"
  tauthon setup.py install --root "$pkgdir"

  # license
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
