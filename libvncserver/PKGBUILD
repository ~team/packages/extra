# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info

pkgname=libvncserver
pkgver=0.9.13
_debver=0.9.13+dfsg
_debrel=2
pkgrel=1
pkgdesc='Cross-platform C libraries that allow you to easily implement VNC server or client functionality'
arch=('i686' 'x86_64')
url="https://libvnc.github.io/"
license=('GPL-2')
depends=('libpng' 'libjpeg' 'gnutls' 'libgcrypt' 'libressl' 'libnsl')
makedepends=('sdl2' 'cmake' 'quilt')
source=("https://github.com/LibVNC/libvncserver/archive/LibVNCServer-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libv/libvncserver/libvncserver_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('18b0a1698d32bbdbfe6f65f76130b2a95860e3cc76e8adb904269663698c7c0ae982f451fda1f25e5461f096045d40a89d9014258f439366d5b4feaa4999d643'
            'f807045647ae4219e997ec82439ad676c8080f5bf0e5c6adffac0a89866d88603df9aef71cfc72e60e8328e99151eac0a8924a74ae7010b4cdcd4991e41c47fd')

prepare() {
  cd $pkgname-LibVNCServer-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-LibVNCServer-$pkgver
  cmake -DCMAKE_INSTALL_PREFIX=/usr .
  make
}

package() {
  cd $pkgname-LibVNCServer-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
