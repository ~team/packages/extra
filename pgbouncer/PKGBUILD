# Maintainer (Arch): Dan McGee <dan@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=pgbouncer
pkgver=1.15.0
pkgrel=1
pkgdesc="A lightweight connection pooler for PostgreSQL"
arch=('i686' 'x86_64')
url="https://wiki.postgresql.org/wiki/PgBouncer"
license=('ISC')
depends=('glibc' 'libevent' 'c-ares' 'libressl')
makedepends=('asciidoc' 'xmlto')
backup=('etc/pgbouncer/pgbouncer.ini'
        'etc/logrotate.d/pgbouncer'
        'etc/conf.d/pgbouncer')
install=$pkgname.install
source=("https://pgbouncer.github.io/downloads/files/$pkgver/$pkgname-$pkgver.tar.gz"
        "pgbouncer.ini"
        "pgbouncer.logrotate"
        "patch-lib_usual_tls_tls_c.patch"
        "pgbouncer.confd"
        "pgbouncer.initd")
sha512sums=('5f78018ab80ab8d81f20ef3df1314ffc9557f1c6469d485d11ac822f596e3d4b554743fd9e9fe19b008a8aaf93bcf3673b42a8fb82bbd9611bd735cd2cbb98c6'
            '39abc7b11c9d7a2593941b4d2a82db998ac1b1e3da131ae276da73c7afc4eda7b69bbfd0acd39f7bce20ecb911baf0adba341ff58dcab1a57e419708e7c8d26f'
            '1dc86704fce211b23afe7962c947c7de80a15bef219928acbf486b915d80d44a6590fbc509fe650c97b694a508bcf95d2152663863f0c372323286e644c6d60a'
            'e4a12313d535b84ce1f9db077d2a4cc818356540cb0f35b7374fdde5c69861ccbb0ce437e8cda943cdd16bca9179c6c862891a2ff6f0b998b7618ac91adb93b2'
            'dbcd7f5860b7f1ec6b6372b718678ba74c5268e89eed0fe291cd292a50460f0e81876367cb86e97f1cfefc0ace8d698c41a313642ec64d2de0747c348665afba'
            'ecd34f017316b2c0aad172e5bf5393137da5ab841f4d6de93eb82a07abe9f234a2426ca7dada2dc047c33ea4c7dc0af3040b249ec1f9d7e0c1782fb0463a7293')

prepare() {
  cd "$srcdir/$pkgname-$pkgver/lib"
  patch -Np1 -i $srcdir/patch-lib_usual_tls_tls_c.patch
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --disable-debug
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir/" install
  install -D -m644 COPYRIGHT -t "$pkgdir/usr/share/licenses/$pkgname"
  install -D -m644 $srcdir/pgbouncer.ini "$pkgdir/etc/pgbouncer/pgbouncer.ini"
  install -D -m644 $srcdir/pgbouncer.logrotate "$pkgdir/etc/logrotate.d/pgbouncer"
  install -D -m644 $srcdir/pgbouncer.confd "$pkgdir/etc/conf.d/pgbouncer"
  install -D -m755 $srcdir/pgbouncer.initd "$pkgdir/etc/init.d/pgbouncer"
  mkdir -p "$pkgdir/var/log/pgbouncer"
}
