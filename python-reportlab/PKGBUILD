# Maintainer (Arch): Morten Linderud <foxboron@archlinux.org>
# Contributor (Arch): Angel 'angvp' Velasquez <angvp[at]archlinux.com.ve>
# Contributor (Arch): William Rea <sillywilly@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

_origname=reportlab
pkgname=python-reportlab
pkgver=3.5.59
_debver=3.5.59
_debrel=2
pkgrel=1
pkgdesc='A proven industry-strength PDF generating solution'
arch=('i686' 'x86_64')
url='https://www.reportlab.com/'
depends=('freetype2' 'python' 'python-setuptools' 'python-pillow')
makedepends=('quilt')
license=('Modified-BSD')
source=(https://files.pythonhosted.org/packages/source/${_origname::1}/${_origname}/${_origname}-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/p/python-reportlab/python-reportlab_${_debver}-${_debrel}.debian.tar.xz)
sha512sums=('c862acb10f4eaed877321afadb52615132dcda0976e648e1767c32cd0398fc2763048f274d31da6088cb824311e8a28fee048cd6e98411e70583cfb36d0050ca'
            'd54534ec0f53bb4b556286a68b86dfaa103028a1029bbb21cc0a5b3845daa30bb5100f463fb3a7b1f65d9c756b7206affb44abc9526f1b0dc627393cc9bd8b47')

prepare() {
  cd ${_origname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${_origname}-${pkgver}
  python setup.py build
}

package() {
  cd ${_origname}-${pkgver}
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -Dm644 LICENSE.txt -t "$pkgdir"/usr/share/licenses/$pkgname
}
