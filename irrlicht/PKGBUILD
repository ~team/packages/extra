# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor (Arch): Hilton Medeiros <medeiros.hilton AT gmail DOT com>
# Contributor (Arch): Ali H. Caliskan <ali.h.caliskan AT gmail DOT com>
# Contributor (Arch): Paolo Fagni <paolo.fagni AT mail DOT com>
# Contributor (Arch): Lukas Kropatschek <lukas.krop AT gmail DOT com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=irrlicht
pkgver=1.8.4
_debver=$pkgver
_debrel=1.1
pkgrel=2
pkgdesc="A free software high performance realtime 3D graphics engine."
arch=('i686' 'x86_64')
url='https://irrlicht.sourceforge.net/'
license=('zlib')
depends=('mesa-libgl' 'libjpeg-turbo' 'bzip2' 'libpng')
makedepends=('mesa' 'unzip' 'libxcursor' 'quilt')
optdepends=('libxcursor: run examples')
source=("https://downloads.sourceforge.net/irrlicht/${pkgname}-${pkgver}.zip"
        "https://deb.debian.org/debian/pool/main/i/irrlicht/irrlicht_${_debver}+dfsg1-${_debrel}.debian.tar.xz")
noextract=($pkgname-$pkgver.zip)
sha512sums=('de69ddd2c6bc80a1b27b9a620e3697b1baa552f24c7d624076d471f3aecd9b15f71dce3b640811e6ece20f49b57688d428e3503936a7926b3e3b0cc696af98d1'
            '5ee61ab9b3f569e74acb92ba911dbf27eb9235e4809eaf2c1ecfc5bebe18f1ab3311f40ec0ae8c35fd6274d26f021d964755240d9b3997e584c55aec6421a818')

prepare() {
  unzip $pkgname-$pkgver.zip
  cd $srcdir/$pkgname-$pkgver

  sed -r '/^#define _IRR_USE_NON_SYSTEM_(JPEG_LIB|LIB_PNG|ZLIB|BZLIB)_/d' -i include/IrrCompileConfig.h
  sed -r '/^(ZLIB|JPEGLIB|LIBPNG|BZIP2)OBJ/d' -i source/Irrlicht/Makefile
  sed -r '/^sharedlib: LDFLAGS/s,\r?$, -lz -ljpeg -lpng -lz -lbz2,' -i source/Irrlicht/Makefile
  rm -r source/Irrlicht/{bzip2,jpeglib,libpng,zlib}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian/use-system-libs.diff || true
    rm -v debian/patches/debian/link-against-needed-libs.diff || true

    quilt push -av
  fi
}

build() {
  cd $srcdir/$pkgname-$pkgver/source/Irrlicht
  make NDEBUG=1 sharedlib
}

package() {
  cd $srcdir/$pkgname-$pkgver/source/Irrlicht

  sed -i "/^INSTALL_DIR/s:=.*:=$pkgdir/usr/lib:" Makefile

  make install

  # fix headers permissions
  chmod 644 "$pkgdir"/usr/include/$pkgname/*

  ln -s libIrrlicht.so.$pkgver "$pkgdir"/usr/lib/libIrrlicht.so.1

  cd $srcdir/$pkgname-$pkgver

  # prepare documentation
  install -d "$pkgdir"/usr/share/$pkgname "$pkgdir"/usr/share/doc/$pkgname

  # install media files for examples
  cp -r media "$pkgdir"/usr/share/$pkgname

  # install documentation
  cp -r doc/* "$pkgdir"/usr/share/doc/$pkgname
  rm -f "$pkgdir"/usr/share/doc/$pkgname/*.txt

  # license
  install -Dm644 $srcdir/$pkgname-$pkgver/readme.txt -t $pkgdir/usr/share/licenses/$pkgname
}
