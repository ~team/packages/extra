# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=libutempter
pkgver=1.2.1
_debver=1.2.1
_debrel=2
pkgrel=1
pkgdesc="Interface for terminal emulators such as screen and xterm to record user sessions to utmp and wtmp files"
arch=('i686' 'x86_64')
url="http://git.altlinux.org/people/ldv/packages/?p=libutempter.git"
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=(http://ftp.altlinux.org/pub/people/ldv/utempter/$pkgname-$pkgver.tar.gz{,.asc}
        https://deb.debian.org/debian/pool/main/libu/libutempter/libutempter_$_debver-$_debrel.debian.tar.xz)
sha512sums=('d3a3bab7d2c2a68534c5ad41dd02bde849eb08df5dbb895a79b50b74d269c48c4cfcd12c4654941ccb7cdd43f486cfdc19148fa470870562f5cd324ce9782429'
            'SKIP'
            '6a6141d1f7d113d89d3ea24b2d18ac11cae2d938bccf0dd07ed815935068310bb14d7f53de2bf788de0386167f322cd6b558d8534085d66d85e53255a8555c52')
validpgpkeys=('296D6F29A020808E8717A8842DB5BD89A340AEB7') # Dmitry V. Levin

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/multiarch-location.diff || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  RPM_OPT_FLAGS="$CFLAGS" make
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install
  chown root:utmp "$pkgdir/usr/lib/utempter/utempter"
  chmod 2755 "$pkgdir/usr/lib/utempter/utempter"
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
