# Maintainer (Arch): Marco Pompili
# Contributor (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Contributor (Arch): Jason Chu <jason@archlinux.org>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=steghide
pkgver=0.5.1
_debver=0.5.1
_debrel=15
pkgrel=1
pkgdesc="Embeds a message in a file by replacing some of the least significant bits"
arch=('i686' 'x86_64')
url='http://steghide.sourceforge.net'
license=('GPL-2')
depends=('libmcrypt' 'gcc-libs' 'mhash' 'libjpeg' 'zlib')
makedepends=('quilt' 'intltool' 'gettext-tiny')
source=("https://iweb.dl.sourceforge.net/project/steghide/steghide/${pkgver}/steghide-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/s/steghide/steghide_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('6cda3e33e91123fbc5caa112efcacf09180505abd4f6f229878cd443817c60a04498aead02509c7532fd6924225c6b752820c51e452a83c520f228273d610a57'
            'fc052925ee971512ab44c665e220b4549b62928ccee678ed9e2759c28be6de8acf15203ebd0968ad0fdbf15272beaac23e89ca38be9c869987f0a10201c6b43a')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/correct-german.patch || true
    rm -v debian/patches/fix-spelling.patch || true
    rm -v debian/patches/no-mkinstalldirs.patch || true

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # touch files needed by GNU Autotools
  touch NEWS AUTHORS ChangeLog

  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
