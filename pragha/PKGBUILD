# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Konstantin Gizdov <arch@kge.pw>
# Contributor (Arch): Alad Wenter <alad@archlinux.org>
# Contributor (Arch): Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor (Arch): J0k3r <moebius282 e4a t gmail D0_T com>
# Contributor (Arch): Alois Nespor <alois.nespor@gmail.com>
# Contributor (Arch): JerichoKru   <jerichokru(AT)hotmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pragha
pkgver=1.3.4
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="Lightweight GTK music player and manager"
arch=('i686' 'x86_64')
url='https://pragha-music-player.github.io/'
license=('GPL-3')
depends=('gst-plugins-base' 'gst-plugins-good' 'gtk' 'libpeas' 'taglib' 'totem-pl-parser')
optdepends=('gst-libav: Extra media codecs'
            'gst-plugins-bad: Extra media codecs'
            'gst-plugins-ugly: Extra media codecs'
            'libcdio-paranoia: Play cdrom plugin'
            'libkeybinder3: Global Hotkeys plugin'
            'libmtp: MTP Devices plugin')
makedepends=('intltool' 'gobject-introspection' 'libcdio-paranoia' 'libkeybinder3' 'libmtp' 'quilt' 'mesa-libgl')
source=("https://github.com/pragha-music-player/${pkgname}/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/p/pragha/pragha_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('65aadf68f427f03ea7ca07d2002806eb94019c8091a80320da7d8ff4c2f0cd912e9c346956188b42d2b7a68c56c79ee5af8e8327c69ad69f4a1fa5b9e3b3ca58'
            'd7b30cf9f0afca0a60e3396eba5ddb7500c38adb25cf440ff8041db19ce9952a4517412f03ed70c23de13741317b2216d0ec1ecacf244f3b4675378dd3dd2423')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -i 's|(datadir)/appdata|(datadir)/metainfo|' data/Makefile.{am,in}
}

build() {
  cd $pkgname-$pkgver
  CFLAGS="$CFLAGS -I/usr/include/gupnp-1.2 -I/usr/include/gssdp-1.2 -I/usr/include/libsoup-2.4"
  ./configure --prefix=/usr
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
