#!/bin/sh

if [ -n "$XDG_CONFIG_HOME" ] && [ -r "$XDG_CONFIG_HOME/alsa.conf" ]; then
  . "$XDG_CONFIG_HOME/alsa.conf"
elif [ -n "$HOME" ] && [ -r "$HOME/.config/alsa.conf" ]; then
  . "$HOME/.config/alsa.conf"
elif [ -r /etc/alsa.conf ]; then
  . /etc/alsa.conf
fi

[ -n "$ALSA_CARD" ]              && export ALSA_CARD
[ -n "$ALSA_CENTER_LFE_CARD" ]   && export ALSA_CENTER_LFE_CARD
[ -n "$ALSA_CENTER_LFE_DEVICE" ] && export ALSA_CENTER_LFE_DEVICE
[ -n "$ALSA_CTL_CARD" ]          && export ALSA_CTL_CARD
[ -n "$ALSA_FRONT_CARD" ]        && export ALSA_FRONT_CARD
[ -n "$ALSA_FRONT_DEVICE" ]      && export ALSA_FRONT_DEVICE
[ -n "$ALSA_HWDEP_CARD" ]        && export ALSA_HWDEP_CARD
[ -n "$ALSA_HWDEP_DEVICE" ]      && export ALSA_HWDEP_DEVICE
[ -n "$ALSA_IEC958_CARD" ]       && export ALSA_IEC958_CARD
[ -n "$ALSA_IEC958_DEVICE" ]     && export ALSA_IEC958_DEVICE
[ -n "$ALSA_MODEM_CARD" ]        && export ALSA_MODEM_CARD
[ -n "$ALSA_MODEM_DEVICE" ]      && export ALSA_MODEM_DEVICE
[ -n "$ALSA_PCM_CARD" ]          && export ALSA_PCM_CARD
[ -n "$ALSA_PCM_DEVICE" ]        && export ALSA_PCM_DEVICE
[ -n "$ALSA_RAWMIDI_CARD" ]      && export ALSA_RAWMIDI_CARD
[ -n "$ALSA_RAWMIDI_DEVICE" ]    && export ALSA_RAWMIDI_DEVICE
[ -n "$ALSA_REAR_CARD" ]         && export ALSA_REAR_CARD
[ -n "$ALSA_REAR_DEVICE" ]       && export ALSA_REAR_DEVICE
[ -n "$ALSA_SIDE_CARD" ]         && export ALSA_SIDE_CARD
[ -n "$ALSA_SIDE_DEVICE" ]       && export ALSA_SIDE_DEVICE
[ -n "$ALSA_SURROUND21_CARD" ]   && export ALSA_SURROUND21_CARD
[ -n "$ALSA_SURROUND21_DEVICE" ] && export ALSA_SURROUND21_DEVICE
[ -n "$ALSA_SURROUND40_CARD" ]   && export ALSA_SURROUND40_CARD
[ -n "$ALSA_SURROUND40_DEVICE" ] && export ALSA_SURROUND40_DEVICE
[ -n "$ALSA_SURROUND41_CARD" ]   && export ALSA_SURROUND41_CARD
[ -n "$ALSA_SURROUND41_DEVICE" ] && export ALSA_SURROUND41_DEVICE
[ -n "$ALSA_SURROUND50_CARD" ]   && export ALSA_SURROUND50_CARD
[ -n "$ALSA_SURROUND50_DEVICE" ] && export ALSA_SURROUND50_DEVICE
[ -n "$ALSA_SURROUND51_CARD" ]   && export ALSA_SURROUND51_CARD
[ -n "$ALSA_SURROUND51_DEVICE" ] && export ALSA_SURROUND51_DEVICE
[ -n "$ALSA_SURROUND71_CARD" ]   && export ALSA_SURROUND71_CARD
[ -n "$ALSA_SURROUND71_DEVICE" ] && export ALSA_SURROUND71_DEVICE
