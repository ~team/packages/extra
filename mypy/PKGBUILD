# Maintainer (Arch): Morten Linderud <foxboron@archlinux.org>
# Contributor (Arch): icasdri <icasdri at gmail dot com>
# Contributor (Arch): hexchain <i@hexchain.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=mypy
pkgver=0.812
_debver=$pkgver
_debrel=1
pkgrel=3
pkgdesc="Optional static typing for Python (PEP484)"
url='https://www.mypy-lang.org/'
arch=('any')
license=('Expat' 'Python')
depends=('python-psutil' 'python-typed-ast' 'python-mypy_extensions' 'python-typing_extensions' 'python-toml')
makedepends=('python-setuptools' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/python/mypy/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/m/$pkgname/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('ee89f56a7a01214540f9b727f153a075a097b161b7f654d926d1080ae540ec68303629a4fe691fcb53d37c3eb08924bf01d22cdf1c3761b414a3bc40af3363e6'
            '271cd28953813926d9d2e703f2d3ac6ba75251e4b27087004529adadddbdde8a72031d8808afe6b10b7cb16c405611e4c45af0819427ea1c483aa048ba2a3e36')

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
    cd "$pkgname-$pkgver"
    export PYTHONHASHSEED=0
    python setup.py build
}

package() {
    cd "$pkgname-$pkgver"
    python setup.py install --prefix="/usr" --root="${pkgdir}" --optimize=1 --skip-build
    install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
