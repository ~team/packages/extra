# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=qt-multimedia
_qtver=5.15.2
pkgver=${_qtver/-/}
_debver=5.15.2
_debrel=3
pkgrel=1
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('LGPL-3')
pkgdesc='Classes for audio, video, radio and camera functionality'
depends=('qt-base' 'gst-plugins-base' 'openal')
makedepends=('qt-declarative' 'gst-plugins-bad' 'quilt')
optdepends=('qt-declarative: QML bindings' 'gst-plugins-good: camera support, additional plugins'
            'gst-plugins-bad: camera support, additional plugins' 'gst-plugins-ugly: additional plugins'
            'gst-libav: ffmpeg plugin')
groups=('qt')
replaces=('qt5-multimedia')
conflicts=('qt5-multimedia')
provides=('qt5-multimedia')
_pkgfqn="${pkgname/-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz"
        "https://deb.debian.org/debian/pool/main/q/qtmultimedia-opensource-src/qtmultimedia-opensource-src_$_debver-$_debrel.debian.tar.xz")
sha512sums=('be58e6934976b04707399f8525dd5d50f73e0539efd1a76c2f9868e35e6f136e0991652df5d46350a9ac39d24313353e4aa826131f268a95e458e57279f448bd'
            '29f1c179bda235bbdf379f82f73ff3b570fc62ba1db1091afea7e6acbd65c5e27592be5bf0fbe7a7749082be9a55e04d74be1d396daff6cf89acfefcb67a5ca0')

prepare() {
  mkdir -p build

  cd ${_pkgfqn}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.LGPL3 "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.LGPL3
}
