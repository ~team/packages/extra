# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-dummy package

pkgname=xenocara-video-dummy-debug
_openbsdver=6.9
pkgver=0.3.8
pkgrel=5
pkgdesc="Xenocara dummy video driver"
arch=(i686 x86_64)
url="https://www.xenocara.org"
license=('Expat')
depends=('glibc')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-video-dummy')
conflicts=('xf86-video-dummy' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25')
replaces=('xf86-video-dummy')
groups=('xenocara-video-drivers-debug' 'xenocara-video-debug' 'xenocara-drivers-debug'
        'xorg-video-drivers-debug' 'xorg-video-debug' 'xorg-drivers-debug')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-dummy-$pkgver.tar.lz{,.sig}
        COPYING)
options=(!strip) # It's required for debug packages
sha512sums=('a7aa27ad61773ca721f6539f4dfe89ebfbee6733686e25bcb9d5ee957a5b2538fa4e581d60bace34e6fea76a7a6bd7b070447ea8dfd71ac1207934990f7e0634'
            'SKIP'
            '436ca9a51107e3df522021d4452540f4bc98361506644c58f3c3bc33fe8a69770eabb2aebc3831ebe6b16593ddd0b395ea31a82f440bcd4b6337f42510b5ad4c')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-dummy"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-dummy"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}
  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-dummy"
  make DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
