# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=hex-a-hop
pkgver=1.1.0
pkgrel=3
pkgdesc="A free and libre puzzle game based on hexagonal tiles"
arch=('i686' 'x86_64')
url='https://hexahop.sourceforge.net'
license=('GPL-2' 'CC-BY-SA-4.0' 'CC0-1.0')
depends=('sdl' 'sdl_mixer' 'sdl_ttf')
groups=('games')
mksource=("https://downloads.sourceforge.net/project/hexahop/${pkgver}/${pkgname}-${pkgver}.tar.gz")
source=("https://repo.hyperbola.info:50000/sources/${pkgname}-libre/${pkgname}-libre-${pkgver}.tar.gz"{,.sig}
        "https://repo.hyperbola.info:50000/sources/${pkgname}-libre/${pkgname}-libre-data.tar.gz"{,.sig}
        "hex-a-hop.desktop"
        "hex-a-hop.png"
        "libre.patch")
mksha512sums=('d8f7c1ab576d499595a09703a699654b88548b7266441af8f8af5287cfdcc998389faba024650214f3cb85f359a238802ecbcecbb699e5aa80ad7c5cf02a6bcd')
sha512sums=('f51c5020178432ae8a641641e88e92ec73e70da8b93a63737ba770782e04d748effff68dfd95e54cb5f726e235a1ce39992fd6b39c8193964e7e7aea10beb280'
            'SKIP'
            '5b4c9376987f8bf9780a758e842d1453d1c61bec4c43fe0b66ae465dee5d0d120f376c53e31f9c49cd8ce5eddc50407571818d6eecd9b1f401228d6ae0b6e239'
            'SKIP'
            'b7d84ecbd5c7d735c0defadf27c14f3a352dd368860e62c71db8a8d489dc7dbefb421cc79cccdff8feebf0d86aee85e08e6d3a5b9fb0e23db69ec55a1968db81'
            'd5c60e794f3f114cfb7e77ce147573cb7dc0641f0073e3b40cbb7cfe37482c4dfb975c2d2bef0b3f4716978a3e456d9e6025421598fdffc59400169d6a2c6bb4'
            '0f7412c8535c5692854253679fb4ed39f9ff67e2f9b9586392d64fa0ad31d096df8b40f879d94689c4999d1cd27339dd6850dc69ce6b2405d1665bfcbd194a0c')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva
noextract=("$pkgname-libre-data.tar.gz")

mksource() {
  cd "$pkgname-$pkgver"

  # remove nonfree data
  cd ./data
  find . -type f -name '*nonfree*' -exec rm -v {} +
}

prepare() {
  cd "$pkgname-$pkgver"

  # replacing missing data
  tar -xzf "$srcdir/$pkgname-libre-data.tar.gz" -C "data/"

  # removing nonfree data-definitions within code
  patch -Np1 -i ${srcdir}/libre.patch
}

build() {
  cd "$pkgname-$pkgver"

  ./configure \
    --prefix=/usr \
    --bindir=/usr/games \
    --datadir=/usr/share/games
  make
}

package() {
  cd "$pkgname-$pkgver"

  make DESTDIR="$pkgdir/" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"

  install -Dm644 "$srcdir/hex-a-hop.desktop" -t "$pkgdir/usr/share/applications"
  install -Dm644 "$srcdir/hex-a-hop.png" -t "$pkgdir/usr/share/icons"
}
