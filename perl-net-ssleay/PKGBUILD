# Maintainer (Arch): Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Manolis Tzanidakis
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=perl-net-ssleay
_cpanname=Net-SSLeay
pkgver=1.88
_debver=1.88
_debrel=3
pkgrel=1
pkgdesc='Perl extension for using LibreSSL'
url='https://search.cpan.org/dist/Net-SSLeay/'
arch=('i686' 'x86_64')
license=('Artistic-2.0')
depends=('libressl')
makedepends=('chrpath' 'quilt')
replaces=('net-ssleay')
provides=('net-ssleay')
options=('!emptydirs')
source=(https://cpan.metacpan.org/authors/id/C/CH/CHRISN/${_cpanname}-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/libn/libnet-ssleay-perl/libnet-ssleay-perl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('37fc32ec003337f3dc18bf928139079a3127628c6fa0fccaed0c27b6bf3b216c2c5338ed1aaf9408b0add3be549df3218d3c5a6d6299793c1329d8a0813a1682'
            '7edb53484a8b51b2a39bcf02e2053f970d78176bf87809f8a217ade9a2e77d79d0dba469128bd8e6d7ddab6a6aa6c907da07de5d8e8e3ce5fb290bb53162afec')

prepare() {
  cd ${srcdir}/${_cpanname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${_cpanname}-${pkgver}
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd ${_cpanname}-${pkgver}

  make install DESTDIR="${pkgdir}"
  install -Dm 644 README -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"

  # strip rpath
  find "${pkgdir}" -name '*.so' -exec chrpath -d '{}' \;
}
