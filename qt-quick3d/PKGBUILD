# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=qt-quick3d
_pkgbasename=qtquick3d
_qtver=5.15.2
pkgver=${_qtver/-/}
pkgrel=1
arch=('i686' 'x86_64')
url='https://www.qt.io'
license=('GPL-3')
pkgdesc="Qt module and API for defining 3D content in Qt Quick"
depends=('qt-declarative')
makedepends=('assimp')
optdepends=('assimp: Import from assimp')
groups=('qt')
_pkgfqn="${_pkgbasename/5-/}-everywhere-src-${_qtver}"
source=("https://download.qt.io/official_releases/qt/${pkgver%.*}/${_qtver}/submodules/${_pkgfqn}.tar.xz"
         qtquick3d-assimp.patch)
sha512sums=('2a1f10430a3e4cd91270f79ab79e3bc6fee856659902f6489f5945a167201133a9a71dcc82c97aa699f97441b377c2981b4d1a65703e469936553f5265fe55ee'
            'ef87c9f0e41930ac7a7c4b5c7c9543426e68831cfb835362a6a198e6ba8bed455d5cc0f7070cc706722801ab4da8e479938d62367d91b75ac502c3ef57553da6')

prepare() {
  mkdir -p build

  cd $_pkgfqn
  patch -p1 -i ${srcdir}/qtquick3d-assimp.patch # fix build with system assimp
}

build() {
  cd build

  qmake ../${_pkgfqn}
  make
}

package() {
  cd build
  make INSTALL_ROOT="$pkgdir" install

  # Drop QMAKE_PRL_BUILD_DIR because reference the build dir
  find "$pkgdir/usr/lib" -type f -name '*.prl' \
    -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

  # Install license
  install -D -m644 "${srcdir}"/${_pkgfqn}/LICENSE.GPL3 -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
