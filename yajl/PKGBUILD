# Maintainer (Arch): Dave Reisner <d@falconindy.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Thomas Dziedzic < gostrc at gmail >
# Contributor (Arch): Andrej Gelenberg <andrej.gelenberg@udo.edu>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=yajl
pkgver=2.1.0
_debver=2.1.0
_debrel=3
pkgrel=2
pkgdesc='Yet Another JSON Library'
arch=('i686' 'x86_64')
url='http://lloyd.github.com/yajl/'
license=('ISC')
makedepends=('cmake' 'quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/lloyd/$pkgname/archive/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/y/yajl/yajl_$_debver-$_debrel.debian.tar.xz")
sha512sums=('9e786d080803df80ec03a9c2f447501e6e8e433a6baf636824bc1d50ecf4f5f80d7dfb1d47958aeb0a30fe459bd0ef033d41bc6a79e1dc6e6b5eade930b19b02'
            '4e9b3751a6aa036524da028d70f1aa57b7bde2d090b039cc091374b668ffca3dbc84ec82e7ce42edfb3eff6d7cb998f3a10b9a3e553267c7a6d46c72135592ed')

prepare() {
  cd "$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/multiarch.patch || true

    quilt push -av
  fi
}

build() {
  cd "$pkgname-$pkgver"

  cmake -DCMAKE_INSTALL_PREFIX=/usr .

  make
}

package() {
  cd "$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/${pkgname}/COPYING"
}
