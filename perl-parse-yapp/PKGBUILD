# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Charles Mauch <cmauch@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-parse-yapp
pkgver=1.21
_debver=1.21
_debrel=2
pkgrel=1
pkgdesc='Perl/CPAN Module Parse::Yapp : Generates OO LALR parser modules'
arch=('any')
url='https://search.cpan.org/dist/Parse-Yapp'
license=('GPL-1')
depends=('perl')
makedepends=('quilt')
source=("https://cpan.metacpan.org/authors/id/W/WB/WBRASWELL/Parse-Yapp-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/libp/libparse-yapp-perl/libparse-yapp-perl_$_debver-$_debrel.debian.tar.xz"
        "LICENSE")
sha512sums=('dbf6182d4813ff7e355ea1713c748bfdf8290040a93f123acec645c7a1733fe457ab6e0ab51c4ec83cf82bc43d7fb35cbf89875df7b5c2ffc9635e85458cfeee'
            'ca94d2c46469eafc27e8a03cdb03aa19ab1e86841ba2dd05c75633e7f96f563337304a0f88bc3cada50743f1f69334e1103bd5c85cf3f2a311350aa2f4ad5d32'
            'd93491d6be05ecb4ccd27ae56915562d81ba436486a36c7b4e4d48608db98f9be1bb5f795cac9ba9ad9e9789c28b48d92373f3dc18a23e087149f0a6f04b67d9')

prepare() {
  cd "$srcdir"/Parse-Yapp-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir"/Parse-Yapp-$pkgver
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd "$srcdir"/Parse-Yapp-$pkgver
  make install DESTDIR="$pkgdir"
  find "$pkgdir" -name '.packlist' -delete
  find "$pkgdir" -name '*.pod' -delete
  install -Dm644 "${srcdir}/LICENSE" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
