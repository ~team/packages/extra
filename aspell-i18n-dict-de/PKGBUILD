#!/bin/ksh

# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>

# Based on aspell-en

_pkgname='aspell'
_pkglang='de'
_srclang="${_pkglang}"
_srcsv='6'
pkgname="${_pkgname}-i18n-dict-${_pkglang}"
# Example: _srcname='aspell6-en'
_srcname="${_pkgname}${_srcsv}-${_srclang}"
pkgver='20161207.7.0'
_srcver='20161207-7-0'
pkgrel='2'
pkgdesc='German dictionary for Aspell'
arch=('i686' 'x86_64')
url='https://ftp.gnu.org/gnu/aspell/dict/0index.html'
# The de_DE.aff.in is Original-BSD,
# but I did not find this file in the source code
license=('GPL-2')
groups=("${_pkgname}-i18n-dict" "${_pkgname}-dict"
        "g${_pkgname}-i18n-dict" "g${_pkgname}-dict"
        "gnu${_pkgname}-i18n-dict" "gnu${_pkgname}-dict"
        "gnu-${_pkgname}-i18n-dict" "gnu-${_pkgname}-dict")
depends=('aspell')
provides=("${_pkgname}-dict-${_pkglang}"
          "g${pkgname}" "g${_pkgname}-dict-${_pkglang}"
          "gnu${pkgname}" "gnu${_pkgname}-dict-${_pkglang}"
          "gnu-${pkgname}" "gnu-${_pkgname}-dict-${_pkglang}"
          "${_pkgname}-${_pkglang}")
conflicts=("${_pkgname}-${_pkglang}")
replaces=("${_pkgname}-${_pkglang}")
_source=("https://ftp.gnu.org/gnu/${_pkgname}/dict/${_srclang}/")
source=("${_source[0]}/${_srcname}-${_srcver}.tar.bz2"
        "${_source[0]}/${_srcname}-${_srcver}.tar.bz2.sig")
sha512sums=('ec29fa49d4e0303d6ceb78da3884ac54ddf34205895f30a95bce0db66f74472b423980200acf01efb1fa40380d6ea6351a58656400af35772266f8a54b1bbec8'
            'SKIP')
validpgpkeys=(# Aspell Dictionary Upload
              '78DEC3DB0AEA763951322BBF71C636695B147849')
unset _pkgname _pkglang _srclang _srcsv _source

build() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  ./configure
  make 'V=1'
}

package() {
  cd "${srcdir}/${_srcname}-${_srcver}"
  make "DESTDIR=${pkgdir}" 'install'

  for i in 'Copyright'; do
    install -Dm '644' "${i}" -t "${pkgdir}/usr/share/licenses/${pkgname}"
  done
  unset i _srcname _srcver
}
