# Maintainer (Arch): Daniel Bermond <dbermond@archlinux.org>
# Contributor (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): damir <damir@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=x11vnc
pkgver=0.9.16
_debver=$pkgver
_debrel=7
pkgrel=2
epoch=1
pkgdesc='VNC server for real X displays'
url='https://github.com/LibVNC/x11vnc'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('libvncserver' 'libressl' 'libxcrypt' 'libxtst' 'libxext' 'libxinerama'
         'libxrandr' 'libxfixes' 'libxdamage' 'libxcomposite' 'libx11'
         'libxi' 'xenocara-xdpyinfo')
optdepends=('tk: for GUI support'
            'net-tools: for -auth guess'
            'xenocara-video-dummy: for Xdummy script')
makedepends=('quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/LibVNC/x11vnc/archive/refs/tags/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/x11vnc/x11vnc_${_debver}-${_debrel}.debian.tar.xz"
        "patch-src_sslhelper.patch")
sha512sums=('69f65ee312f8dede6051b401304987502a213c6c28c7f41e855734f11de1fae14d5d493dc9c28b2e4b7c0be55f8dbd3b35dd2610aae910183772c3e626736fec'
            '4609552c6080a807bc383debb47f734af66c2365387331c9cc029be8969b47c9c3b273a5e22db57d942fc8574a0c0857f6122a8a2a515f9d162c68643b8552fd'
            '3e502a2a482cfb7563d38e84d3d6ef81c66f71d3497acc1b304f4af2d9f9b4270ead24aa76b201c850370202dee7aa51224193fe0efb5b95d2152526b4ba4de2')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi

  # additional fixes
  patch -Np1 -i ${srcdir}/patch-src_sslhelper.patch

  autoreconf -vfi
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix='/usr' --mandir='/usr/share/man'
  make
}

package() {
  make -C ${pkgname}-${pkgver} DESTDIR="$pkgdir" install
  install ${pkgname}-${pkgver}/misc/{rx11vnc,Xdummy} "${pkgdir}/usr/bin"
  install -Dvm644 ${pkgname}-${pkgver}/COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
