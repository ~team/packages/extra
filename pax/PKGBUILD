# Maintainer (Arch): Daniel Bermond <dbermond@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pax
pkgver=20201030
pkgrel=1
pkgdesc="Portable Archive Interchange - the POSIX standard archive tool for cpio and tar formats"
arch=('i686' 'x86_64')
url='https://www.mirbsd.org/pax.htm'
license=('Modified-BSD')
depends=('glibc')
source=("https://www.mirbsd.org/MirOS/dist/mir/cpio/paxmirabilis-${pkgver}.cpio.gz")
sha512sums=('14c913db49c05b92f3448b7ae2e11b4fdb86c2daa5f269d86e044e8779eec7ea19b41534fbd8ebfe31cffe6b50e51c0460c3994307ba2cfa07029602604882de')
noextract=("paxmirabilis-${pkgver}.cpio.gz")

prepare() {
  mkdir -p "${pkgname}-${pkgver}/build"
  bsdtar -x -f "paxmirabilis-${pkgver}.cpio.gz" -C "$pkgname-${pkgver}" --strip-components='1'
}

build() {
  cd "${pkgname}-${pkgver}/build"
  sh ../Build.sh -r -tpax
    
  # license
  sed -n '5,36p' ../pax.h > LICENSE # create file
  sed -i '1,32s/^.\{,3\}//' LICENSE # erase C comments
}

package(){
  # executables
  install -D -m755 "${pkgname}-${pkgver}/build/pax" -t "${pkgdir}/usr/bin"
  ln -s pax "${pkgdir}/usr/bin/paxcpio"
  ln -s pax "${pkgdir}/usr/bin/paxtar"
  
  # man pages
  install -D -m644 "${pkgname}-${pkgver}/build/mans/"*.1 -t "${pkgdir}/usr/share/man/man1"
    
  # license
  install -D -m644 "${pkgname}-${pkgver}/build/LICENSE" -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
