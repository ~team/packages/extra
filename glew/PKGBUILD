# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): SleepyDog
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=glew
pkgver=2.1.0
_debver=2.1.0
_debrel=4
pkgrel=1
pkgdesc="The OpenGL Extension Wrangler Library"
arch=('i686' 'x86_64')
url="https://github.com/nigels-com/glew"
license=('Modified-BSD')
depends=('libxmu' 'libxi' 'glu')
makedepends=('quilt')
# https://sourceforge.net/projects/glew/files/glew/
source=(https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tgz
        https://deb.debian.org/debian/pool/main/g/glew/glew_$_debver-$_debrel.debian.tar.xz)
sha512sums=('9a9b4d81482ccaac4b476c34ed537585ae754a82ebb51c3efa16d953c25cc3931be46ed2e49e79c730cd8afc6a1b78c97d52cd714044a339c3bc29734cd4d2ab'
            '13e26f2b18d8a13e109549c71d750ca50c275abae78218c62d4612c22c6934f94dd697e348dc66d104f3f3f9645147cb7636beaebc7649f43f7888646934d166')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  make \
   STRIP= \
   LIBDIR=/usr/lib
}

package() {
  cd ${pkgname}-${pkgver}
  make install.all DESTDIR="$pkgdir" \
    LIBDIR=/usr/lib
  install -D -m644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"
}
