# Maintainer (Arch):
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Leif Warner <abimelech@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=python-rdflib
pkgver=5.0.0
_debver=5.0.0
_debrel=1.1
pkgrel=1
pkgdesc="A Python library for working with RDF, a simple yet powerful language for representing information"
arch=('any')
url="https://github.com/RDFLib/rdflib"
license=('Modified-BSD')
depends=('python-isodate' 'python-pyparsing')
makedepends=('python-setuptools' 'quilt')
source=(rdflib-$pkgver.tar.gz::https://github.com/RDFLib/rdflib/tarball/$pkgver
        https://deb.debian.org/debian/pool/main/r/rdflib/rdflib_$_debver-$_debrel.debian.tar.xz)
sha512sums=('d879c9312a5e6a69ef2edf7931f5e20f6d5c59ce6feb2397e5aeb09843f3568c284f9175b24ac4d513b720dcea82076776d33bfc4492f5d82a17c644586e7b48'
            'e8831140261b6f66ee0ceb23cb60b6b9e731835d15338562ce6fbf58de36e21afb43aa563933da9d2e88a11be7b43290b309a557f8073216ccc47248512478a9')

prepare() {
  cd RDFLib-rdflib-*

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd RDFLib-rdflib-*
  python setup.py build
}

package() {
  cd RDFLib-rdflib-*
  python setup.py install --root="$pkgdir" --optimize=1
  install -Dm644  LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
