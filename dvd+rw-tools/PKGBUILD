# Maintainer (Arch): damir <damir@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=dvd+rw-tools
pkgver=7.1
_debver=7.1
_debrel=14
pkgrel=1
pkgdesc='dvd burning tools'
arch=('i686' 'x86_64')
license=('GPL-2')
url='http://fy.chalmers.se/~appro/linux/DVD+RW'
depends=('libisoburn' 'gcc-libs')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/d/dvd+rw-tools/${pkgname}_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/dvd+rw-tools/dvd+rw-tools_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('938f9ec5597158af275c7bf63002696ba362f6f22a219108c6a1df28792f0485046a7af5ce57e41695aaaa0d69543bd66cbbeb4415df5c0e0a902a3f1d278a31'
            '9029ad2a36d5a55c1c06e27bef7ba53952e548ef3117996f0d8bf6e4c6fbcbecb5d9b0a6d1528db8563b132ae0370ed2fa00317ab5d1b1031aec328e083c2057')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  make
}

package() {
  cd ${pkgname}-${pkgver}
  install -m755 -d "${pkgdir}/usr/bin"
  install -m755 -d "${pkgdir}/usr/share/man/man1"

  install -m755 growisofs dvd+rw-booktype dvd+rw-format dvd+rw-mediainfo dvd-ram-control "${pkgdir}/usr/bin/"
  install -m644 growisofs.1 "${pkgdir}"/usr/share/man/man1/growisofs.1

  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/$pkgname"
}
