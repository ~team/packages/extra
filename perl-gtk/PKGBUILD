# Maintainer (Arch): Muflone http://www.muflone.com/contacts/english/
# Contributor (Arch): Ordoban <dirk.langer@vvovgonik.de>
# Contributor (Arch): John D Jones III AKA jnbek <jnbek1972 -_AT_- g m a i l -_Dot_- com>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=perl-gtk
pkgver=0.038
pkgrel=1
pkgdesc="Perl interface to the 3.x series of the GTK+ toolkit"
arch=('any')
url="https://metacpan.org/release/Gtk3"
license=('LGPL-2.1')
checkdepends=('ttf-dejavu' 'xenocara-server-xvfb')
depends=('gtk' 'perl-cairo-gobject' 'perl-glib-object-introspection')
options=('!emptydirs')
source=("https://cpan.metacpan.org/authors/id/X/XA/XAOC/Gtk3-${pkgver}.tar.gz")
sha512sums=('2289184a25deec342b9519028cc5ebb3a66137d7891366485e4b6a1ef52a4a28b5ba8083a317a56664d4ea6eb1bc0619477d1ca169baf79b868467fba27f82bf')

build() {
  cd "Gtk3-${pkgver}"
  unset PERL5LIB PERL_MM_OPT PERL_MB_OPT PERL_LOCAL_LIB_ROOT
  export PERL_MM_USE_DEFAULT=1 PERL_AUTOINSTALL=--skipdeps
  perl Makefile.PL
  make
}

check() {
  cd "Gtk3-${pkgver}"
  unset PERL5LIB PERL_MM_OPT PERL_MB_OPT PERL_LOCAL_LIB_ROOT
  export PERL_MM_USE_DEFAULT=1
  xvfb-run -a -s "-extension GLX -screen 0 1280x1024x24" make test
}

package() {
  cd "Gtk3-${pkgver}"
  unset PERL5LIB PERL_MM_OPT PERL_MB_OPT PERL_LOCAL_LIB_ROOT
  make pure_install INSTALLDIRS=vendor DESTDIR="${pkgdir}"
  # Delete unuseful files
  find "${pkgdir}" -name '.packlist' -delete
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}

# vim:set ts=2 sw=2 et:
