# Maintainer (Arch): Antonio Rojas <arojas@archlinux.org>
# Contributor (Arch): Hyacinthe Cartiaux <hyacinthe.cartiaux at free.fr>
# Contributor (Arch): Bartlomiej Piotrowski <nospam at bpiotrowski dot pl>
# Contributor (Arch): Patrick McCarty <pnorcks at gmail dot com>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=discount
pkgver=2.2.6
_debver=2.2.6
_debrel=1
pkgrel=1
pkgdesc='A Markdown implementation written in C'
arch=('i686' 'x86_64')
url="https://www.pell.portland.or.us/~orc/Code/discount/"
license=('Modified-BSD')
depends=('glibc')
makedepends=('quilt')
conflicts=('markdown')
provides=('markdown')
replaces=('markdown')
source=($pkgname-$pkgver.tar.gz::"https://github.com/Orc/discount/archive/v$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/discount/discount_$_debver-$_debrel.debian.tar.xz")
sha512sums=('4c5956dea78aacd3a105ddac13f1671d811a5b2b04990cdf8485c36190c8872c4b1b9432a7236f669c34b07564ecd0096632dced54d67de9eaf4f23641417ecc'
            '5d16cb14e339c95a24bca3f977075cda0380afe35fed50701a6c1958e6f3848145d31277969b9ca166c4045baf3230973437602fde7879bb0e29af3c560d35fb')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure.sh --prefix=/usr --enable-all-features --with-fenced-code --shared
  make -j1
}

package() {
  cd $pkgname-$pkgver
  # fix ldconfig call
  sed -e 's|/sbin/ldconfig|/sbin/ldconfig -n|' -i librarian.sh

  make DESTDIR="$pkgdir" install.everything
  install -Dm644 COPYRIGHT -t "$pkgdir"/usr/share/licenses/$pkgname
}
