# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Florian Pritz <bluewind@xinu.at>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=perl-socket6
pkgver=0.29
_debver=0.29
_debrel=1
pkgrel=1
pkgdesc="A getaddrinfo/getnameinfo support module"
arch=('i686' 'x86_64')
url='http://search.cpan.org/dist/Socket6'
license=('Modified-BSD' 'ISC')
depends=('perl')
makedepends=('quilt')
options=('!emptydirs')
source=(https://search.cpan.org/CPAN/authors/id/U/UM/UMEMOTO/Socket6-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/libs/libsocket6-perl/libsocket6-perl_$_debver-$_debrel.debian.tar.gz
        LICENSE.BSD3
        LICENSE.ISC)
sha512sums=('51d05a04563519010f515a50fb1082c4e72eb4537137ad55f117458c71ec8429a5674083bd68adcbbf2e57632a1cb1bf60693ea98364e7f96c826ff8c9655b33'
            '7a673a47b1ff380c4a8f8c292000704f304e52daa4cc2d066eb69d19742ce194b7169be0d2da75c1eefefd0ffbbcebdcce0db29cefef2a1f042e21fadb6f2b04'
            'daba6aae06f47ca935da7a9fb9385c5da24dfcb17772393cb55460db5e19c1c3f981599087b134410ef85cf14c67d01c9b256223a564e4621da050d8aa043044'
            '46dfd9cc6642b7049c1643aca7b47b0c26af2e9018e18c946e0898c113bd952fb3148497a73403e84a7d01ab25c99ed4732ddbca64c781033d8dd02169db6876')

prepare() {
  cd Socket6-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd Socket6-$pkgver
  perl Makefile.PL INSTALLDIRS=vendor
  make
}

check() {
  cd Socket6-$pkgver
  make test
}

package() {
  cd Socket6-$pkgver
  make DESTDIR="$pkgdir" install
  for i in BSD3 ISC; do
    install -Dm644 $srcdir/LICENSE.$i $pkgdir/usr/share/licenses/$pkgname/LICENSE.$i
  done
}
