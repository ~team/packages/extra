# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Dale Blount <dale@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-sys-hostname-long
_realname=Sys-Hostname-Long
pkgver=1.5
_debver=$pkgver
_debrel=2
pkgrel=1
pkgdesc="Perl module getting a full hostname"
arch=('any')
license=('GPL-1')
url='https://search.cpan.org/dist/Sys-Hostname-Long/'
depends=('perl')
makedepends=('quilt')
options=('!emptydirs')
source=("https://search.cpan.org/CPAN/authors/id/S/SC/SCOTT/${_realname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/libs/libsys-hostname-long-perl/libsys-hostname-long-perl_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e34a5f17e65eb9c8d3aa213a6d930be4d0b6981dae3bf600e782b0e9863e6584061285a894f9f54daff7520bcadbb3e8cd6b1f9f973ad1c1b72c79e9f588133b'
            '60f860d6d2fd0fac137452e9c9f9ee2c6459737bf0dcb04b6af0a4000cb29daa9e3d7adf1c41ea8b978579573430005526b1e557b88eef7619913773ffee2670')

prepare() {
  cd "${srcdir}/${_realname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${_realname}-${pkgver}"
  # install module in vendor directories.
  PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd "${srcdir}/${_realname}-${pkgver}"
  make install DESTDIR="${pkgdir}"
  install -Dm644 debian/copyright -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
