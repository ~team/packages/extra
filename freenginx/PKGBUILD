# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on nginx package

pkgname=freenginx
pkgver=1.26.0
pkgrel=1
pkgdesc="Lightweight, free and libre HTTP server and IMAP/POP3 proxy server"
arch=('i686' 'x86_64')
url='https://freenginx.org'
license=('Simplified-BSD')
depends=('pcre' 'zlib' 'libressl' 'geoip')
makedepends=('quilt')
optdepends=('logger: message logging support')
backup=('etc/freenginx/fastcgi.conf'
        'etc/freenginx/fastcgi_params'
        'etc/freenginx/koi-win'
        'etc/freenginx/koi-utf'
        'etc/freenginx/mime.types'
        'etc/freenginx/freenginx.conf'
        'etc/freenginx/scgi_params'
        'etc/freenginx/uwsgi_params'
        'etc/freenginx/win-utf'
        'etc/logrotate.d/freenginx'
        'etc/conf.d/freenginx')
conflicts=('nginx')
replaces=('nginx')
source=("https://freenginx.org/download/${pkgname}-${pkgver}.tar.gz"{,.asc}
        "logrotate"
        "freenginx.confd"
        "freenginx.initd"
        "freenginx.run")
sha512sums=('5804671c8195c00cb52440585dd84d3fb77964624a8154e2f8c523f859c3d2bd0e35872945acbb3b2edd53c6789938e059208cac8463ff87f1be07b17ca99164'
            'SKIP'
            'e058ff261ab5b810faa39526113553fc560b811b01879998d15fbde9ea4827662b007181f15c94daf4174584cece0327b6b4a143a034e1fb7f97484b5b401672'
            '26ad9c7577286ae4e095991344c6d1abcab32e3f5e71ed53ae6eb53193e2447d52026ef0182750797c226f49bcc5fdf8c62698e90c4ef0d8eafff92bfdd5ed83'
            '9fd98df5970358cf5a14e7511028d2c7e4f0d8cc5357d92280ca0adcdf6ef26875963a5c6c552fd4e6969a3a795b7258ea057c1df7941ced169864145469c859'
            '58024c8091f101bb5f4a5cfbfb77259f0f620ec858e680543c8c2d2a114fa9ce33c08bf8c0fbc8c047704e92106cd717f4526d868054784a2f9676f73566aff9')
validpgpkeys=('B0F4253373F8F6F510D42178520A9993A1C052F8') # Maxim Dounin <mdounin@mdounin.ru>

_common_flags=(
  --with-compat
  --with-debug
  --with-file-aio
  --with-http_addition_module
  --with-http_auth_request_module
  --with-http_dav_module
  --with-http_degradation_module
  --with-http_flv_module
  --with-http_geoip_module
  --with-http_gunzip_module
  --with-http_gzip_static_module
  --with-http_mp4_module
  --with-http_realip_module
  --with-http_secure_link_module
  --with-http_slice_module
  --with-http_ssl_module
  --with-http_stub_status_module
  --with-http_sub_module
  --with-http_v2_module
  --with-mail
  --with-mail_ssl_module
  --with-pcre-jit
  --with-stream
  --with-stream_geoip_module
  --with-stream_realip_module
  --with-stream_ssl_module
  --with-stream_ssl_preread_module
  --with-threads
)

_stable_flags=(
)

build() {
  cd "${pkgname}-${pkgver}"

  ./configure \
    --prefix=/etc/freenginx \
    --conf-path=/etc/freenginx/freenginx.conf \
    --sbin-path=/usr/sbin/freenginx \
    --pid-path=/run/freenginx.pid \
    --lock-path=/run/lock/freenginx.lock \
    --user=http \
    --group=http \
    --http-log-path=/var/log/freenginx/access.log \
    --error-log-path=stderr \
    --http-client-body-temp-path=/var/lib/freenginx/client-body \
    --http-proxy-temp-path=/var/lib/freenginx/proxy \
    --http-fastcgi-temp-path=/var/lib/freenginx/fastcgi \
    --http-scgi-temp-path=/var/lib/freenginx/scgi \
    --http-uwsgi-temp-path=/var/lib/freenginx/uwsgi \
    ${_common_flags[@]} \
    ${_stable_flags[@]}

  make
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  sed -e 's|\<user\s\+\w\+;|user html;|g' \
    -e '44s|html|/usr/share/freenginx/html|' \
    -e '54s|html|/usr/share/freenginx/html|' \
    -i "${pkgdir}/etc/freenginx/freenginx.conf"

  rm "${pkgdir}"/etc/freenginx/*.default

  install -d "${pkgdir}/var/lib/freenginx"
  install -dm700 "${pkgdir}/var/lib/freenginx/proxy"

  chmod 755 "${pkgdir}/var/log/freenginx"
  chown root:root "${pkgdir}/var/log/freenginx"

  install -d "${pkgdir}/usr/share/freenginx"
  mv "${pkgdir}"/etc/freenginx/html/ "${pkgdir}"/usr/share/freenginx
  touch "${pkgdir}"/usr/share/freenginx/html/favicon.ico

  install -Dm644 "${srcdir}/logrotate" "${pkgdir}/etc/logrotate.d/freenginx"
  install -Dm644 "${srcdir}/freenginx.confd" "${pkgdir}/etc/conf.d/freenginx"
  install -Dm755 "${srcdir}/freenginx.initd" "${pkgdir}/etc/init.d/freenginx"
  install -Dm755 "${srcdir}/freenginx.run" "${pkgdir}/etc/sv/freenginx/run"
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  rmdir "${pkgdir}"/run

  install -Dm644 man/nginx.8 -t "${pkgdir}/usr/share/man/man8"

  for i in ftdetect indent syntax; do
    install -Dm644 contrib/vim/$i/nginx.vim "${pkgdir}/usr/share/vim/vimfiles/$i/nginx.vim"
  done
}