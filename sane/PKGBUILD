# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Sarah Hay <sarahhay@mb.sympatico.ca>
# Contributor (Arch): Simo L. <neotuli@yahoo.com>
# Contributor (Arch): eric <eric@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=sane
pkgver=1.0.31
_debver=$pkgver
_debrel=4.1
pkgrel=2
pkgdesc='Scanner Access Now Easy'
url='http://www.sane-project.org/'
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('bash' 'cairo' 'gcc-libs' 'glibc' 'libgphoto2' 'libjpeg-turbo' 'libnl'
         'libpng' 'libtiff' 'libxml2' 'libieee1284' 'net-snmp' 'libressl' 'v4l-utils')
makedepends=('curl' 'glib2' 'libusb' 'poppler-glib' 'quilt' 'intltool' 'gettext-tiny'
             'autoconf-archive' 'python')
backup=(etc/sane.d/{abaton.conf,agfafocus.conf,apple.conf,artec.conf,artec_eplus48u.conf,avision.conf,bh.conf,canon.conf,canon630u.conf,canon_dr.conf,canon_pp.conf,cardscan.conf,coolscan2.conf,coolscan3.conf,coolscan.conf,dc25.conf,dc210.conf,dc240.conf,dell1600n_net.conf,dll.conf,dmc.conf,epjitsu.conf,epson.conf,epson2.conf,epsonds.conf,fujitsu.conf,genesys.conf,gphoto2.conf,gt68xx.conf,hp.conf,hp3900.conf,hp4200.conf,hp5400.conf,hpsj5s.conf,hs2p.conf,ibm.conf,kodak.conf,kodakaio.conf,leo.conf,lexmark.conf,ma1509.conf,magicolor.conf,matsushita.conf,microtek.conf,microtek2.conf,mustek.conf,mustek_pp.conf,mustek_usb.conf,nec.conf,net.conf,p5.conf,pie.conf,pieusb.conf,pixma.conf,plustek.conf,plustek_pp.conf,qcam.conf,ricoh.conf,rts8891.conf,s9036.conf,saned.conf,sceptre.conf,sharp.conf,sm3840.conf,snapscan.conf,sp15c.conf,st400.conf,stv680.conf,tamarack.conf,teco1.conf,teco2.conf,teco3.conf,test.conf,u12.conf,umax.conf,umax1220u.conf,umax_pp.conf,xerox_mfp.conf,v4l.conf})
source=(https://deb.debian.org/debian/pool/main/s/sane-backends/sane-backends_${pkgver}.orig.tar.gz
        https://deb.debian.org/debian/pool/main/s/sane-backends/sane-backends_${_debver}-${_debrel}.debian.tar.xz
        fix-build.patch
        saned.confd
        saned.initd
        saned.run)
sha512sums=('d8ef05cc3aa9c4fa42c9241e1e61fc93e7959df3746a3a2cfaa6e4fb26dfd0911b4d3227b2da28852f8630fa17ad3432a1230a6f4425340e79a3b82ec5eaa9eb'
            '9db8332f6760bf9bccbf49e0f3c290f0d6015f344a3e27ad75663541c47c11802db5dcfeb60e0236ea26a8c2cc0881148dad6b512ead7abfcbdb2d0bb29b8cc8'
            '2ff3d3a4d653c0c03163d9ee6e00c94a547f91e519785548de850e638636dc559e20a764559e9bbd5682b91f13d43e40bcdec51a40382a8969ed014cbccb3cac'
            '8f4a99ef24c19b3a7213b3d25ea16f5bca8623735cf25d21e3a706e9e0135ac8cce5d0b8fb8728845977f41dcefe6bd858076d755515fc91b00e05ee5817d751'
            '3dca312125ecaf9da33165a978f877a43b4040638b98d029dfb59909363966e04b85effb613fc92c0e830d8d94d03898da7290303d32d5c4b2d2bfed4665b498'
            'a5e23f66031557d65e9479a4d3a62b1e7302d06291eae171aaf0262d3aaebe761ac579bc19282f3a5213cf33505f87acf476c824ba0d821f7f97f43376bae7bb')

prepare() {
  cd backends-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0125-multiarch_dll_search_path.patch || true
    rm -v debian/patches/0140-avahi.patch || true
    rm -v debian/patches/0145-avahi.patch || true
    rm -v debian/patches/0155-hurd_PATH_MAX.patch || true
    rm -v debian/patches/0705-kfreebsd.patch || true

    quilt push -av
  fi

  # additional fixes
  patch -Np1 -i ../fix-build.patch

  ./autogen.sh
}

build() {
  cd backends-${pkgver}

  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --enable-pthread \
    --disable-rpath \
    --disable-locking
  make
}

package() {
  cd backends-${pkgver}
  make DESTDIR="${pkgdir}" install

  # fix hp officejets
  echo "#hpaio" >> "${pkgdir}/etc/sane.d/dll.conf"

  # install udev files
  install -D -m0644 tools/udev/libsane.rules \
    "${pkgdir}/lib/udev/rules.d/49-sane.rules"

  # fix udev rules
  sed -i 's|NAME="%k", ||g' "${pkgdir}/lib/udev/rules.d/49-sane.rules"

  # Install the pkg-config file
  install -D -m644 tools/sane-backends.pc \
     "${pkgdir}/usr/lib/pkgconfig/sane-backends.pc"

  # install services
  install -D -m644 ${srcdir}/saned.confd \
      "${pkgdir}/etc/conf.d/saned"
  install -D -m755 ${srcdir}/saned.initd \
      "${pkgdir}/etc/init.d/saned"
  install -D -m755 ${srcdir}/saned.run \
      "${pkgdir}/etc/sv/saned/run"

  # install license file
  install -D -m644 COPYING \
     -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
