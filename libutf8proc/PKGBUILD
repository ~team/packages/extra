# Maintainer (Arch): Alexander F. Rødseth <xyproto@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=libutf8proc
pkgver=2.5.0
_sover=2.3.2
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="C library for processing UTF-8 encoded Unicode strings"
arch=('i686' 'x86_64')
url='https://github.com/JuliaStrings/utf8proc'
license=('Expat' 'Unicode')
makedepends=('cmake' 'ninja' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/JuliaStrings/utf8proc/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/u/utf8proc/utf8proc_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('0c553faf4f3841c17c7aa4cce1e917b1585c430ac3f7f240ab98cbe01b9743f2074532e6f71faf3df030f5af00e483a3faf9716a67e6a4b1bb66a3de48308014'
            '0a32ae86570560b98eb174b33ff819cc07ad945466527def0567e729436491aab410b9d239597d5484c0b07d4dae676f2ebc2ffb09a2bd6ef455abb08e6cd94b')

prepare() {
  cd utf8proc-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd utf8proc-$pkgver
  cmake \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=ON \
    -G Ninja
  ninja -C build
  find .
}

package() {
  cd utf8proc-$pkgver
  install -Dm644 utf8proc.h "$pkgdir/usr/include/utf8proc.h"
  install -Dm644 LICENSE.md "$pkgdir/usr/share/licenses/$pkgname/LICENSE.md"
  install -Dm644 "libutf8proc.pc.in" "$pkgdir/usr/lib/pkgconfig/libutf8proc.pc"
  install -Dm644 build/libutf8proc.so.$_sover "$pkgdir/usr/lib/libutf8proc.so.$_sover"
  ln -s /usr/lib/libutf8proc.so.$_sover "$pkgdir/usr/lib/libutf8proc.so"
}