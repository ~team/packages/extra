# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Vladimir Chizhov <jagoterr@gmail.com>
# Contributor (Arch): Robert Emil Berge <filoktetes@linuxophic.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=recoll
pkgver=1.28.5
pkgrel=1
pkgdesc='Full text search tool based on Xapian backend'
arch=('i686' 'x86_64')
url='https://www.lesbonscomptes.com/recoll/'
license=('GPL-2')
depends=('xapian-core' 'qt-base' 'libressl' 'hicolor-icon-theme' 'qt-webkit' 'chmlib')
makedepends=('python' 'python-setuptools')
optdepends=('libxslt: for XML based formats (fb2,etc)'
	    'unzip: for the OpenOffice.org documents'
	    'poppler: for pdf'
	    'pstotext: for postscipt'
	    'antiword: for msword'
	    'catdoc: for ms excel and powerpoint'
	    'unrtf: for RTF'
	    'djvulibre: for djvu'
	    'id3lib: for mp3 tags support with id3info'
	    'python-mutagen: Audio metadata'
	    'perl-image-exiftool: EXIF data from raw files'
	    'python-lxml: indexing spreadsheets'
            'python-pychm: CHM filter'
	    'aspell-en: English stemming support')
source=("https://www.lesbonscomptes.com/$pkgname/$pkgname-${pkgver/_/}.tar.gz"
        "remove-dbus.patch")
sha512sums=('df4c7a5e624933ccc7ce257a767a4621728a4dbcdd5b551b47845ed0fcffdd5a6f7f617a466404c5e8d8b62c81e57986e1f21e01d643899755623a2dc597d712'
            '0ea49cde0c1681143bb3c2c3150b6f809668a1935cbf8a78dae1240263b84c4412b06a63285d56e44e726c3fd9f68bd8f7e767ac3d188cc673a843d3ce020fca')

prepare() {
  cd "$srcdir/$pkgname-${pkgver/_/}"

  # add patches
  patch -Np1 -i $srcdir/remove-dbus.patch

  sed -i '1,1i#include <QAbstractItemView>' qtgui/ssearch_w.cpp
}

build() {
  cd "$srcdir/$pkgname-${pkgver/_/}"
  QMAKE=qmake-qt5 ./configure --prefix=/usr --mandir=/usr/share/man --enable-recollq
  make
}

package() {
  cd "$srcdir/$pkgname-${pkgver/_/}"
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
