# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: André Silva <emulatorman@hyperbola.info>

pkgname=shared-mime-info
pkgver=1.15
pkgrel=3
pkgdesc="Freedesktop.org Shared MIME Info"
arch=('i686' 'x86_64')
license=('GPL-2')
depends=('libxml2' 'glib2')
makedepends=('itstool' 'gettext-tiny' 'xmlto' 'docbook-xsl')
install=shared-mime-info.install
url="https://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec"
source=("https://gitlab.freedesktop.org/xdg/shared-mime-info/uploads/b27eb88e4155d8fccb8bb3cd12025d5b/$pkgname-$pkgver.tar.xz"
        update-mime-database.hook)
sha512sums=('3666aa500dfa6a28bd0524400c47fa16d90ae61f8c80f350fd895972319ec2f511618b8a7fa3cbde621edee46fde19e4506bda62f0bd2d0ede1b08d7bdb9aef2'
            '365e2bca04e108a6a65c1e8a2b82efcee3916630ca522808e51074db7fb499d7f7c67bb84b304f5f398c17379ff26ec7fd1e44d56991a47581111c1c360ffdb8')
options=(!makeflags)

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  # https://bugs.archlinux.org/task/38836
  # https://bugs.freedesktop.org/show_bug.cgi?id=70366
  export ac_cv_func_fdatasync=no
  ./configure \
      --prefix=/usr \
      --disable-update-mimedb
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make -k check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  install -Dm644 "$srcdir"/update-mime-database.hook "$pkgdir"/usr/share/libalpm/hooks/update-mime-database.hook
}

# vim:set ts=2 sw=2 et:
