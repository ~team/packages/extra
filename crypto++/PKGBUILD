# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Baptiste Jonglez <archlinux at bitsofnetworks.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Alexander Rødseth <rodseth@gmail.com>
# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Kritoke <kritoke@gamebox.net>
# Contributor (Arch): jlvsimoes <jlvsimoes@oninet.pt>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=crypto++
pkgver=8.4.0
pkgrel=1
pkgdesc="A free C++ class library of cryptographic schemes"
arch=('i686' 'x86_64')
url='https://www.cryptopp.com/'
license=('Boost-1.0' 'Modified-BSD' 'Public-Domain')
depends=('gcc-libs')
makedepends=('unzip')
source=("https://deb.debian.org/debian/pool/main/libc/libcrypto++/libcrypto++_${pkgver}.orig.tar.gz"
        "libcryptopp.pc")
sha512sums=('6cbceb5e662d258f2db018e75e0f196092a8ba75350711b83d6b44b328718b348b6c071d9d0268062fb85cf9220c6a1caf30d408892f5a6f0f8bacfa887ca788'
            '19707ade89b451962b09663726f6fcc6cc1deda4fb8a4396bcee0e5beb00239d52b97d540e2500278a0ff2deb853c8e18b0c02a142ba7cf9237cb1405eb32910')

prepare() {
  mv "cryptopp-CRYPTOPP_8_4_0" $pkgname-$pkgver
}

build() {
  export CXXFLAGS="$CXXFLAGS -DNDEBUG -fPIC"
  make -C $pkgname-$pkgver dynamic cryptest.exe libcryptopp.pc
}

package() {
  make DESTDIR="${pkgdir}" PREFIX="/usr" -C $pkgname-$pkgver install
  install -Dm644 $pkgname-$pkgver/License.txt -t "${pkgdir}"/usr/share/licenses/$pkgname/

  # Remove cryptest.exe and test files, bloats the package
  # because cryptest.exe is linked statically.
  rm -rf "${pkgdir}"/usr/{bin,share/cryptopp}

  # Correcting pkg-config
  rm "${pkgdir}"/usr/lib/pkgconfig/libcryptopp.pc
  cp "${srcdir}/libcryptopp.pc" "${pkgdir}"/usr/lib/pkgconfig/
}
