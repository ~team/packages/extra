# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# Based on xorg-xcalc package

pkgname=xenocara-xcalc
_openbsdver=6.9
pkgver=1.1.0
pkgrel=2
pkgdesc="Scientific calculator for X, provided by Xenocara"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('X11')
depends=('libxaw')
optdepends=('xenocara-font-adobe-75dpi: for some symbols'
            'xenocara-font-bitstream-75dpi: for some symbols'
            'xenocara-font-adobe-100dpi: for some symbols'
            'xenocara-font-bitstream-100dpi: for some symbols')
makedepends=('xenocara-util-macros')
provides=('xorg-xcalc')
conflicts=('xorg-xcalc')
replaces=('xorg-xcalc')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/app/xcalc-$pkgver.tar.lz{,.sig})
sha512sums=('e78b4555fd393c633368e8f0bfb5ce076899b3dbdc1057e2f26dae88358e971ee923a10da72557b617b6fea1776e82a38e6cb25d2156b20c3ad7bc73c4384cd5'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/app/xcalc"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/app/xcalc"
  ./configure --prefix=/usr
  make
}

package() {
  cd "xenocara-$_openbsdver/app/xcalc"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
