# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Sergej Pupykin <sergej@aur.archlinux.org>
# Contributor (Arch): Robert Stoffers <rob1@ubuntu.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Enmanuel E. Saravia <saravia@riseup.net>

pkgname=xmlto
pkgver=0.0.28
_debver=0.0.28
_debrel=2.1
pkgrel=1
pkgdesc="Convert xml to many other formats"
arch=('i686' 'x86_64')
url="https://pagure.io/xmlto/"
license=('GPL-2')
depends=('libxslt')
makedepends=('docbook-xsl' 'quilt')
source=("https://releases.pagure.org/xmlto/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/x/xmlto/xmlto_$_debver-$_debrel.debian.tar.xz")
sha512sums=('6e0c4968d4f1b7a3b132904182aa72a73f6167553eabdeb65cfafa6295ef7b960541685769d04144207963cca77b0c44db4f9fbb2796348ffcb37b3b399f18f1'
            '04acef96ed5200c76ea517ebeab528ee9fa8d43623567fa6e7424702eeb72ffa3efef3161ee42d0571ed2a6c7ae2437739867b5a8877e4d2d378c93b56decdd7')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr \
              --mandir=/usr/share/man
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  install -d -m755 "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}

# vim:set ts=2 sw=2 et:
