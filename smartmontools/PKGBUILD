# Maintainer (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Jeff Mickey <jeff@archlinux.org>
# Contributor (Arch): Jani Talikka <jani.talikka@gmail.com>
# Contributor (Arch): Ralf Barth <archlinux.org@haggy.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=smartmontools
pkgver=7.2
_debver=7.2
_debrel=1
pkgrel=1
pkgdesc="Control and monitor S.M.A.R.T. enabled ATA and SCSI Hard Drives"
url="https://www.smartmontools.org/"
license=('GPL-2')
arch=('i686' 'x86_64')
makedepends=('quilt')
depends=('gcc-libs' 'libcap-ng' 'bash')
optdepends=('s-nail: to get mail alerts to work')
backup=('etc/smartd.conf'
        'etc/conf.d/smartd')
validpgpkeys=('847AF8F72DF4C7B492382C94BF0B339C64BCAA8B') # Smartmontools Signing Key (through 2020) <smartmontools-support@listi.jpberlin.de>
source=("https://downloads.sourceforge.net/sourceforge/${pkgname}/${pkgname}-${pkgver}.tar.gz"{,.asc}
        "https://deb.debian.org/debian/pool/main/s/smartmontools/smartmontools_$_debver-$_debrel.debian.tar.xz"
        'smartd.confd'
        'smartd.initd')
sha512sums=('d7e724295b5d53797b5e4136eea5f5cc278db81e4016ba65142438b8c68c54f85a32c582c147a1590b9bc8f74a58952bcb57b9923dd69d34582530a0985799ea'
            'SKIP'
            '636297b09d27993871a2c147348800998a53fe95124302c8d0a10840263526b7dfc09c6127dddef46e6dec3a0fe8e4c0190cbab69c0d034e5bf35efaf8f38ee3'
            'ff45462944f01c0e7cac99443078b253dcbaed4f99ffab85052b12361d9eec4beec81cab334fbede9148b3dd503d5205add32ef4327a788f02a8b1e37f460437'
            '3f8e451abe1159ee135c33cd6e83e65c57ce53fa3a2f4bff6cb8a4b01054d333b51b9f5238ff1ffd4d07120c8192955ac62541e72dbd664e256d2020e458d5b1')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/manpage.diff || true
    rm -v debian/patches/service-alias.patch || true

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-drivedbdir \
    --with-libcap-ng=yes \
    --with-initscriptdir=/etc/init.d \
    --with-smartdscriptdir=/usr/share/smartmontools \
    --with-smartdplugindir=/usr/share/smartmontools/smartd_warning.d

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install
  install -d "$pkgdir/usr/share/licenses/$pkgname"
  install -m644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  rm -rf ${pkgdir}/etc/rc.d
  install -Dm644 ${srcdir}/smartd.confd "${pkgdir}/etc/conf.d/smartd"
  install -Dm755 ${srcdir}/smartd.initd "${pkgdir}/etc/init.d/smartd"
}

# vim:set ts=2 sw=2 et:
