# Maintainer (Arch): Johannes Löthberg <johannes@kyriasis.com>
# Maintainer (Arch): Daniel M. Capella <polyzen@archlinux.org>
# Contributor (Arch): Sébastien Luttringer
# Contributor (Arch): Angel Velasquez <angvp@archlinux.org>
# Contributor (Arch): Fabio Volpe <volpefabio@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=python-sphinx
pkgver=3.0.3
pkgrel=1
pkgdesc='Python documentation generator'
arch=('any')
url=https://www.sphinx-doc.org/
license=('Simplified-BSD')
depends=('python-babel'
         'python-docutils'
         'python-imagesize'
         'python-jinja'
         'python-pygments'
         'python-requests'
         'python-setuptools'
         'python-snowballstemmer'
         'python-sphinx-alabaster-theme'
         'python-sphinxcontrib-'{{apple,dev,html}help,jsmath,qthelp,serializinghtml})
#checkdepends=('cython'
#              'imagemagick' 'librsvg'
#              'python-html5lib'
#              'python-pytest'
#              'texlive-fontsextra' 'texlive-latexextra')
optdepends=('imagemagick: for ext.imgconverter')
source=("https://pypi.org/packages/source/S/Sphinx/Sphinx-$pkgver.tar.gz"{,.asc})
sha512sums=('1e37224d04f0a7836a7132cea55c67b9d15a22f941a790288438b39d455973f254205ffd1e99e6e2305430725a6a623b9a85fb95b495a9f6f912fb94825fdcd0'
            'SKIP')
validpgpkeys=('8A11B79A5D0D749A66F0A030102C2C17498D6B9E'  # Takeshi KOMIYA
              'E9BEABB07E7B9CC3F56E62C91425F8CE5EBA0E07') # Takayuki Shimizukawa

build() {
  cd Sphinx-$pkgver
  make build
}

#check() {
#  cd Sphinx-$pkgver
#  LC_ALL="en_US.UTF-8" make test
#  rm -r tests
#}

package() {
  cd Sphinx-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
  install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE
}

# vim:set ts=2 sw=2 et:
