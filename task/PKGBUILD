# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Maintainer (Arch): Orhun Parmaksız <orhun@archlinux.org>
# Contributor (Arch): Peter Lewis <plewis@aur.archlinux.org>
# Contributor (Arch): TDY <tdy@gmx.com>
# Contributor (Arch): Ray Kohler <ataraxia@gmail.com>
# Contributor (Arch): muflax <muflax@gmail.com>
# Contributor (Arch): coolkehon <coolkehon@gmail.com>
# Contributor: Jesús E. <heckyel@hyperbola.info>

pkgname=task
pkgver=2.5.3
_debver=2.5.3
_debrel=4
pkgrel=1
pkgdesc="A command-line todo list manager"
arch=('i686' 'x86_64')
url="https://taskwarrior.org/"
license=('Expat')
depends=('util-linux' 'gnutls')
makedepends=('cmake' 'quilt')
optdepends=('bash-completion: for bash completion'
            'python: for python export addon'
            'ruby: for ruby export addon'
            'perl: for perl export addon'
            'perl-json: for perl export addon')
source=("https://taskwarrior.org/download/$pkgname-$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/t/task/task_$_debver+dfsg-$_debrel.debian.tar.xz"
        '0011-Lexer-Do-not-allow-leading-zero-for-number-type.patch'
        '0012-Backport-parser-issues-fixes.patch')
sha512sums=('e906c8f42ad4b9a7e20a82defe31b89194d72957f18dd5129ecc41a2a60a9d8b0d01abb9b44ecce79b65cd9064af4a4a4c9dd695f98152e77908f130dc3f9677'
            '64591722cd71e9d5dfa40747d2bf0b78af18694c6e4dcdcdc575ff305d8f7df1f3023646b1fef81c0a0bfc96e8d8964c12b2417829a8a2e1bb4cf7585f2a6eb9'
            '47351e83fa286b0047fe8181cd8b4f4454efcf11b6d92c54d69fda8ee7419d7cba14affe860b02a0ee6305a5ded29d5de3e61b5357b676aeab8ec43fb9265fa8'
            '23c6ab14652bf1304efc35b44ea4fb35363b843d1688ab27b20923ae2ffefd4e3a67cd0a3f7291ba9be0fa37dd725df1930ae63f545eb5bb3bfb26707c30a9cf')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0011-Lexer-Do-not-allow-leading-zero-for-number-type.patch || true
    rm -v debian/patches/0012-Backport-parser-issues-fixes.patch || true
    rm -v debian/patches/0013-Port-testsuite-to-python3.patch || true
    rm -v debian/patches/005_tests-time-independent.patch || true
    rm -v debian/patches/006_update-basetest-prefix.patch || true
    rm -v debian/patches/008_fix_date_tests.patch || true
    rm -v debian/patches/009_fix-test-exit-code.patch || true

    quilt push -av
  fi

  # fix debian patches
  patch -Np1 -i "$srcdir/0011-Lexer-Do-not-allow-leading-zero-for-number-type.patch"
  patch -Np1 -i "$srcdir/0012-Backport-parser-issues-fixes.patch"
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  cmake -DCMAKE_INSTALL_PREFIX=/usr .
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install

  # Note that we rename the bash completion script for bash-completion > 1.99, until upstream does so.
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/bash/task.sh" "$pkgdir/usr/share/bash-completion/completions/task"
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/fish/task.fish" "$pkgdir/usr/share/fish/vendor_completions.d/task.fish"
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/zsh/_task" "$pkgdir/usr/share/zsh/site-functions/_task"

  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/vim/ftdetect/task.vim" "$pkgdir/usr/share/vim/vimfiles/ftdetect/task.vim"
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/vim/syntax/taskdata.vim" "$pkgdir/usr/share/vim/vimfiles/syntax/taskdata.vim"
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/vim/syntax/taskedit.vim" "$pkgdir/usr/share/vim/vimfiles/syntax/taskedit.vim"
  install -Dm644 "$pkgdir/usr/share/doc/task/scripts/vim/syntax/taskrc.vim" "$pkgdir/usr/share/vim/vimfiles/syntax/taskrc.vim"

  install -d "$pkgdir/usr/share/licenses/$pkgname"
  for i in COPYING LICENSE; do
    install -m644 "$i" -t "$pkgdir/usr/share/licenses/$pkgname"
  done
}
