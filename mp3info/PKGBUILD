# Contributor (Arch): Andrea Scarpino <andrea@archlinux.org>
# Contributor (Arch): Vinay S Shastry <vinayshastry@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=mp3info
pkgver=0.8.5a
_debver=$pkgver
_debrel=5
pkgrel=1
pkgdesc="An MP3 technical info viewer and ID3 1.x tag editor"
arch=('i686' 'x86_64')
url='https://ibiblio.org/mp3info/'
license=('GPL-2')
depends=('ncurses' 'gtk2')
makedepends=('quilt')
source=("https://ibiblio.org/pub/linux/apps/sound/mp3-utils/${pkgname}/${pkgname}-${pkgver}.tgz"
        "https://deb.debian.org/debian/pool/main/m/mp3info/mp3info_${_debver}-${_debrel}.debian.tar.xz"
        "gmp3info.desktop"
        "gmp3info.png")
sha512sums=('30e0ef62deaa090fc115317f6c372f408077dfceed7e05faa6744b8516291e38d21f1742732f6532585f0c3d8c705d8c7026c2bed84c97e1f497d433637affd2'
            '6a153c9bf208570f6009d96f7302ac514626bb94b5ae42948377ff74725a9d23e5ded32d4549550e920ad80b76065888138aee02e094f91d9011418d41dfc3af'
            'dc2b5c451f23b3c28c82146aed53f676e0cd84825f798cf4d45e7a04ab67380e760a41fa023519129ed8adc5937bc7c3a3b86700966b596886cc727c5219d785'
            '2b12c605d6de45d5edff1c9a388752e53a9a7231fc09a78ecf30c34353f9f66ecb9bc5dbf1992966ded5e65b74f4922d29fffdb6fa004627622d2261b14cb12a')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed -i "s:-O2:${CFLAGS}:" Makefile
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  install -d "$pkgdir"/usr/bin "$pkgdir"/usr/share/man/man1
  make prefix="$pkgdir"/usr mandir="$pkgdir"/usr/share/man/man1 install
  install -Dm644 "$srcdir"/gmp3info.desktop -t "$pkgdir"/usr/share/applications
  install -Dm644 "$srcdir"/gmp3info.png -t "$pkgdir"/usr/share/pixmaps
  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname
}