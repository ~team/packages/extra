# Maintainer (Arch): Sébastien "Seblu" Luttringer <seblu@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Dale Blount <dale@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=ulogd
pkgver=2.0.7
pkgrel=2
pkgdesc="Userspace Packet Logging for netfilter"
arch=('i686' 'x86_64')
url='https://www.netfilter.org/projects/ulogd/'
license=('GPL-2')
backup=('etc/ulogd.conf')
install=$pkgname.install
depends=('libmnl' 'libnetfilter_acct' 'libnetfilter_conntrack' 'libnetfilter_log' 'libnfnetlink')
makedepends=('libpcap' 'libdbi' 'postgresql-libs' 'sqlite' 'jansson')
optdepends=('sqlite: SQLite3 databases support'
            'postgresql-libs: PostgreSQL databases support'
            'libdbi: DBI abstraction databases support'
            'libpcap: PCAP output support'
            'jansson: JSON output support')
source=("https://www.netfilter.org/projects/ulogd/files/$pkgname-$pkgver.tar.bz2"
        "$pkgname.logrotate"
        "$pkgname.conf"
        "$pkgname.initd"
        "$pkgname.run")
sha512sums=('1ad12bcf91bebe8bf8580de38693318cdabd17146f1f65acf714334885cf13adf5f783abdf2dd67474ef12f82d2cfb84dd4859439bc7af10a0df58e4c7e48b09'
            '387706168b4053bd944c366052b00a1e8bd477d9a419791e92896eee5a35ced041dfd4d2cec3fd69a4da0d9470aef2eb1ec0ebb0ab315c177252c0d06e50d17f'
            'eb228cb53ccd9d359963bc3a01f4bdfcc1da969361ca6135fd765e37f691f3ddde47d8699fbcb4a66982d5b0e65e81fc997a5bf067af0785035b29b8e07610a3'
            '28e75973ca3172d8cdb1b158fbd0b9ead1747668747db7854b181d7867a7fec1d72174b5d5be0ab97e768843582ccce21b709a873aac8f40647689afb3f47c91'
            '810e391a319b2a72b3576cc8f55b3e6649c90a00acadba8be2758c4c7d467bf540c1774c51d79c363cb5d4beb77d69af5aff6623ac3eb262b10d1fceaaf3c9d4')

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --without-mysql \
    --with-pgsql \
    --with-dbi
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
  install -dm755 "$pkgdir/usr/share/doc/ulogd"
  install -Dm644 ulogd.conf "$pkgdir/usr/share/doc/ulogd/ulogd.conf"
  install -m644 doc/*.{table,sql} "$pkgdir/usr/share/doc/ulogd"
  install -Dm644 $srcdir/$pkgname.logrotate "$pkgdir/etc/logrotate.d/$pkgname"
  install -Dm644 $srcdir/$pkgname.conf "$pkgdir/etc/$pkgname.conf"
  install -Dm755 $srcdir/$pkgname.initd "$pkgdir/etc/init.d/$pkgname"
  install -Dm755 $srcdir/$pkgname.run "$pkgdir/etc/sv/$pkgname/run"
}
