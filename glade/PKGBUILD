# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Douglas Soares de Andrade <douglas@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=glade
pkgver=3.38.2
pkgrel=1
pkgdesc='User Interface Builder for GTK+ applications'
url='https://glade.gnome.org/'
arch=('i686' 'x86_64')
license=('GPL-2' 'LGPL-2')
depends=('gtk' 'libxml2')
makedepends=('gobject-introspection' 'python-gobject' 'itstool' 'docbook-xsl'
             'webkitgtk' 'meson' 'ninja' 'gettext-tiny')
source=("https://deb.debian.org/debian/pool/main/g/glade/glade_${pkgver}.orig.tar.xz"
        "fix-build.patch")
sha512sums=('2df8c8363206905663cd3ac9196da44445425a888b64f1b3b3ac6cd41e14c4a82a5d2c299730ef0606c3970a04dd8f245bb5639218794eb67dcbd26fae17565e'
            '37295e12f25c595630066a1fc1a0232988cdfe593c1ca5228b961794a380258ebc17a2e1d71a057678fb842291ef3a50c3d3a2e50c2babd9fbf504deb4f80801')

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 -i ../fix-build.patch
}

build() {
  hyperbola-meson $pkgname-$pkgver build \
    -D gtk_doc=false \
    -D gjs=disabled
  meson compile -C build
}

package() {
  DESTDIR="$pkgdir" meson install -C build

  # license
  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING.GPL" -t "${pkgdir}/usr/share/licenses/$pkgname"
  install -Dm644 "$srcdir/$pkgname-$pkgver/COPYING.LGPL" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
