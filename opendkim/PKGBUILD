# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer (Arch): Thomas Jost <schnouki@schnouki.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=opendkim
pkgver=2.10.3
pkgrel=6
pkgdesc='A free and libre implementation of the DKIM sender authentication system, based on a fork of dkim-milter.'
arch=('i686' 'x86_64')
url='http://www.opendkim.org/'
license=('Modified-BSD' 'custom:Sendmail')
depends=('db' 'libressl' 'libbsd' 'opendbx' 'libldap')
optdepends=('perl: for opendkim-genkey')
makedepends=('libmilter')
backup=(etc/conf.d/opendkim)
options=(!emptydirs)
install=$pkgname.install
source=("https://downloads.sourceforge.net/project/$pkgname/$pkgname-$pkgver.tar.gz"
        "$pkgname.initd"
        "$pkgname.confd"
        "$pkgname.run"
        "$pkgname.conf")
sha512sums=('97923e533d072c07ae4d16a46cbed95ee799aa50f19468d8bc6d1dc534025a8616c3b4b68b5842bc899b509349a2c9a67312d574a726b048c0ea46dd4fcc45d8'
            '220ba494b83189aceba304534d1538cc38414c82a522a9fa8211f9277bf22ae8787581b3913e2a267b766ad33bfa4ee005780ee744633f2c765d15696f5bd0cb'
            'db103c23d03bcc5919e795cd0bb6e9cfa6a0b8b65c55476fce8aaedfa539a707f0e2b7e10fc493e8a18ef9743b1f80bb97c1b3aaa8b6dc51646898ffdc0fff65'
            'c2799e2c879505cb4c06062b8e8277982f9a83125bef3aa12ddf52b839e7752a5fb1b2252cf397093ac51237896d3d29b46789406e033eee4a7362f4f5f93867'
            '7d4dc6862c40efa7fd3d235d00c9c0e4a76113232993c694866ac7af5c63ae6e0374a9e2728ea9b1eef81b102f4ff47aa2201bd9020db01fc4ad7399a400fbef')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  autoreconf -vi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --with-db \
    --with-odbx \
    --with-sql-backend \
    --with-openldap \
    --disable-live-testing
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make DESTDIR="$pkgdir/" install

  # remove remnants from systemd within documentation
  rm "$pkgdir/usr/share/doc/$pkgname/$pkgname.service"

  # sample configuration
  mkdir -p "$pkgdir/etc/$pkgname"
  mv "$pkgdir/usr/share/doc/$pkgname/$pkgname.conf.sample" "$pkgdir/etc/$pkgname/$pkgname.conf.sample"
  chmod 0700 "$pkgdir/etc/$pkgname"
  chmod 0600 "$pkgdir/etc/$pkgname/$pkgname.conf.sample"

  # services
  install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
  install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
  install -Dm644 "$srcdir/$pkgname.conf" "$pkgdir/etc/sv/$pkgname/conf"
  install -Dm755 "$srcdir/$pkgname.run" "$pkgdir/etc/sv/$pkgname/run"

  # create default configuration

  # Some MTAs are known to break DKIM signatures with "simple"
  # canonicalization [1], so we choose the "relaxed" policy
  # over OpenDKIM's current default settings.
  # [1] https://wordtothewise.com/2016/12/dkim-canonicalization-or-why-microsoft-breaks-your-mail/
  sed -E -e 's:^(Canonicalization)[[:space:]]+.*:\1\trelaxed/relaxed:' \
    "$srcdir/$pkgname-$pkgver/$pkgname/$pkgname.conf.simple" > "$pkgdir"/etc/$pkgname/$pkgname.conf
  
  # move tools to /usr/bin for FHS compliance
  install -dm755 "$pkgdir/usr/bin"
  for b in opendkim-{genkey,genzone,testkey,testmsg}; do
    mv "$pkgdir/usr/sbin/$b" "$pkgdir/usr/bin/$b"
  done

  # licenses
  install -Dm644 LICENSE LICENSE.Sendmail -t "$pkgdir/usr/share/licenses/$pkgname"
}
