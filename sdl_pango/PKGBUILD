# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.com>
# Contributor (Arch): gyo <nucleogeek@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=sdl_pango
pkgver=0.1.2
_debver=0.1.2
_debrel=8
pkgrel=1
pkgdesc='Pango SDL binding'
arch=('i686' 'x86_64')
url="http://sdlpango.sourceforge.net/"
license=('LGPL-2.1')
depends=('pango' 'sdl')
makedepends=('quilt')
source=(https://downloads.sourceforge.net/sourceforge/sdlpango/SDL_Pango-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/s/sdlpango/sdlpango_$_debver-$_debrel.debian.tar.xz)
sha512sums=('081ec57e0f55ce541c35393d6db7ea48b662a5008760781076d70c0a645d47f7e994f695c459ed51f8cb71494911a04cd416733fb57934321b806a1ac9878440'
            '645729529a3111ee07bbcabc57f150b597d3bfd106720c8634975315cd274f64e48c441874e756f8bb4c58926b7455a01bae4f5d2a833fac04e438bbd131964c')

prepare() {
  cd SDL_Pango-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd SDL_Pango-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  cd SDL_Pango-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
