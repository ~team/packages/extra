# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Committer (Arch): dorphell <dorphell@gmx.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=screen
pkgver=4.8.0
pkgrel=2
_debname="${pkgname}"
_debver="${pkgver}"
_debrel=6
pkgdesc='Full-screen window manager that multiplexes a physical terminal'
url="https://www.gnu.org/software/${pkgname}/"
arch=('i686' 'x86_64')
license=('GPL-3')
depends=('ncurses')
makedepends=('quilt')
backup=("etc/${pkgname}rc")
options=('!makeflags')
validpgpkeys=('2EE59A5D0C50167B5535BBF1B708A383C53EF3A4')
_source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.gz"
         "https://deb.debian.org/debian/pool/main/${_debname::1}/"
         "${_debname}/${_debname}_${_debver}-${_debrel}.debian.tar.xz")
source=("${_source[0]}" "${_source[0]}.sig" "${_source[1]}${_source[2]}")
sha512sums=('770ebaf6ee9be711bcb8a6104b3294f2bf4523dae6683fdc5eac4b3aff7e511be2d922b6b2ad28ec241113c2e4fe0d80f9a482ae1658adc19c8c3a3680caa25c'
            'SKIP'
            'fee65e34fa2850d752246119d3ac1c3900f406fce4893ccc7a095f516be22896633ee19389cc6477b782243d1c15ffea714b6927c95b3c7e8838f790b8eefbfd')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ "${pkgver%.*}" = "${_debver%.*}" ]]; then
    # Debian patches
    export QUILT_PATCHES='debian/patches'
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'
    QUILT_RM=('05prefer-libtinfo-over-libcurses'
              '11replace_doc_paths'
              '13split_info_files'
              '60-screen-4.2.1-debian4.1.0-compatibility'
              '80_session_creation_docs'
              '81_session_creation_util'
              '82_session_creation_core')

    mv "${srcdir}/debian" '.'

    # Doesn't apply and seems unimportant
    for i in ${QUILT_RM[@]}; do
      rm -v "${QUILT_PATCHES}/$i.patch" || true
    done
    unset i

    quilt push -av

    unset \
      QUILT_PATCHES \
      QUILT_REFRESH_ARGS \
      QUILT_DIFF_ARGS \
      QUILT_RM
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  _ptygroup='5' #the UID of our PTY/TTY group

  ./configure \
    --prefix="/usr" \
    --mandir="/usr/share/man" \
    --infodir="/usr/share/info" \
    --enable-colors256 \
    --disable-pam \
    --enable-rxvt_osc \
    --enable-telnet \
    --with-pty-group="${_ptygroup}" \
    --with-socket-dir="/run/${pkgname}" \
    --with-sys-screenrc="/etc/${pkgname}rc"

  unset _ptygroup

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  install -Dm '644' "etc/etc${pkgname}rc" "${pkgdir}/etc/${pkgname}rc"
  install -Dm '644' "etc/${pkgname}rc" "${pkgdir}/etc/skel/.${pkgname}rc"

  install -Dm '644' 'COPYING' -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
