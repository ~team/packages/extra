# Maintainer (Arch): Balló György <ballogyor+arch at gmail dot com>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): damir <damir@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gocr
pkgver=0.52
_debver=0.52
_debrel=3
pkgrel=1
pkgdesc="OCR (Optical Character Recognition) program, which converts scanned images of text back to text files"
arch=('i686' 'x86_64')
url='https://www-e.uni-magdeburg.de/jschulen/ocr/'
license=('GPL-2')
depends=('libjpeg-turbo' 'netpbm')
makedepends=('quilt')
optdepends=('fig2dev: read xfig images'
            'tk: to use the tk frontend')
source=("https://www-e.uni-magdeburg.de/jschulen/ocr/$pkgname-$pkgver.tar.gz"{,.asc}
        "https://deb.debian.org/debian/pool/main/g/gocr/gocr_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e2e6e690e7a32e32237c8b64b1a11719680c560592349df43f4b230f3c92bf324f98e473979b0913c42e33e8abbaf27952b6af173ea06eb5535878a9f608ce6d'
            'SKIP'
            '02a71278eb3279a2a264dd5f3aeafb11b02784fa04620cd126fbfc3607086d79a0ea51cb7c2fe1d1dec9bbf85451cd2d055ee50cd966049f93d087678aab9462')
validpgpkeys=('3816B803D578F5AD12FDFE065D330C4953BDFBE3')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --bindir=/usr/bin --mandir=/usr/share/man
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 debian/copyright -t "${pkgdir}/usr/share/licenses/$pkgname"
}
