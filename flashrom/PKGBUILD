# Maintainer (Arch): Bruno Pagani <archange@archlinux.org>
# Contributor (Arch): Giovanni Scafora <giovanni@archlinux.org>
# Contributor (Arch): Cilyan Olowen <gaknar@gmail.com>
# Contributor (Arch): fill <mailbox@fillo.ru>
# Contributor (Arch): Anton Shestakov <engored*ya.ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=flashrom
pkgver=1.2
pkgrel=1
pkgdesc='Utility for reading, writing, erasing and verifying flash ROM chips'
arch=('i686' 'x86_64')
url="https://www.flashrom.org/"
license=('GPL-2')
depends=('libftdi' 'pciutils' 'libusb-compat')
makedepends=('meson')
optdepends=('dmidecode: for SMBIOS/DMI table decoder support')
source=("https://download.flashrom.org/releases/${pkgname}-v${pkgver}.tar.bz2"{,.asc})
sha512sums=('8e01395a54d0d1344bc55e7e4975f43bb8e59ebe83fb5f2766d14f76756d718b487254ce9a19ef4093aed04fb9d40fff5fe8f709b18c512f6e6fad894f83d7b8'
            'SKIP')
validpgpkeys=(58A4868B25C7CFD662FB0132A3EB95B8D9780F68) # David Hendricks (packaging key) <david.hendricks@gmail.com>

build() {
  cd ${pkgname}-v${pkgver}
  hyperbola-meson ../build
  ninja -v -C ../build
  # Workaround for meson not installing manpage
  make
}

package() {
  DESTDIR="${pkgdir}" ninja -C build install
  # Workaround for meson not installing manpage
  cd ${pkgname}-v${pkgver}
  make PREFIX=temp/ install
  cp -r temp/share "${pkgdir}"/usr/

  # license
  install -Dm644 "${srcdir}/${pkgname}-v${pkgver}/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
