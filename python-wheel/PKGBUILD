# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer (Arch): Morten Linderud <foxboron@archlinux.org>
# Contributor (Arch): Lance Chen <cyen0312@gmail.com>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_pypiname=wheel
pkgbase=python-wheel
pkgname=('python-wheel' 'tauthon-wheel')
pkgver=0.37.0
pkgrel=5
_pyver=3.8
pkgdesc="A built-package format"
arch=(any)
url='https://pypi.python.org/pypi/wheel'
license=('Expat')
makedepends=('python-packaging' 'python-setuptools' 'tauthon-packaging' 'tauthon-setuptools')
source=("$pkgname-$pkgver.tar.gz::https://github.com/pypa/wheel/archive/$pkgver.tar.gz")
sha512sums=('2e0eb1c5560a0229be9de450da8b2bc62153744abb6d8ab9a0625c150dc00d3fc3e24dbe37cc26f4ca6b6876b5c63298427b2076ee42b8f772c75010bfc135a8')

prepare() {
  # don't depend on python-coverage for tests
  sed -i 's/--cov=wheel//' wheel-$pkgver/setup.cfg

  # https://github.com/pypa/wheel/pull/365
  rm -r wheel-$pkgver/src/wheel/vendored
  sed -i 's/from .vendored.packaging import tags/from packaging import tags/' wheel-$pkgver/src/wheel/bdist_wheel.py

  cp -a wheel-$pkgver{,-tauthon}
}

build() {
  export PYTHONHASHSEED=0

  cd "$srcdir/$_pypiname-$pkgver"
  python setup.py build

  cd "$srcdir/$_pypiname-$pkgver-tauthon"
  tauthon setup.py build
}

package_python-wheel() {
  pkgdesc+=" for Python"
  depends=('python-packaging')
  optdepends=('python-keyring: for wheel.signatures')
  optdepends=('python-pyxdg: for wheel.signatures')

  cd "$srcdir/$_pypiname-$pkgver"
  python setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -D -m644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"
}

package_tauthon-wheel() {
  pkgdesc+=" for Tauthon"
  depends=('tauthon-packaging')
  optdepends=('tauthon-keyring: for wheel.signatures')
  optdepends=('tauthon-pyxdg: for wheel.signatures')

  cd "$srcdir/$_pypiname-$pkgver-tauthon"
  tauthon setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -D -m644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE.txt"
  mv "$pkgdir/usr/bin/wheel" "$pkgdir/usr/bin/wheel2"
}
