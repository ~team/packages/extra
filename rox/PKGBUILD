# Maintainer (Arch): tobias <tobias@archlinux.org>
# Contributor (Arch): Jochem Kossen <j.kossen@home.nl>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=rox
pkgver=2.11
_debver=2.11
_debrel=5
pkgrel=3
pkgdesc='A small and fast file manager which can optionally manage the desktop background and panels'
arch=('i686' 'x86_64')
license=('GPL-2')
url='http://rox.sourceforge.net/desktop/'
depends=('sh' 'libsm' 'gtk2')
makedepends=('librsvg-legacy' 'python' 'quilt')
source=("https://downloads.sourceforge.net/${pkgname}/rox-filer-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/r/rox/rox_$_debver-$_debrel.debian.tar.xz"
        rox.desktop
        rox.svg
        rox.sh)
sha512sums=('2ef5e7a5d6f4bbb825d6f01725ad4149b9cabfb6fe82c33631bb145f5a3c84e345c372b7698170c1ef78b30ffbc4665495cc266da4828cc8b4b256b592b2c50b'
            'f5d83b76758b04fef7c1a8b20aafa3d3191bc1a77e23e9efef463fa3c46ad95fce6a55d3bf548dd9997cdf977beb65e7bd9e7914bd0bf2cb36cdbb201b2b78e1'
            '947eb72638bca26ba319ab5e63ae3729dc9b3d9d9c332e309cc6aed54fc19e2870b22eeac6906e323ba018e305850ea11e6fe1d46f132ee65650b6f090795dbe'
            '8bfbac1d86079817ae107c27b4ad1af779a1bcdec7e06b0a1ad124af0883b7d2b4565b96f71edac3ba42a2a83ddac059c56d0cace4b226bb2f6b6e8cd097d375'
            'e341bb837ba34237005b2bd1851b48bff2704d39ad4ee459960915da32c05e4f269f945039377bc97bdd6dd73b5cbc163e8ccfbbe3764aa05950e68e690d9290')

prepare() {
  cd "$srcdir/rox-filer-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/global || true

    quilt push -av
  fi

  2to3 -w ROX-Filer/src/po/tips.py
}

build() {
  cd "$srcdir/rox-filer-$pkgver"

  ./ROX-Filer/AppRun --compile CFLAGS="$CFLAGS -fcommon" LIBS="-ldl -lm"
  # finally we render a png as fallback for svg unaware menu applications
  # Attention: always make sure you check the dimensions of the source-svg,
  # you can read the dimensions via inkscape's export function
  rsvg-convert -w 125 -h 100 -f png -o "$srcdir/rox.png" "$srcdir/rox.svg"
}

package() {
  cd "$srcdir/rox-filer-$pkgver"

  install -d "$pkgdir/usr/share/Choices/MIME-types"
  install -m755 Choices/MIME-types/* "$pkgdir/usr/share/Choices/MIME-types/"
  cp -rp ROX-Filer "$pkgdir/usr/share/"
  rm -fr "$pkgdir"/usr/share/ROX-Filer/{src,build}
 
  install -Dm755 "$srcdir/rox.sh" "$pkgdir/usr/bin/rox"
  install -Dm644 rox.1 "$pkgdir/usr/share/man/man1/rox.1"
  ln -sf rox.1 "$pkgdir/usr/share/man/man1/ROX-Filer.1"

  install -Dm644 "$srcdir/rox.desktop" "$pkgdir/usr/share/applications/rox.desktop"
  install -Dm644 "$srcdir/rox.svg" "$pkgdir/usr/share/pixmaps/rox.svg"
  install -Dm644 "$srcdir/rox.png" "$pkgdir/usr/share/pixmaps/rox.png"

  install -Dm644 ROX-Filer/Help/COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
