# Maintainer (Arch): bartus <arch-user-repo[at]bartus.33mail.com>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=libart
pkgver=2.3.21
_debver=$pkgver
_debrel=4
pkgrel=1
pkgdesc="A library for high-performance 2D graphics"
url='https://www.levien.com/libart/'
arch=('i686' 'x86_64')
license=('LGPL-2')
makedepends=('quilt')
source=("https://download.gnome.org/sources/libart_lgpl/2.3/libart_lgpl-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/liba/libart-lgpl/libart-lgpl_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('8a632a6a4da59e5e8c02ec2f5a57e36d182b325b46513765425e5f171ff9ae326af1b133725beba28f7e76654309e001aee9bace727b5b4c8589405256a3c020'
            'dfa26b816ebbecbc7b625210e0eba63bd81239a050d67797a05d13156d0b5ef99d796f8d759a9f76f0407afbeb8d17d329a2ca5776edc369764b018e7004c56c')

prepare() {
  cd "${srcdir}/libart_lgpl-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/libart_lgpl-${pkgver}"
  ./configure --prefix=/usr --disable-static
  make
}

package() {
  cd "${srcdir}/libart_lgpl-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/$pkgname"
}
