# Maintainer (Arch): Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributors (Arch): Bmauet <careil.baptiste@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=bam
pkgver=0.5.1
_debver=0.5.1
_debrel=2
pkgrel=1
pkgdesc='A fast and flexible build system using Lua'
arch=('i686' 'x86_64')
url='http://github.com/matricks/bam'
license=('zlib')
depends=('lua')
makedepends=('quilt')
source=("$pkgname-$pkgver.tar.gz::https://github.com/matricks/bam/archive/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/b/bam/bam_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e6f1b3daad6073c58b0e3cbf836bb0a6b66f0c36532d6e6eca9949239ab8b584cc88f892cce6f74974e370a8a688f33a95dde86dd65cc1790e49e5f8aeab0fef'
            '593a6c8c1702f2e239928c525f4ec2df6b969c8bf5ebde187c904037526ba76be26b2b68cdd7962e819d10c7112cd7ed1e92e7a02e0f07fd60f1b6f0e56ec983')

prepare() {
  cd ${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}
  sh make_unix.sh
}

package() {
  cd ${pkgname}-${pkgver}
  install -Dm755 bam "${pkgdir}"/usr/bin/bam
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
