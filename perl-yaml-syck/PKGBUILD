# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=perl-yaml-syck
pkgver=1.34
_debver=1.34
_debrel=1
pkgrel=1
pkgdesc="Fast, lightweight YAML loader and dumper"
arch=('i686' 'x86_64')
url="https://metacpan.org/release/YAML-Syck"
license=('Expat')
depends=('perl')
makedepends=('quilt')
options=('!emptydirs')
source=(https://cpan.metacpan.org/authors/id/T/TO/TODDR/YAML-Syck-$pkgver.tar.gz
        https://deb.debian.org/debian/pool/main/liby/libyaml-syck-perl/libyaml-syck-perl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('18288f54026823e84b2d642d880d7758c5d95ba4b56d3fcf758efe59303ea71a49822d7c000b4c7b0629eccd4dcf3c940bc1c26f2b2ef89e4fdba78a35c04760'
            '9d7174060adcbea759473c408fa20adbb74909e40bc7066cdedb9e71f7c0b7c1406588a8e44288bbeee7f010c3736db3feee72c2ad779383b428dfbd8403fd04')

prepare() {
  cd YAML-Syck-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd YAML-Syck-$pkgver
  perl Makefile.PL INSTALLDIRS=vendor
  make
}

package() {
  cd YAML-Syck-$pkgver
  make install DESTDIR="${pkgdir}"
  install -D -m644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
