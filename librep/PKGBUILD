# Maintainer (Arch): Stefan Husmann <stefan-husmann@t-online.de>
# Contributor (Arch): Joshua Stiefer <facedelajunk@gmail.com>
# Contributor (Arch): Alexander Rødseth <rodseth@gmail.com>
# Contributor (Arch): Juergen Hoetzel <juergen@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=librep
pkgver=0.92.5
_debver=$pkgver
_debrel=3.1
pkgrel=2
pkgdesc="Lisp system for Sawfish"
license=('GPL-2')
arch=('i686' 'x86_64')
url='https://sawfish.fandom.com/wiki/Librep'
depends=('gmp' 'gdbm' 'libffi')
makedepends=('quilt')
options=('!emptydirs' '!libtool')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/libr/librep/librep_${pkgver}.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/libr/$pkgname/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('210fa3daac876dba68084f9e27996354e054ef0993d8d8a4a0498d742285b97f21eaaec218ba6439464c908f24cd8318f0d3edbab5a373b7c8057c8fc910ca55'
            'c4fd88eb4a6a488cae9b2f2d5e917d5ad6317baf5c04a7bcb490b7ef6cb982b4bcf60b226e8a5399a37e0de63afefd974432a12126d3fc785a198172eebf5dba')

prepare() {
  cd "$srcdir"/${pkgname}_${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    cp -r "$srcdir"/debian .

    quilt push -av
  fi
  ./autogen.sh
}

build() {
  cd "$srcdir"/${pkgname}_${pkgver} 
  ./configure \
    --disable-static \
    --prefix=/usr \
    --libexecdir="/usr/libexec/$pkgname" \
    --with-readline \
    --with-ffi
  make
}

package() {
  cd "$srcdir"/${pkgname}_${pkgver}
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
