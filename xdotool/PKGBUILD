# Maintainer (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Contributor (Arch): Rttommy <rttommy@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=xdotool
pkgver=3.20160805.1
_debver=$pkgver
_debrel=5
pkgrel=2
pkgdesc="Command-line X11 automation tool"
arch=('i686' 'x86_64')
url='https://www.semicomplete.com/projects/xdotool/'
license=('Modified-BSD')
depends=('libxtst' 'libxinerama' 'libxkbcommon')
makedepends=('quilt')
source=("https://github.com/jordansissel/xdotool/releases/download/v$pkgver/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/xdotool/xdotool_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e27b1029ee954ea17643ed489a00a1856e5687b03772c0f10a8e50cde3c3c957b57a3ba9890005474a744a228ef4827a83770d6890479c37a1f98f4fd7b1ff6c'
            'f1bc02dfdc1e3bc52a30c6799a8c29857dddd78cf9c985a9fa358da9a4fd8552728589d0e3f2a2e320cde9f98677c3dfe9f6ea9da755293155d070cf4de9962d')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0002-record-upstream-version-info-Closes-795809.patch || true

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  make WITHOUT_RPATH_FIX=1
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make PREFIX="$pkgdir/usr" INSTALLMAN="$pkgdir/usr/share/man" install

  # remove execute bit from header file
  chmod -x "$pkgdir/usr/include/xdo.h"

  install -Dm644 COPYRIGHT -t "$pkgdir/usr/share/licenses/$pkgname"
}
