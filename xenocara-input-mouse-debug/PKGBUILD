# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-input-mouse package

pkgname=xenocara-input-mouse-debug
_openbsdver=6.9
pkgver=1.9.3
pkgrel=4
pkgdesc="Xenocara mouse input driver"
arch=(i686 x86_64)
license=('X11')
url="https://www.xenocara.org"
depends=('glibc')
makedepends=('xenocara-server-devel' 'X-ABI-XINPUT_VERSION=24.1' 'xenocara-proto' 'xenocara-util-macros')
provides=('xf86-input-mouse')
conflicts=('xf86-input-mouse' 'xenocara-server<1.20' 'X-ABI-XINPUT_VERSION<24.1' 'X-ABI-XINPUT_VERSION>=25')
replaces=('xf86-input-mouse')
groups=('xenocara-drivers-debug' 'xorg-drivers-debug')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-input-mouse-$pkgver.tar.lz{,.sig})
options=(!strip) # It's required for debug packages
sha512sums=('bc07156c87cfc91217b7956db8e6f0a070c08d8ec052bddedda3e43b4ed78a5fcd97a8e5292956fe9a2bcb47658290e86dd88e0f00aeed09e9f2e8b32a431f0f'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-input-mouse"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-input-mouse"

  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-input-mouse"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
