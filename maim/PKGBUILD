# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): naelstrof <naelstrof@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=maim
pkgver=5.8.0
pkgrel=1
pkgdesc="Utility to take a screenshot"
arch=('i686' 'x86_64')
url='https://github.com/naelstrof/maim'
license=('GPL-3')
depends=('freetype2' 'libpng' 'libjpeg-turbo' 'libx11' 'libxcomposite' 'libxext' 'libxfixes' 'libxrandr' 'slop')
makedepends=('cmake' 'glm')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/naelstrof/maim/archive/v${pkgver}.tar.gz"
        "fix-build-without-libwebp.patch")
sha512sums=('418725b1950f9374c2ff7508e1c52f6986b253ea1c155ef28604e514f1cf0753185eea43cde76a65b9de80bb8dd65ced48b49ce5dfccaf20f5caa49f41743f5b'
            'ac5c1799edf9fa69fcde81a8a3797b7d31f75c5113ba347004aef600b076b2be446779692ab8e7f98daf95c233145ca292b39a7235408ef169085c840b31dc9d')

prepare() {
  cd "$pkgname-$pkgver"
  patch -Np1 -i ${srcdir}/fix-build-without-libwebp.patch
}

build() {
  cmake \
    -S $pkgname-$pkgver \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -Wno-dev
  make -C build
}

package() {
  make DESTDIR="$pkgdir" -C build install
  install -Dm644 "${srcdir}/$pkgname-$pkgver/COPYING" -t "${pkgdir}/usr/share/licenses/$pkgname"
}
