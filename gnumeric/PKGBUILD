# Maintainer (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): Kritoke <typeolinux@yahoo.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=gnumeric
pkgver=1.12.55
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc="A Spreadsheet Program"
arch=('i686' 'x86_64')
url='https://www.gnome.org/projects/gnumeric/'
license=('GPL-3')
depends=('goffice')
makedepends=('intltool' 'psiconv' 'pygobject-devel' 'docbook-xml'
             'gobject-introspection' 'python-gobject' 'autoconf-archive' 'yelp-tools' 'quilt')
optdepends=('python-gobject: for python plugin support'
            'perl: for perl plugin support'
            'psiconv: for Psion 5 file support'
            'yelp: for viewing the help manual')
source=("${pkgname}-${pkgver}.tar.xz::https://deb.debian.org/debian/pool/main/g/gnumeric/gnumeric_${pkgver}.orig.tar.xz"
        "https://deb.debian.org/debian/pool/main/g/gnumeric/gnumeric_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('04127a353d5109e3a31d9a3bd42d6d177366cc6c5d900516b4c8a7a8fcaecc71bd4e2e94876719246841070964b393af97988cafbd6313cea135c1757889e878'
            'be4cb6a6e4d85474a13ef8d1797b8b5b1683cc06a206fbe6669430729aeed4d2d52cc7fee2f91da6fca91af44ff2249f2bff8cacd38bb49bf967be603fe86c37')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    GLIB_COMPILE_SCHEMAS="/usr/bin/glib-compile-schemas" \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-schemas-compile \
    --enable-introspection \
    --disable-silent-rules \
    --disable-gtk-doc

  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING-gpl3 -t "${pkgdir}/usr/share/licenses/$pkgname"
}