# Maintainer (Arch): Florian Pritz< flo@xinu.at>
# Contributor (Arch): Dany Martineau <dany.luc.martineau@gmail.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

pkgname=qrencode
pkgver=4.1.1
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="C library for encoding data in a QR Code symbol"
url='https://fukuchi.org/works/qrencode/'
license=('LGPL-2.1')
arch=('i686' 'x86_64')
depends=('libpng')
makedepends=('sdl' 'quilt')
source=("https://fukuchi.org/works/${pkgname}/${pkgname}-${pkgver}.tar.bz2"
        "https://deb.debian.org/debian/pool/main/q/qrencode/qrencode_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('981b3242c213e1b40ac88711912dd8c27ea11f3437d1a04b6b226499a6e991a8f0b3b10f818bcc104812192a1dac4ce05f16ecd847a50cb03d4859b84bad89b8'
            'dc50409af0ddb58ef3972ad16db87ca1758d93a8d15ed06a8fe40fed6e4e7232964c4053b74ff8e77b2cc6b434d87021ec28788317bca9911a951bf8e766805a')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf
}

build() {
  cd "${srcdir}/$pkgname-$pkgver"
  ./configure --prefix=/usr
  make
}

package() {
  cd "${srcdir}/$pkgname-$pkgver"
  make prefix="$pkgdir/usr" install
  install -Dm644 COPYING -t "$pkgdir"/usr/share/licenses/$pkgname
}
