# Maintainer (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Damir Perisa <damir.perisa@bluewin.ch>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=xosd
pkgver=2.2.14
_debver=2.2.14
_debrel=2.1
pkgrel=1
pkgdesc='Displays text on your screen. On-Screen-Display-libs for some tools'
arch=('i686' 'x86_64')
license=('GPL-2')
url="https://sourceforge.net/projects/libxosd/"
depends=('libxt' 'libxinerama' 'sh' 'xenocara-fonts-misc-meta')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/libxosd/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/x/xosd/xosd_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('3caf6d106d34488f4823e2a58fdff7a69b90f505b8da2443485167ff0362c6f27614c9a5019e738ff1f897d3c2249c934ff60953e3775566d66e8e9b30e4e473'
            'ba5d86d2391e6dfdc6a2994e486f9c5ecd1934438815f4dc01c0633192d74a2fa2cabe647574052c4a87203e0c12b9bb8c38851975c36f19d113787dae3681ee')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure --prefix=/usr --mandir=/usr/share/man
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install
  sed -i 's/AC_DEFUN(AM_PATH_LIBXOSD,/AC_DEFUN([AM_PATH_LIBXOSD],/' \
    "$pkgdir/usr/share/aclocal/libxosd.m4"
  install -Dm644 COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}
}
