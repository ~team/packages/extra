# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lcms2
pkgver=2.9
_debver=2.9
_debrel=4
pkgrel=2
pkgdesc='Small-footprint color management engine, version 2'
arch=('i686' 'x86_64')
url='https://www.littlecms.com/'
license=('Expat')
depends=('libtiff')
makedepends=('quilt')
source=("https://downloads.sourceforge.net/sourceforge/lcms/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/l/lcms2/lcms2_$_debver-$_debrel.debian.tar.xz")
sha512sums=('70b1c51fa8d137d5072425e580745ff1fbf49c6e8bb1da0a8adb0647d3b7c095208793cb02de1e8d1a01363b8575fa60c61bedbff99bbec57a44228239cb00e5'
            '0f3527accbf235dc642b3b3f082414c7e5ccce043d94b07fe323ec8aa9982e658ade3a35503931d777a43abecad9c055ece193772af6094145c7ac3194cda338')

prepare() {
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${pkgname}-${pkgver}

  ./configure \
    --prefix=/usr
  make
}

check() {
  cd ${pkgname}-${pkgver}

  make check
}

package() {
  cd ${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -D -m0644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
