# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Arch): Jason A. Donenfeld <Jason@zx2c4.com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad
# Contributor: Jesús E.

pkgname=wireguard-tools
pkgver=1.0.20210223
_debver=$pkgver
_debrel=1
pkgrel=3
pkgdesc="Next generation secure network tunnel, including tools for configuration"
arch=('i686' 'x86_64')
url='https://www.wireguard.com/'
license=('GPL-2' 'LGPL-2.1' 'Expat')
depends=('bash' 'openresolv')
makedepends=('quilt')
optdepends=('opendoas: elevate privileges')
source=("https://git.zx2c4.com/${pkgname}/snapshot/${pkgname}-${pkgver}.tar"{.xz,.asc}
        "https://deb.debian.org/debian/pool/main/w/wireguard/wireguard_${_debver}-${_debrel}.debian.tar.xz"
        "wg-quick.initd"
        "wg-quick.confd"
        "wg-quick.run")
sha512sums=('98855853a3ecfce23a0a6bf4a885c85efaf2b08c9a92e0d3800fe40ae9adf05fdf4443150a71319bd9b53e314c8032694ea978db850a238813232a0c04edf692'
            'SKIP'
            '98cb1b9c04b8861e5b3c4a106b0a70e6eb34915b00ab405d15e2feaa47cd1ed439261ac6c21c4fddb7f45b7bb80f38bb1dec067cf14dff3f0cca2dc73053f3b3'
            '52d151c4017f16a12c0d01a582838d590bda43abef47b5f7b7953e99a647d836167db9b4d4cf061dd05a7d349a24d290f62886772c9b5662b7cd1dcd98a97fb2'
            '5e1bbd9b9b26da8360c5b6349e453f6b26110a5055e4ec5e52a392672e53777002c05ece8c5d1e5892e4c8c6a28061143e467c835416b94e3f715c5c9bf63651'
            'e64f218b8d89e809c18889556c497967ada963fc867826611f168f7a3a54c76d96d4c82bd720d8875b7bcb2912bb0f0c520cdb32b2ff547bcfc409425b1e862d')
validpgpkeys=('AB9942E6D4A4CFC3412620A749FC7012A5DE03AE') # Jason A. Donenfeld <Jason@zx2c4.com>

prepare() {
  cd "${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  find contrib/ -name '.gitignore' -delete
}

build() {
  cd "${pkgname}-${pkgver}"
  make -C src/
}

package() {
  cd "${pkgname}-${pkgver}"

  make -C src/ \
       DESTDIR="${pkgdir}" \
       WITH_BASHCOMPLETION=yes \
       WITH_WGQUICK=yes \
       WITH_SYSTEMDUNITS=no \
       install

  install -d -m0755 "${pkgdir}/usr/share/${pkgname}"
  cp -r contrib/ "${pkgdir}/usr/share/${pkgname}/examples"

  install -Dm755 "${srcdir}/wg-quick.initd" "${pkgdir}/etc/init.d/wg-quick"
  install -Dm644 "${srcdir}/wg-quick.confd" "${pkgdir}/etc/conf.d/wg-quick"
  install -Dm755 "${srcdir}/wg-quick.run" "${pkgdir}/etc/sv/wg-quick/run"

  install -Dm644 COPYING -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
