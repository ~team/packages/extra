# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Ray Rashif <schiv@archlinux.org>
# Contributor (Arch): speps <speps at aur dot archlinux dot org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lilv
pkgver=0.24.12
_debver=0.24.12
_debrel=2
pkgrel=1
pkgdesc="A C library interface to the LV2 plug-in standard"
arch=('i686' 'x86_64')
url="https://drobilla.net/software/lilv/"
license=('ISC')
depends=('glibc')
makedepends=('libsndfile' 'lv2' 'python' 'serd' 'sord' 'sratom' 'swig' 'waf' 'quilt')
optdepends=('bash-completion: completion for bash'
            'libsndfile: for lv2apply'
            'python: for Python bindings')
source=("https://download.drobilla.net/${pkgname}-${pkgver}.tar.bz2"{,.asc}
        "https://deb.debian.org/debian/pool/main/l/lilv/lilv_$_debver-$_debrel.debian.tar.xz")
sha512sums=('ea22db4e995792b62d60d793169c792549b8fb0255c2cf7a85780dd149811921e2fae5eaea0fb83465f01b14dfa66361af3be40bf7cb3733e98655b943f4faee'
            'SKIP'
            'f1e76e512a8eb318dbfa12f18b55863b4e6c7e2e1ae03a7bf7fb7e1ba39bd01a7c18e2b87d0e9678f3af257b568d98cfe11f58b2b68b2a4fd801f4e9d9453869')
validpgpkeys=('907D226E7E13FA337F014A083672782A9BF368F3') # David Robillard <d@drobilla.net>

prepare() {
  cd "${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  # let wscript(s) find the custom waf scripts
  mkdir -pv tools
  touch __init__.py
  cp -v waflib/extras/{autoship,autowaf,lv2}.py tools/
  mkdir -pv plugins/tools/
  cp -v waflib/extras/{autoship,autowaf,lv2}.py plugins/tools/
  rm -rv waflib
  sed -e 's/waflib.extras/tools/g' \
      -e "s/load('autowaf'/load('autowaf', tooldir='tools'/g" \
      -e "s/load('lv2'/load('lv2', tooldir='tools'/g" \
      -i wscript
}

build() {
  cd "${pkgname}-${pkgver}"
  waf configure --prefix=/usr \
                --no-bash-completion \
                --dyn-manifest \
                --test
  waf -v build
}

check() {
  cd "${pkgname}-${pkgver}"
  waf test
}

package() {
  cd "${pkgname}-${pkgver}"
  waf install --destdir="${pkgdir}"
  # license
  install -vDm 644 COPYING \
    -t "${pkgdir}/usr/share/licenses/${pkgname}"
  # bash completion
  install -vDm 644 utils/lilv.bash_completion\
    "${pkgdir}/usr/share/bash-completion/completions/${pkgname}"
}
# vim:set ts=2 sw=2 et:
