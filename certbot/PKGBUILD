# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Gordian Edenhofer <gordian.edenhofer[at]yahoo[dot]de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>
# Contributor: Jesús E.

pkgname=certbot
pkgver=2.1.0
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc="ACME client"
arch=('any')
license=('Apache-2.0')
url='https://certbot.eff.org'
depends=('ca-certificates' 'python-acme' 'python-configargparse' 'python-configobj'
         'python-cryptography' 'python-distro' 'python-parsedatetime'
         'python-pyrfc3339' 'python-pytz' 'python-setuptools' 'python-zope-component'
         'python-zope-interface')
makedepends=('quilt')
optdepends=('certbot-nginx: Nginx plugin for Let’s Encrypt client')
source=("https://pypi.io/packages/source/c/$pkgname/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/p/python-certbot/python-certbot_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e032c42cb0d14f26ca2ec75dee6335ea00d14c491c3e1d45ae993273918421b32539c2c4ea2a76dad1cfedab5bdcb6250a8ada00363265804687518b877beac9'
            '86073c4a0b7b89584b071aa9ddaf124fe9fa360a3ff4e5bb0c8622014e086f872cf3764b8e40a9873053a8cd8566ca104d36829ad9c281afac4dceeda125f21e')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  python setup.py build
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  python setup.py install --root="$pkgdir"
  install -d "$pkgdir"/{etc,var/lib,var/log}/letsencrypt
  chmod 700 "$pkgdir"/var/log/letsencrypt
  install -Dm644 LICENSE.txt -t "$pkgdir/usr/share/licenses/$pkgname"
}
