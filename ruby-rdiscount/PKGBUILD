# Maintainer (Arch): Anatol Pomozov <anatol.pomozov@gmail.com>
# Contributor (Arch): Massimiliano Torromeo <massimiliano.torromeo at gmail dot com>
# Contributor (Arch): Alexsandr Pavlov <kidoz at mail dot ru>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>
# Contributor: rachad

_gemname=rdiscount
pkgname=ruby-$_gemname
pkgver=2.1.8
_debver=$pkgver
_debrel=1
pkgrel=2
pkgdesc="Fast Implementation of Gruber's Markdown in C"
arch=('i686' 'x86_64')
url='https://dafoster.net/projects/rdiscount/'
license=('Modified-BSD')
depends=('ruby')
makedepends=('quilt')
options=(!emptydirs)
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/davidfstr/${_gemname}/archive/refs/tags/${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/${pkgname}/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('998beda4e8771b00c4d6795834b9de7cacffb1add3cff96c951da3aecb22c386b4eaa44b6eca744e63c2a86656916d45423f88a743313e528bdee56ad0486f4e'
            'fd5abbfad04618b1711f210773e902a2e10a8159a8d5521d5db48393850e93e0d98b8b1f7a6dd0bc47bcf6b3e3e278cee2a1c4cb3c1eb6dfe50ca2682328032f')

prepare() {
  cd $_gemname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build () {
  cd $_gemname-$pkgver
  gem build
}

package() {
  cd $_gemname-$pkgver
  local _gemdir="$(ruby -e'puts Gem.default_dir')"
  gem install --ignore-dependencies --no-user-install -i "$pkgdir/$_gemdir" -n "$pkgdir/usr/bin" $_gemname-$pkgver.gem
  rm "$pkgdir/$_gemdir/cache/$_gemname-$pkgver.gem"
  install -D -m644 "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  rm -rf "$pkgdir/$_gemdir/gems/$_gemname-$pkgver/ext"
}
