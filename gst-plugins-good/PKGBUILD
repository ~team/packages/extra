# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=gst-plugins-good
pkgname=(gst-plugins-good gst-plugin-gtk gst-plugin-qmlgl)
pkgver=1.18.3
pkgrel=7
pkgdesc="Multimedia graph framework - good plugins"
url='https://gstreamer.freedesktop.org/'
arch=(i686 x86_64)
license=(LGPL-2.1)
depends=(gst-plugins-base-libs wavpack aalib taglib gdk-pixbuf2 libcaca libxdamage
         v4l-utils cairo speex flac lame mpg123 twolame)
makedepends=(gettext-tiny python meson jack2 gtk qt-{tools,declarative,x11extras} nasm)
options=(!emptydirs)
source=(https://gstreamer.freedesktop.org/src/$pkgbase/$pkgbase-$pkgver.tar.xz)
sha512sums=('33d8b07bd98e9a04ee9f4dccaf4ff9c0a967336f3f368c7c5c63387fdaad829f445cc4d4abd617c3c1d530df9c04a2c7c12cc348afd98793aa7bea1d0ac710f8')

build() {
  hyperbola-meson $pkgbase-$pkgver build \
    -D doc=disabled \
    -D rpicamsrc=disabled \
    -D gobject-cast-checks=disabled \
    -D pulse=disabled \
    -D soup=disabled \
    -D shout2=disabled \
    -D v4l2-gudev=disabled \
    -D vpx=disabled \
    -D dv=disabled \
    -D dv1394=disabled \
    -D package-name="GStreamer Good Plugins (Hyperbola GNU/Linux-libre)" \
    -D package-origin="https://www.hyperbola.info/"
  meson compile -C build
}

package_gst-plugins-good() {
  DESTDIR="$pkgdir" meson install -C build

  install -Dm644 "$srcdir/$pkgbase-$pkgver/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  for _x in gtk qmlgl; do
    mkdir -p "$_x/lib/gstreamer-1.0"
    _f="lib/gstreamer-1.0/libgst${_x}.so"
    mv "$pkgdir/usr/$_f" "$_x/$_f"
  done
}

package_gst-plugin-gtk() {
  pkgdesc="${pkgdesc%-*}- gtk plugin"
  depends=(gst-plugins-base-libs gtk)

  mv gtk "$pkgdir/usr"

  install -Dm644 "$srcdir/$pkgbase-$pkgver/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

package_gst-plugin-qmlgl() {
  pkgdesc="${pkgdesc%-*}- qmlgl plugin"
  depends=(gst-plugins-base-libs qt-{declarative,x11extras})

  mv qmlgl "$pkgdir/usr"

  install -Dm644 "$srcdir/$pkgbase-$pkgver/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
