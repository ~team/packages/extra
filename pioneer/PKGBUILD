# Maintainer (Arch): Stephan Springer <buzo+arch@Lini.de>
# Contributor (Arch): Darren Wu
# Contributor (Arch): Tarn Burton <twburton at gmail dot com>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=pioneer
pkgver=20240203
pkgrel=1
pkgdesc="A game of lonely space adventure"
arch=('i686' 'x86_64')
url='https://github.com/pioneerspacesim/pioneer'
license=('Apache-2.0' 'Boost-1.0' 'Modified-BSD' 'CC-BY-SA-3.0' 'GPL-3' 'custom:Bitstream-Vera')
depends=('assimp' 'curl' 'freetype2' 'glew' 'hicolor-icon-theme' 'libpng' 'libsigc++' 'libvorbis' 'lua52' 'mesa' 'sdl2' 'sdl2_image')
makedepends=('cmake' 'ninja' 'pkg-config')
groups=('games')
source=("$pkgname-$pkgver.tar.gz::https://github.com/pioneerspacesim/pioneer/archive/$pkgver.tar.gz"
        "fix-build.patch")
sha512sums=('644f70dbb83f351b9cba46c95ef4d8dafdb0dce04e01981176425e4088a1c2f5ccb3df9b88128db2f940e8703347c289fc6cc9103b5b23cc1f3f7ef0efd49fa8'
            '2eb1ace3c623e848b7b5387c199561ebc571b13bfa6fe31d523821782b58ba3fca85134a6985cd9b7457df0bbc5d9bb91d59022eeb6a4aced27b2a55d481c8cc')

prepare() {
  cd "$pkgname-$pkgver"

  # fix version string, don't use the build date
  sed -i 's|PROJECT_VERSION "%Y%m%d"|PROJECT_VERSION "'$pkgver'"|' CMakeLists.txt

  # fix build
  patch -Np1 -i ${srcdir}/fix-build.patch
}

build() {
  cmake -S "$pkgname-$pkgver" -B build -G Ninja \
        -D CMAKE_INSTALL_PREFIX:PATH=/usr \
        -D CMAKE_INSTALL_BINDIR:PATH=/usr/games/ \
        -D PIONEER_INSTALL_DATADIR:PATH=/usr/share/games/pioneer \
        -D PIONEER_DATA_DIR:PATH=/usr/share/games/pioneer/data \
        -D USE_SYSTEM_LIBGLEW:BOOL=ON \
        -D USE_SYSTEM_LIBLUA:BOOL=ON \
        -D CMAKE_EXPORT_COMPILE_COMMANDS=1 \
        -Wno-dev

  cmake --build build --target all build-data
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  # remove empty directories
  rmdir "$pkgdir"/usr/share/games/$pkgname/data/music/core/{{un,}docked,near-planet}

  # move internal structure
  rm -rf "$pkgdir"/usr/share/games/$pkgname/licenses

  # licenses
  install -Dm644 "$pkgname-$pkgver"/licenses/{Apache-2.0.txt,Boost-1.0.txt,BSD-3-Clause.txt,CC-BY-SA-3.0.txt,DejaVu-license.txt,GLEW.txt,GPL-3.txt,LZ4.txt,SIL-1.1.txt} \
    -t "$pkgdir"/usr/share/licenses/$pkgname
  install -Dm644 "$pkgname-$pkgver/licenses/Image Use Policy - NASA Spitzer Space Telescope.html" \
    -t "$pkgdir"/usr/share/licenses/$pkgname
}