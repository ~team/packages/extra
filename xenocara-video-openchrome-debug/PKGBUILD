# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-openchrome package

pkgname=xenocara-video-openchrome-debug
_openbsdver=6.9
pkgver=0.6.182
pkgrel=4
pkgdesc="Xenocara Openchrome drivers"
arch=(i686 x86_64)
license=('Expat')
url="https://www.xenocara.org"
depends=('libdrm' 'libxvmc' 'libeudev')
optdepends=('unichrome-dri')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'eudev' 'xenocara-util-macros')
provides=('xf86-video-openchrome')
conflicts=('xf86-video-openchrome' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25'
           'xf86-video-via' 'xf86-video-unichrome' 'openchrome')
replaces=('xf86-video-openchrome' 'openchrome' 'xf86-video-via')
groups=('xenocara-video-drivers-debug' 'xenocara-video-debug' 'xenocara-drivers-debug'
        'xorg-video-drivers-debug' 'xorg-video-debug' 'xorg-drivers-debug')
options=('!emptydirs' '!strip') # !strip is required for debug packages
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-openchrome-$pkgver.tar.lz{,.sig})
sha512sums=('706ede56de32c1682aa8f35e0004171d4c2d392bd6b59ec22cabe43b3091024dd2fd5fdb72c0eef9ee8444ce55d8f8001f7fb7e6096fff2adbc9055d86920893'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-openchrome"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-openchrome"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}
  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr
  make
}

check() {
  cd "xenocara-$_openbsdver/driver/xf86-video-openchrome"
  make check
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-openchrome"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
