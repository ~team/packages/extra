# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on xf86-video-intel package

pkgname=xenocara-video-intel-debug
_openbsdver=6.9
pkgver=2.99.916
pkgrel=2
pkgdesc="Xenocara Intel i810/i830/i915/945G/G965+ video drivers"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('Expat')
install="$pkgname.install"
depends=('mesa-dri' 'libxvmc' 'pixman' 'xcb-util>=0.4.0' 'libeudev')
makedepends=('xenocara-server-devel' 'X-ABI-VIDEODRV_VERSION=24.0' 'libx11' 'libxrender' 'xenocara-util-macros'
             # additional deps for intel-virtual-output
             'libxrandr' 'libxinerama' 'libxcursor' 'libxtst' 'libxss')
optdepends=('libxrandr: for intel-virtual-output'
            'libxinerama: for intel-virtual-output'
            'libxcursor: for intel-virtual-output'
            'libxtst: for intel-virtual-output'
            'libxss: for intel-virtual-output')
provides=('xf86-video-intel' 'xf86-video-intel-uxa' 'xf86-video-intel-sna')
conflicts=('xf86-video-intel' 'xenocara-server<1.20' 'X-ABI-VIDEODRV_VERSION<24' 'X-ABI-VIDEODRV_VERSION>=25'
           'xf86-video-intel-sna' 'xf86-video-intel-uxa' 'xf86-video-i810' 'xf86-video-intel-legacy')
replaces=('xf86-video-intel' 'xf86-video-intel-uxa' 'xf86-video-intel-sna' 'xorg-video-intel-debug')
groups=('xenocara-video-drivers-debug' 'xenocara-video-debug' 'xenocara-drivers-debug'
        'xorg-video-drivers-debug' 'xorg-video-debug' 'xorg-drivers-debug')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/driver/xf86-video-intel-$pkgver.tar.lz{,.sig}
        include-missing-sys-sysmacros-h.patch
        0001-legacy-i810-Fix-compilation-with-Video-ABI-23-change.patch
        0002-Fix-build-on-i686.patch)
options=(!strip) # It's required for debug packages
sha512sums=('7dbc8fd96e7d20438fb6805eac445fef3ba5e1f0374d1e1c3434e145d7cbac8fd2cc8e46a2e8036993c005ac6c5a0757eb4f646ee0a769f174d2e0646feeec8f'
            'SKIP'
            '315a0fbb559cfc57dc5a012a4c1fa4d5105bd73bf84195af8d21b5926460110f413ee500b507a988caa5c2c7a1b755e23caecd5ba942f09d54f24b54e780d7b8'
            '579135d097f324eae2c471ab18a0296f201acebfa5785b4f71c8666b0aa268cf596d28f34e285a4fb5ddaab195d46efea39638c6bffea9169d07d1ff50147c3b'
            '2fed088c25bf6b439783ea53e1fdf445eecf46a2fb75ca715df9db3b4f29309f3627e517bcb639a60bad7287ca99bd1deb7f18224393fe43e2f7d7e42caab2cc')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/driver/xf86-video-intel"
  patch -p1 -i "$srcdir/include-missing-sys-sysmacros-h.patch"
  patch -p1 -i "$srcdir/0001-legacy-i810-Fix-compilation-with-Video-ABI-23-change.patch"
  patch -p1 -i "$srcdir/0002-Fix-build-on-i686.patch"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/driver/xf86-video-intel"

  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}
  # It's required for debug packages
  export CFLAGS=${CFLAGS/-O2/-O0 -g3}
  export CXXFLAGS=${CXXFLAGS/-O2/-O0 -g3}

  ./configure --prefix=/usr \
      --with-default-dri=3
  make
}

check() {
  cd "xenocara-$_openbsdver/driver/xf86-video-intel"
  make check
}

package() {
  cd "xenocara-$_openbsdver/driver/xf86-video-intel"
  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
