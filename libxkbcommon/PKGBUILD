# Maintainer (Arch): Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor (Arch): Daniel Micay <danielmicay@gmail.com>
# Contributor (Arch): Mladen Pejakovic <pejakm@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=libxkbcommon
pkgname=(libxkbcommon libxkbcommon-doc libxkbcommon-x11)
pkgver=1.0.3
pkgrel=1
pkgdesc="Keymap handling library for toolkits and window systems"
url="https://xkbcommon.org/"
arch=(i686 x86_64)
license=(Expat)
makedepends=(xkeyboard-config libxml2 libxcb doxygen meson)
source=("https://xkbcommon.org/download/$pkgname-$pkgver.tar.xz")
sha512sums=('b05a2db0acd6628427baad1967979a161019dec90a078a926d41f4dfeffb84eab63e627803df1a401e42c8e48953d6c434ce08d7206eae21e890890f29b03d3c')

prepare() {
  mkdir -p doc/usr/share
  mkdir -p x11/usr/{include/xkbcommon,lib/pkgconfig,libexec/xkbcommon,share/man/man1}
}

build() {
  hyperbola-meson $pkgbase-$pkgver build \
    -D enable-wayland=false
  meson compile -C build
}

package_libxkbcommon() {
  depends=(xkeyboard-config libxml2)
  optdepends=('libxkbcommon-x11: xkbcli interactive-x11')

  DESTDIR="$pkgdir" meson install -C build
  install -Dm644 $pkgbase-$pkgver/LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

### Split libxkbcommon-doc
  mv "$pkgdir"/usr/share/doc "$srcdir/doc/usr/share"

### Split libxkbcommon-x11
  mv "$pkgdir"/usr/lib/*x11* "$srcdir/x11/usr/lib"
  mv "$pkgdir"/usr/lib/pkgconfig/*x11* "$srcdir/x11/usr/lib/pkgconfig"
  mv "$pkgdir"/usr/libexec/xkbcommon/*x11* "$srcdir/x11/usr/libexec/xkbcommon"
  mv "$pkgdir"/usr/include/xkbcommon/*x11* "$srcdir/x11/usr/include/xkbcommon"
  mv "$pkgdir"/usr/share/man/man1/*x11* "$srcdir/x11/usr/share/man/man1"
}

package_libxkbcommon-doc() {
  pkgdesc="API documentation for libxkbcommon"

  mv doc/* "$pkgdir"
  install -Dm644 $pkgbase-$pkgver/LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_libxkbcommon-x11() {
  pkgdesc="Keyboard handling library using XKB data for X11 XCB clients"
  depends=(libxkbcommon libxcb)

  mv x11/* "$pkgdir"
  install -Dm644 $pkgbase-$pkgver/LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
