# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=xcb-util-keysyms
_openbsdver=6.9
pkgver=0.4.0
pkgrel=3
pkgdesc="Utility libraries for XC Binding - Standard X key constants and conversion to/from keycodes, provided by Xenocara"
arch=('i686' 'x86_64')
url="https://www.xenocara.org"
license=('X11')
depends=('libxcb>=1.14')
makedepends=('xenocara-util-macros')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/xcb-util-keysyms-$pkgver.tar.lz{,.sig}
        COPYING)
sha512sums=('634ff482b01283a14f5a4b63b43335c7ec2025ab85d6926f80b7deb3612fa1fca7239d8deff1873cb0e12af9c61eeb744ea283acbd0e0e0d1c4af0a536b83672'
            'SKIP'
            '62f83468f9d9503a5a8c1ebaac78cbaefe3c485f8bfd7192817ec59254872fe2914ed66ae8b9e7f7a6ef5af283048c11390f44eb8f0f70b1c136ef5c75282e8c')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/xcb-util-keysyms"
  autoreconf -vfi
}

build() {
  cd "xenocara-$_openbsdver/lib/xcb-util-keysyms"
  ./configure --prefix=/usr --disable-static
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/xcb-util-keysyms"
  make check
}

package() {
  cd "xenocara-$_openbsdver/lib/xcb-util-keysyms"
  make DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir/COPYING" "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
