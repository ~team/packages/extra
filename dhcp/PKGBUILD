# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Daniel Isenmann <daniel @archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: rachad <rachad @ hyperbola . info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=dhcp
pkgname=('dhcp' 'dhclient')

# Separate patch levels with a period to maintain proper versioning.
pkgver=4.4.1
_debver=$pkgver
_debrel=2.3
pkgrel=2
arch=('i686' 'x86_64')
license=('ISC' 'MPL-2.0')
url='https://www.isc.org/dhcp/'
makedepends=('bash' 'iproute2' 'openldap' 'quilt')
source=("ftp://ftp.isc.org/isc/${pkgbase}/${pkgver}/${pkgbase}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/i/isc-dhcp/isc-dhcp_${_debver}-${_debrel}.debian.tar.xz"
        "0002-iproute2.patch"
        "dhcpd.run"
        "dhcpd6.run"
        "dhclient.run"
        "dhclient6.run"
        "dhcpd.initd"
        "dhcpd6.initd"
        "dhclient.initd"
        "dhclient6.initd")
sha512sums=('684ae349f224918c9f8cec7bd6c55cd0b83ad2b5827375b2876ca088eb05b7ff1364e50f6dc24f2485c610d9be94d4ba3020f60a0fa0ef63962349d191b887e7'
            '4a613564196815bf9df8b95b3524e3dc90a8479bff799b56f681e39b5c8272c564d4758f46f1ba199fe059f92196b4b4e6fa540d593c28beb4166b356b7a4d27'
            '5103474b3b25d69bbb1433884ffd0bc57da427df73b1c49385e4b2e37b264826fa6b19861fbd2f80a869edabd5189f2a4c2a02f4ff4438fa5c55152fec697fcf'
            '6c4c69955bfc3942cc3091e71da80dd982401548e06d6a83f364f2f3e654538861509765cfdd205a8e0a087841d0f76e40d776b738b70f378d749bb65e681833'
            'c877eccebc44edd435c4238b04a378142d42ebd3cbafc7bb8802cfb7522fdb7eff631460a6c6649eb68dca69dfe17773e08ef17b5a0dffd1f8e6c6a8b7fadf89'
            'e48b5a5162587392ebe4ef2abcc8b9c8fb78f0c59a6ebcc6f5bbb11542cb1b15f5180e8ff82b0b0b3a12190581a652710f4b521f67526685ec5e7349694cce7c'
            'a842a2b871cbe955d4a07f93ab31611af8393be6dd9f84a4acc4d87b2ec3f958b8e1deb8382e179321289c83181430b18a873cbe2f6f8a95386a85fe83d56be7'
            '5371e883207797aedc91b5fcab79c41ee0b13096a20359c8086fbeefd5cfa164267b65442f694b7416263b3ac51794208ec026ddba4064b94cad1a8400e63323'
            '9ca80a230ef675b0b48ba242687c2474ac54f7468d39b036cfbc7bea03a799a93436602e5c48f58ff6a72b3039076ce4e22954d1c0471ba2c219bd2827f24c3c'
            '988d5d7855b8cc9782ec8b4e56fbc210df35cf6fdbe5099a8191fc96bc483b952a8b4030b256217a3927923ac3b437e7d3b570d1152266c17c1446b61cc2aca8'
            '37b47a81f90e9befe429fd2df31ed60e78aa2ed8820f94bfd0c23ac67b7580a1be573293387fe99f698c30453969e158f1057da3131c4cc1e28ff8ca17d1771e')

prepare() {
  cd "${srcdir}/${pkgbase}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  # No ifconfig, use ip from iproute2
  patch -Np1 -i ../0002-iproute2.patch
}

build() {
  cd "${srcdir}/${pkgbase}-${pkgver}"

  ./configure \
      --prefix=/usr \
      --sysconfdir=/etc \
      --enable-binary-leases \
      --enable-paranoia \
      --with-ldap \
      --with-ldapcrypto \
      --with-srv-lease-file=/var/lib/dhcp/dhcpd.leases \
      --with-srv6-lease-file=/var/lib/dhcp/dhcpd6.leases \
      --with-cli-lease-file=/var/lib/dhclient/dhclient.leases \
      --with-cli6-lease-file=/var/lib/dhclient/dhclient6.leases

  make -j1
}

package_dhcp(){
  pkgdesc="A DHCP server, client, and relay agent"
  depends=('glibc' 'libldap')
  optdepends=('logger: message logging support')
  backup=('etc/dhcpd.conf' 'etc/dhcpd6.conf')
  install=dhcp.install

  cd "${srcdir}/${pkgbase}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  install -Dm755 "$srcdir/dhcpd.initd" "$pkgdir/etc/init.d/dhcpd"
  install -Dm755 "$srcdir/dhcpd6.initd" "$pkgdir/etc/init.d/dhcpd6"
  install -Dm755 "$srcdir/dhcpd.run" "$pkgdir/etc/sv/dhcpd/run"
  install -Dm755 "$srcdir/dhcpd6.run" "$pkgdir/etc/sv/dhcpd6/run"

  # move and install config files in place
  mv "${pkgdir}/etc/dhcpd.conf.example" "${pkgdir}/etc/dhcpd.conf"
  install -D -m644 doc/examples/dhcpd-dhcpv6.conf "${pkgdir}/etc/dhcpd6.conf"

  # Remove dhclient
  make -C client DESTDIR="${pkgdir}" uninstall

  # Install license
  install -m644 -D LICENSE "${pkgdir}/usr/share/licenses/dhcp/LICENSE"
}

package_dhclient(){
  pkgdesc="A standalone DHCP client from the dhcp package"
  depends=('glibc' 'bash' 'iproute2')
  provides=('dhcp-client')
  backup=('etc/dhclient.conf' 'etc/dhclient6.conf')

  cd "${srcdir}/${pkgbase}-${pkgver}"
  make -C client DESTDIR="${pkgdir}" install

  install -m755 -d "${pkgdir}/usr/share/dhclient"

  install -Dm755 "$srcdir/dhclient.initd" "$pkgdir/etc/init.d/dhclient"
  install -Dm755 "$srcdir/dhclient6.initd" "$pkgdir/etc/init.d/dhclient6"
  install -Dm755 "$srcdir/dhclient.run" "$pkgdir/etc/sv/dhclient/run"
  install -Dm755 "$srcdir/dhclient6.run" "$pkgdir/etc/sv/dhclient6/run"

  install -m755 -d "${pkgdir}/usr/share/dhclient"
  mv "${pkgdir}/etc/dhclient.conf.example" "${pkgdir}/usr/share/dhclient/"

  install -d "${pkgdir}/var/lib/dhclient"

  # Install dhclient script
  install -m755 client/scripts/linux "${pkgdir}/usr/sbin/dhclient-script"

  # Install license
  install -m644 -D LICENSE "${pkgdir}/usr/share/licenses/dhclient/LICENSE"
}
