# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Maintainer: Tobias Dausend <throgh@hyperbola.info>

pkgname=rrdtool
pkgver=1.7.2
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc="Data logging and graphing application"
arch=('i686' 'x86_64')
url='https://www.rrdtool.org'
license=('GPL-2' 'LGPL-2.1' 'Public-Domain' 'Simplified-BSD')
depends=('libxml2' 'pango' 'ttf-dejavu')
makedepends=('intltool' 'gettext-tiny' 'ruby' 'python-setuptools' 'tcl' 'lua51' 'quilt')
optdepends=('perl: to use corresponding binding'
            'tcl: to use corresponding binding'
            'python: to use corresponding binding'
            'ruby: to use corresponding binding'
            'lua51: to use corresponding binding')
options=('!emptydirs' '!makeflags')
source=("${pkgname}-${pkgver}.tar.gz::http://deb.debian.org/debian/pool/main/r/rrdtool/rrdtool_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/r/rrdtool/rrdtool_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('e4fe115037454d80f589c1cbb38c0b526f5858d67ae1feda6ddd2f2060c53865a07ced46bbef392086e821146434f62ac0f07ad5319783718013a2792bd6c93f'
            '1ad5c3063b4d7d40e6264e8bfc5003d11a7f384f5d1398e90aa4944cea3eaec62a59319d6c17012233f32e2d549bc79d3f57e3fca5a2c5397bc15cc2f5c1b522')

prepare() {
  mv "${pkgname}-1.x-${pkgver}" "${pkgname}-${pkgver}"
  cd ${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    rm -rf ./debian
    mv "$srcdir"/debian .

    quilt push -av
  fi

  # make ruby install to vendor_ruby instead of site_ruby
  sed -e 's/$(RUBY) ${abs_srcdir}\/ruby\/extconf.rb/& --vendor/' -i bindings/Makefile.am

  # https://github.com/oetiker/rrdtool-1.x/issues/1135
  autoreconf -vi
}

build() {
  cd ${pkgname}-${pkgver}
  LUA=/usr/bin/lua5.1 \
    LUA_CFLAGS="-I/usr/include/lua5.1 -llua5.1" LUA_INSTALL_CMOD="/usr/lib/lua/5.1" \
    ./configure --prefix=/usr --localstatedir=/var --disable-rpath \
    --enable-perl --enable-perl-site-install --with-perl-options='INSTALLDIRS=vendor' \
    --enable-ruby --enable-ruby-site-install --enable-python \
    --enable-lua --enable-lua-site-install --enable-tcl --disable-libwrap
  make
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" includedir=/usr/include install
  install -Dm644 COPYRIGHT -t "${pkgdir}/usr/share/licenses/${pkgname}"
}